# Farm Manager API

Farm Manager currently consists of the following projects:

- FarmManager.Api
- FarmManager.Common
- FarmManager.Core
- FarmManager.Data
- FarmManager.Migrations
- FarmManager.Services
- FarmManager.Web

These pages provide documentation related to those projects and the included classes, functions, and objects.