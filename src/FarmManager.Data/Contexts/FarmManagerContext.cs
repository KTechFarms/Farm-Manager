﻿using EntityFrameworkCore.Triggers;
using FarmManager.Data.Entities;
using FarmManager.Data.Entities.Devices;
using FarmManager.Data.Entities.Generic;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Data.Entities.Reports.Production;
using FarmManager.Data.Entities.System;
using FarmManager.Data.Triggers;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Data.Contexts
{
    /// <summary>
    /// A data context representing the Farm Manager database
    /// </summary>
    public class FarmManagerContext : DbContextWithTriggers
    {
        public FarmManagerContext(DbContextOptions<FarmManagerContext> options) : base(options)
        {
        }

        // System Tables
        public DbSet<Log> Logs { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<SettingCategory> SettingCategories { get; set; }

        // Generic Tables
        public DbSet<Enum> Enums { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Note> Notes { get; set; }

        // Device Tables
        public DbSet<PendingDevice> PendingDevices { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<DeviceMethod> DeviceMethods { get; set; }
        public DbSet<Interface> Interfaces { get; set; }
        public DbSet<InterfaceType> InterfaceTypes { get; set; }
        public DbSet<Telemetry> Telemetry { get; set; }

        // Operations Tables
        public DbSet<BusinessLocation> BusinessLocations { get; set; }
        public DbSet<FarmLocation> FarmLocations { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<ProductVariant> ProductVariants { get; set; }
        public DbSet<ProductVariantBarcode> VariantBarcodes { get; set; }
        public DbSet<Inventory> Inventory { get; set; }

        // Production Tables
        public DbSet<Lot> Lots { get; set; }
        public DbSet<Crop> Crops { get; set; }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<ProductionItem> ProductionItems { get; set; }
        public DbSet<ProductionItemLocationHistory> ProductionItemLocationHistory { get; set; }
        public DbSet<LotProgression> LotProgressions { get; set; }
        public DbSet<LotNoteMap> LotNoteMaps { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<LabelVersion> LabelVersions { get; set; }

        // Report Tables
        public DbSet<CropStatistic> CropStatistics { get; set; }

        /// <summary>
        /// Enables triggers on the FarmManagerContext
        /// </summary>
        public static void SetTriggers()
        {
            SetBaseTriggers();
        }

        /// <summary>
        /// Iterates through all tables in the database, getting a list of those
        /// that extend EntityBase, to apply the DefaultTriggers
        /// </summary>
        protected static void SetBaseTriggers()
        {
            List<Type?> dbSets = typeof(FarmManagerContext)
                .GetProperties()
                .Where(p => p.PropertyType.Name == "DbSet`1")
                .Select(p => p.PropertyType.GetGenericArguments().FirstOrDefault())
                .ToList();

            List<Type> baseTypes = typeof(FarmManagerContext)
                .Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(typeof(EntityBase)) && t.IsClass)
                .ToList();

            List<Type> baseSets = baseTypes
                .Where(t => dbSets.Contains(t))
                .ToList();

            MethodInfo? defaultTriggersMethod = typeof(FarmManagerContext)
                .GetMethod(nameof(SetDefaultEntityTriggers));

            if (defaultTriggersMethod != null)
                foreach (Type t in baseSets)
                {
                    MethodInfo genericTriggerMethod = defaultTriggersMethod.MakeGenericMethod(t);
                    genericTriggerMethod.Invoke(null, null);
                }
        }

        /// <summary>
        /// Sets insert and update triggers on the Created, Updated, and IsDeleted
        /// columns of a given entity, T
        /// </summary>
        /// <typeparam name="T">Any entity that inherits the FarmCraftBase class</typeparam>
        public static void SetDefaultEntityTriggers<T>() where T : EntityBase
        {
            Triggers<T>.Inserting += BaseTriggers<T>.OnInserting;
            Triggers<T>.Updating += BaseTriggers<T>.OnUpdating;
        }
    }
}
