﻿using EntityFrameworkCore.Triggers;
using FarmManager.Data.Entities;

namespace FarmManager.Data.Triggers
{
    /// <summary>
    /// Base triggers for Farm Manager Contexts to update the Created,
    /// Modified, and IsDeleted Fields
    /// </summary>
    /// <typeparam name="T">Any entity that inherits the EntityBase class</typeparam>
    public static class BaseTriggers<T> where T : EntityBase
    {
        /// <summary>
        /// When inserting a row, sets the Created and Modified fields to the 
        /// current UTC time and IsDeleted to false
        /// </summary>
        /// <param name="entry">An EntityBase entity from the DbContext</param>
        public static void OnInserting(IInsertingEntry entry)
        {
            EntityBase? entity = entry.Entity as EntityBase;
            if (entity != null)
            {
                DateTimeOffset now = DateTimeOffset.UtcNow;

                SetCreated(entity, now);
                SetModified(entity, now);
                SetIsDeleted(entity);
            }
        }

        /// <summary>
        /// When updating a row, sets the Modified field to the current
        /// UTC time
        /// </summary>
        /// <param name="entry">An EntityBase entity from the DbContext</param>
        public static void OnUpdating(IUpdatingEntry entry)
        {
            EntityBase? entity = entry.Entity as EntityBase;
            if (entity != null)
            {
                DateTimeOffset now = DateTimeOffset.UtcNow;

                SetModified(entity, now);
            }
        }

        /// <summary>
        /// Sets the Created column of an EntityBase entity to the given time
        /// </summary>
        /// <param name="entity">An EntityBase entity from the DbContext</param>
        /// <param name="now">The current UTC time</param>
        public static void SetCreated(EntityBase entity, DateTimeOffset now)
        {
            entity.Created = now;
        }

        /// <summary>
        /// Sets the Modified column of an EntityBase entity to the given time
        /// </summary>
        /// <param name="entity">An EntityBase entity from the DbContext</param>
        /// <param name="now">The current UTC time</param>
        public static void SetModified(EntityBase entity, DateTimeOffset now)
        {
            entity.Modified = now;
        }

        /// <summary>
        /// Sets the IsDeleted column of an EntityBase entity to false
        /// </summary>
        /// <param name="entity">An EntityBase entity from the DbContext</param>
        public static void SetIsDeleted(EntityBase entity)
        {
            entity.IsDeleted = false;
        }
    }
}
