﻿namespace FarmManager.Data.ActorMessages.Reports
{
    /// <summary>
    /// A common interface shared by all report-related messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IReportMessage : IFarmManagerMessage
    {
    }
}
