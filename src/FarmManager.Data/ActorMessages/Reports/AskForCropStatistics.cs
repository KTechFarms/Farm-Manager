﻿namespace FarmManager.Data.ActorMessages.Reports
{
    /// <summary>
    /// Used to retrieve a list of crop statistics for 
    /// your farm
    /// </summary>
    public class AskForCropStatistics : IReportMessage
    {
        public AskForCropStatistics() { }
    }
}
