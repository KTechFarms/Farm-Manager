﻿namespace FarmManager.Data.ActorMessages.Events
{
    /// <summary>
    /// Request a publisher that can be used to publish
    /// messages to the message broker
    /// </summary>
    public class AskForMessageBrokerPublisher : IEventMessage
    {
        /// <summary>
        /// The name of the queue to send a message to
        /// </summary>
        public string QueueName { get; private set; }

        /// <summary>
        /// An identifier used to identify where the message came from
        /// </summary>
        public string Identifier { get; private set; }

        public AskForMessageBrokerPublisher(string queueName, string identifier)
        {
            QueueName = queueName;
            Identifier = identifier;
        }
    }
}
