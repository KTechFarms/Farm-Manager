﻿namespace FarmManager.Data.ActorMessages.Events
{
    /// <summary>
    /// A common interface shared by all event-related messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IEventMessage : IFarmManagerMessage
    {
    }
}
