﻿namespace FarmManager.Data.ActorMessages.Events
{
    /// <summary>
    /// A message used to tell an actor to begin processing 
    /// messages from message broker 
    /// </summary>
    public class BeginProcessing : IEventMessage
    {
    }
}
