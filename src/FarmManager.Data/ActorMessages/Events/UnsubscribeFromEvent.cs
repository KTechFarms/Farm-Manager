﻿using Akka.Actor;

namespace FarmManager.Data.ActorMessages.Events
{
    /// <summary>
    /// Used to unsubscribe from a certain type of event
    /// from the message broker
    /// </summary>
    public class UnsubscribeFromEvent : IEventMessage
    {
        /// <summary>
        /// The type of event that should be unsubscribed from
        /// </summary>
        public string EventType { get; private set; }

        /// <summary>
        /// A reference to the actor that should no longer receive 
        /// notifications
        /// </summary>
        public IActorRef Subscriber { get; private set; }

        public UnsubscribeFromEvent(string eventType, IActorRef subscriber)
        {
            EventType = eventType; 
            Subscriber = subscriber;
        }
    }
}
