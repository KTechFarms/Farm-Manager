﻿namespace FarmManager.Data.ActorMessages.Events
{
    /// <summary>
    /// An object defining an event that has been received via
    /// the message broker
    /// </summary>
    public class EventReceived : IEventMessage
    {
        /// <summary>
        /// The type of message
        /// </summary>
        public string? MessageType { get; private set; }

        /// <summary>
        /// Where the message came from
        /// </summary>
        public string? Source { get; private set; }

        /// <summary>
        /// The timestamp the message was created
        /// </summary>
        public DateTimeOffset TimeStamp { get; private set; }

        /// <summary>
        /// The data that the message contains
        /// </summary>
        public object? Data { get; private set; }

        /// <summary>
        /// The assembly of the data object
        /// </summary>
        public string? DataAssembly { get; private set; }

        /// <summary>
        /// The class of the data object
        /// </summary>
        public string? DataClass { get; private set; }

        public EventReceived(
            string? messageType,
            string? source, 
            DateTimeOffset timeStamp, 
            object? data, 
            string? dataAssembly, 
            string? dataClass
        )
        {
            MessageType = messageType;
            Source = source;
            TimeStamp = timeStamp;
            Data = data;
            DataAssembly = dataAssembly;
            DataClass = dataClass;
        }
    }
}
