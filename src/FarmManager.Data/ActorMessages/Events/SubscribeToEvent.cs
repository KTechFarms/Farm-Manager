﻿using Akka.Actor;

namespace FarmManager.Data.ActorMessages.Events
{
    /// <summary>
    /// Used to subscribe to a certain type of event 
    /// from the message broker
    /// </summary>
    public class SubscribeToEvent : IEventMessage
    {
        /// <summary>
        /// The type of event that you want to be notified of
        /// </summary>
        public string EventType { get; private set; }

        /// <summary>
        /// A reference to the actor that should be notified
        /// </summary>
        public IActorRef Subscriber { get; private set; }

        public SubscribeToEvent(string eventType, IActorRef subscriber)
        {
            EventType = eventType;
            Subscriber = subscriber;
        }
    }
}
