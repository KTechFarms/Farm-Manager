﻿using FarmManager.Data.DTOs.Queries;

namespace FarmManager.Data.ActorMessages.System
{
    /// <summary>
    /// Asked to query the Farm Manager system for a list of
    /// logs
    /// </summary>
    public class AskToRetrieveLogs : ILogMessage
    {
        /// <summary>
        /// The query parameters that are used to retrieve logs
        /// </summary>
        public LogQueryParams Query { get; private set; }

        public AskToRetrieveLogs(LogQueryParams query)
        {
            Query = query;
        }
    }
}
