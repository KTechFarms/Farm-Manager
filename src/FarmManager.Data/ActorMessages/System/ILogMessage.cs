﻿namespace FarmManager.Data.ActorMessages.System
{
    /// <summary>
    /// A common interface shared by all log-related messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface ILogMessage : IFarmManagerMessage
    {
    }
}
