﻿namespace FarmManager.Data.ActorMessages.System.Settings
{
    /// <summary>
    /// Used to retrieve a list of settings
    /// </summary>
    public class AskForSettings : ISettingsMessage
    {
        public AskForSettings()
        {
        }
    }
}
