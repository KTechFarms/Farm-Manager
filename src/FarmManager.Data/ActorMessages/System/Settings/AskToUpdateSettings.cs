﻿namespace FarmManager.Data.ActorMessages.System.Settings
{
    /// <summary>
    /// Used to update the settings
    /// </summary>
    public class AskToUpdateSettings : ISettingsMessage
    {
        /// <summary>
        /// A dictionary of setting names that should be updated with their
        /// associated values
        /// </summary>
        public Dictionary<string, string> Settings { get; private set; }

        public AskToUpdateSettings(Dictionary<string, string> settings)
        {
            Settings = settings;
        }
    }
}
