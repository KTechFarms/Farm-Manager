﻿namespace FarmManager.Data.ActorMessages.System.Settings
{
    /// <summary>
    /// A common interface shared by all settings-related messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface ISettingsMessage : IFarmManagerMessage
    {
    }
}
