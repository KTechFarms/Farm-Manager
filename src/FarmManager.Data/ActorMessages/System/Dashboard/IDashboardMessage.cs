﻿namespace FarmManager.Data.ActorMessages.System.Dashboard
{
    /// <summary>
    /// A common interface shared by all dashboard-related messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IDashboardMessage : IFarmManagerMessage
    {
    }
}
