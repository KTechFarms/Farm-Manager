﻿namespace FarmManager.Data.ActorMessages.System.Dashboard
{
    /// <summary>
    /// Used to retrieve the number of devices connected to the
    /// Farm Manager system
    /// </summary>
    public class AskForDeviceCounts : IDashboardMessage
    {
        public AskForDeviceCounts() { }
    }
}
