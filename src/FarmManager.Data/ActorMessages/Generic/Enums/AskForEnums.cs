﻿namespace FarmManager.Data.ActorMessages.Generic.Enums
{
    /// <summary>
    /// Asked to query a list of enumerations from Farm Manager
    /// </summary>
    public class AskForEnums : IEnumMessage
    {
        /// <summary>
        /// The dot-delimited EntityId that the enumerations
        /// apply to
        /// </summary>
        public string? EntityId { get; private set; }

        public AskForEnums(string? entityId = null)
        {
            EntityId = entityId;
        }
    }
}
