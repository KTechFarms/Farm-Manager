﻿namespace FarmManager.Data.ActorMessages.Generic.Enums
{
    /// <summary>
    /// A common interface shared by all enum-related messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IEnumMessage : IFarmManagerMessage
    {
    }
}
