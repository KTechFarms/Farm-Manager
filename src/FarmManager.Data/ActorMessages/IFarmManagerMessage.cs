﻿namespace FarmManager.Data.ActorMessages
{
    /// <summary>
    /// A generic interface that all Farm Manager
    /// messages inherit
    /// </summary>
    public interface IFarmManagerMessage
    {
    }
}
