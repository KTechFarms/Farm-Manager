﻿using FarmManager.Data.Entities.Operations;

namespace FarmManager.Data.ActorMessages.Operations.BusinessLocations
{
    /// <summary>
    /// Used to create a new business location
    /// </summary>
    public class AskToCreateBusinessLocation : IBusinessLocationMessage
    {
        /// <summary>
        /// The definition of the business location that should be created
        /// </summary>
        public BusinessLocation Location { get; private set; }

        public AskToCreateBusinessLocation(BusinessLocation location)
        {
            Location = location;
        }
    }
}
