﻿namespace FarmManager.Data.ActorMessages.Operations.BusinessLocations
{
    /// <summary>
    /// A common interfaced shared by all business location messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IBusinessLocationMessage : IOperationsMessage
    {
    }
}
