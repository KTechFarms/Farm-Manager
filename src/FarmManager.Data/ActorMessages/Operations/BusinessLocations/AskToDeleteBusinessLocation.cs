﻿namespace FarmManager.Data.ActorMessages.Operations.BusinessLocations
{
    /// <summary>
    /// Used to delete a business location
    /// </summary>
    public class AskToDeleteBusinessLocation : IBusinessLocationMessage
    {
        /// <summary>
        /// The id of the location that should be deleted
        /// </summary>
        public int Id { get; private set; }

        public AskToDeleteBusinessLocation(int id)
        {
            Id = id;
        }
    }
}
