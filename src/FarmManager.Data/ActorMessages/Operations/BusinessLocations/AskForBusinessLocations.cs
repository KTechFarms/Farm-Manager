﻿namespace FarmManager.Data.ActorMessages.Operations.BusinessLocations
{
    /// <summary>
    /// Used to request a list of existing business locations
    /// </summary>
    public class AskForBusinessLocations : IBusinessLocationMessage
    {
        public AskForBusinessLocations() 
        {
        }
    }
}
