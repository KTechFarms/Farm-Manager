﻿namespace FarmManager.Data.ActorMessages.Operations
{
    /// <summary>
    /// A common interface shared by all operations messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IOperationsMessage : IFarmManagerMessage
    {
    }
}
