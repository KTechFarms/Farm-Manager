﻿namespace FarmManager.Data.ActorMessages.Operations.Categories
{
    /// <summary>
    /// Used to delete a category
    /// </summary>
    public class AskToDeleteCategory : ICategoryMessage
    {
        /// <summary>
        /// The id of the category that should be deleted
        /// </summary>
        public int Id { get; private set; }

        public AskToDeleteCategory(int id)
        {
            Id = id;
        }
    }
}
