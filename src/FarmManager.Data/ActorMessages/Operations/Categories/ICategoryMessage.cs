﻿namespace FarmManager.Data.ActorMessages.Operations.Categories
{
    /// <summary>
    /// A common interface used by all category messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface ICategoryMessage : IOperationsMessage
    {
    }
}
