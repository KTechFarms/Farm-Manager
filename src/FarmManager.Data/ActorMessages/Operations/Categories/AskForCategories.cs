﻿namespace FarmManager.Data.ActorMessages.Operations.Categories
{
    /// <summary>
    /// Used to request a list of existing product categories
    /// </summary>
    public class AskForCategories : ICategoryMessage
    {
        public AskForCategories()
        {
        }
    }
}
