﻿using FarmManager.Data.Entities.Operations;

namespace FarmManager.Data.ActorMessages.Operations.Categories
{
    /// <summary>
    /// Used to create a new product category
    /// </summary>
    public class AskToCreateCategory : ICategoryMessage
    {
        /// <summary>
        /// An object defining the category that should be created
        /// </summary>
        public ProductCategory Category { get; private set; }

        public AskToCreateCategory(ProductCategory category)
        {
            Category = category;
        }
    }
}
