﻿using FarmManager.Data.Entities.Operations;

namespace FarmManager.Data.ActorMessages.Operations.Products
{
    /// <summary>
    /// Used to update a product
    /// </summary>
    public class AskToUpdateProduct : IProductMessage
    {
        /// <summary>
        /// An object representing the updated version of a product
        /// </summary>
        public Product Product { get; private set; }

        public AskToUpdateProduct(Product product) 
        {
            Product = product;
        }
    }
}
