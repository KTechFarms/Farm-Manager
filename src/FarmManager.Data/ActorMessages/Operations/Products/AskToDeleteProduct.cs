﻿namespace FarmManager.Data.ActorMessages.Operations.Products
{
    /// <summary>
    /// Used to delete a product
    /// </summary>
    public class AskToDeleteProduct : IProductMessage
    {
        /// <summary>
        /// The id of the product that should be deleted
        /// </summary>
        public int ProductId { get; private set; }

        public AskToDeleteProduct(int productId)
        {
            ProductId = productId;
        }
    }
}
