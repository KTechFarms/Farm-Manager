﻿namespace FarmManager.Data.ActorMessages.Operations.Products
{
    /// <summary>
    /// Used to request a list of existing products
    /// </summary>
    public class AskForProducts : IProductMessage
    {
        public AskForProducts() 
        {
        }
    }
}
