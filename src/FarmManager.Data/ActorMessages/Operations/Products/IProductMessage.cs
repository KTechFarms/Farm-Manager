﻿namespace FarmManager.Data.ActorMessages.Operations.Products
{
    /// <summary>
    /// A common interface shared by all product messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IProductMessage : IOperationsMessage
    {
    }
}
