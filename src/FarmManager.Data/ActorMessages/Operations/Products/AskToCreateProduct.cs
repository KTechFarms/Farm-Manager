﻿using FarmManager.Data.Entities.Operations;

namespace FarmManager.Data.ActorMessages.Operations.Products
{
    /// <summary>
    /// Used to create a new product
    /// </summary>
    public class AskToCreateProduct : IProductMessage
    {
        /// <summary>
        /// An object representing the product that should be created
        /// </summary>
        public Product Product { get; set; }

        public AskToCreateProduct(Product product)
        {
            Product = product;
        }
    }
}
