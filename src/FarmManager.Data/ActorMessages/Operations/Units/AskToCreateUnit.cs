﻿using FarmManager.Data.Entities.Operations;

namespace FarmManager.Data.ActorMessages.Operations.Units
{
    /// <summary>
    /// Used to create a new unit
    /// </summary>
    public class AskToCreateUnit : IUnitMessage
    {
        /// <summary>
        /// An object representing the unit that should
        /// be created
        /// </summary>
        public Unit Unit { get; private set; }

        public AskToCreateUnit(Unit unit) 
        { 
            Unit = unit;
        }
    }
}
