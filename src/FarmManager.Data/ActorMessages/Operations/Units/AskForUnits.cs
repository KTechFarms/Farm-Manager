﻿namespace FarmManager.Data.ActorMessages.Operations.Units
{
    /// <summary>
    /// Used to request a list of existing units
    /// </summary>
    public class AskForUnits : IUnitMessage
    {
        public AskForUnits() { }
    }
}
