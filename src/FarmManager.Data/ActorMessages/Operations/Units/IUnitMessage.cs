﻿namespace FarmManager.Data.ActorMessages.Operations.Units
{
    /// <summary>
    /// A common interface shared by all unit messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IUnitMessage : IOperationsMessage
    {
    }
}
