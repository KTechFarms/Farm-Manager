﻿namespace FarmManager.Data.ActorMessages.Operations.Units
{
    /// <summary>
    /// Used to delete a unit
    /// </summary>
    public class AskToDeleteUnit : IUnitMessage
    {
        /// <summary>
        /// The id of the unit that should be deleted
        /// </summary>
        public int Id { get; private set; }

        public AskToDeleteUnit(int id) 
        {
            Id = id;
        }
    }
}
