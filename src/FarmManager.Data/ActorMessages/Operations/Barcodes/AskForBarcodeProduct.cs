﻿namespace FarmManager.Data.ActorMessages.Operations.Barcodes
{
    /// <summary>
    /// Used to find an inventory product that uses the provided
    /// barcode
    /// </summary>
    public class AskForBarcodeProduct : IBarcodeMessage
    {
        /// <summary>
        /// The barcode used to search for a product
        /// </summary>
        public string Barcode { get; private set; }

        public AskForBarcodeProduct(string barcode)
        {
            Barcode = barcode;
        }
    }
}
