﻿namespace FarmManager.Data.ActorMessages.Operations.Barcodes
{
    /// <summary>
    /// A common interface shared by all barcode messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IBarcodeMessage : IOperationsMessage
    {
    }
}
