﻿namespace FarmManager.Data.ActorMessages.Devices
{
    /// <summary>
    /// Used to request that a device be registered to the 
    /// Farm Manager system. 
    /// </summary>
    public class AskToRegisterDevice : IDeviceMessage
    {
        /// <summary>
        /// The name or identifier of the device you would
        /// like to register
        /// </summary>
        public string DeviceName { get; private set; }

        public AskToRegisterDevice(string deviceName)
        {
            DeviceName = deviceName;
        }
    }
}
