﻿namespace FarmManager.Data.ActorMessages.Devices
{
    /// <summary>
    /// Used to invoke a direct method on a Farm Manager 
    /// device
    /// </summary>
    public class AskToInvokeMethod : IDeviceMessage
    {
        /// <summary>
        /// The id of the device
        /// </summary>
        public string DeviceId { get; private set; }

        /// <summary>
        /// The name of the method to execute
        /// </summary>
        public string MethodName { get; private set; }

        /// <summary>
        /// Parameters required by the direct method
        /// </summary>
        public object? Parameters { get; private set; }

        public AskToInvokeMethod(
            string deviceId,
            string methodName,
            object? parameters
        )
        {
            DeviceId = deviceId;
            MethodName = methodName;
            Parameters = parameters;
        }
    }
}
