﻿namespace FarmManager.Data.ActorMessages.Devices
{
    /// <summary>
    /// Used to handle a telemetry message received from
    /// the message broker
    /// </summary>
    public class TellToHandleTelemetry : IFarmManagerMessage
    {
        /// <summary>
        /// The source of the message. This could be an
        /// IoT Hub DeviceId or the name of a service
        /// </summary>
        public string? Source { get; private set; }

        /// <summary>
        /// The date at which the message was submitted to the
        /// message broker
        /// </summary>
        public DateTimeOffset TimeStamp { get; private set; }

        /// <summary>
        /// Data specific to the message being sent
        /// </summary>
        public object? Data { get; private set; }

        public TellToHandleTelemetry(
            string? source,
            DateTimeOffset timestamp,
            object? data
        )
        {
            Source = source;
            TimeStamp = timestamp;
            Data = data;
        }
    }
}
