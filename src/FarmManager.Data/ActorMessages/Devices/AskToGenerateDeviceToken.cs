﻿namespace FarmManager.Data.ActorMessages.Devices
{
    /// <summary>
    /// Used to request a SAS token for a given device, so that
    /// it can connect to the Service Bus and communicate with 
    /// the Farm Manager system
    /// </summary>
    public class AskToGenerateDeviceToken : IDeviceMessage
    {
        /// <summary>
        /// The name or identifier of the device requesting a 
        /// SAS token
        /// </summary>
        public string Device { get; private set; }

        /// <summary>
        /// The duration that the token should be valid for.
        /// </summary>
        public int DurationSeconds { get; private set; }

        /// <summary>
        /// The device key that was provided when registering with
        /// Farm Manager. Without the key, a SAS will not be generated.
        /// </summary>
        public Guid DeviceKey { get; private set; }

        public AskToGenerateDeviceToken(
            string device,
            int durationSeconds,
            string? deviceKey = null
        )
        {
            Device = device;
            DurationSeconds = durationSeconds;

            if (Guid.TryParse(deviceKey, out Guid key))
                DeviceKey = key;
            else
                DeviceKey = Guid.Empty;
        }
    }
}
