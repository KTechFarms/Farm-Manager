﻿using FarmManager.Data.Entities.Devices;

namespace FarmManager.Data.ActorMessages.Devices
{
    /// <summary>
    /// The first message a device sends when turning on and
    /// connecting to a network
    /// </summary>
    public class TellToInitializeDevice : IDeviceMessage
    {
        /// <summary>
        /// The definition of the device sending the message, including
        /// it's interfaces and interface types.
        /// </summary>
        public Device Device { get; private set; }

        /// <summary>
        /// The time at which the message was sent
        /// </summary>
        public DateTimeOffset TimeStamp { get; private set; }

        public TellToInitializeDevice(Device device, DateTimeOffset timeStamp)
        {
            Device = device;
            TimeStamp = timeStamp;
        }
    }
}
