﻿namespace FarmManager.Data.ActorMessages.Devices
{
    /// <summary>
    /// An enumeration specifying how telemetry is grouped
    /// </summary>
    public enum TelemetryGrouping
    {
        Time,
        Interface
    }

    /// <summary>
    /// Used to retrieve information about a particular device
    /// </summary>
    public class AskForDeviceInformation : IDeviceMessage
    {
        /// <summary>
        /// The id of the device
        /// </summary>
        public string DeviceId { get; private set; }

        /// <summary>
        /// The past number of minutes that you want telemetry from
        /// </summary>
        public int HistoryMinutes { get; private set; } 
            = 360;

        /// <summary>
        /// How telemetry should be grouped
        /// </summary>
        public TelemetryGrouping TelemetryGrouping { get; private set; }
            = TelemetryGrouping.Interface;

        public AskForDeviceInformation(
            string deviceId,
            int? historyMinutes,
            TelemetryGrouping? grouping
        )
        {
            DeviceId = deviceId;
            
            if (historyMinutes != null)
                HistoryMinutes = (int)historyMinutes;

            if (grouping != null)
                TelemetryGrouping = (TelemetryGrouping)grouping;
        }
    }
}
