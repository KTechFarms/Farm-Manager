﻿namespace FarmManager.Data.ActorMessages.Devices
{
    /// <summary>
    /// A common interface shared by all device-related messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IDeviceMessage : IFarmManagerMessage
    {
    }
}
