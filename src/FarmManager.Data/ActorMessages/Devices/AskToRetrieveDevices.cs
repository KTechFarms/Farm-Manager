﻿using FarmManager.Data.DTOs.Queries;

namespace FarmManager.Data.ActorMessages.Devices
{
    /// <summary>
    /// Asked to query the Farm Manager system for a list of 
    /// devices
    /// </summary>
    public class AskToRetrieveDevices : IDeviceMessage
    {
        /// <summary>
        /// The query parameters that are used to retrieve logs
        /// </summary>
        public DeviceQueryParams Query { get; private set; }

        public AskToRetrieveDevices(DeviceQueryParams query)
        {
            Query = query;
        }
    }
}
