﻿namespace FarmManager.Data.ActorMessages.Production.FarmLocations
{
    /// <summary>
    /// Used to delete a specified location
    /// </summary>
    public class AskToDeleteFarmLocation : IFarmLocationMessage
    {
        /// <summary>
        /// The id of the location that should be deleted
        /// </summary>
        public int LocationId { get; private set; }

        public AskToDeleteFarmLocation(int locationId)
        {
            LocationId = locationId;
        }
    }
}
