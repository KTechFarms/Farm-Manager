﻿using FarmManager.Data.Entities.Operations;

namespace FarmManager.Data.ActorMessages.Production.FarmLocations
{
    /// <summary>
    /// Used to create a new farm location within the Farm Manager system
    /// </summary>
    public class AskToCreateFarmLocation : IFarmLocationMessage
    {
        /// <summary>
        /// Information about the location that should be created
        /// </summary>
        public FarmLocation Location { get; private set; }

        public AskToCreateFarmLocation(FarmLocation location)
        {
            Location = location;
        }
    }
}
