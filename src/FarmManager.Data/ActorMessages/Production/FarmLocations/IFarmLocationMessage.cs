﻿namespace FarmManager.Data.ActorMessages.Production.FarmLocations
{
    /// <summary>
    /// A common interface shared by all farm location messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IFarmLocationMessage : IProductionMessage
    {
    }
}
