﻿using FarmManager.Data.DTOs.Queries;

namespace FarmManager.Data.ActorMessages.Production.FarmLocations
{
    /// <summary>
    /// Used to retrieve a list of farm locations
    /// </summary>
    public class AskForFarmLocations : IFarmLocationMessage
    {
        /// <summary>
        /// The query parameters that are used to retrieve locations
        /// </summary>
        public FarmLocationQueryParams Query { get; private set; }

        public AskForFarmLocations(FarmLocationQueryParams query) 
        {
            Query = query;
        }
    }
}
