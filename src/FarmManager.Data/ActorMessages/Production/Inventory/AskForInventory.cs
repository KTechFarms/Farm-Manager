﻿namespace FarmManager.Data.ActorMessages.Production.Inventory
{
    /// <summary>
    /// Used to request existing inventory for a particular location
    /// </summary>
    public class AskForInventory : IInventoryMessage
    {
        /// <summary>
        /// The Id of the Farm Location to retrieve inventory for
        /// </summary>
        public int FarmLocationId { get; private set; }

        public AskForInventory(int farmLocationId)
        {
            FarmLocationId = farmLocationId;
        }
    }
}
