﻿namespace FarmManager.Data.ActorMessages.Production.Inventory
{
    /// <summary>
    /// A commoon interface shared by all inventory-related messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface IInventoryMessage : IProductionMessage
    {
    }
}
