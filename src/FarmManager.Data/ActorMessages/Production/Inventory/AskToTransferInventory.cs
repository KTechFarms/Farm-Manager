﻿namespace FarmManager.Data.ActorMessages.Production.Inventory
{
    /// <summary>
    /// Used to move inventory from one location to another
    /// </summary>
    public class AskToTransferInventory : IInventoryMessage
    {
        /// <summary>
        /// The farm location the associated items are being
        /// transferred from
        /// </summary>
        public int FromFarmLocationId { get; private set; }

        /// <summary>
        /// The farm location the associated items are being
        /// transferred to
        /// </summary>
        public int ToFarmLocationId { get; private set; }

        /// <summary>
        /// The ProductionItemIds that are being transferred
        /// </summary>
        public List<long> ProductionItemIds { get; private set; }

        /// <summary>
        /// The InventoryIds that are being transferred
        /// </summary>
        public List<long> InventoryIds { get; private set; }

        public AskToTransferInventory(
            int fromFarmLocationId,
            int toFarmLocationId, 
            List<long> productionItemIds,
            List<long> inventoryIds
        )
        {
            FromFarmLocationId = fromFarmLocationId;
            ToFarmLocationId = toFarmLocationId;
            ProductionItemIds = productionItemIds;
            InventoryIds = inventoryIds;
        }
    }
}
