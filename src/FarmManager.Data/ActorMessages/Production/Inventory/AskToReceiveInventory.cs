﻿using FarmManager.Data.DTOs.Production;

namespace FarmManager.Data.ActorMessages.Production.Inventory
{
    /// <summary>
    /// Used to receive inventory at a particular location
    /// </summary>
    public class AskToReceiveInventory : IInventoryMessage
    {
        /// <summary>
        /// The location at which inventory is being received
        /// </summary>
        public int FarmLocationId { get; private set; }

        /// <summary>
        /// The inventory items being received
        /// </summary>
        public List<InventoryItem> Inventory { get; private set; }

        public AskToReceiveInventory(
            int farmLocationId, 
            List<InventoryItem> inventory
        )
        {
            FarmLocationId = farmLocationId;
            Inventory = inventory;
        }
    }
}
