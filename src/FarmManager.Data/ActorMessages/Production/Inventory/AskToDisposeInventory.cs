﻿using FarmManager.Data.DTOs.Production;

namespace FarmManager.Data.ActorMessages.Production.Inventory
{
    /// <summary>
    /// Used to dispose of inventory
    /// </summary>
    public class AskToDisposeInventory : IInventoryMessage
    {
        /// <summary>
        /// The inventory disposal request
        /// </summary>
        public InventoryDisposal Request { get; private set; }

        public AskToDisposeInventory(InventoryDisposal request) 
        { 
            Request = request;
        }
    }
}
