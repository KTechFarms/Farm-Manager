﻿namespace FarmManager.Data.ActorMessages.Production.Labels
{
    /// <summary>
    /// A common interface shared by all label messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface ILabelMessage : IProductionMessage
    {
    }
}
