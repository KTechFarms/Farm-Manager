﻿using FarmManager.Data.DTOs.Production;

namespace FarmManager.Data.ActorMessages.Production.Labels
{
    /// <summary>
    /// Used to create a new label
    /// </summary>
    public class AskToCreateLabel : ILabelMessage
    {
        /// <summary>
        /// An object representing the label that should be
        /// created
        /// </summary>
        public LabelDTO Label { get; private set; }

        public AskToCreateLabel(LabelDTO label)
        {
            Label = label;
        }
    }
}
