﻿namespace FarmManager.Data.ActorMessages.Production.Labels
{
    /// <summary>
    /// Used to update a label
    /// </summary>
    public class AskToUpdateLabel : ILabelMessage
    {
        /// <summary>
        /// The name of the label to update
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The new content of the label
        /// </summary>
        public string Content { get; private set; }

        public AskToUpdateLabel(string name, string content)
        {
            Name = name;
            Content = content;
        }
    }
}
