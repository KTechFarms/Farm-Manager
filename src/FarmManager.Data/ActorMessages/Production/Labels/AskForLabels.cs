﻿namespace FarmManager.Data.ActorMessages.Production.Labels
{
    /// <summary>
    /// Used to request a list of existing labels
    /// </summary>
    public class AskForLabels : ILabelMessage
    {
        public AskForLabels()
        {
        }
    }
}
