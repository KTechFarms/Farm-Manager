﻿namespace FarmManager.Data.ActorMessages.Production.Labels
{
    /// <summary>
    /// Used to delete a label
    /// </summary>
    public class AskToDeleteLabel : ILabelMessage
    {
        /// <summary>
        /// The id of the label that should be deleted
        /// </summary>
        public int Id { get; private set; }

        public AskToDeleteLabel(int id)
        {
            Id = id;
        }
    }
}
