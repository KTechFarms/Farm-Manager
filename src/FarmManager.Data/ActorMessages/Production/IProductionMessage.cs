﻿namespace FarmManager.Data.ActorMessages.Production
{
    /// <summary>
    /// A common interface shared by all production-related message.
    /// Primarily used to route message to the correct location
    /// </summary>
    public interface IProductionMessage : IFarmManagerMessage
    {
    }
}
