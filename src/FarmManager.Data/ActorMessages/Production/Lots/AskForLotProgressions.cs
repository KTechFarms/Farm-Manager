﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to find possible progressions for the given lot
    /// </summary>
    public class AskForLotProgressions : ILotMessage
    {
        /// <summary>
        /// The Id of the lot you want progression options for
        /// </summary>
        public int LotId { get; private set; }

        public AskForLotProgressions(int lotId)
        {
            LotId = lotId;
        }
    }
}
