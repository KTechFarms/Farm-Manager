﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to create a new note for a particular lot
    /// </summary>
    public class AskToCreateLotNote : ILotMessage
    {
        /// <summary>
        /// The contents of the note
        /// </summary>
        public string Note { get; private set; }

        /// <summary>
        /// The lot number to which the note belongs
        /// </summary>
        public string LotNumber { get; private set; }

        /// <summary>
        /// The id of the user creating the note
        /// </summary>
        public string UserId { get; private set; }

        public AskToCreateLotNote(
            string note, 
            string lotNumber,
            string userId
        )
        {
            Note = note;
            LotNumber = lotNumber;
            UserId = userId;
        }
    }
}
