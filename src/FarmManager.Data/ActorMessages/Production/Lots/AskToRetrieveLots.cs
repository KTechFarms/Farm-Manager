﻿using FarmManager.Data.DTOs.Queries;

namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to retrieve a list of lots
    /// </summary>
    public class AskToRetrieveLots : ILotMessage
    {
        /// <summary>
        /// The query parameters that are used to retrieve lots
        /// </summary>
        public LotQueryParams Query { get; private set; }

        public AskToRetrieveLots(LotQueryParams query)
        {
            Query = query;
        }
    }
}
