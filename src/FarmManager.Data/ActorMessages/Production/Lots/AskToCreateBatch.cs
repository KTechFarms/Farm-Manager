﻿using FarmManager.Data.Entities.Production;

namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to create a batch for a given lot
    /// </summary>
    public class AskToCreateBatch : ILotMessage
    {
        /// <summary>
        /// The lot number that should have the batch should
        /// belong to
        /// </summary>
        public string LotNumber { get; private set; }

        /// <summary>
        /// The batch to create
        /// </summary>
        public Batch Batch { get; private set; }

        public AskToCreateBatch(string lotNumber, Batch batch)
        {
            LotNumber = lotNumber;
            Batch = batch;
        }
    }
}
