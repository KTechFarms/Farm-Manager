﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to retrieve information for the next batch
    /// </summary>
    public class AskForNextBatchData : ILotMessage
    {
        /// <summary>
        /// The lot number we need data for
        /// </summary>
        public string LotNumber { get; private set; }

        public AskForNextBatchData(string lotNumber)
        {
            LotNumber = lotNumber;
        }
    }
}
