﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to retrieve notes for a particular lot
    /// </summary>
    public class AskForLotNotes : ILotMessage
    {
        /// <summary>
        /// The lot number for which notes should be retrieved
        /// </summary>
        public string LotNumber { get; private set; }

        public AskForLotNotes(string lotNumber)
        {
            LotNumber = lotNumber;
        }
    }
}
