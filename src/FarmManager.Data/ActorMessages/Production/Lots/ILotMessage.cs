﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// A common interface shared by all lot-related messages.
    /// Primarily used to route messages to the correct location
    /// </summary>
    public interface ILotMessage : IProductionMessage
    {
    }
}
