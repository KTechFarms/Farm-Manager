﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to retrieve information for the next product
    /// </summary>
    public class AskForNextProductData : ILotMessage
    {
        /// <summary>
        /// The lot number that the product will belong to
        /// </summary>
        public string LotNumber { get; private set; }

        /// <summary>
        /// The batch that the product will belong to
        /// </summary>
        public int BatchNumber { get; private set; }

        public AskForNextProductData(string lotNumber, int batchNumber)
        {
            LotNumber = lotNumber;
            BatchNumber = batchNumber;
        }
    }
}
