﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to complete and close a batch
    /// </summary>
    public class AskToCloseBatch : ILotMessage
    {
        /// <summary>
        /// The lot number that the batch belongs to
        /// </summary>
        public string LotNumber { get; private set; }

        /// <summary>
        /// The batch number that should be closed
        /// </summary>
        public int BatchNumber { get; private set; }

        public AskToCloseBatch(string lotNumber, int batchNumber)
        {
            LotNumber = lotNumber;
            BatchNumber = batchNumber;
        }
    }
}
