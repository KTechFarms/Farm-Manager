﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used for retrieving the next lot number for the current
    /// system
    /// </summary>
    public class AskForNextLotNumber : ILotMessage
    {
    }
}
