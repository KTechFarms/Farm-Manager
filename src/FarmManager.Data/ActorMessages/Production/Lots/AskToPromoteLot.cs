﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to promote a lot to a given status
    /// </summary>
    public class AskToPromoteLot : ILotMessage
    {
        /// <summary>
        /// The id of the lot to promote
        /// </summary>
        public int LotId { get; private set; }

        /// <summary>
        /// The id of the status to promote to
        /// </summary>
        public int StatusId { get; private set; }

        /// <summary>
        /// The location at which the lot was transplanted to
        /// </summary>
        public int? TransplantLocationId { get; private set; }

        public AskToPromoteLot(int lotid, int statusId, int? transplantLocationId)
        {
            LotId = lotid;
            StatusId = statusId;
            TransplantLocationId = transplantLocationId;
        }
    }
}
