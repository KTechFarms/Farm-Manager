﻿namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to retrieve batches from a specified lot
    /// </summary>
    public class AskForLotBatches : ILotMessage
    {
        /// <summary>
        /// The lot number that batches should be 
        /// retrieved for
        /// </summary>
        public string LotNumber { get; private set; }

        public AskForLotBatches(string lotNumber)
        {
            LotNumber = lotNumber;
        }
    }
}
