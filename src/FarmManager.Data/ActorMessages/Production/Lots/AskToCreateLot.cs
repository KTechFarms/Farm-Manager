﻿using FarmManager.Data.Entities.Production;

namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to create a new lot in the system
    /// </summary>
    public class AskToCreateLot : ILotMessage
    {
        /// <summary>
        /// The lot that should be created
        /// </summary>
        public Lot Lot { get; private set; }

        public AskToCreateLot(Lot lot)
        {
            Lot = lot;
        }
    }
}
