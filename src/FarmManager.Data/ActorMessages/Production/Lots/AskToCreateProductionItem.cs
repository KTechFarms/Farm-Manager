﻿using FarmManager.Data.DTOs.Production;

namespace FarmManager.Data.ActorMessages.Production.Lots
{
    /// <summary>
    /// Used to create a new production item
    /// </summary>
    public class AskToCreateProductionItem : ILotMessage
    {
        /// <summary>
        /// The lot number the production item belongs to
        /// </summary>
        public string LotNumber { get; private set; }

        /// <summary>
        /// The batch number the production item belongs to
        /// </summary>
        public int BatchNumber { get; private set; }

        /// <summary>
        /// Information about the product that will be created
        /// </summary>
        public CreateProductionItemDTO ProductInfo { get; private set; }

        public AskToCreateProductionItem(
            string lotNumber, 
            int batchNumber,
            CreateProductionItemDTO productInfo
        )
        {
            LotNumber = lotNumber;
            BatchNumber = batchNumber;
            ProductInfo = productInfo;
        }
    }
}
