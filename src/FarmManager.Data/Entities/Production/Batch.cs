﻿using FarmManager.Data.Entities.Generic;
using FarmManager.Data.Entities.Operations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Production
{
    /// <summary>
    /// Represents a batch. Batches belong to a lot
    /// and typically represent a single harvest
    /// or production event
    /// </summary>
    [Table(nameof(Batch))]
    public class Batch : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The id of the lot to which the batch belongs
        /// </summary>
        public int LotId { get; set; }

        /// <summary>
        /// The number of the batch
        /// </summary>
        public int BatchNumber { get; set; }

        /// <summary>
        /// The ProductVariant Id of the variety that is
        /// being produced
        /// </summary>
        public int VariantId { get; set; }
        [ForeignKey(nameof(VariantId))]
        public ProductVariant? Variant { get; set; }

        /// <summary>
        /// The Farm Location where the batch was harvested
        /// </summary>
        public int FarmLocationId { get; set; }
        [ForeignKey(nameof(FarmLocationId))]
        public FarmLocation? FarmLocation { get; set; }

        /// <summary>
        /// The current status of the batch
        /// </summary>
        public int StatusId { get; set; }
        [ForeignKey(nameof(StatusId))]
        public Status? Status { get; set; }

        /// <summary>
        /// The date harvesting was started for this batch
        /// </summary>
        public DateTimeOffset BatchBeginDate { get; set; }

        /// <summary>
        /// The date harvesting was completed for this batch.
        /// Should generally be the same as BatchBeginDate
        /// </summary>
        public DateTimeOffset? BatchEndDate { get; set; }

        /// <summary>
        /// A collection of production items harvested from the batch
        /// </summary>
        public ICollection<ProductionItem>? ProductionItems { get; set; }
    }
}
