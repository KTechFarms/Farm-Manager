﻿using FarmManager.Data.Entities.Operations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Production
{
    /// <summary>
    /// Represents a label for a particular product
    /// </summary>
    [Table(nameof(Label))]
    public class Label : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The name of the label
        /// </summary>
        public string LabelName { get; set; }

        /// <summary>
        /// Whether the label is active and can be used
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// The Square Id of the variety that this label
        /// is for
        /// </summary>
        public int ProductVariantId { get; set; }
        [ForeignKey(nameof(ProductVariantId))]
        public ProductVariant? ProductVariant { get; set; }

        /// <summary>
        /// A collection of versions for this label
        /// </summary>
        public ICollection<LabelVersion>? Versions { get; set; }
    }
}
