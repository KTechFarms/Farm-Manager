﻿using FarmManager.Data.Entities.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Production
{
    /// <summary>
    /// Maps a note to a lot
    /// </summary>
    [Table(nameof(LotNoteMap))]
    public class LotNoteMap : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The Id of the lot this note belongs to
        /// </summary>
        public int LotId { get; set; }
        [ForeignKey(nameof(LotId))]
        public Lot Lot { get; set; }

        /// <summary>
        /// The Id of the actual note
        /// </summary>
        public long NoteId { get; set; }
        [ForeignKey(nameof(NoteId))]
        public Note Note { get; set; }
    }
}
