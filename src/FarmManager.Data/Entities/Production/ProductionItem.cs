﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Production
{
    /// <summary>
    /// Represents an item created or harvested 
    /// on the farm
    /// </summary>
    [Table(nameof(ProductionItem))]
    public class ProductionItem : ItemBase
    {
        /// <summary>
        /// The id of the batch the item was created from
        /// </summary>
        public int BatchId { get; set; }

        /// <summary>
        /// The date the item was produced
        /// </summary>
        public DateTimeOffset ProductionDate { get; set; }

        /// <summary>
        /// The date the item was packaged
        /// </summary>
        public DateTimeOffset? PackageDate { get; set; }

        /// <summary>
        /// The Square SKU of the product
        /// </summary>
        public string? SKU { get; set; }

        /// <summary>
        /// The total weight of the product, including
        /// the container
        /// </summary>
        public double? GrossWeight { get; set; }

        /// <summary>
        /// The weight of the product, not including the 
        /// container
        /// </summary>
        public double? NetWeight { get; set; }

        /// <summary>
        /// The weight of the container
        /// </summary>
        public double? TareWeight { get; set; }

        /// <summary>
        /// The weight as provided on the label
        /// </summary>
        public double? LabelWeight { get; set; }

        /// <summary>
        /// A history of where this item has been
        /// </summary>
        public ICollection<ProductionItemLocationHistory>? LocationHistory { get; set; }
    }
}
