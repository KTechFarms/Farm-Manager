﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Production
{
    /// <summary>
    /// Represents a single version of a particular Label
    /// </summary>
    [Table(nameof(LabelVersion))]
    public class LabelVersion : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The label to which this version belongs
        /// </summary>
        public int LabelId { get; set; }

        /// <summary>
        /// A unique name identifying the label version
        /// in a storage container
        /// </summary>
        public string StorageName { get; set; }

        /// <summary>
        /// The version of the label
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Whether or not this version of the label 
        /// is active and can be used
        /// </summary>
        public bool IsActive { get; set; }
    }
}
