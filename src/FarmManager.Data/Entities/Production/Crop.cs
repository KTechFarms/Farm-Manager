﻿using FarmManager.Data.Entities.Generic;
using FarmManager.Data.Entities.Operations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Production
{
    /// <summary>
    /// Crops are a type of Lot in which
    /// produce is grown and harvested
    /// </summary>
    [Table(nameof(Crop))]
    public class Crop : EntityBase
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The Product Id of the particular seed variant
        /// for this lot
        /// </summary>
        public int? SeedVariantId { get; set; }

        /// <summary>
        /// An identifier for the specific seed being used. This is 
        /// for traceability and will typically be the seed's lot number
        /// or package barcode, if available
        /// </summary>
        public string? SeedIdentifier { get; set; }

        /// <summary>
        /// The number of places that were planted in this lot
        /// </summary>
        public int? PlantedCount { get; set; }

        /// <summary>
        /// The number of seeds planted at each location
        /// </summary>
        public double? SeedsPerPlanting { get; set; }

        /// <summary>
        /// The current status of the crop
        /// </summary>
        public int StatusId { get; set; }
        [ForeignKey(nameof(StatusId))]
        public Status? Status { get; set; }

        /// <summary>
        /// The date at which the lot was planted
        /// </summary>
        public DateTimeOffset PlantedDate { get; set; }

        /// <summary>
        /// The date at which the majority of the lot germinated
        /// </summary>
        public DateTimeOffset? GerminatedDate { get; set; }

        /// <summary>
        /// The date at which the lot was transplanted
        /// </summary>
        public DateTimeOffset? TransplantedDate { get; set; }

        public int? TransplantLocationId { get; set; }
        [ForeignKey(nameof(TransplantLocationId))]
        public FarmLocation? TranplantLocation { get; set; }

        /// <summary>
        /// The date at which harvesting of the lot began
        /// </summary>
        public DateTimeOffset? HarvestBeginDate { get; set; }

        /// <summary>
        /// The date at which harvesting of the lot ended
        /// </summary>
        public DateTimeOffset? HarvestEndDate { get; set; }

        /// <summary>
        /// The date at which this lot was disposed. This should
        /// only be used durring culling and harvesting is not
        /// an option
        /// </summary>
        public DateTimeOffset? DisposalDate { get; set; }
    }
}
