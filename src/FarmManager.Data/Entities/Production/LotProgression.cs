﻿using FarmManager.Data.Entities.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Production
{
    /// <summary>
    /// A table containing information about which 
    /// lot statuses can be promoted to other statuses
    /// </summary>
    [Table(nameof(LotProgression))]
    public class LotProgression : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// A reference to the current status of a lot
        /// </summary>
        public int CurrentStatusId { get; set; }
        [ForeignKey(nameof(CurrentStatusId))]
        public Status CurrentStatus { get; set; }

        /// <summary>
        /// A reference to the next status that a lot
        /// could be promoted to
        /// </summary>
        public int NextStatusId { get; set; }
        [ForeignKey(nameof(NextStatusId))]
        public Status NextStatus { get; set; }

        /// <summary>
        /// The overall status of the lot for the 
        /// given "next status". ie pending, production, or complete
        /// </summary>
        public int NextLotStatusId { get; set; }
        [ForeignKey(nameof(NextLotStatusId))]
        public Status NextLotStatus { get; set; }
    }
}
