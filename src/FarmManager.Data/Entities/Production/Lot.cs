﻿using FarmManager.Data.Entities.Generic;
using FarmManager.Data.Entities.Operations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Production
{
    /// <summary>
    /// Lots are the main unit of production and 
    /// can contain multiple batches
    /// </summary>
    [Table(nameof(Lot))]
    public class Lot : EntityBase
    {
        /// <summary>
        /// The primary Key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// A unique identifier for the lot
        /// </summary>
        public string LotNumber { get; set; }

        /// <summary>
        /// The id of the status of this lot
        /// </summary>
        public int StatusId { get; set; }
        [ForeignKey(nameof(StatusId))]
        public Status? Status { get; set; }

        /// <summary>
        /// The square Id of the product that is 
        /// being produced
        /// </summary>
        public int ProductId { get; set; }
        [ForeignKey(nameof(ProductId))]
        public Product? Product { get; set; }

        /// <summary>
        /// The id of the specific farm location where
        /// the lot is being produced
        /// </summary>
        public int FarmLocationId { get; set; }
        [ForeignKey(nameof(FarmLocationId))]
        public FarmLocation? FarmLocation { get; set; }

        /// <summary>
        /// Batches that were generated from this lot
        /// </summary>
        public ICollection<Batch>? Batches { get; set; }

        /// <summary>
        /// The crop-related information of this lot
        /// </summary>
        public int? CropId { get; set; }
        [ForeignKey(nameof(CropId))]
        public Crop? Crop { get; set; }
    }
}
