﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FarmManager.Data.Entities.Operations;

namespace FarmManager.Data.Entities.Production
{
    /// <summary>
    /// Stores a record of a particular production item's 
    /// location history
    /// </summary>
    [Table(nameof(ProductionItemLocationHistory))]
    public class ProductionItemLocationHistory : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// The id of the production item this record
        /// belongs to
        /// </summary>
        public long ProductionItemId { get; set; }

        /// <summary>
        /// The FarmLocation that the production item resided at
        /// during the time period represented by this record
        /// </summary>
        public int FarmLocationId { get; set; }
        [ForeignKey(nameof(FarmLocationId))]
        public FarmLocation FarmLocation { get; set; }

        /// <summary>
        /// The time at which the item arrived at the specified
        /// location
        /// </summary>
        public DateTimeOffset ArrivalDate { get; set; }

        /// <summary>
        /// The time at which the item departed the specified
        /// location
        /// </summary>
        public DateTimeOffset? DepartureDate { get; set; }
    }
}
