﻿using FarmManager.Data.Entities.Generic;
using FarmManager.Data.Entities.Operations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Data.Entities
{
    /// <summary>
    /// A collection of properties that are common to
    /// all "item" entities
    /// </summary>
    public class ItemBase : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// The location at which the item currently 
        /// resides
        /// </summary>
        public int LocationId { get; set; }
        [ForeignKey(nameof(LocationId))]
        public FarmLocation? Location { get; set; }

        /// <summary>
        /// The barcode for the item, which contains 
        /// various product information
        /// </summary>
        public string? Barcode { get; set; }

        /// <summary>
        /// The current status of the item
        /// </summary>
        public int StatusId { get; set; }
        [ForeignKey(nameof(StatusId))]
        public Status? Status { get; set; }

        /// <summary>
        /// The reason the item was disposed
        /// </summary>
        public int? DisposalReasonId { get; set; }
        [ForeignKey(nameof(DisposalReasonId))]
        public Enum? DisposalReason { get; set; }

        /// <summary>
        /// The date the item was disposed
        /// </summary>
        public DateTimeOffset? DisposalDate { get; set; }

        /// <summary>
        /// The date the item was sold
        /// </summary>
        public DateTimeOffset? SaleDate { get; set; }

        /// <summary>
        /// The date the item was consumed
        /// </summary>
        public DateTimeOffset? ConsumptionDate { get; set; }

        /// <summary>
        /// The date the item was donated
        /// </summary>
        public DateTimeOffset? DonationDate { get; set; }
    }
}
