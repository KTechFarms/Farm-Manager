﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Operations
{
    /// <summary>
    /// Represents an on-hand item in inventory that is
    /// NOT created by a process on the farm
    /// </summary>
    [Table(nameof(Inventory))]
    public class Inventory : ItemBase
    {
        /// <summary>
        /// The product variant to which this
        /// inventory item represents
        /// </summary>
        public int ProductVariantId { get; set; }
        [ForeignKey(nameof(ProductVariantId))]
        public ProductVariant? ProductVariant { get; set; }
    }
}
