﻿using FarmManager.Data.Entities.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Operations
{
    /// <summary>
    /// Represents a location in which business is conducted
    /// </summary>
    [Table(nameof(BusinessLocation))]
    public class BusinessLocation : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The name of the location
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// The address of the location
        /// </summary>
        public int AddressId { get; set; }
        [ForeignKey(nameof(AddressId))]
        public Address? Address { get; set; }

        /// <summary>
        /// A key that ties this location to the location
        /// in an external payment processor, such as Square
        /// or Stripe
        /// </summary>
        public string? ExternalId { get; set; }
    }
}
