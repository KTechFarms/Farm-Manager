﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Data.Entities.Operations
{
    /// <summary>
    /// Represents a particular location on the farm, such as
    /// a cooler, a field, a growing system, or a part of a growing system
    /// </summary>
    [Table(nameof(FarmLocation))]
    public class FarmLocation : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The Id of the business location to which this 
        /// farm location belongs
        /// </summary>
        public int BusinessLocationId { get; set; }
        [ForeignKey(nameof(BusinessLocationId))]
        public BusinessLocation? BusinessLocation { get; set; }

        /// <summary>
        /// The name of the location
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of location, such as production or storage
        /// </summary>
        public int LocationTypeId { get; set; }
        [ForeignKey(nameof(LocationTypeId))]
        public Enum? LocationType { get; set; }

        /// <summary>
        /// A foreign key to the farm location that 
        /// this location is a child of
        /// </summary>
        public int? ParentLocationId { get; set; }
        [NotMapped]
        public FarmLocation? ParentLocation { get; set; }

        /// <summary>
        /// A barcode to represent the location
        /// </summary>
        public string? Barcode { get; set; }
    }
}
