﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Operations
{
    /// <summary>
    /// Represents a product produced or held in inventory
    /// </summary>
    [Table(nameof(Product))]
    public class Product : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// A key that ties this product to the product in an 
        /// external payment processsor, such as Square or stripe
        /// </summary>
        public string? ExternalId { get; set; }

        /// <summary>
        /// The name of the product
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A description of the product
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// The category of the product
        /// </summary>
        public int? CategoryId { get; set; }
        [ForeignKey(nameof(CategoryId))]
        public ProductCategory? Category { get; set; }

        /// <summary>
        /// Whether or not the product is for sale
        /// </summary>
        public bool AvailableForSale { get; set; }

        /// <summary>
        /// A collection of variants for this product
        /// </summary>
        public ICollection<ProductVariant>? Variants { get; set; }
    }
}
