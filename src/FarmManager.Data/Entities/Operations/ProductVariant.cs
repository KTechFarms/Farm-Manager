﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Operations
{
    /// <summary>
    /// Represents a variant of a particular product
    /// </summary>
    [Table(nameof(ProductVariant))]
    public class ProductVariant : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// A key that ties this product variant to the variant in an 
        /// external payment processsor, such as Square or stripe
        /// </summary>
        public string? ExternalId { get; set; }

        /// <summary>
        /// The id of the product to wich this entity belongs
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// The name of the variant
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The sku of the variant
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// The unit of measurement for this variant
        /// </summary>
        public int MeasurementUnitId { get; set; }
        [ForeignKey(nameof(MeasurementUnitId))]
        public Unit? MeasurementUnit { get; set; }

        /// <summary>
        /// The price paid for the variant
        /// </summary>
        //[Column(TypeName = "decimal(18,2)")]
        [Precision(18, 2)]
        public decimal? Price { get; set; }

        /// <summary>
        /// The price at which the variant should be sold
        /// </summary>
        [Precision(18, 2)]
        public decimal? SalePrice { get; set; }

        /// <summary>
        /// Whether alerts should be sent when the amount of product 
        /// on hand reaches a certain number
        /// </summary>
        public bool TrackInventory { get; set; }

        /// <summary>
        /// The lowest count of inventory before an alert should be sent
        /// </summary>
        public int? LowInventoryThreshold { get; set; }

        /// <summary>
        /// The highest count of inventory before an alert should be sent
        /// </summary>
        public int? HighInventoryThreshold { get; set; }
    }
}
