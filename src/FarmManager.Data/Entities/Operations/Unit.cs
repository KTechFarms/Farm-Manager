﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Data.Entities.Operations
{
    /// <summary>
    /// Represents a unit of measure
    /// </summary>
    [Table(nameof(Unit))]
    public class Unit : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// An external Id for this unit for use by 
        /// a payment processing service
        /// </summary>
        public string? ExternalId { get; set; }

        /// <summary>
        /// The name of the unit
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The SI Symbol of the unit
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// The decimal precision of the unit
        /// </summary>
        public int Precision { get; set; }
    }
}
