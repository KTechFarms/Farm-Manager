﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Operations
{
    /// <summary>
    /// A table for storing and maintaining the relationships
    /// between a product variant and their barcodes. Used
    /// when receiving additional inventory
    /// </summary>
    [Table(nameof(ProductVariantBarcode))]
    public class ProductVariantBarcode : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The Id of the product variant that uses the associated
        /// barcode
        /// </summary>
        public int ProductVariantId { get; set; }
        [ForeignKey(nameof(ProductVariantId))]
        public ProductVariant? ProductVariant { get; set; }

        /// <summary>
        /// A barcode used for the associatedd product variant
        /// </summary>
        public string Barcode { get; set; }
    }
}
