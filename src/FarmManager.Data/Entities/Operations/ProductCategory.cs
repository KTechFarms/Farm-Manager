﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Operations
{
    /// <summary>
    /// Represents a category that products can be assigned to 
    /// for organization and tracking purposes
    /// </summary>
    [Table(nameof(ProductCategory))]
    public class ProductCategory : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// An external Id for this category for use by 
        /// a payment processing service
        /// </summary>
        public string? ExternalId { get; set; }

        /// <summary>
        /// The name of the category
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The description of the category
        /// </summary>
        public string? Description { get; set; }
    }
}
