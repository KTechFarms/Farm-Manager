﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Reports.Production
{
    /// <summary>
    /// Represents yield statistics for a given
    /// crop (product) variant
    /// </summary>
    public class CropStatisticYield
    {
        /// <summary>
        /// The name of the variant
        /// </summary>
        public string Variant { get; set; }

        /// <summary>
        /// The average number of production items created 
        /// per batch
        /// </summary>
        public int ItemsPerBatch { get; set; }

        /// <summary>
        /// The average number of production items created
        /// per lot
        /// </summary>
        public int ItemsPerLot { get; set; }

        /// <summary>
        /// The average number of batches harvested per
        /// lot
        /// </summary>
        public int BatchesPerLot { get; set; }

        /// <summary>
        /// The highest number of items harvested in
        /// a single batch
        /// </summary>
        public int MaxItemsPerBatch { get; set; }

        /// <summary>
        /// The lowest number of items harvested in 
        /// a single batch
        /// </summary>
        public int MinItemsPerBatch { get; set; }
    }

    /// <summary>
    /// Stores statistics for crops produced on the farm
    /// </summary>
    [Table(nameof(CropStatistic))]
    public class CropStatistic
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The name of the crop
        /// </summary>
        public string CropName { get; set; }

        /// <summary>
        /// The average number of days for the crop to 
        /// move from "planted" to "germinated" status
        /// </summary>
        public int DaysToGerminate { get; set; }

        /// <summary>
        /// The average number of days for the crop too
        /// move from "planted" to "producing" status
        /// </summary>
        public int DaysToHarvest { get; set; }

        /// <summary>
        /// The total number of lots for the crop
        /// </summary>
        public int LotCount { get; set; }

        /// <summary>
        /// The average number of batches per lot
        /// </summary>
        public int BatchCount { get; set; }

        /// <summary>
        /// A string representation of yields for each 
        /// variant. Stored this way to keep all information
        /// for a report contained within a single table
        /// </summary>
        public string Yields { get; set; }

        /// <summary>
        /// A deserialized representation of Yields
        /// </summary>
        [NotMapped]
        public IEnumerable<CropStatisticYield>? YieldStatistics => 
            JsonConvert.DeserializeObject<List<CropStatisticYield>>(Yields);
    }
}
