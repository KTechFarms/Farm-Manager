﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Devices
{
    /// <summary>
    /// Represents a single reading from any
    /// Device Interface
    /// </summary>
    [Table(nameof(Telemetry))]
    public class Telemetry : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// The Id of the interface to which the telemetry
        /// reading belongs
        /// </summary>
        public int InterfaceId { get; set; }

        /// <summary>
        /// The time at which the telemetry was recorded
        /// </summary>
        public DateTimeOffset TimeStamp { get; set; }

        /// <summary>
        /// The identifier for the value being recorded
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// A numeric value received from the interface
        /// </summary>
        public double? Value { get; set; }

        /// <summary>
        /// A boolean value received from the interface
        /// </summary>
        public bool? BoolValue { get; set; }

        /// <summary>
        /// A string value received from the interface
        /// </summary>
        public string? StringValue { get; set; }
    }
}
