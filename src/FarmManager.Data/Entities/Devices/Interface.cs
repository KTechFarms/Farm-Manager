﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Devices
{
    /// <summary>
    /// Represents an interface between a device and 
    /// the physical world, such as a sensor
    /// </summary>
    [Table(nameof(Interface))]
    public class Interface : EntityBase
    {
        /// <summary>
        /// A Primary Key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// String representing how the interface is identified
        /// on the device itself
        /// </summary>
        public string InterfaceId { get; set; }

        /// <summary>
        /// The Id of the device to which the interface
        /// belongs
        /// </summary>
        public int DeviceId { get; set; }

        /// <summary>
        /// The manufacturer of the interface
        /// </summary>
        public string? Manufacturer { get; set; }

        /// <summary>
        /// Foreign key to the interface's type
        /// entity
        /// </summary>
        public int InterfaceTypeId { get; set; }

        /// <summary>
        /// The actual InterfaceType object referenced
        /// by the InterfaceTypeId
        /// </summary>
        [ForeignKey(nameof(InterfaceTypeId))]
        public InterfaceType InterfaceType { get; set; }

        /// <summary>
        /// The serial number of the interface, if it
        /// has one
        /// </summary>
        public string? SerialNumber { get; set; }

        /// <summary>
        /// A collection of telemetry readings taken by the
        /// interface
        /// </summary>
        public ICollection<Telemetry> Telemetry { get; set; }
    }
}
