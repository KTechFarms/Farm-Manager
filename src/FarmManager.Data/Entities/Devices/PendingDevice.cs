﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Devices
{
    /// <summary>
    /// A device that has been registered in the 
    /// IoT Hub, but has not yet connected to the 
    /// Farm Manager system
    /// </summary>
    [Table(nameof(PendingDevice))]
    public class PendingDevice : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The device's identifier in the IoT Hub.
        /// This value must be unique
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// A unique key generated when the device is
        /// registered that must be provided when 
        /// requesting a SAS token
        /// </summary>
        public Guid DeviceKey { get; set; }

        /// <summary>
        /// Whether or not the device has sent it's
        /// first initialization message to the Farm Manager
        /// system
        /// </summary>
        public bool HasRegistered { get; set; }
    }
}
