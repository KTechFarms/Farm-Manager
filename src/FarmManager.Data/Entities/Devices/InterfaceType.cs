﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Devices
{
    /// <summary>
    /// The type of interface for a device, such
    /// as a 
    /// </summary>
    [Table(nameof(InterfaceType))]
    public class InterfaceType : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The model of the interface type, such as DHT11 or DHT22
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// A description of the interface type
        /// </summary>
        public string Description { get; set; }
    }
}
