﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Devices
{
    /// <summary>
    /// The representation of a physical device within
    /// the FarmManager system
    /// </summary>
    [Table(nameof(Device))]
    public class Device : EntityBase
    {
        /// <summary>
        /// The primary key 
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Foreign key to the device registration
        /// entity
        /// </summary>
        public int PendingDeviceId { get; set; }

        /// <summary>
        /// The actual device registration object referenced
        /// by the PendingDeviceId
        /// </summary>
        [ForeignKey(nameof(PendingDeviceId))]
        public PendingDevice Registration { get; set; }

        /// <summary>
        /// The device's identifier, used by the IoT hub
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// A friendly name for the device
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// The model of the device
        /// </summary>
        public string? Model { get; set; }

        /// <summary>
        /// The serial number of the device
        /// </summary>
        public string? SerialNumber { get; set; }

        /// <summary>
        /// The last time the device communicated with the
        /// FarmManager system
        /// </summary>
        public DateTimeOffset LastConnection { get; set; }

        /// <summary>
        /// A collection of interfaces that interact with the 
        /// physical world
        /// </summary>
        public ICollection<Interface> Interfaces { get; set; }

        /// <summary>
        /// A collection of functions that can be executed remotely
        /// </summary>
        public ICollection<DeviceMethod> Methods { get; set; }

        /// <summary>
        /// A device key that should match the value for the
        /// associated row in the PendingDevice table
        /// </summary>
        [NotMapped]
        public Guid DeviceKey { get; set; }
    }
}
