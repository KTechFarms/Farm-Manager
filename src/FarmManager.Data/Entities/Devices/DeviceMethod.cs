﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Devices
{
    /// <summary>
    /// The definition of a function that can be executed
    /// remotely on a device
    /// </summary>
    [Table(nameof(DeviceMethod))]
    public class DeviceMethod : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The id of the device to which the function belongs
        /// </summary>
        public int DeviceId { get; set; }

        /// <summary>
        /// The name of the function to call
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A description of the function's purpose
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// A json formatted array of parameters that the
        /// function accepts
        /// </summary>
        public string? Parameters { get; set; }
    }
}
