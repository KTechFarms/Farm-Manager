﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Generic
{
    /// <summary>
    /// A table for keeping track of the status of various
    /// entities within Farm Manager
    /// </summary>
    [Table(nameof(Status))]
    public class Status : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// A lowercase, dot delimited string specifying an entity type
        /// to which the status refers
        /// </summary>
        public string EntityId{ get; set; }

        /// <summary>
        /// The snake-case format of the status
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A user-friendly display name
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// A description of the status and what it means
        /// </summary>
        public string? Description { get; set; }
    }
}
