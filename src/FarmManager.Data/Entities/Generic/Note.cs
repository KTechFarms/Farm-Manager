﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Generic
{
    /// <summary>
    /// Represents a note relating to another entity
    /// </summary>
    [Table(nameof(Note))]
    public class Note : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// The contents of the note
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// The Id of the user who created the note
        /// </summary>
        public string UserId { get; set; }
    }
}
