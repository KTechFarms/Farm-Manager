﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.Generic
{
    /// <summary>
    /// Represents the address of a particular entity
    /// </summary>
    [Table(nameof(Address))]
    public class Address : EntityBase
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The street of the address
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// The city of the address
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// The state of the address
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// The zipcode of the address
        /// </summary>
        public string ZipCode { get; set; }
    }
}
