﻿namespace FarmManager.Data.Entities
{
    /// <summary>
    /// A collection of properties that are common to all
    /// Farm Manager entities
    /// </summary>
    public class EntityBase
    {
        /// <summary>
        /// The time that an entity was created in the database
        /// </summary>
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// The last time an entity was modified in the database
        /// </summary>
        public DateTimeOffset Modified { get; set; }

        /// <summary>
        /// Whether or not the entity has been soft deleted
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
