﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.System
{
    /// <summary>
    /// Represents a setting within Farm Manager
    /// </summary>
    [Table(nameof(Setting))]
    public class Setting
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The category to which the setting belongs
        /// </summary>
        public int CategoryId { get; set; }
        [ForeignKey(nameof(CategoryId))]
        public SettingCategory? Category { get; set; }

        /// <summary>
        /// The name of the setting
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The user-facing label for the setting
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// A description as to what the setting is for
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The provided Value for the setting
        /// </summary>
        public string? Value { get; set; }
    }
}
