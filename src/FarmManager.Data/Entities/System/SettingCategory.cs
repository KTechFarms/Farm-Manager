﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.System
{
    /// <summary>
    /// Represents a category for related settings
    /// </summary>
    [Table(nameof(SettingCategory))]
    public class SettingCategory
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The name of the category
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The user-facing label for the category
        /// </summary>
        public string Label { get; set; }
    }
}
