﻿using FarmManager.Data.Converters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmManager.Data.Entities.System
{
    /// <summary>
    /// A table for logging events and informatioon from 
    /// the Farm Manager system
    /// </summary>
    [Table(nameof(Log))]
    public class Log
    {
        /// <summary>
        /// The primary key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The time at which the log was written
        /// </summary>
        public DateTimeOffset Timestamp { get; set; }

        /// <summary>
        /// The severity level of the log
        /// </summary>
        [JsonConverter(typeof(LogLevelConverter))]
        public LogLevel LogLevel { get; set; }

        /// <summary>
        /// The message of the log
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Where the log originated
        /// </summary>
        public string? Source { get; set; }

        /// <summary>
        /// The data associated with the log, in json form
        /// </summary>
        public string? Data { get; set; }

        /// <summary>
        /// The stacktrace, if logging an exception
        /// </summary>
        public string? StackTrace { get; set; }
    }
}
