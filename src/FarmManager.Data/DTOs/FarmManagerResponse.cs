﻿using FarmManager.Data.Converters;
using Newtonsoft.Json;

namespace FarmManager.Data.DTOs
{
    /// <summary>
    /// The possible results of any Farm Manager request
    /// </summary>
    public enum ResponseStatus
    {
        Success,
        Failure
    }

    /// <summary>
    /// Properties contained within every response from
    /// a Farm Manager system or service
    /// </summary>
    public class FarmManagerResponse
    {
        /// <summary>
        /// Whether or not the request was successful
        /// </summary>
        [JsonConverter(typeof(ResponseStatusConverter))]
        public ResponseStatus Status { get; set; }

        /// <summary>
        /// Data returned from the api or actor
        /// </summary>
        public object? Data { get; set; }

        /// <summary>
        /// An error message if something goes wrong
        /// </summary>
        public string? Error { get; set; }
    }
}
