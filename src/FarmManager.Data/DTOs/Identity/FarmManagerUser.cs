﻿namespace FarmManager.Data.DTOs.Identity
{
    /// <summary>
    /// A DTO representing a user in the Farm Manager System
    /// </summary>
    public class FarmManagerUser
    {
        /// <summary>
        /// The user's id from the Identity provider
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// The user's email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// The user's first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The user's last name
        /// </summary>
        public string LastName { get; set; }
    }
}
