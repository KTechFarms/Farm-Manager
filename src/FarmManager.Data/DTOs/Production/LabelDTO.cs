﻿namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// Information relating to labels
    /// </summary>
    public class LabelDTO
    {
        /// <summary>
        /// The id of the label
        /// </summary>
        public int? LabelId { get; set; }

        /// <summary>
        /// The version number of the label
        /// </summary>
        public int? LabelVersionId { get; set; }

        /// <summary>
        /// The user provided name of the label
        /// </summary>
        public string LabelName { get; set; }

        /// <summary>
        /// The version of the label
        /// </summary>
        public int? Version { get; set; }

        /// <summary>
        /// The id of the product variant the label is for
        /// </summary>
        public int? ProductVariant { get; set; }

        /// <summary>
        /// The name of the product variant the label is for
        /// </summary>
        public string? ProductVariantName { get; set; }

        /// <summary>
        /// Whether or not the label is active
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// The name of the label file 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// The binary content of the label file
        /// </summary>
        public byte[] FileContent { get; set; }
    }
}
