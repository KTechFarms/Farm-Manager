﻿using FarmManager.Data.Entities.Production;

namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// Represents a custom barcode object
    /// </summary>
    public class Barcode
    {
        /// <summary>
        /// The raw barcode content
        /// </summary>
        public string RawBarcode { get; set; }

        /// <summary>
        /// The user-friendly, readable barcode
        /// </summary>
        public string UserFriendlyBarcode { get; set; }

        /// <summary>
        /// A collection of AIs within the barcode
        /// </summary>
        public Dictionary<string, string> AIs { get; set; }
    }

    /// <summary>
    /// Information about an inventory lot
    /// </summary>
    public class InventoryLot
    {
        /// <summary>
        /// The lot number of the inventory item
        /// </summary>
        public string LotNumber { get; set; }

        /// <summary>
        /// The product id of the inventory item
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// The name of the inventory item
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// The batches that belong to the lot
        /// </summary>
        public IEnumerable<InventoryBatch> Batches { get; set; }
    }

    /// <summary>
    /// Information about an inventory batch
    /// </summary>
    public class InventoryBatch
    {

        /// <summary>
        /// The batch number of the inventory item
        /// </summary>
        public int BatchNumber { get; set; }

        /// <summary>
        /// The variant id of the inventoroy item
        /// </summary>
        public int VariantId { get; set; }

        /// <summary>
        /// The variant name of the inventory item
        /// </summary>
        public string VariantName { get; set; }

        /// <summary>
        /// The production items that are part of the inventory item
        /// </summary>
        public IEnumerable<ProductionItem> ProductionItems { get; set; }
    }

    /// <summary>
    /// Information about an inventory item
    /// </summary>
    public class InventoryItem
    {
        /// <summary>
        /// Thhe Id of the inventory this object represents
        /// </summary>
        public long InventoryId { get; set; }

        /// <summary>
        /// The Id of the production item this object represents
        /// </summary>
        public long ProductionItemId { get; set; }

        /// <summary>
        /// The Id of the product of this object
        /// </summary>
        public int ProductId { get; set; }
        
        /// <summary>
        /// The name of the product
        /// </summary>
        public string Product { get; set; }
        
        /// <summary>
        /// The Id of the variant of this object
        /// </summary>
        public int VariantId { get; set; }

        /// <summary>
        /// The name of the product variant
        /// </summary>
        public string Variant { get; set; }

        /// <summary>
        /// The barcode of the production item
        /// </summary>
        public Barcode? Barcode { get; set; }

        /// <summary>
        /// The production date of the item
        /// </summary>
        public DateTimeOffset ProductionDate { get; set; }

        /// <summary>
        /// The Lot number of the production item
        /// </summary>
        public string LotNumber { get; set; }

        /// <summary>
        /// The batch number of the production item
        /// </summary>
        public int BatchNumber { get; set; }

        /// <summary>
        /// The Quantity of Inventory Items represented by 
        /// this object
        /// </summary>
        public int Quantity { get; set; }
    }
}
