﻿namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// Used to define a transfer inventory from one location to another
    /// </summary>
    public class InventoryTransfer
    {
        /// <summary>
        /// The id of the location where the inventory currently resides
        /// </summary>
        public int FromLocationId { get; set; }

        /// <summary>
        /// The id of the location where the inventory is being moved to
        /// </summary>
        public int ToLocationId { get; set; }

        /// <summary>
        /// The production item ids that are being transferred
        /// </summary>
        public List<long> ProductionItemIds { get; set; }

        /// <summary>
        /// The inventroy ids that are being transferred
        /// </summary>
        public List<long> InventoryIds { get; set; }
    }
}
