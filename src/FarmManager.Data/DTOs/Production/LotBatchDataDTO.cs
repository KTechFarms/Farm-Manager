﻿using FarmManager.Data.Entities.Operations;

namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// Information about a lot's batch
    /// </summary>
    public class LotBatchDataDTO
    {
        /// <summary>
        /// The batch number
        /// </summary>
        public int BatchNumber { get; set; }

        /// <summary>
        /// Variants that can be created for the batch
        /// </summary>
        public List<ProductVariant> Variants { get; set; }

        /// <summary>
        /// Locations that can be used for the batch
        /// </summary>
        public List<FarmLocation> Locations { get; set; }
    }
}
