﻿using FarmManager.Data.Entities.Production;

namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// A DTO containing lot progression information
    /// </summary>
    public class LotProgressionDTO
    {
        /// <summary>
        /// Possible progressions that a lot could revert to
        /// </summary>
        public List<LotProgression> PastProgressionOptions { get; set; }

        /// <summary>
        /// Possible progressions that a lot could be promoted to
        /// </summary>
        public List<LotProgression> FutureProgressionOptions { get; set; }
    }
}
