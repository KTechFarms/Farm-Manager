﻿namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// Represents pertinent information about a batch
    /// </summary>
    public class BatchDTO
    {
        /// <summary>
        /// The batch number
        /// </summary>
        public int BatchNumber { get; set; }

        /// <summary>
        /// The display name of the status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The name of the location where the batch resides
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// The number of items at the location
        /// </summary>
        public int ItemCount { get; set; }
    }
}
