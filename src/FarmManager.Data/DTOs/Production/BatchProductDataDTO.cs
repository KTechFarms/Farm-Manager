﻿namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// Pertinent information about a batch's prooduct
    /// </summary>
    public class BatchProductDataDTO
    {
        /// <summary>
        /// The name of the product
        /// </summary>
        public string Product { get; set; }

        /// <summary>
        /// The name of the variant
        /// </summary>
        public string Variant { get; set; }

        /// <summary>
        /// The name of the location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// What the next product count will be
        /// </summary>
        public int NextProductCount { get; set; }
    }
}
