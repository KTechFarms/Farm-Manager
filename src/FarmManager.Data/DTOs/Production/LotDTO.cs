﻿using FarmManager.Data.Entities.Production;

namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// Information about a lot
    /// </summary>
    public class LotDTO
    {
        /// <summary>
        /// The id of the lot
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The Lot Number
        /// </summary>
        public string LotNumber { get; set; }

        /// <summary>
        /// The product being created in the lot
        /// </summary>
        public string? Product { get; set; }

        /// <summary>
        /// The production location of the lot
        /// </summary>
        public string? Location { get; set; }

        /// <summary>
        /// The production location of the lot after transplanting
        /// </summary>
        public string? TransplantedLocation { get; set; }

        /// <summary>
        /// The current status of the lot
        /// </summary>
        public string? Status { get; set; }

        /// <summary>
        /// The current product-specific status. 
        /// For example the crop status, if the lot
        /// is for crops
        /// </summary>
        public string? ProductStatus { get; set; }

        /// <summary>
        /// The date the lot was planted
        /// </summary>
        public DateTimeOffset? CreatedDate { get; set; }

        /// <summary>
        /// The number of notes created for this lot
        /// </summary>
        public int? NoteCount { get; set; }
    }
}
