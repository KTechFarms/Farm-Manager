﻿namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// Used to define an inventory disposal request
    /// </summary>
    public class InventoryDisposal
    {
        /// <summary>
        /// The id of the reason inventory is being disposed
        /// </summary>
        public int DisposalReasonId { get; set; }

        /// <summary>
        /// A list of production item ids that are being disposed
        /// </summary>
        public List<long> ProductionItemIds { get; set; }

        /// <summary>
        /// A list of inventory ids that are being disposed
        /// </summary>
        public List<long> InventoryIds { get; set; }
    }
}
