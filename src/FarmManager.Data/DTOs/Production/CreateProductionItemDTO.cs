﻿namespace FarmManager.Data.DTOs.Production
{
    /// <summary>
    /// Pertinent information needed to create a production item
    /// </summary>
    public class CreateProductionItemDTO
    {
        /// <summary>
        /// How many items are being created
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// The tare weight of the product
        /// </summary>
        public double? TareWeight { get; set; }

        /// <summary>
        /// the gros weight of the product
        /// </summary>
        public double? GrossWeight { get; set; }

        /// <summary>
        /// The label weight of the product
        /// </summary>
        public double? LabelWeight { get; set; }
    }
}
