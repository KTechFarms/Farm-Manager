﻿namespace FarmManager.Data.DTOs.Queries
{
    /// <summary>
    /// A collection of parameters used for querying lots 
    /// from Farm Manager
    /// </summary>
    public class LotQueryParams
    {
        /// <summary>
        /// The name of the lot type
        /// </summary>
        public string? LotType { get; set; }

        /// <summary>
        /// A comma delimited string containing the lot
        /// statuses to retrieve
        /// </summary>
        public string? Status { get; set; }
    }
}
