﻿using Microsoft.Extensions.Logging;

namespace FarmManager.Data.DTOs.Queries
{
    /// <summary>
    /// A collection of parameters used for querying logs
    /// from Farm Manager
    /// </summary>
    public class LogQueryParams
    {
        // TODO: Enable pagination
        //private const int _maxPageSize = 100;
        //private int _pageSize = 50;
        //private int _page = 1;

        /// <summary>
        /// The source of the log. Sources will be the
        /// full class name of where the log was generated
        /// </summary>
        public string? Source { get; set; }

        /// <summary>
        /// The text that you are searching a log for
        /// </summary>
        public string? SearchText { get; set; }

        /// <summary>
        /// The fields that should be matched against to 
        /// find the SearchText
        /// </summary>
        public string? SearchFields { get; set; }

        /// <summary>
        /// Whether the most recent logs should be retrieved
        /// first, or the oldest
        /// </summary>
        public bool MostRecent { get; set; } = true;

        /// <summary>
        /// The level of logs that should be returned
        /// </summary>
        public LogLevel? LogLevel { get; set; }

        /// <summary>
        /// The lower bound for when a log was created
        /// </summary>
        public DateTimeOffset? StartTime { get; set; }

        /// <summary>
        /// The upper bound for when a log was created
        /// </summary>
        public DateTimeOffset? StopTime { get; set; }

        //public int Page
        //{
        //    get { return _page; }
        //    set
        //    {
        //        if (value < 1)
        //            _page = 1;
        //        else
        //            _page = value;
        //    }
        //}

        //public int PageSize
        //{
        //    get { return _pageSize; }
        //    set
        //    {
        //        if (value > _maxPageSize)
        //            _pageSize = _maxPageSize;
        //        else
        //            _pageSize = value;
        //    }
        //}
    }
}
