﻿namespace FarmManager.Data.DTOs.Queries
{
    /// <summary>
    /// Query parameters used to find farm locations
    /// </summary>
    public class FarmLocationQueryParams
    {
        /// <summary>
        /// The type of locations to retrieve
        /// </summary>
        public string? Type { get; set; }
    }
}
