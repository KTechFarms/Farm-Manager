﻿namespace FarmManager.Data.DTOs.Queries
{
    /// <summary>
    /// A collection of parameters used for querying devices
    /// from Farm Manager
    /// </summary>
    public class DeviceQueryParams
    {
        /// <summary>
        /// The DeviceId of the device
        /// </summary>
        public string? DeviceId { get; set; }

        /// <summary>
        /// The user friendly name of the device
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// The model of the device
        /// </summary>
        public string? Model { get; set; }

        /// <summary>
        /// The serial number of the device
        /// </summary>
        public string? SerialNumber { get; set; }
    }
}
