﻿namespace FarmManager.Data.DTOs.System.Dashboard
{
    /// <summary>
    /// An object containing data about the number of devices connected
    /// to the Farm Manager system
    /// </summary>
    public class DeviceCountSummary
    {
        /// <summary>
        /// The number of active devices that have made contact 
        /// with the system
        /// </summary>
        public int ActiveDevices { get; set; }

        /// <summary>
        /// The number of devices registered with Farm Manager that
        /// have not yet connected or initialized.
        /// </summary>
        public int PendingDevices { get; set; }
    }
}
