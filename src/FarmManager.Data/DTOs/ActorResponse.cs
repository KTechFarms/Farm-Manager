﻿namespace FarmManager.Data.DTOs
{
    /// <summary>
    /// A response from any Farm Manager ActorSystem
    /// </summary>
    public class FarmManagerActorResponse : FarmManagerResponse
    {
    }

    /// <summary>
    /// A helper class to return various types of responses
    /// from Farm Manager Actors
    /// </summary>
    public static class ActorResponse
    {
        /// <summary>
        /// A response indicating a successful result
        /// </summary>
        /// <param name="data">The data that should be returned</param>
        /// <returns></returns>
        public static FarmManagerActorResponse Success(object? data) =>
            new FarmManagerActorResponse
            {
                Status = ResponseStatus.Success,
                Data = data,
                Error = null
            };

        /// <summary>
        /// A response indicating a failed result
        /// </summary>
        /// <param name="message">The error message that should be returned</param>
        /// <returns></returns>
        public static FarmManagerActorResponse Failure(string message) =>
            new FarmManagerActorResponse
            {
                Status = ResponseStatus.Failure,
                Data = null,
                Error = message
            };
    }
}
