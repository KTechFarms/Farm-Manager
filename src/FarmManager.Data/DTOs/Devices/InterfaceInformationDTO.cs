﻿namespace FarmManager.Data.DTOs.Devices
{
    /// <summary>
    /// A data transfer object for returning interface
    /// information to FarmManager.Web
    /// </summary>
    public class InterfaceInformationDTO
    {
        /// <summary>
        /// The primary key of the interface
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The id of the interface
        /// </summary>
        public string InterfaceId { get; set; }

        /// <summary>
        /// The model of the interface
        /// </summary>
        public string Model { get; set; }
    }
}
