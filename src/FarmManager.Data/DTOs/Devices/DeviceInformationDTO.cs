﻿namespace FarmManager.Data.DTOs.Devices
{
    /// <summary>
    /// A data transfer object for returning device information to 
    /// FarmManager.Web
    /// </summary>
    public class DeviceInformationDTO
    {
        /// <summary>
        /// The Id of the device
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The name of the device
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// The model of the device
        /// </summary>
        public string? Model { get; set; }

        /// <summary>
        /// The serial number of the device
        /// </summary>
        public string? SerialNumber { get; set; }

        /// <summary>
        /// The last time the device connected to Farm Manager
        /// </summary>
        public DateTimeOffset? LastConnection { get; set; }

        /// <summary>
        /// The private key for the device
        /// </summary>
        public Guid DeviceKey { get; set; }

        /// <summary>
        /// A list of interface information for the device
        /// </summary>
        public List<InterfaceInformationDTO> Interfaces { get; set; }

        /// <summary>
        /// A collection of telemetry collected from the device over 
        /// a specified time frame
        /// </summary>
        public List<TimeGroupedTelemetry> Telemetry { get; set; }

        /// <summary>
        /// A collection of methods on the device that can be called 
        /// remotely
        /// </summary>
        public List<MethodInformationDTO> Methods { get; set; }
    }
}
