﻿namespace FarmManager.Data.DTOs.Devices
{
    /// <summary>
    /// A Data transfer object for returning devices to 
    /// FarmManager.Web
    /// </summary>
    public class DeviceDTO
    {
        /// <summary>
        /// The id of the device
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The name of the device
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// The serial number of the device
        /// </summary>
        public string? SerialNumber { get; set; }

        /// <summary>
        /// The model of the device
        /// </summary>
        public string? Model { get; set; }

        /// <summary>
        /// Whether the device is pending an intial connection
        /// </summary>
        public bool IsPending { get; set; }

        /// <summary>
        /// The last time the device connected to the Farm Manager System
        /// </summary>
        public DateTimeOffset? LastConnection { get; set; }
    }
}
