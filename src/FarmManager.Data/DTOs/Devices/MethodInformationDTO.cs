﻿namespace FarmManager.Data.DTOs.Devices
{
    /// <summary>
    /// The represesntation of a direct method that can
    /// easily be displayed and utilized by FarmManager.Web
    /// </summary>
    public class MethodInformationDTO
    {
        /// <summary>
        /// The name of the direct method
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A description of what the direct method does
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// A collection of parameter names and types
        /// required by the direct method
        /// </summary>
        public Dictionary<string, Type>? Parameters { get; set; }
    }
}
