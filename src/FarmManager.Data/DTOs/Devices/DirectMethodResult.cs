﻿namespace FarmManager.Data.DTOs.Devices
{
    /// <summary>
    /// A DTO representing the result of 
    /// executing a direct method remotely
    /// </summary>
    public class DirectMethodResult
    {
        /// <summary>
        /// Whether or not the execution was successfull
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// A message, if the execution was 
        /// unsuccessfull
        /// </summary>
        public string Message { get; set; }
    }
}
