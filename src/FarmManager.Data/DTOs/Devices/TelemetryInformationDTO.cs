﻿namespace FarmManager.Data.DTOs.Devices
{
    /*
    /// <summary>
    /// An interface representing telemetry readings
    /// </summary>
    public interface ITelemetryInformation { }

    /// <summary>
    /// A data transfer object for returning telemetry information
    /// to FarmManager.Web where telemetry is grouped by interface
    /// </summary>
    public class InterfaceGroupedTelemetry //: ITelemetryInformation
    {
        /// <summary>
        /// The interface id from which the telemetry was collected
        /// </summary>
        public string InterfaceId { get; set; }

        /// <summary>
        /// A dictionary of times and values collected from the interface
        /// </summary>
        public Dictionary<DateTimeOffset, double> Values { get; set; }
    }
    */

    /// <summary>
    /// A data transfer object for returning telemetry information
    /// to FarmManager.Web where telemetry is grouped by timestamp
    /// </summary>
    public class TimeGroupedTelemetry //: ITelemetryInformation
    {
        /// <summary>
        /// The time at which telemetry was recorded
        /// </summary>
        public DateTimeOffset Timestamp { get; set; }

        /// <summary>
        /// A dictionary of interface readings for at the time 
        /// represented by Timestamp
        /// </summary>
        public Dictionary<string, double> Readings { get; set; }
    }
}