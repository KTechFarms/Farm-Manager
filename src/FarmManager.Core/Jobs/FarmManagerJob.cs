﻿using FarmManager.Services.Logging;

namespace FarmManager.Core.Jobs
{
    /// <summary>
    /// A generic job class that all Farm Manager Jobs inherit
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class FarmManagerJob<T> where T : class
    {
        /// <summary>
        /// A logger to utilize during the job
        /// </summary>
        protected readonly IFarmLogger _logger;

        public FarmManagerJob(IFarmLogger logger)
        {
            _logger = logger;
            _logger.Initialize<T>();
        }
    }
}
