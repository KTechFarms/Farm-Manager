﻿using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Production;
using FarmManager.Data.Entities.Reports.Production;
using FarmManager.Services.Logging;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace FarmManager.Core.Jobs
{
    /// <summary>
    /// Jobs that calculate and pre-stage data for the various Farm Manager reports
    /// </summary>
    public class ReportJobs : FarmManagerJob<ReportJobs>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public ReportJobs(
            IFarmLogger logger, 
            IDbContextFactory<FarmManagerContext> dbFactory
        ) : base(logger)
        {
            _dbFactory = dbFactory;
        }

        //////////////////////////////////////////
        //           Report Logic               //
        //////////////////////////////////////////
        
        /// <summary>
        /// Creates or updates the statistics data for each crop in Farm Manager
        /// </summary>
        /// <returns></returns>
        public async Task GenerateStatisticsReport()
        {
            _logger.LogInformation("Beginning crop statistic calculations");

            try
            {
                using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

                List<IGrouping<string, Lot>> productGroupedLots = await dbContext.Lots
                    .Include("Batches.Variant")
                    .Include("Batches.ProductionItems")
                    .Include("Crop")
                    .AsNoTracking()
                    .Where(l => l.CropId != null)
                    .GroupBy(l => l.Product.Name)
                    .ToListAsync();

                List<CropStatistic> currentStatistics = CalculateCurrentStatistics(productGroupedLots);
                List<CropStatistic> dbStatistics = await dbContext.CropStatistics.ToListAsync();

                foreach (CropStatistic statistic in currentStatistics)
                {
                    CropStatistic? dbStatistic = dbStatistics
                        .Where(s => s.CropName == statistic.CropName)
                        .FirstOrDefault();

                    if (dbStatistic == null)
                        dbContext.Add(statistic);
                    else
                    {
                        dbStatistic.DaysToGerminate = statistic.DaysToGerminate;
                        dbStatistic.DaysToHarvest = statistic.DaysToHarvest;
                        dbStatistic.LotCount = statistic.LotCount;
                        dbStatistic.BatchCount = statistic.BatchCount;
                        dbStatistic.Yields = statistic.Yields;
                    }
                }

                await dbContext.SaveChangesAsync();

                _logger.LogInformation("Crop statistic calculation completed");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error calculating crop statistics", ex);
            }
        }

        //////////////////////////////////////////
        //               Helpers                //
        //////////////////////////////////////////
    
        /// <summary>
        /// Calculates the statistics for a group of lots
        /// </summary>
        /// <param name="productGroupedLots">Lots retrieved from the database, grouped according to product name</param>
        /// <returns></returns>
        private List<CropStatistic> CalculateCurrentStatistics(List<IGrouping<string, Lot>> productGroupedLots)
        {
            List<CropStatistic> statistics = new();

            foreach (IGrouping<string, Lot> productLots in productGroupedLots)
            {
                CropStatistic productStatistics = new()
                {
                    CropName = productLots.Key,
                    LotCount = productLots.Count()
                };

                List<int> daysToGerminate = new();
                List<int> daysToHarvest = new();
                List<int> batchCount = new();
                List<CropStatisticYield> lotYields = new();

                foreach (Lot lot in productLots)
                {
                    int germinationTime = lot.Crop?.PlantedDate != null && lot.Crop?.GerminatedDate != null
                        ? lot.Crop.GerminatedDate.Value.Subtract(lot.Crop.PlantedDate).Days
                        : 0;

                    int harvestTime = lot.Crop?.PlantedDate != null && lot.Crop?.HarvestBeginDate != null
                        ? lot.Crop.HarvestBeginDate.Value.Subtract(lot.Crop.PlantedDate).Days
                        : 0;

                    daysToGerminate.Add(germinationTime);
                    daysToHarvest.Add(harvestTime);
                    batchCount.Add(lot.Batches?.Count ?? 0);

                    if (lot.Batches != null)
                        lotYields.AddRange(CalculateLotBatchYields(lot.Batches));
                }

                productStatistics.DaysToGerminate = daysToGerminate.Sum() / daysToGerminate.Count;
                productStatistics.DaysToHarvest = daysToHarvest.Sum() / daysToHarvest.Count;
                productStatistics.BatchCount = batchCount.Sum() / batchCount.Count;
                productStatistics.Yields = JsonConvert.SerializeObject(CalculateProductYields(lotYields));
                statistics.Add(productStatistics);
            }

            return statistics;
        }

        /// <summary>
        /// Calculates statistics for a collection of batches
        /// </summary>
        /// <param name="batches">The batches belonging to a single lot</param>
        /// <returns></returns>
        private List<CropStatisticYield> CalculateLotBatchYields(ICollection<Batch> batches)
        {
            List<CropStatisticYield> lotYields = new();

            List<IGrouping<string, Batch>> variantGroupedBatches = batches
                .GroupBy(b => b.Variant.Name)
                .ToList();

            foreach (IGrouping<string, Batch> variantBatches in variantGroupedBatches)
            {
                CropStatisticYield variantYield = new()
                {
                    Variant = variantBatches.Key,
                    BatchesPerLot = variantBatches.Count()
                };

                List<int> batchItems = new();
                int maxBatchItems = 0;
                int minBatchItems = 0;

                foreach (Batch batch in variantBatches)
                {
                    int batchItemCount = batch.ProductionItems?.Count ?? 0;

                    batchItems.Add(batchItemCount);
                    if (batchItemCount > maxBatchItems)
                        maxBatchItems = batchItemCount;
                    if (minBatchItems == 0 || batchItemCount < minBatchItems)
                        minBatchItems = batchItemCount;
                }

                variantYield.ItemsPerBatch = batchItems.Sum() / batchItems.Count;
                variantYield.MaxItemsPerBatch = maxBatchItems;
                variantYield.MinItemsPerBatch = minBatchItems;
                lotYields.Add(variantYield);
            }

            return lotYields;
        }

        /// <summary>
        /// Calculates the overall yields for products of a given lot
        /// </summary>
        /// <param name="lotYields">All of the batch-level yields for the lot</param>
        /// <returns></returns>
        private List<CropStatisticYield> CalculateProductYields(List<CropStatisticYield> lotYields)
        {
            List<CropStatisticYield> productStatistics = new();

            List<IGrouping<string, CropStatisticYield>> variantGroupedYields = lotYields
                .GroupBy(y => y.Variant)
                .ToList();

            foreach (IGrouping<string, CropStatisticYield> variantYieldGroup in variantGroupedYields)
            {
                int groupYieldCount = variantYieldGroup.Count();

                CropStatisticYield productYield = new()
                {
                    Variant = variantYieldGroup.Key,
                };

                List<int> itemsPerBatch = new();
                List<int> maxItemsPerBatch = new();
                List<int> minItemsPerBatch = new();
                List<int> batchesPerLot = new();
                List<int> itemsPerLot = new();

                foreach (CropStatisticYield yield in variantYieldGroup)
                {
                    itemsPerBatch.Add(yield.ItemsPerBatch);
                    maxItemsPerBatch.Add(yield.MaxItemsPerBatch);
                    minItemsPerBatch.Add(yield.MinItemsPerBatch);
                    batchesPerLot.Add(yield.BatchesPerLot);
                    itemsPerLot.Add(yield.ItemsPerBatch * yield.BatchesPerLot);
                }

                productYield.BatchesPerLot = batchesPerLot.Sum() / batchesPerLot.Count;
                productYield.ItemsPerBatch = itemsPerBatch.Sum() / itemsPerBatch.Count;
                productYield.ItemsPerLot = itemsPerLot.Sum() / itemsPerLot.Count;
                productYield.MaxItemsPerBatch = maxItemsPerBatch.Max();
                productYield.MinItemsPerBatch = minItemsPerBatch.Min();

                productStatistics.Add(productYield);
            }

            return productStatistics;
        }
    }
}
