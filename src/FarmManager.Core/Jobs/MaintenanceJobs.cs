﻿using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.System;
using FarmManager.Services.Logging;
using Microsoft.EntityFrameworkCore;

namespace FarmManager.Core.Jobs
{
    /// <summary>
    /// Jobs that help maintain a clean system
    /// </summary>
    public class MaintenanceJobs : FarmManagerJob<MaintenanceJobs>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public MaintenanceJobs(
            IFarmLogger logger,
            IDbContextFactory<FarmManagerContext> dbFactory
        ) : base(logger)
        {
            _dbFactory = dbFactory;
        }

        /// <summary>
        /// Removes system logs that are older than 60 days
        /// </summary>
        /// <returns></returns>
        public async Task SystemLogCleanup()
        {
            _logger.LogInformation("Beginning system log cleanup");

            try
            {
                using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

                List<Log> oldLogs = await dbContext.Logs
                    .Where(l => l.Timestamp < DateTimeOffset.UtcNow.AddDays(-60))
                    .ToListAsync();

                dbContext.RemoveRange(oldLogs);
                await dbContext.SaveChangesAsync();

                _logger.LogInformation("System log cleanup completed");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error cleaning system logs", ex);
            }
        }
    }
}
