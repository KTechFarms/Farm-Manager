﻿using FarmManager.Data.ActorMessages.Production.Labels;
using FarmManager.Data.Contexts;
using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Services.Storage;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Actors.Production.Labels
{
    /// <summary>
    /// The worker responsible for handling all label related logic
    /// </summary>
    public class LabelWorker : FarmManagerActor<LabelWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;
        private readonly IStorageService _storageService;

        private readonly string _labelContainer;

        public LabelWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            _storageService = _scope.ServiceProvider
                .GetRequiredService<IStorageService>();

            _labelContainer = _settings?.Storage?.LabelContainer ?? string.Empty;

            Receive<AskForLabels>(message => HandleWithAsync(RetrieveLabels, message));
            Receive<AskToCreateLabel>(message => HandleWithAsync(CreateLabel, message));
            Receive<AskToUpdateLabel>(message => HandleWithAsync(UpdateLabel, message));
            Receive<AskToDeleteLabel>(message => HandleWithAsync(DeleteLabel, message));
        }

        /// <summary>
        /// Retrieves a list of labels
        /// </summary>
        /// <param name="message">A message requesting a list of labels</param>
        /// <returns></returns>
        private async Task<IEnumerable<LabelDTO>> RetrieveLabels(AskForLabels message)
        {
            List<LabelDTO> results = new();
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            List<Label> labels = await dbContext.Labels
                .AsNoTracking()
                .Include("Versions")
                .Include("ProductVariant")
                .Where(l => l.IsDeleted == false)
                .ToListAsync();

            foreach(Label label in labels)
            {
                foreach (LabelVersion version in label.Versions ?? new List<LabelVersion>())
                    results.Add(new LabelDTO
                    {
                        LabelId = label.Id,
                        LabelVersionId = version.Id,
                        LabelName = label.LabelName,
                        Version = version.Version,
                        ProductVariant = label.ProductVariantId,
                        ProductVariantName = label.ProductVariant?.Name,
                        IsActive = version.IsActive,
                    });
            }

            return results;
        }

        /// <summary>
        /// Creates a new label
        /// </summary>
        /// <param name="message">A message containing the information needed for
        /// creating a label</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<Label> CreateLabel(AskToCreateLabel message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            if (
                string.IsNullOrEmpty(message.Label.LabelName)
                || string.IsNullOrEmpty(message.Label.FileName)
                || message.Label.ProductVariant == 0
                || message.Label.FileContent == null
            )
                throw new ApplicationException("Name and content are required");

            ProductVariant? variant = await dbContext.ProductVariants
                .AsNoTracking()
                .Where(v => v.Id == message.Label.ProductVariant)
                .FirstOrDefaultAsync();

            if (variant == null)
                throw new ApplicationException("Variant not found");

            string fileName = await _storageService
                .SaveFileAsync(
                    message.Label.FileName, 
                    message.Label.FileContent, 
                    _labelContainer
                );

            Label newLabel = new()
            {
                LabelName = message.Label.LabelName,
                IsActive = true,
                ProductVariantId = variant.Id,
                Versions = new List<LabelVersion>
                {
                    new LabelVersion
                    {
                        StorageName = fileName,
                        Version = 1,
                        IsActive = true,
                    }
                }
            };

            await dbContext.AddAsync(newLabel);
            await dbContext.SaveChangesAsync();

            return newLabel;
        }

        /// <summary>
        /// Updates a label
        /// </summary>
        /// <param name="message">A message containing the information needed
        /// to update a label</param>
        /// <returns></returns>
        private async Task<Label> UpdateLabel(AskToUpdateLabel message)
        {
            return new Label();
        }

        /// <summary>
        /// Deletes a label
        /// </summary>
        /// <param name="message">A message containing the Id of the label
        /// that should be deleted</param>
        /// <returns></returns>
        private async Task<IEnumerable<int>> DeleteLabel(AskToDeleteLabel message)
        {
            return new List<int>();
        }
    }
}
