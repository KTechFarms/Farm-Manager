﻿using Akka.Actor;
using Akka.DependencyInjection;

namespace FarmManager.Core.Actors.Production.Lots
{
    /// <summary>
    /// The actor responsible for managing all production logic surrounding 
    /// lots and batches
    /// </summary>
    public class LotManager : FarmManagerActor<LotManager>
    {
        private IActorRef? _lotWorker;

        public LotManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            ReceiveAny(ForwardToWorker);
        }

        protected override void PreStart()
        {
            Props lotProps = DependencyResolver.For(Context.System).Props<LotWorker>();
            _lotWorker = Context.ActorOf(lotProps, "LotWorker");
        }

        /// <summary>
        /// Forwards the request to the lot worker
        /// </summary>
        /// <param name="message"></param>
        private void ForwardToWorker(object message)
        {
            IUntypedActorContext context = Context;
            IActorRef sender = Sender;
            TimeSpan timeout = TimeSpan.FromSeconds(_settings.ActorWaitSeconds);

            _lotWorker.Ask(message, timeout)
                .ContinueWith(result => sender.Tell(result.Result));
        }
    }
}
