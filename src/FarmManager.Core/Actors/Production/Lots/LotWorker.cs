﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Common.Helpers;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.ActorMessages.Operations.Products;
using FarmManager.Data.ActorMessages.Production.FarmLocations;
using FarmManager.Data.ActorMessages.Production.Lots;
using FarmManager.Data.Contexts;
using FarmManager.Data.DTOs;
using FarmManager.Data.DTOs.Identity;
using FarmManager.Data.DTOs.Production;
using FarmManager.Data.DTOs.Queries;
using FarmManager.Data.Entities.Generic;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Services.Identity;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using Status = FarmManager.Data.Entities.Generic.Status;

namespace FarmManager.Core.Actors.Production.Lots
{
    /// <summary>
    /// The actual worker that carries out logic related to production lots
    /// </summary>
    public class LotWorker : FarmManagerActor<LotWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;
        private readonly IIdentityProviderService _idpService;
        private readonly string _inventoryQueue;
        private IMessageBrokerPublisher? _inventoryPublisher;

        private string LotDate { get; set; }
        private int CurrentLotCount { get; set; }

        public LotWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            _idpService = _scope.ServiceProvider
                .GetRequiredService<IIdentityProviderService>();

            _inventoryQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcesing)
                ?.QueueName ?? string.Empty;

            Receive<AskToRetrieveLots>(message => HandleWithAsync(RetrieveLots, message));
            Receive<AskForLotProgressions>(message => HandleWithAsync(RetrieveLotProgressions, message));
            Receive<AskForLotBatches>(message => HandleWithAsync(RetrieveLotBatches, message));
            Receive<AskForLotNotes>(message => HandleWithAsync(RetrieveLotNotes, message));

            Receive<AskForNextLotNumber>(message => HandleWith(RetrieveNextLotNumber, message));
            Receive<AskForNextBatchData>(message => HandleWith(RetrieveNextBatchData, message));
            Receive<AskForNextProductData>(message => HandleWith(RetrieveNextProductData, message));
            Receive<AskToCreateLot>(message => HandleWith(CreateLot, message));
            Receive<AskToPromoteLot>(message => HandleWith(PromoteLot, message));
            Receive<AskToCreateBatch>(message => HandleWith(CreateBatch, message));
            Receive<AskToCloseBatch>(message => HandleWith(CloseBatch, message));
            Receive<AskToCreateProductionItem>(message => HandleWith(CreateProductionItem, message));
            Receive<AskToCreateLotNote>(message => HandleWith(CreateNote, message));
        }

        protected override void PreStart()
        {
            _inventoryPublisher = _farmManager.Ask(
                new AskForMessageBrokerPublisher(
                    _inventoryQueue,
                    $"{Self}"
                )).Result as IMessageBrokerPublisher;

            SetLotNumber();
        }

        protected override void PostStop()
        {
            _inventoryPublisher?.Dispose();
            base.PostStop();
        }

        /// <summary>
        /// Sets the current lot number on startup
        /// </summary>
        private void SetLotNumber()
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            var julianDateTime = DateTimeOffset.UtcNow.Date;
            LotDate = GenerateLotDate();

            CurrentLotCount = dbContext.Lots
                .Where(l => l.LotNumber.StartsWith(LotDate))
                .Count();
        }

        /// <summary>
        /// Generates the 7-digit julian date
        /// </summary>
        /// <returns></returns>
        private string GenerateLotDate()
        {
            var julianDateTime = DateTimeOffset.UtcNow.Date;
            return julianDateTime.ToString("yyyy") + julianDateTime.DayOfYear.ToString("000");
        }


        /// <summary>
        /// Returns the next available lot number based on the current date
        /// and lots produced
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private string RetrieveNextLotNumber(AskForNextLotNumber message)
        {
            string currentLotDateString = GenerateLotDate();
            if (
                int.TryParse(currentLotDateString, out int currentLotDate)
                && int.TryParse(LotDate, out int savedLotDate)
                && currentLotDate > savedLotDate
            )
            {
                LotDate = currentLotDateString;
                CurrentLotCount = 0;
            }
            else
            {
                using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

                CurrentLotCount = dbContext.Lots
                    .Where(l => l.LotNumber.StartsWith(LotDate))
                    .Count();
            }

            int nextLot = CurrentLotCount + 1;
            return $"{LotDate}{nextLot.ToString("000")}";
        }

        /// <summary>
        /// Retrieves pertinent information about the next batch that
        /// can be displayed in the FarmManager UI
        /// </summary>
        /// <param name="message">The message received from the manager</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private LotBatchDataDTO RetrieveNextBatchData(AskForNextBatchData message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Lot? lot = dbContext.Lots
                .Include("Crop")
                .AsNoTracking()
                .Where(l => 
                    l.LotNumber == message.LotNumber
                    && l.IsDeleted == false
                )
                .FirstOrDefault();

            if (lot == null)
                throw new ApplicationException("Lot not found");

            // Get location data
            List<FarmLocation> allLocations = RetrieveFarmLocations(
                new FarmLocationQueryParams { Type = "production" }).Result;

            FarmLocation? lotLocation = allLocations
                .Where(l => l.Id == lot.FarmLocationId)
                .FirstOrDefault();
            FarmLocation? transplantLocation = lot.Crop != null && lot.Crop.TransplantLocationId.HasValue
                ? allLocations.Where(l => l.Id == lot.Crop.TransplantLocationId.Value).FirstOrDefault()
                : null;

            if (lotLocation == null)
                throw new ApplicationException("Lot location not found");

            List<FarmLocation> batchLocations = transplantLocation != null
                ? LocationHelper.FlattenInferiorLocations(transplantLocation, allLocations)
                : LocationHelper.FlattenInferiorLocations(lotLocation, allLocations);

            // Get product data
            // TODO: Refactor once query params are implemented so we aren't getting everything
            List<Product> products = RetrieveProducts().Result;
            Product? lotProduct = products
                .Where(p => p.Id == lot.ProductId)
                .FirstOrDefault();

            if (lotProduct == null || lotProduct.Variants == null)
                throw new ApplicationException("Product not found");

            List<ProductVariant> batchVariants = lotProduct
                .Variants
                .Select(v => new ProductVariant
                {
                    Id = v.Id,
                    Name = v.Name
                })
                .ToList();

            // Get batch number
            int batchNumber = dbContext.Batches
                .Where(b => b.LotId == lot.Id)
                .Count();

            return new LotBatchDataDTO
            {
                BatchNumber = batchNumber + 1,
                Variants = batchVariants,
                Locations = batchLocations
            };
        }

        /// <summary>
        /// Receives pertinent information about the next production item
        /// that can be displayed in the FarmManager UI
        /// </summary>
        /// <param name="message">The message received from the manager</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private BatchProductDataDTO RetrieveNextProductData(AskForNextProductData message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            // Get lot / batch data
            Lot? lot = dbContext.Lots
                .AsNoTracking()
                .Include("Batches.ProductionItems")
                .Where(l =>
                    l.LotNumber == message.LotNumber
                    && l.IsDeleted == false
                )
                .FirstOrDefault();

            if (lot == null || lot.Batches == null)
                throw new ApplicationException("Lot not found");

            Batch? batch = lot.Batches.Where(b =>
                    b.BatchNumber == message.BatchNumber
                    && b.IsDeleted == false
                )
                .FirstOrDefault();

            if (batch == null || batch.ProductionItems == null)
                throw new ApplicationException("Batch not found");

            // Get location data
            List<FarmLocation> allLocations = RetrieveFarmLocations(
                new FarmLocationQueryParams { Type = "production" }).Result;

            FarmLocation? parentedLocation = allLocations
                .Where(l => l.Id == batch.FarmLocationId)
                .FirstOrDefault();

            if (parentedLocation == null)
                throw new ApplicationException("Location not found");

            // Get product data
            // TODO: Refactor once query params are implemented so we aren't getting everything
            List<Product> products = RetrieveProducts().Result;
            Product? lotProduct = products
                .Where(p => p.Id == lot.ProductId)
                .FirstOrDefault();

            if (lotProduct == null || lotProduct.Variants == null)
                throw new ApplicationException("Product not found");

            ProductVariant? batchVariant = lotProduct
                .Variants
                .Where(v => v.Id == batch.VariantId)
                .FirstOrDefault();

            if (batchVariant == null)
                throw new ApplicationException("Variant not found");

            return new BatchProductDataDTO
            {
                Product = lotProduct.Name,
                Variant = batchVariant.Name,
                Location = LocationHelper.GenerateFarmLocationString(parentedLocation),
                NextProductCount = batch.ProductionItems.Count() + 1,
            };
        }

        /// <summary>
        /// Retrieves a list of lots based on the query parameters of the message
        /// </summary>
        /// <param name="message">The message received from the manager</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<IEnumerable<LotDTO>> RetrieveLots(AskToRetrieveLots message)
        {
            string[] statuses = string.IsNullOrEmpty(message.Query.Status)
                ? new string[] { }
                : message.Query.Status.Split(",");

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            IQueryable<Lot> lots = dbContext.Lots
                .AsQueryable()
                .AsNoTracking()
                // TODO: Apply includes dynamically
                .Include("Status")
                .Include("Crop.Status")
                .Where(l => l.IsDeleted == false);

            if(!string.IsNullOrEmpty(message.Query.LotType))
            {
                switch (message.Query.LotType)
                {
                    case "crop":
                        lots = lots.Where(l => l.CropId != null);
                        if (statuses.Count() > 0)
                            lots = lots.Where(l =>
                                statuses.Contains(l.Crop.Status.Name));
                        break;

                    default:
                        break;
                }
            }

            // Retrieve the items
            Task<List<Lot>> lotTask = lots.ToListAsync();
            Task<List<Product>> productTask = RetrieveProducts();
            Task<List<FarmLocation>> locationTask = RetrieveFarmLocations(
                new FarmLocationQueryParams { Type = "production" });

            await Task.WhenAll(lotTask, productTask, locationTask);

            List<int> lotIds = lotTask.Result
                .Select(l => l.Id)
                .Distinct()
                .ToList();

            IEnumerable<IGrouping<int, LotNoteMap>> lotNotes = 
                (await dbContext.LotNoteMaps
                    .AsNoTracking()
                    .Where(m =>
                        lotIds.Contains(m.LotId)
                        && m.IsDeleted == false
                        && m.Lot.IsDeleted == false
                        && m.Note.IsDeleted == false
                    )
                    .ToListAsync()
                )
                .GroupBy(m => m.LotId);

            List<LotDTO> results = new();
            foreach (Lot lot in lotTask.Result)
            {
                string? productName = productTask.Result
                    .Where(p => p.Id == lot.ProductId)
                    .FirstOrDefault()?.Name;
                string? productStatus = string.Empty;
                FarmLocation location = locationTask.Result
                    .Where(l => l.Id == lot.FarmLocationId)
                    .FirstOrDefault() ?? new();
                string? transplantedLocationString = null;
                DateTimeOffset createdDate = lot.Created;

                int? noteCount = lotNotes
                    .Where(g => g.Key == lot.Id)
                    .FirstOrDefault()
                    ?.Count();

                if (lot.Crop != null)
                {
                    productStatus = lot.Crop.Status?.Label;
                    FarmLocation? transplantedLocation = lot.Crop.TransplantLocationId.HasValue
                        ? locationTask.Result
                            .Where(l => l.Id == lot.Crop.TransplantLocationId)
                            .FirstOrDefault()
                        : null;

                    transplantedLocationString = transplantedLocation != null
                        ? $"{transplantedLocation.Name} {LocationHelper.GenerateBusinessLocationParentString(transplantedLocation)}"
                        : null;

                    createdDate = lot.Crop.PlantedDate;
                }

                results.Add(new LotDTO
                {
                    Id = lot.Id,
                    LotNumber = lot.LotNumber,
                    Product = productName,
                    Location = $"{location.Name} {LocationHelper.GenerateBusinessLocationParentString(location)}",
                    TransplantedLocation = transplantedLocationString,
                    Status = lot.Status?.Name,
                    ProductStatus = productStatus,
                    CreatedDate = createdDate,
                    NoteCount = noteCount
                });
            }

            return results;
        }

        /// <summary>
        /// Retrieves the available progressions for a given lot
        /// </summary>
        /// <param name="message">A message requesting the lot progressions</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<LotProgressionDTO> RetrieveLotProgressions(AskForLotProgressions message)
        {
            using FarmManagerContext pastContext = _dbFactory.CreateDbContext();
            using FarmManagerContext futureContext = _dbFactory.CreateDbContext();

            Lot? lot = await pastContext.Lots
                .AsNoTracking()
                .Include("Crop")
                .Where(l => l.Id == message.LotId)
                .FirstOrDefaultAsync();

            if (lot == null || lot.Crop == null)
                throw new ApplicationException("Lot not found");

            Task<List<LotProgression>> pastProgressions = pastContext.LotProgressions
                .AsNoTracking()
                .Include("CurrentStatus")
                .Include("NextStatus")
                .Where(p => p.NextStatusId == lot.Crop.StatusId)
                .ToListAsync();
            Task<List<LotProgression>> futureProgressions = futureContext.LotProgressions
                .AsNoTracking()
                .Include("CurrentStatus")
                .Include("NextStatus")
                .Where(p => p.CurrentStatusId == lot.Crop.StatusId)
                .ToListAsync();

            await Task.WhenAll(pastProgressions, futureProgressions);

            return new LotProgressionDTO
            {
                PastProgressionOptions = pastProgressions.Result,
                FutureProgressionOptions = futureProgressions.Result
            };
        }

        /// <summary>
        /// Retrieves the batches for a given lot
        /// </summary>
        /// <param name="message">A message requesting the batches for a given lot</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<List<BatchDTO>> RetrieveLotBatches(AskForLotBatches message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Lot? lot = dbContext.Lots
                .AsNoTracking()
                .Include("Batches.Status")
                .Include("Batches.ProductionItems")
                .Where(l =>
                    l.LotNumber == message.LotNumber
                    && l.IsDeleted == false
                )
                .FirstOrDefault();

            if (lot == null || lot.Batches == null)
                throw new ApplicationException("Lot not found");

            List<FarmLocation> locations = RetrieveFarmLocations(
                new FarmLocationQueryParams { Type = "production" }).Result;

            List<BatchDTO> results = new();
            foreach (Batch batch in lot.Batches)
            {
                FarmLocation? batchLocation = locations
                    .Where(l => l.Id == batch.FarmLocationId)
                    .FirstOrDefault();

                results.Add(new BatchDTO
                {
                    BatchNumber = batch.BatchNumber,
                    Status = batch.Status?.Label ?? string.Empty,
                    Location = LocationHelper.GenerateFarmLocationString(batchLocation ?? new()),
                    ItemCount = batch.ProductionItems?.Count ?? 0
                });
            }

            return results;
        }

        /// <summary>
        /// Retrieves the notes for a given lot
        /// </summary>
        /// <param name="message">A messaging containing the Lot number of the lot
        /// for which notes should be retrieved</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<List<Note>> RetrieveLotNotes(AskForLotNotes message)
        {
            if (string.IsNullOrEmpty(message.LotNumber))
                throw new ApplicationException("Lot not found");

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            List<Note> notes = await dbContext.LotNoteMaps
                .AsNoTracking()
                .Where(m =>
                    m.Lot.LotNumber == message.LotNumber
                    && m.IsDeleted == false
                    && m.Lot.IsDeleted == false
                    && m.Note.IsDeleted == false
                )
                .Select(m => m.Note)
                .ToListAsync();

            List<string> noteUserIds = notes
                .Select(n => n.UserId)
                .ToList();

            List<FarmManagerUser> users = noteUserIds.Count > 0
                ? (await _idpService
                    .RetrieveUsersByIdAsync(noteUserIds))
                    .ToList()
                : new();

            return notes
                .Select(n =>
                {
                    FarmManagerUser? user = users
                        ?.Where(u => u.ExternalId == n.UserId)
                        .FirstOrDefault();

                    string userName = $"{user?.FirstName} {user?.LastName}";

                    n.UserId = userName;
                    return n;
                })
                .ToList();
        }

        /// <summary>
        /// Creates a new loot
        /// </summary>
        /// <param name="message">A message containing the required information
        /// for creating a lot</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private Lot CreateLot(AskToCreateLot message)
        {
            ValidateNewLot(message.Lot);

            message.Lot.LotNumber = RetrieveNextLotNumber(new AskForNextLotNumber());

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();
            if (message.Lot.Crop != null)
            {
                Status? planted = dbContext.Statuses
                    .AsNoTracking()
                    .Where(s =>
                        s.EntityId == "crop"
                        && s.Name == "planted"
                        && s.IsDeleted == false
                    )
                    .FirstOrDefault();

                Status? pending = dbContext.Statuses
                    .AsNoTracking()
                    .Where(s =>
                        s.EntityId == "lot"
                        && s.Name == "pending"
                        && s.IsDeleted == false
                    )
                    .FirstOrDefault();

                if (planted == null || pending == null)
                    throw new Exception("Missing status");

                message.Lot.StatusId = pending.Id;
                message.Lot.Crop.StatusId = planted.Id;
                message.Lot.Crop.PlantedDate = DateTimeOffset.UtcNow;
            }

            dbContext.Add(message.Lot);
            dbContext.SaveChanges();

            return message.Lot;
        }

        /// <summary>
        /// Promotes a lot to the provided status
        /// </summary>
        /// <param name="message">The message received from the manager</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private Lot PromoteLot(AskToPromoteLot message)
        {
            if (message.StatusId == default || message.LotId == default)
                throw new ApplicationException("LotId and StatusId are required");

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Lot? lot = dbContext.Lots
                .Include("Crop")
                .Where(l =>
                    l.Id == message.LotId
                    && l.IsDeleted == false
                )
                .FirstOrDefault();

            if (lot == null || lot.Crop == null)
                throw new ApplicationException("Lot not found");

            LotProgression? progression = dbContext.LotProgressions
                .AsNoTracking()
                .Include("NextStatus")
                .Where(p =>
                    p.NextStatusId == message.StatusId
                    && p.CurrentStatusId == lot.Crop.StatusId
                    && p.IsDeleted == false
                )
                .FirstOrDefault();

            if (progression == null)
                throw new ApplicationException("Status not found");

            lot.Crop.StatusId = progression.NextStatusId;
            lot.StatusId = progression.NextLotStatusId;

            DateTimeOffset now = DateTimeOffset.UtcNow;
            if (progression.NextStatus.Name == "germinated")
                lot.Crop.GerminatedDate = now;
            if (progression.NextStatus.Name == "transplanted")
            {
                lot.Crop.TransplantedDate = now;
                lot.Crop.TransplantLocationId = message.TransplantLocationId;
            }
            if (progression.NextStatus.Name == "producing")
                lot.Crop.HarvestBeginDate = now;
            if (progression.NextStatus.Name == "harvested")
            {
                lot.Crop.HarvestEndDate = now;
                if (!lot.Crop.HarvestBeginDate.HasValue)
                    lot.Crop.HarvestBeginDate = now;
            }
            if (progression.NextStatus.Name == "disposed")
                lot.Crop.DisposalDate = now;

            dbContext.SaveChanges();

            // Return the new status as part of the response
            lot.Status = progression.NextStatus;
            return lot;
        }

        /// <summary>
        /// Creates a batch for the requested lot
        /// </summary>
        /// <param name="message">The message received from the manager</param>
        /// <returns></returns>
        private Batch CreateBatch(AskToCreateBatch message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Lot? lot = dbContext.Lots
                .AsNoTracking()
                .Include("Status")
                .Where(l => 
                    l.LotNumber == message.LotNumber
                    && l.IsDeleted == false
                )
                .FirstOrDefault();

            if (lot == null)
                throw new ApplicationException("Lot not found");

            Product? product = RetrieveProducts().Result
                .Where(p => p.Id == lot.ProductId)
                .FirstOrDefault();
            ProductVariant? variant = product?.Variants
                ?.Where(v => v.Id == message.Batch.VariantId)
                ?.FirstOrDefault();

            if (product == null)
                throw new ApplicationException("Product not found");

            if (variant == null)
                throw new ApplicationException("Variant not found");

            int existingBatches = dbContext.Batches
                .AsNoTracking()
                .Where(b => b.LotId == lot.Id)
                .Count();

            int openId = dbContext.Statuses
                .AsNoTracking()
                .Where(s =>
                    s.EntityId == "batch"
                    && s.Name == "open"
                    && s.IsDeleted == false
                )
                .Select(s => s.Id)
                .FirstOrDefault();

            if (openId == default)
                throw new ApplicationException("Open status not found");

            message.Batch.LotId = lot.Id;
            message.Batch.BatchNumber = existingBatches + 1;
            message.Batch.BatchBeginDate = DateTimeOffset.UtcNow;
            message.Batch.StatusId = openId;

            dbContext.Add(message.Batch);
            dbContext.SaveChanges();

            return message.Batch;
        }

        /// <summary>
        /// Closes a currently open batch, preventing additional harvests
        /// </summary>
        /// <param name="message">A message containing the lot number and batch number
        /// of the batch that should be closed</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private Batch CloseBatch(AskToCloseBatch message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Lot? lot = dbContext.Lots
                .Include("Batches")
                .Where(l =>
                    l.LotNumber == message.LotNumber
                    && l.IsDeleted == false
                )
                .FirstOrDefault();

            Batch? batch = lot?.Batches?
                .Where(b => 
                    b.BatchNumber == message.BatchNumber
                    && b.IsDeleted == false
                )
                .FirstOrDefault();

            if (batch == null)
                throw new ApplicationException("Batch not found");

            Status? closed = dbContext.Statuses
                .AsNoTracking()
                .Where(s =>
                    s.EntityId == "batch"
                    && s.Name == "closed"
                    && s.IsDeleted == false
                )
                .FirstOrDefault();

            if (closed == null)
                throw new ApplicationException("Status not found");

            batch.StatusId = closed.Id;
            batch.BatchEndDate = DateTimeOffset.UtcNow;
            dbContext.SaveChanges();

            return batch;
        }

        /// <summary>
        /// Creates a new production item
        /// </summary>
        /// <param name="message">A message containing the information needed to
        /// create a new production item</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private List<ProductionItem> CreateProductionItem(AskToCreateProductionItem message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            // Retrieve the required information
            Lot? lot = dbContext.Lots
                .Include("Crop")
                .Include("Batches.ProductionItems")
                .Include("Batches.FarmLocation.BusinessLocation")
                .AsNoTracking()
                .Where(l =>
                    l.LotNumber == message.LotNumber
                    && l.IsDeleted == false
                )
                .FirstOrDefault();

            Batch? batch = lot?.Batches?
                .Where(b =>
                    b.BatchNumber == message.BatchNumber
                    && b.IsDeleted == false
                )
                .FirstOrDefault();

            if (lot == null || batch == null || lot.Crop == null || batch.FarmLocation == null)
                throw new ApplicationException("Batch not found");

            ProductVariant? batchVariant = RetrieveProducts().Result
                .Where(p => p.Variants.Any(v => v.Id == batch.VariantId))
                .Select(p => p.Variants.Where(v => v.Id == batch.VariantId))
                .FirstOrDefault()
                ?.Where(v => v.Id == batch.VariantId)
                ?.FirstOrDefault();

            if (batchVariant == null)
                throw new ApplicationException("Variant not found");

            Status? stocked = dbContext.Statuses
                .Where(s =>
                    s.EntityId == "inventory.item"
                    && s.Name == "stocked"
                )
                .FirstOrDefault();

            if (stocked == null)
                throw new ApplicationException("Stocked status not found");

            Unit batchUnit = batchVariant.MeasurementUnit;

            DateTimeOffset now = DateTimeOffset.UtcNow;
            DateTimeOffset plantedDate = lot.Crop.PlantedDate;
            DateTimeOffset? transplantDate = lot.Crop.TransplantedDate;

            // Create the actual items
            List<ProductionItem> newItems = new();
            for (int i = 0; i < message.ProductInfo.Quantity; i++)
            {
                List<ProductionItemLocationHistory> locationHistory = new();
                if (transplantDate.HasValue)
                {
                    locationHistory.Add(new ProductionItemLocationHistory
                    {
                        FarmLocationId = lot.FarmLocationId,
                        ArrivalDate = plantedDate,
                        DepartureDate = transplantDate
                    });

                    locationHistory.Add(new ProductionItemLocationHistory
                    {
                        FarmLocationId = batch.FarmLocationId,
                        ArrivalDate = transplantDate.Value,
                    });
                }
                else
                {
                    locationHistory.Add(new ProductionItemLocationHistory
                    {
                        FarmLocationId = batch.FarmLocationId,
                        ArrivalDate = plantedDate
                    });
                }

                newItems.Add(new ProductionItem
                {
                    BatchId = batch.Id,
                    ProductionDate = now,
                    PackageDate = now,
                    SKU = batchVariant.Sku,
                    StatusId = stocked.Id,
                    GrossWeight = message.ProductInfo.GrossWeight,
                    NetWeight = message.ProductInfo.GrossWeight - message.ProductInfo.TareWeight,
                    TareWeight = message.ProductInfo.TareWeight,
                    LabelWeight = message.ProductInfo.LabelWeight,
                    LocationId = batch.FarmLocationId,
                    LocationHistory = locationHistory
                });
            }

            // Save the production items so we have the Ids when
            // generating barcodes
            dbContext.AddRange(newItems);
            dbContext.SaveChanges();

            string gtin = dbContext.Settings
                .AsNoTracking()
                .Where(s => s.Name == "gtin")
                .Select(s => s.Value)
                .FirstOrDefault() ?? "00000000000000";

            foreach(ProductionItem item in newItems)
            {
                item.Barcode = GenerateProductionItemBarcode(
                    gtin,
                    batchVariant.Sku,
                    item
                );

                // TODO: Print Barcode
            }

            dbContext.SaveChanges();

            // Publish the inventory to payment processor
            if(
                _inventoryPublisher != null
                && !string.IsNullOrEmpty(batch?.FarmLocation?.BusinessLocation?.ExternalId)
            )
            {
                InventoryUpdate update = new();
                MessageBrokerMessage mbMesssage = new();
                try
                {
                    update = new()
                    {
                        Quantity = newItems.Count,
                        VariantId = batchVariant.ExternalId,
                        BusinessLocationId = batch.FarmLocation.BusinessLocation.ExternalId,
                        Timestamp = now,
                    };

                    mbMesssage = new()
                    {
                        Source = $"{Context.Self}",
                        MessageType = Events.PaymentProcessing.Inventory.Create,
                        Assembly = typeof(InventoryUpdate).Assembly.FullName,
                        Class = nameof(InventoryUpdate),
                        TimeStamp = now,
                        Data = update
                    };

                    _inventoryPublisher.PublishMessageAsync(mbMesssage).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish inventory update", ex, mbMesssage);
                }
            }

            return newItems;
        }

        /// <summary>
        /// Creates a new note for a specified lot
        /// </summary>
        /// <param name="message">A message containing the information 
        /// needed for creating a note for a specific lot</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private Note CreateNote(AskToCreateLotNote message)
        {
            if (string.IsNullOrEmpty(message.UserId) || string.IsNullOrEmpty(message.Note))
                throw new ApplicationException("Note and UserId are required");

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Lot? lot = dbContext.Lots
                .AsNoTracking()
                .Where(l =>
                    l.LotNumber == message.LotNumber
                    && l.IsDeleted == false
                )
                .FirstOrDefault();

            if (lot == null)
                throw new ApplicationException("Lot not found");

            Note note = new()
            {
                Text = message.Note,
                UserId = message.UserId,
            };

            dbContext.Add(new LotNoteMap
            {
                LotId = lot.Id,
                Note = note
            });
            dbContext.SaveChanges();

            return note;
        }

        /// <summary>
        /// Validates a new lot to make sure that it can actually be created
        /// </summary>
        /// <param name="lot">The lot to validate</param>
        /// <exception cref="Exception"></exception>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateNewLot(Lot lot)
        {
            if (lot == null)
                throw new Exception("No lot provided");

            if (lot.ProductId == 0 || lot.FarmLocationId == 0)
                throw new Exception("product and location are required");

            List<Product> products = RetrieveProducts().Result;
            List<int> productIds = new() { lot.ProductId };

            if (lot.Crop != null && lot.Crop.SeedVariantId == 0)
                productIds.Add((int)lot.Crop.SeedVariantId);

            List<Product> existingProducts = products
                .Where(p => productIds.Contains(p.Id))
                .ToList();

            if (existingProducts.Count != productIds.Count)
                throw new ApplicationException("Unable to verify chosen products");

            List<FarmLocation> location = RetrieveFarmLocations(
                new FarmLocationQueryParams { Type = "production"}).Result;
            FarmLocation? dbLocation = location
                .Where(l => l.Id == lot.FarmLocationId)
                .FirstOrDefault();

            if (dbLocation == null)
                throw new ApplicationException("Unable to verify chosen location");
        }

        /// <summary>
        /// Retrieves a list of farm locations
        /// </summary>
        /// <param name="query">A query defining which locations should be retrieved</param>
        /// <returns></returns>
        private async Task<List<FarmLocation>> RetrieveFarmLocations(FarmLocationQueryParams query)
        {
            List<FarmLocation> results = new();
            FarmManagerResponse? locationResponse = await _farmManager
                .Ask(
                    new AskForFarmLocations(query),
                    TimeSpan.FromSeconds(_settings.ActorWaitSeconds)
                ) as FarmManagerResponse;

            if (locationResponse != null && locationResponse.Status == ResponseStatus.Success)
                results = locationResponse.Data as List<FarmLocation> ?? new();

            return results;
        }

        /// <summary>
        /// Retrieves a list of products
        /// </summary>
        /// <returns></returns>
        private async Task<List<Product>> RetrieveProducts()
        {
            List<Product> results = new();
            FarmManagerResponse? productResponse = await _farmManager
                .Ask(
                    new AskForProducts(),
                    TimeSpan.FromSeconds(_settings.ActorWaitSeconds)
                ) as FarmManagerResponse;

            if (productResponse != null && productResponse.Status == ResponseStatus.Success)
                results = productResponse.Data as List<Product> ?? new();

            return results;
        }

        /// <summary>
        /// Generates a barcode for new production items
        /// </summary>
        /// <param name="gtin">The GTIN of the business</param>
        /// <param name="lotNumber">The lot number of the production item</param>
        /// <param name="sku">The sku of the production item</param>
        /// <param name="crop">The crop from which the production item is created</param>
        /// <param name="batch">The batch from which the production item is created</param>
        /// <param name="prodItem">The actual production item being created</param>
        /// <returns></returns>
        private string GenerateProductionItemBarcode(
            string gtin,
            string sku,
            ProductionItem prodItem
        )
        {
            string productionDate = prodItem.ProductionDate.ToString("yyMMdd");
            //string itemNumber = prodItem.Id.ToString("00000000000000000000");

            // TODO: Move this to a setting
            return $"01{gtin}" +
                $"\\F11{productionDate}" +
                $"\\F22{sku}" +
                $"\\F91{prodItem.Id}";
        }
    }
}
