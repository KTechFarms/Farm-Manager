﻿using Akka.Actor;
using Akka.DependencyInjection;
using FarmManager.Core.Actors.Production.FarmLocations;
using FarmManager.Core.Actors.Production.Inventory;
using FarmManager.Core.Actors.Production.Labels;
using FarmManager.Core.Actors.Production.Lots;
using FarmManager.Data.ActorMessages.Production.FarmLocations;
using FarmManager.Data.ActorMessages.Production.Inventory;
using FarmManager.Data.ActorMessages.Production.Labels;
using FarmManager.Data.ActorMessages.Production.Lots;
using FarmManager.Data.DTOs;

namespace FarmManager.Core.Actors.Production
{
    /// <summary>
    /// The actor responsible for managing all production-related actors and messages.
    /// Primarily forwards the messages to the appropriate actor / sub-manager
    /// </summary>
    public class ProductionManager : FarmManagerActor<ProductionManager>
    {
        private IActorRef? _lotManager;
        private IActorRef? _inventoryManager;

        public ProductionManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            ReceiveAny(RouteMessage);
        }

        protected override void PreStart()
        {
            Props lotProps = DependencyResolver.For(Context.System).Props<LotManager>();
            _lotManager = Context.ActorOf(lotProps, "LotManager");

            Props inventoryProps = DependencyResolver.For(Context.System).Props<InventoryManager>();
            _inventoryManager = Context.ActorOf(inventoryProps, "InventoryManager");
        }

        /// <summary>
        /// Routes the messages to the next appropriate location
        /// </summary>
        /// <param name="message"></param>
        private void RouteMessage(object message)
        {
            IUntypedActorContext context = Context;
            IActorRef sender = Sender;
            TimeSpan timeout = TimeSpan.FromSeconds(_settings.ActorWaitSeconds);

            switch (message)
            {
                case IFarmLocationMessage:
                    HandleWithInstanceOfAsync<FarmLocationWorker>(message, sender, context);
                    break;

                case IInventoryMessage:
                    _inventoryManager.Ask(message, timeout)
                        .ContinueWith(result => sender.Tell(result.Result));
                    break;

                case ILabelMessage:
                    HandleWithInstanceOfAsync<LabelWorker>(message, sender, context);
                    break;

                case ILotMessage:
                    _lotManager.Ask(message, timeout)
                        .ContinueWith(result => sender.Tell(result.Result));
                    break;

                default:
                    string messageType = message.GetType().Name;
                    _logger.LogWarning($"Unhandled message of type {messageType}");
                    sender.Tell(ActorResponse.Failure($"Unsupported message type: {messageType}"));
                    break;
            }
        }
    }
}
