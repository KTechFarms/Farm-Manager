﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Common.Helpers;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.ActorMessages.Operations.Products;
using FarmManager.Data.ActorMessages.Production.Inventory;
using FarmManager.Data.Contexts;
using FarmManager.Data.DTOs;
using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using Enum = FarmManager.Data.Entities.Generic.Enum;
using Status = FarmManager.Data.Entities.Generic.Status;

namespace FarmManager.Core.Actors.Production.Inventory
{
    /// <summary>
    /// The worker responsible for all inventory related logic
    /// </summary>
    public class InventoryWorker : FarmManagerActor<InventoryWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;
        private readonly string _inventoryQueue;
        private IMessageBrokerPublisher? _inventoryPublisher;

        public InventoryWorker(IServiceProvider serviceProvider): base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            _inventoryQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcesing)
                ?.QueueName ?? string.Empty;

            Receive<AskForInventory>(message => HandleWith(RetrieveInventory, message));
            Receive<AskToTransferInventory>(message => HandleWith(TransferInventory, message));
            Receive<AskToReceiveInventory>(message => HandleWith(ReceiveInventory, message));
            Receive<AskToDisposeInventory>(message => HandleWith(DisposeInventory, message));
        }

        protected override void PreStart()
        {
            _inventoryPublisher = _farmManager.Ask(
                new AskForMessageBrokerPublisher(
                    _inventoryQueue,
                    $"{Self}"
                )).Result as IMessageBrokerPublisher;
        }

        protected override void PostStop()
        {
            _inventoryPublisher?.Dispose();
            base.PostStop();
        }

        //////////////////////////////////////////
        //            Implementation            //
        //////////////////////////////////////////

        /// <summary>
        /// Retrieves a list of inventory items for a given location
        /// </summary>
        /// <param name="message">A message containing the Id of the location to
        /// retrieve inventory for</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private IEnumerable<InventoryItem> RetrieveInventory(AskForInventory message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            // Retrieve the production items
            List<InventoryLot> inventoryLots = dbContext.Lots
                .AsNoTracking()
                .Include("Batches.ProductionItems.Status")
                .Where(l =>
                    l.Batches.Any(b =>
                        b.ProductionItems.Any(i =>
                            i.LocationId == message.FarmLocationId
                            && i.IsDeleted == false
                        )
                    )
                )
                .Select(l => new InventoryLot
                {
                    LotNumber = l.LotNumber,
                    ProductId = l.ProductId,
                    Batches = l.Batches
                        .Where(b => b.ProductionItems.Any(i =>
                            i.LocationId == message.FarmLocationId
                            && i.IsDeleted == false))
                        .Select(b => new InventoryBatch
                        {
                            BatchNumber = b.BatchNumber,
                            VariantId = b.VariantId,
                            ProductionItems = b.ProductionItems
                                .Where(i => i.LocationId == message.FarmLocationId
                                    && i.IsDeleted == false
                                )
                        })
                })
                .ToList();

            List<int> distinctProductIds = inventoryLots
                .Select(l => l.ProductId)
                .Distinct()
                .ToList();

            List<int> distinctVariantIds = inventoryLots
                .Select(l => l.Batches
                    .Select(b => b.VariantId)
                    .Distinct()
                )
                .SelectMany(id => id)
                .Distinct()
                .ToList();

            List<Product> products = RetrieveProducts().Result;

            List<InventoryItem> results = new();
            foreach(InventoryLot lot in inventoryLots)
            {
                Product? product = products
                    .Where(p => p.Id == lot.ProductId)
                    .FirstOrDefault();

                if (product == null)
                    throw new ApplicationException($"Product not found for lot {lot.LotNumber}");

                string productName = product.Name;

                foreach(InventoryBatch batch in lot.Batches)
                {
                    string variantName = product.Variants
                        ?.Where(v => v.Id == batch.VariantId)
                        ?.Select(v => v.Name)
                        ?.FirstOrDefault() ?? string.Empty;

                    results.AddRange(batch.ProductionItems
                        .Where(pi => pi.Status.Name == "stocked")
                        .Select(pi => new InventoryItem
                        {
                            ProductionItemId = pi.Id,
                            Product = productName,
                            Variant = variantName,
                            Barcode = !string.IsNullOrEmpty(pi.Barcode)
                                ? BarcodeHelper.ParseBarcode(pi.Barcode)
                                : null,
                            ProductionDate = pi.ProductionDate,
                            LotNumber = lot.LotNumber,
                            BatchNumber = batch.BatchNumber
                        }));
                }
            }

            // Retrieve the inventory items
            List<InventoryItem> inventory = dbContext.Inventory
                .Where(i => 
                    i.LocationId == message.FarmLocationId
                    && i.Status.Name == "stocked"
                )
                .Select(i => new InventoryItem
                {
                    InventoryId = i.Id,
                    ProductId = i.ProductVariant.ProductId,
                    ProductionDate = i.Created,
                    Variant = i.ProductVariant.Name,
                    Barcode = !string.IsNullOrEmpty(i.Barcode)
                        ? new Barcode { RawBarcode = i.Barcode, UserFriendlyBarcode = i.Barcode }
                        : null,
                    LotNumber = string.Empty,
                })
                .ToList();

            List<int> inventoryProductIds = inventory
                .Select(i => i.ProductId)
                .Distinct()
                .ToList();

            List<Product> inventoryProducts = dbContext.Products
                .Where(p => inventoryProductIds.Contains(p.Id))
                .ToList();

            foreach(Product product in inventoryProducts)
            {
                results.AddRange(
                    inventory
                        .Where(i => i.ProductId == product.Id)
                        .Select(i =>
                        {
                            i.Product = product.Name;
                            return i;
                        }
                ));
            }

            return results;
        }

        /// <summary>
        /// Transfers inventory between two locations and publishes
        /// the transfer to the payment processor if the business
        /// location is changing
        /// </summary>
        /// <param name="message">A message containing the details of
        /// the transfer</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private bool TransferInventory(AskToTransferInventory message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            List<FarmLocation> farmLocations = dbContext.FarmLocations
                .AsNoTracking()
                .Include(nameof(FarmLocation.BusinessLocation))
                .Where(l =>
                    (l.Id == message.FromFarmLocationId
                    || l.Id == message.ToFarmLocationId)
                    && l.IsDeleted == false
                )
                .ToList();

            if (farmLocations.Count != 2)
                throw new ApplicationException("Requested locations not found");

            FarmLocation? toLocation = farmLocations
                .Where(l => l.Id == message.ToFarmLocationId)
                .FirstOrDefault();

            FarmLocation? fromLocation = farmLocations
                .Where(l => l.Id == message.FromFarmLocationId)
                .FirstOrDefault();

            if (toLocation == null || fromLocation == null)
                throw new ApplicationException("Cannot transfer items using the provided locations");

            List<ProductionItem> dbItems = dbContext.ProductionItems
                .Include(pi => pi.LocationHistory.Where(lh => lh.DepartureDate == null))
                .Where(pi => 
                    message.ProductionItemIds.Contains(pi.Id)
                    && pi.LocationId == fromLocation.Id
                    && pi.IsDeleted == false
                )
                .ToList();

            List<Data.Entities.Operations.Inventory> dbInventory = dbContext.Inventory
                .Include(nameof(Data.Entities.Operations.Inventory.ProductVariant))
                .Where(i => 
                    message.InventoryIds.Contains(i.Id)
                    && i.LocationId == fromLocation.Id
                    && i.IsDeleted == false
                )
                .ToList();

            DateTimeOffset now = DateTimeOffset.UtcNow;
            foreach(ProductionItem item in dbItems)
            {
                if (item.LocationHistory == null)
                {
                    _logger.LogCritical($"Missing history on ProductionItem {item.Id}", null, item);
                    // TODO: Send alert
                    throw new ApplicationException("Unable to update products. Missing location history!");
                }

                item.LocationId = toLocation.Id;
                item.LocationHistory.First().DepartureDate = now;
                item.LocationHistory.Add(new ProductionItemLocationHistory
                {
                    ArrivalDate = now,
                    FarmLocationId = toLocation.Id
                });
            }

            foreach(Data.Entities.Operations.Inventory inventory in dbInventory)
                inventory.LocationId = toLocation.Id;

            dbContext.SaveChanges();

            // Publish the location change to square
            if (
                toLocation.BusinessLocationId != fromLocation.BusinessLocationId
                && _inventoryPublisher != null
                && !string.IsNullOrEmpty(toLocation?.BusinessLocation?.ExternalId)
                && !string.IsNullOrEmpty(fromLocation?.BusinessLocation?.ExternalId)
            )
            {
                try
                {
                    List<int> batchIds = dbItems.Select(pi => pi.BatchId).ToList();
                    List<string> variants = dbContext.Batches
                        .AsNoTracking()
                        .Where(b => batchIds.Contains(b.Id) && b.Variant != null)
                        .Select(b => b.Variant.ExternalId ?? string.Empty)
                        .Distinct()
                        .ToList();

                    variants = variants
                        .Where(v => !string.IsNullOrEmpty(v))
                        .ToList();

                    List<MessageBrokerMessage> messages = new();
                    foreach (string? variant in variants)
                    {
                        InventoryUpdate update = new()
                        {
                            Quantity = dbItems.Count,
                            VariantId = variant,
                            BusinessLocationId = toLocation.BusinessLocation.ExternalId,
                            PriorBusinessLocationId = fromLocation.BusinessLocation.ExternalId,
                            Timestamp = now,
                        };

                        messages.Add(new()
                        {
                            Source = $"{Context.Self}",
                            MessageType = Events.PaymentProcessing.Inventory.Transfer,
                            Assembly = typeof(InventoryUpdate).Assembly.FullName,
                            Class = nameof(InventoryUpdate),
                            TimeStamp = now,
                            Data = update
                        });
                    }


                    IEnumerable<IGrouping<string, Data.Entities.Operations.Inventory>> groupedInventory = dbInventory
                        .GroupBy(i => i.ProductVariant?.ExternalId);

                    foreach(IGrouping<string, Data.Entities.Operations.Inventory> group in groupedInventory)
                    {
                        InventoryUpdate update = new()
                        {
                            Quantity = group.Count(),
                            VariantId = group.Key,
                            BusinessLocationId = toLocation.BusinessLocation.ExternalId,
                            PriorBusinessLocationId = fromLocation.BusinessLocation.ExternalId,
                            Timestamp = now,
                        };

                        messages.Add(new MessageBrokerMessage
                        {
                            Source = $"{Context.Self}",
                            MessageType = Events.PaymentProcessing.Inventory.Transfer,
                            Assembly = typeof(InventoryUpdate).Assembly.FullName,
                            Class = nameof(InventoryUpdate),
                            TimeStamp = now,
                            Data = update
                        });
                    }

                    _inventoryPublisher.PublishMessageBatchAsync(messages).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Error submitting inventory transfer update", ex, message);
                }
            }

            return true;
        }

        /// <summary>
        /// Adds received inventory to the Farm Manager system
        /// </summary>
        /// <param name="message">The message containing information about the 
        /// inventory being received</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private List<Data.Entities.Operations.Inventory> ReceiveInventory(AskToReceiveInventory message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            // Validate and retrieve the required information
            FarmLocation? existingLocation = dbContext.FarmLocations
                .AsNoTracking()
                .Include(nameof(FarmLocation.BusinessLocation))
                .Where(l => l.Id == message.FarmLocationId)
                .FirstOrDefault();

            if (existingLocation == null)
                throw new ApplicationException("Location not found");

            List<int> variantIds = message.Inventory
                .Select(i => i.VariantId)
                .ToList();

            List<ProductVariant> dbVariants = dbContext.ProductVariants
                .AsNoTracking()
                .Where(v => variantIds.Contains(v.Id))
                .ToList();

            List<ProductVariantBarcode> dbBarcodes = dbContext.VariantBarcodes
                .AsNoTracking()
                .Where(vb => variantIds.Contains(vb.ProductVariantId))
                .ToList();

            Status? stocked = dbContext.Statuses
                .Where(s =>
                    s.EntityId == "inventory.item"
                    && s.Name == "stocked"
                )
                .FirstOrDefault();

            if (stocked == null)
                throw new ApplicationException("Stocked status not found");

            // Create the actual inventory and keep track of how many
            // items are created
            Dictionary<string, int> inventoryAdditions = new();
            List<Data.Entities.Operations.Inventory> allNewInventory = new();
            foreach(InventoryItem item in message.Inventory)
            {
                ProductVariant? dbVariant = dbVariants
                    .Where(v => v.Id == item.VariantId)
                    .FirstOrDefault();

                ProductVariantBarcode? existingBarcode = dbBarcodes
                    .Where(b => b.Barcode == item.Barcode?.RawBarcode)
                    .FirstOrDefault();

                if(dbVariant == null)
                {
                    _logger.LogWarning("Variant not found for inventory item", item);
                    continue;
                }

                if (existingBarcode == null && !string.IsNullOrEmpty(item.Barcode?.RawBarcode))
                    dbContext.Add(new ProductVariantBarcode
                    {
                        Barcode = item.Barcode.RawBarcode,
                        ProductVariantId = dbVariant.Id
                    });

                List<Data.Entities.Operations.Inventory> newInventory = new();
                for (int i = 0; i < item.Quantity; i++)
                    newInventory.Add(new Data.Entities.Operations.Inventory
                    {
                        ProductVariantId = dbVariant.Id,
                        Barcode = item.Barcode?.RawBarcode,
                        LocationId = existingLocation.Id,
                        StatusId = stocked.Id
                    });

                dbContext.AddRange(newInventory);
                allNewInventory.AddRange(newInventory);

                if(!string.IsNullOrEmpty(dbVariant.ExternalId))
                {
                    if (inventoryAdditions.TryGetValue(dbVariant.ExternalId, out var inventoryAdditionQuantity))
                        inventoryAdditions[dbVariant.ExternalId] = inventoryAdditionQuantity + newInventory.Count;
                    else
                        inventoryAdditions.Add(dbVariant.ExternalId, newInventory.Count);   
                }
            }

            if (allNewInventory.Any())
                dbContext.SaveChanges();

            // publish the inventory to payment processor
            if (
                !string.IsNullOrEmpty(existingLocation.BusinessLocation?.ExternalId)
                && inventoryAdditions.Any() 
                && _inventoryPublisher != null
            )
            {
                DateTimeOffset now = DateTimeOffset.UtcNow;
                List<MessageBrokerMessage> adjustmentMessages = new();
                foreach(KeyValuePair<string, int> addition in inventoryAdditions)
                {
                    InventoryUpdate update = new()
                    {
                        Quantity = addition.Value,
                        VariantId = addition.Key,
                        BusinessLocationId = existingLocation.BusinessLocation.ExternalId,
                        Timestamp = now
                    };

                    adjustmentMessages.Add(new()
                    {
                        Source = $"{Context.Self}",
                        MessageType = Events.PaymentProcessing.Inventory.Create,
                        Assembly = typeof(InventoryUpdate).Assembly.FullName,
                        Class = nameof(InventoryUpdate),
                        TimeStamp = now,
                        Data = update
                    });
                }

                try
                {
                    _inventoryPublisher.PublishMessageBatchAsync(adjustmentMessages).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish inventory update", ex, adjustmentMessages);
                }
            }

            return allNewInventory;
        }

        /// <summary>
        /// Disposes of the selected inventory
        /// </summary>
        /// <param name="message">The message containing the disposal request</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private List<long> DisposeInventory(AskToDisposeInventory message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Enum? dbReason = dbContext.Enums
                .AsNoTracking()
                .Where(e =>
                    e.Id == message.Request.DisposalReasonId
                    && e.EntityId == "inventory.disposal.reason"
                    && e.IsDeleted == false
                )
                .FirstOrDefault();

            if (dbReason == null)
                throw new ApplicationException("Disposal reason not found");

            // TODO: This will eventually be removed. Sold, Consumed,
            // and Donated will be handled through other workflows
            List<string> notDisposed = new() { "sold", "consumed", "donated" };
            Status? newStatus = notDisposed.Contains(dbReason.Name)
                ? dbContext.Statuses
                    .Where(s => 
                        s.Name == dbReason.Name
                        && s.EntityId == "inventory.item"
                    )
                    .FirstOrDefault()
                : dbContext.Statuses
                    .Where(s => 
                        s.Name == "disposed"
                        && s.EntityId == "inventory.item"
                    )
                    .FirstOrDefault();

            if (newStatus == null)
                throw new ApplicationException("Disposal status not found");

            DateTimeOffset now = DateTimeOffset.UtcNow;
            List<ProductionItem> productionItems = dbContext.ProductionItems
                .Where(pi => message.Request.ProductionItemIds.Contains(pi.Id))
                .ToList();
            List<Data.Entities.Operations.Inventory> inventoryItems = dbContext.Inventory
                .Where(i => message.Request.InventoryIds.Contains(i.Id))
                .ToList();

            foreach(ProductionItem item in productionItems)
            {
                item.StatusId = newStatus.Id;
                switch(newStatus.Name)
                {
                    case "sold":
                        item.SaleDate = now;
                        break;
                    case "consumed":
                        item.ConsumptionDate = now;
                        break;
                    case "donated":
                        item.DonationDate = now;
                        break;
                    default:
                        item.DisposalDate = now;
                        item.DisposalReasonId = dbReason.Id;
                        break;
                }
            }

            foreach(Data.Entities.Operations.Inventory item in inventoryItems)
            {
                item.StatusId = newStatus.Id;
                switch (newStatus.Name)
                {
                    case "sold":
                        item.SaleDate = now;
                        break;
                    case "consumed":
                        item.ConsumptionDate = now;
                        break;
                    case "donated":
                        item.DonationDate = now;
                        break;
                    default:
                        item.DisposalDate = now;
                        item.DisposalReasonId = dbReason.Id;
                        break;
                }
            }

            dbContext.SaveChanges();

            return message.Request.ProductionItemIds
                .Concat(message.Request.InventoryIds)
                .ToList();
        }

        //////////////////////////////////////////
        //               Helpers                //
        //////////////////////////////////////////

        /// <summary>
        /// Retrieves a list of products
        /// </summary>
        /// <returns></returns>
        private async Task<List<Product>> RetrieveProducts()
        {
            List<Product> results = new();
            FarmManagerResponse? productResponse = await _farmManager
                .Ask(
                    new AskForProducts(),
                    TimeSpan.FromSeconds(_settings.ActorWaitSeconds)
                ) as FarmManagerResponse;

            if (productResponse != null && productResponse.Status == ResponseStatus.Success)
                results = productResponse.Data as List<Product> ?? new();

            return results;
        }
    }
}
