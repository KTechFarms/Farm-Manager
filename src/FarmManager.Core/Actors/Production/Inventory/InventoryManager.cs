﻿using Akka.Actor;
using Akka.DependencyInjection;

namespace FarmManager.Core.Actors.Production.Inventory
{
    /// <summary>
    /// The manager responsible for routing inventory related requests
    /// to the correct destination
    /// </summary>
    public class InventoryManager : FarmManagerActor<InventoryManager>
    {
        private IActorRef? _worker;

        public InventoryManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            ReceiveAny(ForwardToWorker);
        }

        protected override void PreStart()
        {
            Props workerProps = DependencyResolver.For(Context.System).Props<InventoryWorker>();
            _worker = Context.ActorOf(workerProps, "InventoryWorker");
        }

        /// <summary>
        /// Forwards a message to the worker
        /// </summary>
        /// <param name="message"></param>
        private void ForwardToWorker(object message)
        {
            IUntypedActorContext context = Context;
            IActorRef sender = Sender;
            TimeSpan timeout = TimeSpan.FromSeconds(_settings.ActorWaitSeconds);

            _worker.Ask(message, timeout)
                .ContinueWith(result => sender.Tell(result.Result));
        }
    }
}
