﻿using FarmManager.Data.ActorMessages.Production.FarmLocations;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.DependencyInjection;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Core.Actors.Production.FarmLocations
{
    /// <summary>
    /// A worker who performs logic for farm location related tasks
    /// </summary>
    public class FarmLocationWorker : FarmManagerActor<FarmLocationWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public FarmLocationWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<AskForFarmLocations>(message => HandleWithAsync(RetrieveLocationsAsync, message));
            Receive<AskToCreateFarmLocation>(message => HandleWithAsync(CreateLocationAsync, message));
            Receive<AskToDeleteFarmLocation>(message => HandleWith(DeleteLocation, message));
        }

        /// <summary>
        /// Retrieves a list of locations
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private async Task<List<FarmLocation>> RetrieveLocationsAsync(AskForFarmLocations message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            List<FarmLocation> allLocations = await dbContext.FarmLocations
                .AsNoTracking()
                .Include(nameof(FarmLocation.LocationType))
                .Include(nameof(FarmLocation.BusinessLocation))
                .ToListAsync();

            IEnumerable<FarmLocation> requestedLocations = allLocations
                .Where(l => l.IsDeleted == false);

            if (!string.IsNullOrEmpty(message.Query.Type))
                requestedLocations = requestedLocations.Where(l =>
                    l.LocationType.Name == message.Query.Type);

            foreach (FarmLocation location in requestedLocations)
                if (location.ParentLocationId != null)
                    location.ParentLocation = allLocations
                        .FirstOrDefault(l => l.Id == location.ParentLocationId);

            return requestedLocations.ToList();
        }

        /// <summary>
        /// Creates a new location within Farm Manager
        /// </summary>
        /// <param name="message">The message received from the manager</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<FarmLocation> CreateLocationAsync(AskToCreateFarmLocation message)
        {
            if (message.Location?.BusinessLocationId == null)
                throw new ApplicationException("Business location is required");

            using FarmManagerContext locationContext = _dbFactory.CreateDbContext();
            using FarmManagerContext enumContext = _dbFactory.CreateDbContext();
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Task<FarmLocation?> dbLocationRequest = locationContext.FarmLocations
                .AsNoTracking()
                .Where(l => 
                    l.Name == message.Location.Name
                    && l.ParentLocationId == message.Location.ParentLocationId
                )
                .FirstOrDefaultAsync();
            Task<Enum?> dbEnumRequest = enumContext.Enums
                .AsNoTracking()
                .Where(e => e.Id == message.Location.LocationTypeId)
                .FirstOrDefaultAsync();
            Task<FarmLocation?> parentDbLocationRequest = dbContext.FarmLocations
                .AsNoTracking()
                .Where(l => l.Id == message.Location.ParentLocationId)
                .FirstOrDefaultAsync();

            await Task.WhenAll(dbLocationRequest, dbEnumRequest, parentDbLocationRequest);

            if (
                dbLocationRequest.Result != null
                && dbLocationRequest.Result.BusinessLocationId == message.Location.BusinessLocationId
            )
                throw new ApplicationException("A location with the chosen name already exists!");

            if (dbEnumRequest.Result == null)
                throw new ApplicationException("Invalid location type");

            if (
                message.Location.ParentLocationId != null
                && parentDbLocationRequest.Result == null
            )
                throw new ApplicationException("Parent location not found");

            EntityEntry<FarmLocation> dbLocation = dbContext.FarmLocations.Add(message.Location);
            await dbContext.SaveChangesAsync();

            return dbLocation.Entity;
        }

        /// <summary>
        /// Deletes a farm location
        /// </summary>
        /// <param name="message">A message containing the id of the location
        /// that should be deleted</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private List<int> DeleteLocation(AskToDeleteFarmLocation message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            FarmLocation? deleteLocation = dbContext.FarmLocations
                .Where(l => l.Id == message.LocationId)
                .FirstOrDefault();

            if (deleteLocation == null)
                throw new ApplicationException("Location not found");

            List<FarmLocation> allLocations = dbContext.FarmLocations
                .Where(l => l.IsDeleted == false)
                .ToList();

            List<int> childLocationIds = GetChildLocations(deleteLocation.Id, allLocations);

            Lot? lot = dbContext.Lots
                .AsNoTracking()
                .Where(l =>
                    childLocationIds.Contains(l.FarmLocationId)
                    || (
                        l.Crop != null
                        && l.Crop.TransplantLocationId != null
                        && childLocationIds.Contains((int)l.Crop.TransplantLocationId)
                    )
                )
                .FirstOrDefault();

            if (lot != null)
                throw new ApplicationException("Location has been used in production. Cannot delete");

            ProductionItem? product = dbContext.ProductionItems
                .AsNoTracking()
                .Where(i =>
                    (i.LocationId != null
                        && childLocationIds.Contains((int)i.LocationId))
                    || i.LocationHistory.Any(h =>
                        childLocationIds.Contains(h.FarmLocationId)
                ))
                .FirstOrDefault();

            if (product != null)
                throw new ApplicationException("Location has been used in production. Cannot delete");

            Batch? batch = dbContext.Batches
                .AsNoTracking()
                .Where(b => childLocationIds.Contains(b.FarmLocationId))
                .FirstOrDefault();

            if (batch != null)
                throw new ApplicationException("Location has been used in production. Cannot delete");

            List<FarmLocation> removeLocations = allLocations
                .Where(l => childLocationIds.Contains(l.Id))
                .ToList();

            dbContext.RemoveRange(removeLocations);
            dbContext.SaveChanges();

            return childLocationIds;
        }

        /// <summary>
        /// Retrieves child locations of a given LocationId
        /// </summary>
        /// <param name="locationId">The id of the location we want children for</param>
        /// <param name="allLocations">A list of all possible locations</param>
        /// <returns></returns>
        private List<int> GetChildLocations(int locationId, List<FarmLocation> allLocations)
        {
            List<int> results = new() { locationId };

            var children = allLocations.Where(l => l.ParentLocationId == locationId).ToList();
            foreach (var child in children)
            {
                results.Add(child.Id);
                results.AddRange(GetChildLocations(child.Id, allLocations));
            }

            return results;
        }
    }
}
