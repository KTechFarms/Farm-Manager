using Akka.Actor;
using Akka.DependencyInjection;
using FarmManager.Common.Consts;
using FarmManager.Core.Actors.Devices;
using FarmManager.Core.Actors.Generic;
using FarmManager.Core.Actors.Operations;
using FarmManager.Core.Actors.Production;
using FarmManager.Core.Actors.Reports;
using FarmManager.Core.Actors.System;
using FarmManager.Core.Actors.Telemetry;
using FarmManager.Core.Jobs;
using FarmManager.Data.ActorMessages.Devices;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.ActorMessages.Generic.Enums;
using FarmManager.Data.ActorMessages.Operations;
using FarmManager.Data.ActorMessages.Production;
using FarmManager.Data.ActorMessages.Reports;
using FarmManager.Data.ActorMessages.System;
using FarmManager.Data.ActorMessages.System.Dashboard;
using FarmManager.Data.ActorMessages.System.Settings;
using FarmManager.Data.Contexts;
using FarmManager.Data.DTOs;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing.DTOs;
using FluentMigrator.Runner;
using Hangfire;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Actors
{
    /// <summary>
    /// The root actor of the FarmManager application. It's responsible for
    /// overall management of the system and delegating messages to the appropriate
    /// actors
    /// </summary>
    public class FarmManager : FarmManagerActor<FarmManager>
    {
        private IActorRef? _telemetryManager;
        private IActorRef? _deviceManager;
        private IActorRef? _productionManager;
        private IActorRef? _operationsManager;
        private IActorRef? _eventManager;

        private readonly string _paymentProcessorQueue;
        private IMessageBrokerPublisher? _publisher;

        public FarmManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _paymentProcessorQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcesing)
                ?.QueueName ?? string.Empty;

            ReceiveAny(RouteMessage);
        }

        protected override void PreStart()
        {
            RunMigrations();
            CreateManagerActors();
            StartRecurringJobs();
            RetrieveMessageBrokerPublisher();
            PublishInitialPaymentProcessorData();
        }

        //////////////////////////////////////////
        //               Handlers               //
        //////////////////////////////////////////

        /// <summary>
        /// Routes messages to the appropriate actor
        /// </summary>
        /// <param name="message"></param>
        private void RouteMessage(object message)
        {
            IUntypedActorContext context = Context;
            IActorRef sender = Sender;
            TimeSpan timeout = TimeSpan.FromSeconds(_settings.ActorWaitSeconds);

            switch (message)
            {
                case IDashboardMessage:
                    HandleWithInstanceOfAsync<DashboardWorker>(message, sender, context);
                    break;

                case IDeviceMessage:
                    _deviceManager?.Ask(message, timeout)
                        .ContinueWith(result => sender.Tell(result.Result));
                    break;

                case IEnumMessage:
                    HandleWithInstanceOfAsync<EnumWorker>(message, sender, context);
                    break;

                case IEventMessage:
                    if (message is AskForMessageBrokerPublisher)
                        _eventManager?.Ask(message, timeout)
                            .ContinueWith(result => sender.Tell(result.Result));
                    else
                        _eventManager?.Tell(message);
                    break;

                case ILogMessage:
                    HandleWithInstanceOfAsync<LogWorker>(message, sender, context);
                    break;

                case IOperationsMessage:
                    _operationsManager?.Ask(message, timeout)
                        .ContinueWith(result => sender.Tell(result.Result));
                    break;

                case IProductionMessage:
                    _productionManager?.Ask(message, timeout)
                        .ContinueWith(result => sender.Tell(result.Result));
                    break;

                case IReportMessage:
                    HandleWithInstanceOfAsync<ReportFetcher>(message, sender, context);
                    break;

                case ISettingsMessage:
                    HandleWithInstanceOfAsync<SettingsWorker>(message, sender, context);
                    break;

                default:
                    string messageType = message.GetType().Name;
                    _logger.LogWarning($"Unhandled message of type {messageType}");
                    sender.Tell(ActorResponse.Failure($"Unsupported message type: {messageType}"));
                    break;
            }
        }

        //////////////////////////////////////////
        //               Helpers                //
        //////////////////////////////////////////

        /// <summary>
        /// Runs the database migrations
        /// </summary>
        private void RunMigrations()
        {
            IMigrationRunner runner = _scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
            runner.MigrateUp();
        }

        /// <summary>
        /// Initializes the other Farm Manager manager actors
        /// </summary>
        private void CreateManagerActors()
        {
            Props telemetryProps = DependencyResolver.For(Context.System).Props<TelemetryManager>();
            _telemetryManager = Context.ActorOf(telemetryProps, "TelemetryManager");

            Props deviceProps = DependencyResolver.For(Context.System).Props<DeviceManager>();
            _deviceManager = Context.ActorOf(deviceProps, "DeviceManager");

            Props productionProps = DependencyResolver.For(Context.System).Props<ProductionManager>();
            _productionManager = Context.ActorOf(productionProps, "ProductionManager");

            Props operationsProps = DependencyResolver.For(Context.System).Props<OperationsManager>();
            _operationsManager = Context.ActorOf(operationsProps, "OperationsManager");

            Props eventProps = DependencyResolver.For(Context.System).Props<FarmEventManager>();
            _eventManager = Context.ActorOf(eventProps, "EventManager");
        }

        /// <summary>
        /// Retrieves a message broker publisher from the event manager
        /// </summary>
        private void RetrieveMessageBrokerPublisher()
        {
            _publisher = _eventManager.Ask<IMessageBrokerPublisher>(
                new AskForMessageBrokerPublisher(
                    _paymentProcessorQueue,
                    $"{Self}"
                ),
                TimeSpan.FromSeconds(_settings.ActorWaitSeconds)
            ).Result;
        }

        /// <summary>
        /// Creates or updates any recurring jobs
        /// </summary>
        private void StartRecurringJobs()
        {
            RecurringJob.AddOrUpdate<MaintenanceJobs>(
                nameof(MaintenanceJobs.SystemLogCleanup),
                x => x.SystemLogCleanup(),
                Cron.Daily(0, 0),
                TimeZoneInfo.Utc
            );

            RecurringJob.AddOrUpdate<ReportJobs>(
                nameof(ReportJobs.GenerateStatisticsReport),
                x => x.GenerateStatisticsReport(),
                Cron.Daily(0, 1),
                TimeZoneInfo.Utc
            );
        }

        // TODO: This will eventually move to a generic syncing job that
        // runs at startup and whenever the payment processor changes
        /// <summary>
        /// Publishes the pre-generated product categories to the payment 
        /// processor if it hasn't already been done
        /// </summary>
        private void PublishInitialPaymentProcessorData()
        {
            try
            {
                IDbContextFactory<FarmManagerContext> dbFactory = _scope.ServiceProvider
                    .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

                using FarmManagerContext dbContext = dbFactory.CreateDbContext();

                List<ProductCategory> initialCategories = dbContext.ProductCategories
                    .AsNoTracking()
                    .Where(c =>
                        new[] { "Produce", "Seed" }.Contains(c.Name)
                        && string.IsNullOrEmpty(c.ExternalId)
                    )
                    .ToList();

                if (_publisher != null && initialCategories.Any())
                {
                    List<MessageBrokerMessage> messages = new();
                    foreach (ProductCategory category in initialCategories)
                    {
                        CategoryCreation categoryCreation = new()
                        {
                            Id = category.Id,
                            Name = category.Name,
                        };

                        messages.Add(new MessageBrokerMessage
                        {
                            Source = typeof(FarmManager).AssemblyQualifiedName,
                            MessageType = Events.PaymentProcessing.Category.Create,
                            Assembly = typeof(CategoryCreation).Assembly.FullName,
                            Class = nameof(CategoryCreation),
                            TimeStamp = DateTimeOffset.UtcNow,
                            Data = categoryCreation
                        });
                    }

                    _publisher.PublishMessageBatchAsync(messages).Wait();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error publishing initial categories to payment processor", ex);
            }
        }
    }
}
