﻿using FarmManager.Data.ActorMessages.System.Settings;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Actors.System
{
    /// <summary>
    /// The worker responsible for all settings related logic
    /// </summary>
    public class SettingsWorker : FarmManagerActor<SettingsWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public SettingsWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<AskForSettings>(message => HandleWithAsync(RetrieveSettings, message));
            Receive<AskToUpdateSettings>(message => HandleWithAsync(UpdateSettings, message));
        }

        /// <summary>
        /// Retrieves a list of Farm Manager settings
        /// </summary>
        /// <param name="message">A message asking for the settings</param>
        /// <returns></returns>
        private async Task<IEnumerable<Setting>> RetrieveSettings(AskForSettings message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            return await dbContext.Settings
                .AsNoTracking()
                .Include(nameof(Setting.Category))
                .ToListAsync();
        }

        /// <summary>
        /// Updates the requested settings
        /// </summary>
        /// <param name="message">A message containing a dicitionary of setting names and values
        /// that should be updated</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<IEnumerable<Setting>> UpdateSettings(AskToUpdateSettings message)
        {
            if (message.Settings == null || message.Settings.Count() == 0)
                throw new ApplicationException("No settings to update");

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            List<string> settingNames = message.Settings.Keys.ToList();

            List<Setting> settings = await dbContext.Settings
                .Where(s => settingNames.Contains(s.Name))
                .ToListAsync();

            foreach(Setting setting in settings)
                setting.Value = message.Settings[setting.Name] ?? setting.Value;

            await dbContext.SaveChangesAsync();
            return settings;
        }
    }
}
