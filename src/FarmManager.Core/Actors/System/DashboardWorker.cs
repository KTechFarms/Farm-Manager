﻿using FarmManager.Data.ActorMessages.System.Dashboard;
using FarmManager.Data.Contexts;
using FarmManager.Data.DTOs.System.Dashboard;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Actors.System
{
    /// <summary>
    /// The worker responsible for retrieving data for display by the Farm Manager
    /// dashboard
    /// </summary>
    public class DashboardWorker : FarmManagerActor<DashboardWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public DashboardWorker(IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<AskForDeviceCounts>(message => HandleWith(GetDeviceCounts, message));
        }

        /// <summary>
        /// Retrieves the count of devices on the system, both active and pending
        /// </summary>
        /// <param name="message">The message received by the Farm Manager</param>
        /// <returns></returns>
        private DeviceCountSummary GetDeviceCounts(AskForDeviceCounts message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            int activeCount = dbContext.Devices
                .AsNoTracking()
                .Where(d => d.IsDeleted == false)
                .Count();

            int pendingCount = dbContext.PendingDevices
                .Where(p =>
                    p.HasRegistered == false
                    && p.IsDeleted == false
                )
                .Count();

            return new DeviceCountSummary
            {
                ActiveDevices = activeCount,
                PendingDevices = pendingCount
            };
        }
    }
}
