﻿using FarmManager.Data.ActorMessages.System;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.System;
using FarmManager.Data.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq.Expressions;
using System.Reflection;

namespace FarmManager.Core.Actors.System
{
    /// <summary>
    /// A short-lived worker that handles any log-related logic.
    /// </summary>
    public class LogWorker : FarmManagerActor<LogWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public LogWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<AskToRetrieveLogs>(message => HandleWithAsync(RetrieveLogs, message));
        }

        /// <summary>
        /// Builds a dynamic Queryable object using the provided query parameters
        /// and then retrieves logs from the database
        /// </summary>
        /// <param name="message">The message that was received by the Farm Manager,
        /// containing query parameters</param>
        /// <returns></returns>
        private async Task<IEnumerable<Log>> RetrieveLogs(AskToRetrieveLogs message)
        {
            if (message.Query.StartTime == null || message.Query.StopTime == null)
                throw new ApplicationException("Start and Stop times are required");
            
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            IQueryable<Log> logs = dbContext.Logs.AsQueryable();

            // Apply the date range
            logs = logs.Where(l => 
                l.Timestamp >= message.Query.StartTime
                && l.Timestamp <= message.Query.StopTime
            );

            // Apply the source
            if (!string.IsNullOrEmpty(message.Query.Source))
                logs = logs.Where(l => l.Source != null && l.Source.Contains(message.Query.Source));

            // Apply the log level
            if (message.Query.LogLevel != null)
            {
                logs = logs.Where(l => l.LogLevel == message.Query.LogLevel);
            }

            // Apply the search text
            if (!string.IsNullOrEmpty(message.Query.SearchText))
            {
                List<string> availableFields = typeof(Log)
                    .GetProperties()
                    .Where(p => p.PropertyType == typeof(string))
                    .Select(p => p.Name)
                    .ToList();

                List<string> searchFields = string.IsNullOrEmpty(message.Query.SearchFields)
                    ? availableFields
                    : message.Query.SearchFields
                        .Split(',')
                        .ToList();

                List<Expression<Func<Log, bool>>> filters = new List<Expression<Func<Log, bool>>>();
                try
                {
                    foreach(string field in searchFields)
                    {
                        if(availableFields.Contains(field))
                        {
                            // https://entityframeworkcore.com/knowledge-base/49003404/how-to-create-linq-expression-dynamically-in-csharp-for-contains
                            MethodInfo? method = typeof(string).GetMethod("Contains", new Type[] { typeof(string) });
                            if (method == null)
                                throw new Exception("string.Contains method not found");

                            ParameterExpression parameter = Expression.Parameter(typeof(Log), "Log");
                            MemberExpression member = Expression.Property(parameter, field);
                            ConstantExpression constant = Expression.Constant(message.Query.SearchText);
                            Expression body = Expression.Call(member, method, constant);
                            filters.Add(Expression.Lambda<Func<Log, bool>>(body, parameter));
                        }
                    }

                    if(filters.Count > 0)
                    {
                        Expression<Func<Log, bool>> combinedFilters = filters.Aggregate((x, y) => x.OrElse(y));
                        logs = logs.Where(combinedFilters);
                    }
                }
                catch(Exception ex)
                {
                    _logger.LogError("Unable to generate search field expressions", ex, message.Query);
                }
            }

            logs = message.Query.MostRecent
                ? logs.OrderByDescending(l => l.Timestamp)
                : logs.OrderBy(l => l.Timestamp);

            return await logs
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
