﻿using Akka.Actor;
using Akka.DependencyInjection;

namespace FarmManager.Core.Actors.Telemetry
{
    /// <summary>
    /// The actor responsible for managing all telemetry-related events.
    /// It manages the subscribers which will connect to the message broker
    /// </summary>
    public class TelemetryManager : FarmManagerActor<TelemetryManager>
    {
        private IActorRef? _worker;

        public TelemetryManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        protected override void PreStart()
        {
            Props workerProps = DependencyResolver.For(Context.System).Props<TelemetryWorker>();
            _worker = Context.ActorOf(workerProps, "TelemetryWorker");
        }
    }
}
