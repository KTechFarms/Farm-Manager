﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Core.Hubs;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Devices;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;

namespace FarmManager.Core.Actors.Telemetry
{
    /// <summary>
    /// The actual worker that handles telemetry 
    /// messages. It will interact with the database,
    /// perform any additional actions, and report
    /// the results back to the subscriber
    /// </summary>
    public class TelemetryWorker : FarmManagerActor<TelemetryWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;
        private readonly IHubContext<TelemetryHub> _telemetryHub;

        public TelemetryWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();
            _telemetryHub = _scope.ServiceProvider
                .GetRequiredService<IHubContext<TelemetryHub>>();

            Receive<EventReceived>(message => SaveTelemetry(message));
        }

        protected override void PreStart()
        {
            _farmManager.Tell(new SubscribeToEvent(Events.Telemetry.Reading, Self));
        }

        protected override void PreRestart(Exception reason, object message)
        {
            _logger.LogWarning($"Restarting {nameof(TelemetryWorker)}: {reason.Message}");
            base.PreRestart(reason, message);
        }

        protected override void PostStop()
        {
            _farmManager.Tell(new UnsubscribeFromEvent(Events.Telemetry.Reading, Self));

            base.PostStop();
        }

        //////////////////////////////////////////
        //            Implementation            //
        //////////////////////////////////////////

        /// <summary>
        /// Saves telemetry data to the Farm Manager database
        /// </summary>
        /// <param name="message">The message received by the actor</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        private async Task SaveTelemetry(EventReceived message)
        {
            if (message.Data == null)
                throw new ArgumentNullException(nameof(EventReceived.Data));

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Device? device = await dbContext.Devices
                .Include(nameof(Device.Interfaces))
                .Where(d => d.DeviceId == message.Source)
                .FirstOrDefaultAsync();

            if (device == null)
                throw new ApplicationException("Device not found");

            List<Data.Entities.Devices.Telemetry> savedTelemetry = await SaveTelemetryData(
                dbContext, 
                device, 
                message.TimeStamp, 
                message.Data
            );

            await _telemetryHub.Clients
                .Group(device.DeviceId)
                .SendAsync(
                    ClientMethods.Telemetry.NewTelemetry, 
                    savedTelemetry
                );
        }

        //////////////////////////////////////////
        //               Helpers                //
        //////////////////////////////////////////

        /// <summary>
        /// Saves telemetry to the Farm Manager database
        /// </summary>
        /// <param name="dbContext">The dbContext to save to</param>
        /// <param name="device">The device that sent the telemetry</param>
        /// <param name="timestamp">The time at which the telemetry was sent</param>
        /// <param name="telemetry">The actual telemetry data</param>
        /// <returns></returns>
        private async Task<List<Data.Entities.Devices.Telemetry>> SaveTelemetryData(
            FarmManagerContext dbContext, 
            Device device, 
            DateTimeOffset timestamp, 
            object telemetry
        )
        {
            List<Data.Entities.Devices.Telemetry> telemetryList = new();

            JArray dataArray = telemetry as JArray ?? new JArray();
            foreach(JToken item in dataArray)
            {
                string? deviceInterface = (string?)item["Interface"];
                if (string.IsNullOrEmpty(deviceInterface))
                    throw new ApplicationException("Telemetry is missing an interface");

                Interface? dbInterface = device.Interfaces
                    .Where(i => i.InterfaceId == deviceInterface)
                    .FirstOrDefault();

                if (dbInterface == null)
                    throw new ApplicationException("Provided interface does not exist on the device record");

                JObject? telemetryData = item["Telemetry"]?.ToObject<JObject>();
                if (telemetryData == null)
                    throw new ApplicationException("Data is missing telemetry");

                foreach(KeyValuePair<string, JToken?> dataPoint in telemetryData)
                {
                    string key = dataPoint.Key;
                    JToken? value = dataPoint.Value;

                    if (value != null)
                    {
                        Data.Entities.Devices.Telemetry record = new()
                        {
                            InterfaceId = dbInterface.Id,
                            TimeStamp = timestamp,
                            Key = key
                        };

                        switch (value.Type)
                        {
                            case JTokenType.String:
                                record.StringValue = (string?)value;
                                break;
                            case JTokenType.Boolean:
                                record.BoolValue = (bool?)value;
                                break;
                            case JTokenType.Float:
                                record.Value = Convert.ToDouble((float?)value);
                                break;
                            case JTokenType.Integer:
                                record.Value = Convert.ToDouble((int?)value);
                                break;
                            default:
                                _logger.LogWarning($"Unhandled token of type {value.Type}");
                                break;
                        }

                        telemetryList.Add(record);
                    }
                }
            }

            await dbContext.AddRangeAsync(telemetryList);
            device.LastConnection = timestamp;
            await dbContext.SaveChangesAsync();
            return telemetryList;
        }
    }
}
