﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Services.MessageBroker;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Actors
{
    /// <summary>
    /// A manager resposible for subscribing to the message broker and publishing 
    /// the events to all other actors that have subscribed to the events
    /// </summary>
    public class FarmEventManager : FarmManagerActor<FarmEventManager>
    {
        private readonly IMessageBrokerClient _mbClient;
        private readonly IMessageBrokerReceiver? _telemetryReceiver;
        private readonly IMessageBrokerReceiver? _inventoryReceiver;
        private readonly IMessageBrokerReceiver? _paymentProcessingReceiver;

        private Dictionary<string, HashSet<IActorRef>> _subscribers = new();

        public FarmEventManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            MessageBrokerChannelSettings? inventorySetings = _settings.MessageBroker
                .GetSettings(MessageBrokerChannel.Inventory);

            MessageBrokerChannelSettings? telemetrySetings = _settings.MessageBroker
                .GetSettings(MessageBrokerChannel.Telemetry);

            MessageBrokerChannelSettings? paymentEventSetings = _settings.MessageBroker
                .GetSettings(MessageBrokerChannel.PaymentProcessingEvents);

            if (string.IsNullOrEmpty(inventorySetings?.QueueName))
                throw new ArgumentNullException(nameof(MessageBrokerChannelSettings));

            if (string.IsNullOrEmpty(telemetrySetings?.QueueName))
                throw new ArgumentNullException(nameof(MessageBrokerChannelSettings));

            if (string.IsNullOrEmpty(paymentEventSetings?.QueueName))
                throw new ArgumentNullException(nameof(MessageBrokerChannelSettings));

            _mbClient = _scope.ServiceProvider.GetRequiredService<IMessageBrokerClient>();
            _inventoryReceiver = _mbClient
                .CreateMessageBrokerReceiver(inventorySetings.QueueName);
            _telemetryReceiver = _mbClient
                .CreateMessageBrokerReceiver(telemetrySetings.QueueName);
            _paymentProcessingReceiver = _mbClient
                .CreateMessageBrokerReceiver(paymentEventSetings.QueueName);

            Receive<BeginProcessing>(BeginProcessing);
            Receive<SubscribeToEvent>(HandleSubscription);
            Receive<UnsubscribeFromEvent>(HandleUnSubscription);
            Receive<AskForMessageBrokerPublisher>(CreateMessageBrokerPublisher);
        }

        protected override void PreStart()
        {
            if (_telemetryReceiver == null)
                throw new ArgumentException(nameof(IMessageBrokerClient));

            if (!_telemetryReceiver.TryRegisterHandler(Events.Telemetry.RegisterDevice, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.Telemetry.RegisterDevice}");

            if (!_telemetryReceiver.TryRegisterHandler(Events.Telemetry.Reading, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.Telemetry.Reading}");

            if (_inventoryReceiver == null)
                throw new ArgumentException(nameof(IMessageBrokerClient));

            if (!_inventoryReceiver.TryRegisterHandler(Events.PaymentProcessing.Inventory.Create, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Inventory.Create}");

            if (!_inventoryReceiver.TryRegisterHandler(Events.PaymentProcessing.Inventory.Transfer, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Inventory.Create}");

            if (_paymentProcessingReceiver == null)
                throw new ArgumentException(nameof(IMessageBrokerClient));

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Unit.Update, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Unit.Update}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Category.Update, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Category.Update}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Location.Update, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Location.Update}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Product.Update, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Product.Update}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.ProductVariant.Update, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.ProductVariant.Update}");

            // Give the initial actors time to startup before receiving messages
            Context.System.Scheduler.ScheduleTellOnce(
                _settings.ActorWaitSeconds, 
                Self, 
                new BeginProcessing(), 
                Self
            );
        }

        protected override void PostStop()
        {
            _inventoryReceiver?.Dispose();
            _telemetryReceiver?.Dispose();
            _paymentProcessingReceiver?.Dispose();
            _mbClient.Dispose();

            base.PostStop();
        }

        private void BeginProcessing(BeginProcessing message)
        {
            _telemetryReceiver?.BeginProcessingAsync();
            _inventoryReceiver?.BeginProcessingAsync();
            _paymentProcessingReceiver?.BeginProcessingAsync();
        }

        //////////////////////////////////////////
        //           Message Handlers           //
        //////////////////////////////////////////

        /// <summary>
        /// Handles registering a subscriber for a given message type
        /// </summary>
        /// <param name="message">A message containing the subscription information</param>
        private void HandleSubscription(SubscribeToEvent message)
        {
            if (_subscribers.TryGetValue(message.EventType, out HashSet<IActorRef>? subscribers))
            {
                if (!subscribers.TryGetValue(message.Subscriber, out IActorRef? existingSubscriber))
                    subscribers.Add(message.Subscriber);
            }
            else
                _subscribers.TryAdd(message.EventType, new HashSet<IActorRef> { message.Subscriber });
        }

        /// <summary>
        /// Handles unregistering a subscriber for a given message type
        /// </summary>
        /// <param name="message"></param>
        private void HandleUnSubscription(UnsubscribeFromEvent message)
        {
            if(_subscribers.TryGetValue(message.EventType, out HashSet<IActorRef>? subscribers))
                if(!subscribers.Remove(message.Subscriber))
                    _logger.LogWarning($"Unable to remove {message.Subscriber} from {message.EventType} subscribers");
        }

        /// <summary>
        /// Creates a message broker publisher that can be used by other actors
        /// </summary>
        /// <param name="message">A message requesting a publisher</param>
        private void CreateMessageBrokerPublisher(AskForMessageBrokerPublisher message)
        {
            IMessageBrokerPublisher publisher = _mbClient.CreateMessageBrokerPublisher(
                message.QueueName,
                message.Identifier
            );

            Sender.Tell(publisher);
        }

        //////////////////////////////////////////
        //            Event Handlers            //
        //////////////////////////////////////////

        /// <summary>
        /// Publishes an event received by the message broker receiver to any subscribers
        /// of that event
        /// </summary>
        /// <param name="message"></param>
        private void PublishEventToSubscribers(MessageBrokerMessage message)
        {
            if (
                !string.IsNullOrEmpty(message.MessageType)
                && _subscribers.TryGetValue(message.MessageType, out HashSet<IActorRef>? subscribers)
                && subscribers != null
            )
                foreach (IActorRef subscriber in subscribers)
                    subscriber.Tell(new EventReceived(
                        message.MessageType,
                        message.Source,
                        message.TimeStamp,
                        message.Data,
                        message.Assembly,
                        message.Class
                    ));
        }
    }
}
