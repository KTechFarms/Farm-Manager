﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Core.Config.IoTHub;
using FarmManager.Data.ActorMessages.Devices;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Contexts;
using FarmManager.Data.DTOs;
using FarmManager.Data.DTOs.Devices;
using FarmManager.Data.Entities.Devices;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

using Device = Microsoft.Azure.Devices.Device;
using FarmDevice = FarmManager.Data.Entities.Devices.Device;

namespace FarmManager.Core.Actors.Devices
{
    /// <summary>
    /// The actual worker that handles device messages.
    /// It will interact with the database, perform any additional
    /// actions, and report the results back to the manager
    /// </summary>
    public class DeviceWorker : FarmManagerActor<DeviceWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;
        private readonly RegistryManager _manager;
        private readonly ServiceClient _serviceClient;
        private readonly IotHubSettings _iotSettings;

        public DeviceWorker(
            IServiceProvider serviceProvider, 
            RegistryManager manager,
            ServiceClient serviceClient
        ) : base(serviceProvider)
        {
            _manager = manager;
            _serviceClient = serviceClient;
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();
            _iotSettings = _settings?.IotHub ?? throw new ApplicationException($"{nameof(IotHubSettings)} not found");

            Receive<EventReceived>(HandleDeviceInitializationEvent);
            Receive<AskToRegisterDevice>(HandleDeviceRegistration);
            Receive<TellToInitializeDevice>(message => HandleWithAsync(InitializeDevice, message));
            Receive<AskToGenerateDeviceToken>(message => HandleWithAsync(GenerateToken, message));
            Receive<AskToRetrieveDevices>(message => HandleWithAsync(RetrieveDevices, message));
            Receive<AskForDeviceInformation>(message => HandleWithAsync(RetrieveDeviceInformation, message));
            Receive<AskToInvokeMethod>(message => HandleWithAsync(InvokeDeviceMethod, message));
        }

        protected override void PreStart()
        {
            _farmManager.Tell(new SubscribeToEvent(Events.Telemetry.RegisterDevice, Self));
        }

        protected override void PostStop()
        {
            _farmManager.Tell(new UnsubscribeFromEvent(Events.Telemetry.RegisterDevice, Self));

            base.PostStop();
        }

        //////////////////////////////////////////
        //               Handlers               //
        //////////////////////////////////////////

        /// <summary>
        /// Receives a message when a device attempts to intialize with 
        /// Farm Manager and sends it to the InitializeDevice function
        /// </summary>
        /// <param name="message">The message received from the message broker</param>
        /// <exception cref="ArgumentNullException"></exception>
        private void HandleDeviceInitializationEvent(EventReceived message)
        {
            if (message.Data == null)
                throw new ArgumentNullException(nameof(EventReceived.Data));

            string deviceString = JsonConvert.SerializeObject(message.Data);
            FarmDevice? device = JsonConvert.DeserializeObject<FarmDevice>(deviceString);

            if (device != null)
                InitializeDevice(new TellToInitializeDevice(device, message.TimeStamp)).Wait();
        }

        /// <summary>
        /// Receives a message when a device is registered to Farm Manager.
        /// </summary>
        /// <param name="message">The message containing device information</param>
        private void HandleDeviceRegistration(AskToRegisterDevice message)
        {
            IActorRef sender = Sender;

            RegisterDeviceToIoTHubAsync(message.DeviceName)
                .ContinueWith(hubResult =>
                {
                    if (hubResult.Exception != null)
                    {
                        sender.Tell(ActorResponse.Failure(hubResult.Exception.Message));
                        return;
                    }

                    RegisterDeviceToFarmManager(hubResult.Result)
                        .ContinueWith(farmManagerResult => HandleResult(farmManagerResult, sender));
                });
        }

        //////////////////////////////////////////
        //            Implementation            //
        //////////////////////////////////////////

        /// <summary>
        /// Registers a device with the IoT Hub. If a device already exists, it
        /// merely returns that, instead.
        /// </summary>
        /// <param name="deviceName">The name of the device that's being registered</param>
        /// <returns></returns>
        private async Task<Device> RegisterDeviceToIoTHubAsync(string deviceName)
        {
            if (deviceName.Contains(" "))
                throw new ApplicationException("Device name cannot contain spaces");

            Device device = new Device(deviceName);
            try
            {
                device = await _manager.AddDeviceAsync(device);
            }
            catch(DeviceAlreadyExistsException)
            {
                device = await _manager.GetDeviceAsync(deviceName);
            }

            return device;
        }

        /// <summary>
        /// Registers a PendingDevice with Farm Manager and generates a key that
        /// can be used to create SAS tokens for the device
        /// </summary>
        /// <param name="iotDevice">The device that was returned from the IoT Hub</param>
        /// <returns></returns>
        private async Task<PendingDevice> RegisterDeviceToFarmManager(Device iotDevice)
        {
            // DeviceId has a unique constraint, so we don't have to check for duplicates
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            EntityEntry<PendingDevice> deviceEntry = await dbContext
                .AddAsync(new PendingDevice
                {
                    DeviceId = iotDevice.Id,
                    DeviceKey = Guid.NewGuid(),
                    HasRegistered = false
                });

            await dbContext.SaveChangesAsync();
            return deviceEntry.Entity;
        }

        /// <summary>
        /// Handles the initial device message, either generating the 
        /// device and interface definitions in the database, or updating the 
        /// last connected status of the device
        /// </summary>
        /// <param name="device">The definition of the device that's connecting</param>
        /// <param name="timestamp">The time at which the device connected</param>
        /// <returns></returns>
        private async Task InitializeDevice(TellToInitializeDevice message)
        {
            ValidateDeviceData(message.Device);

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            FarmDevice? registeredDevice = await dbContext.Devices
                .Include(nameof(FarmDevice.Interfaces))
                .Where(d => d.DeviceId == message.Device.DeviceId)
                .FirstOrDefaultAsync();

            if (registeredDevice == null)
            {
                PendingDevice? pendingDevice = await dbContext.PendingDevices
                    .Where(pd =>
                        pd.DeviceId == message.Device.DeviceId
                        && pd.DeviceKey == message.Device.DeviceKey
                        && pd.IsDeleted == false
                    )
                    .FirstOrDefaultAsync();

                if (pendingDevice == null)
                    throw new ApplicationException("Device not found");

                FarmDevice refDevice = message.Device;
                refDevice.LastConnection = message.TimeStamp;
                refDevice.PendingDeviceId = pendingDevice.Id;
                UpdateInterfaceData(dbContext, ref refDevice);
                await dbContext.AddAsync(refDevice);
                pendingDevice.HasRegistered = true;
            }
            else
                registeredDevice.LastConnection = message.TimeStamp;

            await dbContext.SaveChangesAsync();
        }

        // https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-dev-guide-sas?tabs=csharp
        /// <summary>
        /// Generates a SAS token that can be used by a device to connect to the IoT hub
        /// </summary>
        /// <param name="deviceId">The identifier of the device requesting a token</param>
        /// <param name="durationSeconds">The duration that the token should be valid for</param>
        /// <param name="deviceKey">The key for the device, provided when registering with the Farm Manager system</param>
        /// <returns></returns>
        private async Task<string> GenerateToken(AskToGenerateDeviceToken message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            if (!await IsValidDevice(message.Device, message.DeviceKey))
                throw new ApplicationException("Unauthorized device");

            string resourceUri = $"{_iotSettings.ResourceUri}/{message.Device}";

            TimeSpan fromEpochStart = DateTimeOffset.UtcNow - new DateTime(1970, 1, 1);
            string expiry = Convert.ToString((int)fromEpochStart.TotalSeconds + message.DurationSeconds);

            string stringToSign = WebUtility.UrlEncode(resourceUri) + "\n" + expiry;

            HMACSHA256 hmac = new HMACSHA256(Convert.FromBase64String(_iotSettings.HubAccessKey));
            string signature = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(stringToSign)));

            string token = string.Format(
                CultureInfo.InvariantCulture,
                "SharedAccessSignature sr={0}&sig={1}&se={2}",
                WebUtility.UrlEncode(resourceUri),
                WebUtility.UrlEncode(signature),
                expiry
            );

            if (!string.IsNullOrEmpty(_iotSettings.HubAccessPolicy))
                token += "&skn=" + _iotSettings.HubAccessPolicy;

            return token;
        }

        /// <summary>
        /// Builds a dynamic queryable object using the provided query parameters and then
        /// retrieves devices from the database
        /// </summary>
        /// <param name="message">The message that was received by the Farm Manager, containing
        /// query parameters</param>
        /// <returns></returns>
        private async Task<IEnumerable<DeviceDTO>> RetrieveDevices(AskToRetrieveDevices message)
        {
            using FarmManagerContext deviceContext = _dbFactory.CreateDbContext();
            using FarmManagerContext pendingContext = _dbFactory.CreateDbContext();

            IQueryable<FarmDevice> devices = deviceContext.Devices.AsQueryable();

            devices = devices.Where(d => d.IsDeleted == false);

            PropertyInfo[] properties = message.Query.GetType().GetProperties();
            foreach (PropertyInfo prop in properties)
            {
                if (prop.PropertyType == typeof(string))
                {
                    string? propValue = prop.GetValue(message.Query) as string;
                    if (!string.IsNullOrEmpty(propValue))
                        devices = devices.Where(d => EF.Property<string>(d, prop.Name) == propValue);
                }
            }

            Task<List<FarmDevice>> registeredDevices = devices
                .AsNoTracking()
                .ToListAsync();
            Task<List<PendingDevice>> pendingDevices = pendingContext.PendingDevices
                .AsNoTracking()
                .Where(pd =>
                    pd.IsDeleted == false
                    && pd.HasRegistered == false
                )
                .ToListAsync();

            await Task.WhenAll(registeredDevices, pendingDevices);

            List<DeviceDTO> results = registeredDevices.Result
                .Select(d => new DeviceDTO
                {
                    DeviceId = d.DeviceId,
                    Name = d.Name,
                    SerialNumber = d.SerialNumber,
                    Model = d.Model,
                    IsPending = false,
                    LastConnection = d.LastConnection
                }).ToList();

            results.AddRange(pendingDevices.Result
                .Select(p => new DeviceDTO
                {
                    DeviceId = p.DeviceId,
                    IsPending = true
                }));

            return results;
        }

        /// <summary>
        /// Builds a DeviceInformationDTO object and returns it to the sender
        /// </summary>
        /// <param name="message">The message that was received from the Farm Manager</param>
        /// <returns></returns>
        private async Task<DeviceInformationDTO> RetrieveDeviceInformation(AskForDeviceInformation message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            FarmDevice? device = await dbContext.Devices
                .AsNoTracking()
                .Include("Registration")
                .Include("Methods")
                .Include("Interfaces.InterfaceType")
                .Where(d =>
                    d.DeviceId == message.DeviceId
                    && d.IsDeleted == false
                )
                .FirstOrDefaultAsync();

            if (device == null)
                return await BuildPendingDeviceInformation(message.DeviceId, dbContext);

            int cutoffAdjustment = message.HistoryMinutes * -1;
            var cutoff = DateTimeOffset.UtcNow.AddMinutes(cutoffAdjustment);

            //IEnumerable<ITelemetryInformation> telemetry = message.TelemetryGrouping == TelemetryGrouping.Time
            //    ? await GroupTelemetryByTime(cutoff, device, dbContext)
            //    : await GroupTelemetryByInterface(cutoff, device, dbContext);
            var telemetry = await GroupTelemetryByTime(cutoff, device, dbContext);
            var methods = FormatMethodData(device);

            return new DeviceInformationDTO
            {
                DeviceId = device.DeviceId,
                Name = device.Name,
                Model = device.Model,
                SerialNumber = device.SerialNumber,
                LastConnection = device.LastConnection,
                DeviceKey = device.Registration.DeviceKey,
                Interfaces = device.Interfaces
                    .Select(i => new InterfaceInformationDTO
                    {
                        Id = i.Id,
                        InterfaceId = i.InterfaceId,
                        Model = i.InterfaceType.Model
                    }).ToList(),
                Telemetry = telemetry.ToList(),
                Methods = methods
            };
        }

        /// <summary>
        /// Invokes a direct method on a remote device
        /// </summary>
        /// <param name="message">The message that was received from the Farm Manager</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private async Task<object> InvokeDeviceMethod(AskToInvokeMethod message) 
        {
            try
            {
                CloudToDeviceMethod invocation = new CloudToDeviceMethod(message.MethodName)
                {
                    ResponseTimeout = TimeSpan.FromSeconds(_iotSettings.MethodTimeoutSeconds)
                };

                string payload = ParseMessagePayload(message.Parameters);
                if (!string.IsNullOrEmpty(payload))
                    invocation.SetPayloadJson(payload);

                CloudToDeviceMethodResult response = await _serviceClient
                    .InvokeDeviceMethodAsync(message.DeviceId, invocation);

                int status = response.Status;
                string responsePayloadString = response.GetPayloadAsJson();
                object? responsePayload = JsonConvert.DeserializeObject(responsePayloadString);

                if (status == 200)
                    return responsePayload ?? responsePayloadString;

                else
                {
                    if (responsePayload == null)
                        throw new ApplicationException($"Unable to invoke method: Code {status}");

                    var responseObj = responsePayload as JObject;
                    throw new ApplicationException(responseObj?["Message"]?.ToString() ?? "Unable to invoke method");
                }
            }
            catch(ApplicationException ex)
            {
                // Log our exception, but re-throw so we get an appropriate ActorResponse error message
                _logger.LogError(ex.Message);
                throw;
            }
            catch(Exception ex)
            {
                // Ignore our re-thrown application exceptions and let HandleResult deal with them
                if (ex.GetType() == typeof(ApplicationException))
                    throw;

                try
                {
                    string reason = ParseDeviceInvokedMethodException(ex);
                    _logger.LogError(reason, data: message);
                    throw new Exception(reason);
                }
                catch (ApplicationException)
                {
                    _logger.LogWarning($"Failed parsing {nameof(InvokeDeviceMethod)} exception", ex.Message);
                    throw new Exception($"Failed parsing {nameof(InvokeDeviceMethod)} exception");
                }
            }
        }

        //////////////////////////////////////////
        //               Helpers                //
        //////////////////////////////////////////

        /// <summary>
        /// Validates that the device's definition has the correct properties
        /// prior to attempting initialization
        /// </summary>
        /// <param name="device">The device definition</param>
        private void ValidateDeviceData(FarmDevice device)
        {
            if (
                string.IsNullOrEmpty(device.DeviceId)
                || !device.Interfaces.Any()
            )
                throw new ApplicationException("Device is missing required information");

            foreach (Interface i in device.Interfaces)
            {
                if (
                    string.IsNullOrEmpty(i.InterfaceId)
                    || string.IsNullOrEmpty(i.InterfaceType.Model)
                    || string.IsNullOrEmpty(i.InterfaceType.Description)
                )
                    throw new ApplicationException("Interface is missing required information");
            }

            List<string>? methodNames = device.Methods
                ?.Select(m => m.Name)
                .ToList();

            int distinctMethodNames = methodNames?.Distinct().Count() ?? 0;

            if (
                methodNames != null
                && methodNames.Any()
                && distinctMethodNames != methodNames.Count()
            )
                throw new ApplicationException("All method names must be distinct");
        }

        /// <summary>
        /// Iterates through the device's interfaces, seeing if a 
        /// duplicate interface already exists within Farm Manager. If so,
        /// that is used, otherwise the interface type will be created during
        /// initialization
        /// </summary>
        /// <param name="dbContext">The database context to search for existing interface types</param>
        /// <param name="device">The device that's being initialized</param>
        private void UpdateInterfaceData(FarmManagerContext dbContext, ref FarmDevice device)
        {
            List<InterfaceType> cachedInterfaceTypes = new();
            for (int i = 0; i < device.Interfaces.Count; i++)
            {
                string currentInterfaceModel = device.Interfaces.ElementAt(i).InterfaceType.Model;
                string normalizedInterfaceModel = currentInterfaceModel.ToLower();

                // Check our cached values for an interface type first
                InterfaceType? currentDbInterfaceType = cachedInterfaceTypes
                    .Where(it => it.Model == normalizedInterfaceModel)
                    .FirstOrDefault();

                // If we don't have one, then check the database
                if (currentDbInterfaceType == null)
                    currentDbInterfaceType = dbContext.InterfaceTypes
                        .Where(it => it.Model == normalizedInterfaceModel)
                        .FirstOrDefault();

                // If we have an existing type, store it in our cached list and
                // update the device to use that instead of creating a new one
                if (currentDbInterfaceType != null)
                {
                    device.Interfaces.ElementAt(i).InterfaceTypeId = currentDbInterfaceType.Id;
                    device.Interfaces.ElementAt(i).InterfaceType = currentDbInterfaceType;

                    if (!cachedInterfaceTypes.Contains(currentDbInterfaceType))
                        cachedInterfaceTypes.Add(currentDbInterfaceType);
                }
            }
        }

        /// <summary>
        /// Returns whether the device is a valid Farm Manager device
        /// </summary>
        /// <param name="deviceId">The identifier of the device</param>
        /// <param name="deviceKey">The key for the device</param>
        /// <returns></returns>
        private async Task<bool> IsValidDevice(string deviceId, Guid? deviceKey = null)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            FarmDevice? device = await dbContext.Devices
                .Where(d => d.DeviceId == deviceId)
                .FirstOrDefaultAsync();

            if (device == null)
            {
                PendingDevice? pendingDevice = await dbContext.PendingDevices
                    .Where(pd =>
                        pd.DeviceId == deviceId
                        && pd.HasRegistered == false
                        && pd.DeviceKey == deviceKey
                    )
                    .FirstOrDefaultAsync();

                if (pendingDevice == null)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Retrieves information about a pending device
        /// </summary>
        /// <param name="deviceId">The id of the device that was requested</param>
        /// <param name="dbContext">A data context where the device record can be found</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<DeviceInformationDTO> BuildPendingDeviceInformation(
            string deviceId,
            FarmManagerContext dbContext
        )
        {
            PendingDevice? pending = await dbContext.PendingDevices
                .AsNoTracking()
                .Where(pd =>
                    pd.DeviceId == deviceId
                    && pd.HasRegistered == false
                    && pd.IsDeleted == false
                )
                .FirstOrDefaultAsync();

            if (pending == null)
                throw new ApplicationException("Device not found");

            return new DeviceInformationDTO
            {
                DeviceId = pending.DeviceId,
                DeviceKey = pending.DeviceKey
            };
        }

        /// <summary>
        /// Retrieves a devices telemetry, grouped by the timestamp of the telemetry
        /// </summary>
        /// <param name="cutoff">The earliest time for which telemetry should be returned</param>
        /// <param name="device">The device to retrieve telemetry for</param>
        /// <param name="dbContext">The data context to retrieve data from</param>
        /// <returns></returns>
        private async Task<IEnumerable<TimeGroupedTelemetry>> GroupTelemetryByTime(
            DateTimeOffset cutoff,
            FarmDevice device,
            FarmManagerContext dbContext
        )
        {
            List<int> interfaceIds = device.Interfaces
                .Select(i => i.Id)
                .ToList();

            var telemetry = (await dbContext.Telemetry
                .Where(t =>
                    t.TimeStamp > cutoff
                    && t.Value != null
                    && interfaceIds.Contains(t.InterfaceId)
                )
                .ToListAsync())
                .GroupBy(t => t.TimeStamp);

            List<TimeGroupedTelemetry> telemetryInfo = new();
            foreach (var time in telemetry)
            {
                var readings = new Dictionary<string, double>();
                foreach (var reading in time)
                {
                    var interfaceId = device.Interfaces
                        .Where(di => di.Id == reading.InterfaceId)
                        .Select(di => di.InterfaceId)
                        .FirstOrDefault();

                    readings.TryAdd(
                        $"{interfaceId} - {reading.Key}" ?? string.Empty,
                        reading.Value ?? 0
                    );
                }

                telemetryInfo.Add(new TimeGroupedTelemetry
                {
                    Timestamp = time.Key,
                    Readings = new Dictionary<string, double>(readings)
                });
            }

            return telemetryInfo;
        }

        /*
        /// <summary>
        /// Retrieves a devices telemetry, grouped by the interface of the device
        /// </summary>
        /// <param name="cutoff">The earliest time for which telemetry should be returned</param>
        /// <param name="device">The device to retrieve telemetry for</param>
        /// <param name="dbContext">The data context to retrieve data from</param>
        /// <returns></returns>
        private async Task<IEnumerable<ITelemetryInformation>> GroupTelemetryByInterface(
            DateTimeOffset cutoff,
            FarmDevice device,
            FarmManagerContext dbContext
        )
        {
            List<int> interfaceIds = device.Interfaces
                .Select(i => i.Id)
                .ToList();

            var telemetry = (await dbContext.Telemetry
                .Where(t =>
                    t.TimeStamp > cutoff
                    && interfaceIds.Contains(t.InterfaceId)
                )
                .ToListAsync())
                .GroupBy(t => t.InterfaceId);

            List<ITelemetryInformation> telemetryInfo = new();
            foreach (var interfaceGroup in telemetry)
            {
                var id = device.Interfaces
                    .Where(i => i.Id == interfaceGroup.Key)
                    .Select(i => i.InterfaceId)
                    .FirstOrDefault();

                var groupedTelemetryReadings = interfaceGroup
                    .Where(t => t.Value != null)
                    .GroupBy(t => t.Key);

                foreach (var keyGroup in groupedTelemetryReadings)
                {
                    var key = keyGroup.Key;
                    var keyReadings = keyGroup
                        .Select(t => new KeyValuePair<DateTimeOffset, double>(t.TimeStamp, t.Value ?? 0));

                    telemetryInfo.Add(new InterfaceGroupedTelemetry
                    {
                        InterfaceId = $"{id} - {key}",
                        Values = new Dictionary<DateTimeOffset, double>(keyReadings)
                    });
                }
            }

            return telemetryInfo;
        }
        */

        /// <summary>
        /// Formats the device's method data for easy consumption by 
        /// FarmManager.Web
        /// </summary>
        /// <param name="device">The device that needs it's method data formatted</param>
        /// <returns></returns>
        private List<MethodInformationDTO> FormatMethodData(FarmDevice device)
        {
            List<MethodInformationDTO> result = new();
            if(device.Methods != null && device.Methods.Any())
            {
                foreach(DeviceMethod method in device.Methods)
                {
                    MethodInformationDTO methodInfo = new()
                    {
                        Name = method.Name,
                        Description = method.Description
                    };

                    if (!string.IsNullOrEmpty(method.Parameters))
                    {
                        try
                        {
                            Dictionary<string, Type>? parameters = new();
                            Dictionary<string, string>? paramDict = JsonConvert
                                .DeserializeObject<Dictionary<string, string>>(method.Parameters);

                            if(paramDict != null)
                                foreach(var kvp in paramDict)
                                {
                                    Type? type = Type.GetType(kvp.Value);
                                    if(type != null)
                                        parameters.TryAdd(kvp.Key, type);
                                }

                            methodInfo.Parameters = parameters;
                        }
                        catch(Exception ex)
                        {
                            _logger.LogError("Error parsing method parameters", ex, device.Methods);
                        }
                    }

                    result.Add(methodInfo);
                }
            }

            return result;
        }

        /// <summary>
        /// Parses the direct method payload prior to sending
        /// the request
        /// </summary>
        /// <param name="payload">The payload being sent to the remote device</param>
        /// <returns></returns>
        private string ParseMessagePayload(object? payload)
        {
            string result = string.Empty;
            if (payload != null)
            {
                switch (payload)
                {
                    case string:
                        result = (string)payload;
                        break;

                    case int:
                        result = ((int)payload).ToString();
                        break;

                    case object:
                        result = JsonConvert.SerializeObject(payload);
                        break;

                    default:
                        _logger.LogWarning($"Uncaught device method payload type: {payload?.GetType().Name}");
                        break;
                }
            }

            return result;
        }

        /// <summary>
        /// Parses an error received from the IoT Hub if unable
        /// to execute a direct method
        /// </summary>
        /// <param name="exception">The exception received from the IoT Hub</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private string ParseDeviceInvokedMethodException(Exception exception)
        {
            var exMessageObj = JsonConvert.DeserializeObject<JObject>(exception.Message);
            if (exMessageObj == null)
                throw new ApplicationException("Unable to retrieve exception message");

            var exMessage = exMessageObj["Message"]?.ToString() ?? "";
            var errorObj = JsonConvert.DeserializeObject<JObject>(exMessage);

            if (errorObj == null)
                throw new ApplicationException("Unable to retrieve error object");

            return errorObj["message"]?.ToString() ?? "";
        }
    }
}
