﻿using Akka.Actor;
using Akka.DependencyInjection;
using Microsoft.Azure.Devices;

namespace FarmManager.Core.Actors.Devices
{
    /// <summary>
    /// The actor responsible for managing all device-related events.
    /// It manages the workers which will handle device logic
    /// </summary>
    public class DeviceManager : FarmManagerActor<DeviceManager>
    {
        private readonly RegistryManager _registryManager;
        private readonly ServiceClient _serviceClient;

        private IActorRef? _worker;

        public DeviceManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _registryManager = RegistryManager
                .CreateFromConnectionString(_settings?.IotHub?.ConnectionString);
            _serviceClient = ServiceClient
                .CreateFromConnectionString(_settings?.IotHub?.ConnectionString);

            ReceiveAny(ForwardMessageToWorker);
        }

        protected override void PreStart()
        {
            Props workerProps = DependencyResolver
                .For(Context.System)
                .Props<DeviceWorker>(_registryManager, _serviceClient);

            _worker = Context.ActorOf(workerProps, nameof(DeviceWorker));
        }

        protected override void PostStop()
        {
            _registryManager?.Dispose();

            base.PostStop();
        }

        /// <summary>
        /// Forwards the message to a worker
        /// </summary>
        /// <param name="message">The message to foward</param>
        private void ForwardMessageToWorker(object message)
        {
            IUntypedActorContext context = Context;
            IActorRef sender = Sender;
            TimeSpan timeout = TimeSpan.FromSeconds(_settings.ActorWaitSeconds);

            _worker?.Ask(message, timeout)
                .ContinueWith(result => sender.Tell(result.Result));
        }
    }
}
