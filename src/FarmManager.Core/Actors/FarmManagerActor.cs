﻿using Akka.Actor;
using Akka.DependencyInjection;
using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages;
using FarmManager.Data.DTOs;
using FarmManager.Services.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace FarmManager.Core.Actors
{
    /// <summary>
    /// A base actor with common properties and functions that are
    /// utilized other Farm Manager actors. All Farm Manager actors 
    /// sould inherit this.
    /// </summary>
    public class FarmManagerActor<A> : ReceiveActor
    {
        protected readonly IServiceScope _scope;
        protected readonly IActorRef _farmManager;
        protected readonly IFarmLogger _logger;
        protected readonly AppSettings _settings;

        public FarmManagerActor(IServiceProvider serviceProvider)
        {
            ActorRegistry registry = ActorRegistry.For(Context.System);
            _farmManager = registry.Get<FarmManager>();

            _scope = serviceProvider.CreateScope();

            _settings = _scope.ServiceProvider
                .GetRequiredService<IOptions<AppSettings>>().Value;

            _logger = _scope.ServiceProvider
                .GetRequiredService<IFarmLogger>();
            _logger.Initialize<A>();
        }

        protected override void PostStop()
        {
            _scope.Dispose();
        }

        /// <summary>
        /// Creates a short-lived instance of an actor T to handle a given message 
        /// </summary>
        /// <typeparam name="T">The type of actor that should be created to handle the message</typeparam>
        /// <param name="message">The message to be handled</param>
        /// <param name="sender">The sender who should be notified upon task completion</param>
        /// <param name="context">The actor context</param>
        /// <param name="parameters">Any parameters needed to create an instance of the specified actor</param>
        /// <returns></returns>
        protected void HandleWithInstanceOfAsync<T>(
            object message,
            IActorRef sender,
            IUntypedActorContext context,
            params object[]? parameters
        ) where T : ReceiveActor
        {
            string actorName = $"{typeof(T).Name}-{Guid.NewGuid()}";
            TimeSpan timeout = TimeSpan.FromSeconds(_settings.ActorWaitSeconds);

            Props props = DependencyResolver.For(context.System).Props<T>(parameters);
            IActorRef actorRef = context.ActorOf(props, actorName);

            actorRef.Ask(message, timeout)
                .ContinueWith(result =>
                {
                    actorRef.Tell(PoisonPill.Instance);
                    HandleResult(result, sender);
                });
        }

        /// <summary>
        /// Creates a short-lived instance of an actor T to handle a given message 
        /// </summary>
        /// <typeparam name="T">The type of actor that should be created to handle the message</typeparam>
        /// <param name="message">The message to be handled</param>
        /// <param name="sender">The sender who should be notified upon task completion</param>
        /// <param name="context">The actor context</param>
        /// <param name="parameters">Any parameters needed to create an instance of the specified actor</param>
        /// <returns></returns>
        protected async Task HandleWithInstanceOf<T>(
            object message,
            IActorRef sender,
            IUntypedActorContext context,
            params object[]? parameters
        ) where T : ReceiveActor
        {
            string actorName = $"{typeof(T).Name}-{Guid.NewGuid()}";
            TimeSpan timeout = TimeSpan.FromSeconds(_settings.ActorWaitSeconds);

            Props props = DependencyResolver.For(context.System).Props<T>(parameters);
            IActorRef actorRef = context.ActorOf(props, actorName);

            object result = await actorRef.Ask(message, timeout);
            actorRef.Tell(PoisonPill.Instance);
            sender.Tell(result);
        }

        /// <summary>
        /// A default handler for synchronous message handling logic
        /// </summary>
        /// <typeparam name="T">The message type that is received by the handler</typeparam>
        /// <typeparam name="O">The return type of the handler</typeparam>
        /// <param name="func">The function that should be used to handle the message</param>
        /// <param name="message">The message received by an actor</param>
        protected void HandleWith<T, O>(Func<T, O> func, T message)
        {
            try
            {
                O result = func(message);
                Sender.Tell(ActorResponse.Success(result));
            }
            catch(Exception ex)
            {
                Sender.Tell(ActorResponse.Failure(ex.Message));
            }
        }

        /// <summary>
        /// Handles a message asyncronously with a given function
        /// </summary>
        /// <typeparam name="T">The message type that should be handled</typeparam>
        /// <typeparam name="O">The output type of the function handling the message</typeparam>
        /// <param name="func">The function that should be used to handle the message</param>
        /// <param name="message">The message received by an actor</param>
        protected void HandleWithAsync<T, O>(Func<T, Task<O>> func, T message)
            where T : IFarmManagerMessage
        {
            IActorRef sender = Sender;

            func(message).ContinueWith(result =>
                HandleResult(result, sender));
        }

        /// <summary>
        /// Handles a message asyncronously with a given function
        /// </summary>
        /// <typeparam name="T">The message type that should be handled</typeparam>
        /// <param name="func">The function that should be used to handle the message</param>
        /// <param name="message">The message received by an actor</param>
        protected void HandleWithAsync<T>(Func<T, Task> func, T message)
        {
            IActorRef sender = Sender;

            func(message).ContinueWith(result =>
                HandleResult(result, sender));
        }

        /// <summary>
        /// Common logic for handling the result of a request
        /// </summary>
        /// <typeparam name="T">The output type of the handler</typeparam>
        /// <param name="result">The result of the handler</param>
        /// <param name="sender">The sender that the result should be sent to</param>
        protected void HandleResult<T>(
            Task<T> result,
            IActorRef sender
        )
        {
            FarmManagerResponse? managerResult = result.IsCompletedSuccessfully
                ? result.Result as FarmManagerResponse
                : null;

            if (managerResult != null)
                sender.Tell(managerResult);

            else if (result.Exception != null)
            {
                Exception ex = HandleException(result.Exception);
                sender.Tell(ActorResponse.Failure(ex.Message));
            }

            else
                sender.Tell(ActorResponse.Success(result.Result));
        }

        /// <summary>
        /// Common logic for handling the result of a request
        /// </summary>
        /// <param name="result">The result of the handler</param>
        /// <param name="sender">The sender that the result should be sent to</param>
        protected void HandleResult(Task result, IActorRef sender)
        {
            if (result.Exception != null)
            {
                Exception ex = HandleException(result.Exception);
                sender.Tell(ActorResponse.Failure(ex.Message));
            }
            else
                sender.Tell(ActorResponse.Success(null));
        }

        /// <summary>
        /// Common logic for dealing with an exception from a message handler
        /// </summary>
        /// <param name="ex">The exception that was received</param>
        /// <returns></returns>
        private Exception HandleException(Exception ex)
        {
            Exception innermostException = ex;
            while(innermostException.InnerException != null)
                innermostException = innermostException.InnerException;

            _logger.LogError(innermostException.Message, innermostException);
            return innermostException;
        }
    }
}
