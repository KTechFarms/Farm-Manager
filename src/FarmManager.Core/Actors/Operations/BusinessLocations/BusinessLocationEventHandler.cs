﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Actors.Operations.BusinessLocations
{
    /// <summary>
    /// Responsible for subscribing to and handling business location related events
    /// that come from the message broker
    /// </summary>
    public class BusinessLocationEventHandler : FarmManagerActor<BusinessLocationEventHandler>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public BusinessLocationEventHandler(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<EventReceived>(UpdateExternalId);
        }

        protected override void PreStart()
        {
            _farmManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Location.Update, Self));
        }

        protected override void PostStop()
        {
            _farmManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Location.Update, Self));

            base.PostStop();
        }

        /// <summary>
        /// Updates the ExternalId of a business location
        /// </summary>
        /// <param name="message">The message received from the message broker</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ApplicationException"></exception>
        private void UpdateExternalId(EventReceived message)
        {
            try
            {
                if (message.Data == null)
                    throw new ArgumentException("Data missing");

                string dataString = JsonConvert.SerializeObject(message.Data);
                BusinessLocation? location = JsonConvert.DeserializeObject<BusinessLocation>(dataString);

                if (location == null)
                    throw new ApplicationException("No business location provided in event");

                using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

                BusinessLocation? dbLocation = dbContext.BusinessLocations
                    .FirstOrDefault(l => l.Id == location.Id);

                if (dbLocation != null)
                {
                    dbLocation.ExternalId = location.ExternalId;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating location's external Id", ex, message);
            }
        }
    }
}
