﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.ActorMessages.Operations.BusinessLocations;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using Address = FarmManager.Data.Entities.Generic.Address;

namespace FarmManager.Core.Actors.Operations.BusinessLocations
{
    /// <summary>
    /// The worker responsible for performing any business location logic
    /// </summary>
    public class BusinessLocationWorker : FarmManagerActor<BusinessLocationWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;
        private IMessageBrokerPublisher? _locationPublisher;

        private readonly string _locationQueue;

        public BusinessLocationWorker(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            _locationQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcesing)
                ?.QueueName ?? string.Empty;

            Receive<AskForBusinessLocations>(message => HandleWithAsync(RetrieveBusinessLocations, message));
            Receive<AskToCreateBusinessLocation>(message => HandleWith(CreateBusinessLocation, message));
            Receive<AskToDeleteBusinessLocation>(message => HandleWith(DeleteBusinessLocation, message));

        }

        protected override void PreStart()
        {
            _locationPublisher = _farmManager.Ask(
                new AskForMessageBrokerPublisher(
                    _locationQueue,
                    $"{Self}"
            )).Result as IMessageBrokerPublisher;
        }

        protected override void PostStop()
        {
            _locationPublisher?.Dispose();
            base.PostStop();
        }

        /// <summary>
        /// Retrieves a list of business locations
        /// </summary>
        /// <param name="message">A request for business locations</param>
        /// <returns></returns>
        private async Task<IEnumerable<BusinessLocation>> RetrieveBusinessLocations(AskForBusinessLocations message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            return await dbContext.BusinessLocations
                .AsNoTracking()
                .Include(nameof(BusinessLocation.Address))
                .Where(l => l.IsDeleted == false)
                .ToListAsync();
        }

        /// <summary>
        /// Creates a new business location, and publishes the location to
        /// the payment processor
        /// </summary>
        /// <param name="message">A message containing the location to createe</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private BusinessLocation CreateBusinessLocation(AskToCreateBusinessLocation message)
        {
            if (
                message.Location == null 
                || message.Location.Address == null
                || string.IsNullOrEmpty(message.Location.Name)
                || string.IsNullOrEmpty(message.Location.Address.State)
                || string.IsNullOrEmpty(message.Location.Address.Street)
                || string.IsNullOrEmpty(message.Location.Address.City)
                || string.IsNullOrEmpty(message.Location.Address.ZipCode)
            )
                throw new ApplicationException("Incomplete location information");

            if (
                !int.TryParse(message.Location.Address.ZipCode, out var zipDigits)
                || message.Location.Address.ZipCode.Length < 5
                || message.Location.Address.ZipCode.Length > 9
            )
                throw new ApplicationException("Invalid zipcode format");

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            BusinessLocation? existingLocation = dbContext.BusinessLocations
                .AsNoTracking()
                .Where(l => l.Name == message.Location.Name)
                .FirstOrDefault();

            if (existingLocation != null)
                throw new ApplicationException("Location already exists with the requested name");

            Address? existingAddress = dbContext.Addresses
                .AsNoTracking()
                .Where(a =>
                    a.State == message.Location.Address.State
                    && a.Street == message.Location.Address.Street
                    && a.City == message.Location.Address.City
                    && a.ZipCode == message.Location.Address.ZipCode
                )
                .FirstOrDefault();

            Address tempAddress = existingAddress ?? message.Location.Address;
            if(existingAddress != null)
            {
                message.Location.AddressId = existingAddress.Id;
                message.Location.Address = null;
            }

            BusinessLocation newLocation = dbContext
                .Add(message.Location).Entity;

            FarmLocation? unknownLocation = dbContext.FarmLocations
                .AsNoTracking()
                .Where(l => l.LocationType.Name == "unknown")
                .FirstOrDefault();

            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    // Save to get the Business Location Id
                    dbContext.SaveChanges();

                    // Add an unkown location if it doesn't exist
                    if (unknownLocation == null)
                    {
                        int unknownType = dbContext.Enums
                            .AsNoTracking()
                            .Where(e => e.Name == "unknown")
                            .Select(e => e.Id)
                            .FirstOrDefault();

                        dbContext.Add(new FarmLocation
                        {
                            BusinessLocationId = newLocation.Id,
                            Name = "Unknown",
                            LocationTypeId = unknownType
                        });

                        dbContext.SaveChanges();
                    }

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw new ApplicationException("Error creating the location");
                }
            }

            if (_locationPublisher != null)
            {
                MessageBrokerMessage mbMessage = new();
                try
                {
                    LocationCreation locationCreation = new()
                    {
                        Id = message.Location.Id,
                        Name = message.Location.Name,
                        Street = tempAddress.Street,
                        City = tempAddress.City,
                        State = tempAddress.State,
                        ZipCode = tempAddress.ZipCode,
                    };

                    mbMessage = new()
                    {
                        Source = typeof(BusinessLocationWorker).AssemblyQualifiedName,
                        MessageType = Events.PaymentProcessing.Location.Create,
                        Assembly = typeof(LocationCreation).Assembly.FullName,
                        Class = nameof(LocationCreation),
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = locationCreation
                    };

                    _locationPublisher.PublishMessageAsync(mbMessage).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish location creation", ex, mbMessage);
                }
            }

            return newLocation;
        }

        /// <summary>
        /// Deletes a location based on a provided Id and publishes the
        /// deletion to the payment processing platform
        /// </summary>
        /// <param name="message">A message containing the location id
        /// to delete</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private int DeleteBusinessLocation(AskToDeleteBusinessLocation message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            BusinessLocation? dbLocation = dbContext.BusinessLocations
                .FirstOrDefault(l => l.Id == message.Id);

            if (dbLocation == null)
                throw new ApplicationException("Location not found");

            // Make sure the business location isn't tied to any products or history
            List<int> farmLocationIds = dbContext.FarmLocations
                .AsNoTracking()
                .Where(l => l.BusinessLocationId == dbLocation.Id)
                .Select(l => l.Id)
                .ToList();

            ProductionItem? locationProduct = dbContext.ProductionItems
                .AsNoTracking()
                .Where(i =>
                    (i.LocationId != null
                        && farmLocationIds.Contains((int)i.LocationId))
                    || i.LocationHistory.Any(h =>
                        farmLocationIds.Contains(h.FarmLocationId)
                ))
                .FirstOrDefault();

            Lot? locationLot = dbContext.Lots
                .AsNoTracking()
                .Where(l => 
                    farmLocationIds.Contains(l.FarmLocationId)
                    || (
                        l.Crop != null
                        && l.Crop.TransplantLocationId != null
                        && farmLocationIds.Contains((int)l.Crop.TransplantLocationId)
                    )
                )
                .FirstOrDefault();

            Batch? locationBatch = dbContext.Batches
                .AsNoTracking()
                .Where(b => farmLocationIds.Contains(b.FarmLocationId))
                .FirstOrDefault();

            if (locationProduct != null || locationLot != null || locationBatch != null)
                throw new ApplicationException("Location has been used and cannot be deleted");

            dbContext.Remove(dbLocation);
            dbContext.SaveChanges();

            if (_locationPublisher != null)
            {
                MessageBrokerMessage mbMessage = new();
                try
                {
                    mbMessage = new()
                    {
                        Source = typeof(BusinessLocationWorker).AssemblyQualifiedName,
                        MessageType = Events.PaymentProcessing.Location.Delete,
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = dbLocation.ExternalId
                    };

                    _locationPublisher.PublishMessageAsync(mbMessage).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish location deletion", ex, mbMessage);
                }
            }

            return dbLocation.Id;
        }
    }
}
