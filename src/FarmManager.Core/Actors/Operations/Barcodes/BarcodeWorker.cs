﻿using FarmManager.Data.ActorMessages.Operations.Barcodes;
using FarmManager.Data.Contexts;
using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Actors.Operations.Barcodes
{
    /// <summary>
    /// The worker responsible for handling all barcode requests
    /// </summary>
    public class BarcodeWorker : FarmManagerActor<BarcodeWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public BarcodeWorker(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<AskForBarcodeProduct>(message => HandleWithAsync(SearchForBarcodeProduct, message));
        }

        /// <summary>
        /// Searches for an product's inventory item based on the provided barcode
        /// </summary>
        /// <param name="message">The message requesting a barcode's information</param>
        /// <returns></returns>
        private async Task<InventoryItem> SearchForBarcodeProduct(AskForBarcodeProduct message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();
            
            InventoryItem result = new();

            ProductVariant? variant = await dbContext.VariantBarcodes
                .AsNoTracking()
                .Where(vb => vb.Barcode == message.Barcode)
                .Select(vb => vb.ProductVariant)
                .FirstOrDefaultAsync();

            if(variant != null)
            {
                Product? product = await dbContext.Products
                    .AsNoTracking()
                    .Where(p => p.Id == variant.ProductId)
                    .FirstOrDefaultAsync();

                result.Barcode = new Barcode
                {
                    RawBarcode = message.Barcode,
                    UserFriendlyBarcode = message.Barcode,
                    AIs = new() { }
                };
                result.Product = product?.Name ?? string.Empty;
                result.VariantId = variant.Id;
                result.Variant = variant.Name;
                result.Quantity = 1;
                result.LotNumber = string.Empty;
            }

            return result;
        }
    }
}
