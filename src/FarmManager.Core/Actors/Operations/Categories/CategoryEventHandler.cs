﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Actors.Operations.Categories
{
    /// <summary>
    /// An event handler that's responsible for subscribing to category
    /// related events that come from the message broker
    /// </summary>
    public class CategoryEventHandler : FarmManagerActor<CategoryEventHandler>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public CategoryEventHandler(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<EventReceived>(UpdateExternalId);
        }

        protected override void PreStart()
        {
            _farmManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Category.Update, Self));
        }

        protected override void PostStop()
        {
            _farmManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Category.Update, Self));

            base.PostStop();
        }

        /// <summary>
        /// Updates the external Id of a product category
        /// </summary>
        /// <param name="message">The message received from the message broker</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ApplicationException"></exception>
        private void UpdateExternalId(EventReceived message)
        {
            try
            {
                if (message.Data == null)
                    throw new ArgumentException("Data Missing");

                string dataString = JsonConvert.SerializeObject(message.Data);
                ProductCategory? category = JsonConvert.DeserializeObject<ProductCategory>(dataString);

                if (category == null)
                    throw new ApplicationException("No category provided in event");

                using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

                ProductCategory? dbCategory = dbContext.ProductCategories
                    .FirstOrDefault(c => c.Id == category.Id);

                if (dbCategory != null)
                {
                    dbCategory.ExternalId = category.ExternalId;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating category's external Id", ex, message);
            }
        }
    }
}
