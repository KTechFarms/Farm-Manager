﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.ActorMessages.Operations.Categories;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Actors.Operations.Categories
{
    /// <summary>
    /// Responsible for handling all product category requests
    /// </summary>
    public class CategoryWorker : FarmManagerActor<CategoryWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;
        private IMessageBrokerPublisher? _categoryPublisher;

        private readonly string _categoryQueue;

        public CategoryWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            _categoryQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcesing)
                ?.QueueName ?? string.Empty;

            Receive<AskForCategories>(message => HandleWithAsync(RetrieveCategories, message));
            Receive<AskToCreateCategory>(message => HandleWith(CreateCategory, message));
            Receive<AskToDeleteCategory>(message => HandleWith(DeleteCategory, message));
        }

        protected override void PreStart()
        {
            _categoryPublisher = _farmManager.Ask(
                new AskForMessageBrokerPublisher(
                    _categoryQueue,
                    $"{Self}"
            )).Result as IMessageBrokerPublisher;
        }

        protected override void PostStop()
        {
            _categoryPublisher?.Dispose();
            base.PostStop();
        }

        /// <summary>
        /// Retrieves a list of product categories
        /// </summary>
        /// <param name="message">Message asking for categories</param>
        /// <returns></returns>
        private async Task<IEnumerable<ProductCategory>> RetrieveCategories(AskForCategories message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            return await dbContext.ProductCategories
                .AsNoTracking()
                .Where(c => c.IsDeleted == false)
                .ToListAsync();
        }

        /// <summary>
        /// Creates a new product category
        /// </summary>
        /// <param name="message">A message containing the category to create</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private ProductCategory CreateCategory(AskToCreateCategory message)
        {
            if (string.IsNullOrEmpty(message.Category.Name))
                throw new ApplicationException("Category must have a name");

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            ProductCategory? existingCategory = dbContext.ProductCategories
                .AsNoTracking()
                .Where(c => c.Name == message.Category.Name)
                .FirstOrDefault();

            if (existingCategory != null)
                throw new ApplicationException("Cannot create a duplicate category");

            ProductCategory newCategory = dbContext
                .Add(message.Category).Entity;
            dbContext.SaveChanges();

            if(_categoryPublisher != null)
            {
                MessageBrokerMessage mbMessage = new();
                try
                {
                    CategoryCreation categoryCreation = new()
                    {
                        Id = newCategory.Id,
                        Name = newCategory.Name
                    };

                    mbMessage = new()
                    {
                        Source = typeof(CategoryWorker).AssemblyQualifiedName,
                        MessageType = Events.PaymentProcessing.Category.Create,
                        Assembly = typeof(CategoryCreation).Assembly.FullName,
                        Class = nameof(CategoryCreation),
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = categoryCreation
                    };

                    _categoryPublisher.PublishMessageAsync(mbMessage).Wait();
                }
                catch(Exception ex)
                {
                    _logger.LogError("Unable to publish category creation", ex, mbMessage);
                }
            }

            return newCategory;
        }

        /// <summary>
        /// Deletes a product category
        /// </summary>
        /// <param name="message">A message containing the id of the
        /// product category to delete</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private int DeleteCategory(AskToDeleteCategory message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            ProductCategory? dbCategory = dbContext.ProductCategories
                .FirstOrDefault(c => c.Id == message.Id);

            if (dbCategory == null)
                throw new ApplicationException("Category not found");

            // Don't allow deleting the produce or seed categories, since those are added
            // during the initial migraiton and used to limit the available options when creating lots
            if (dbCategory.Name == "Produce" || dbCategory.Name == "Seed")
                throw new ApplicationException("Category is required and cannot be deleted");

            // Make sure the category isn't tied to any product
            Product? categoryProduct = dbContext.Products
                .AsNoTracking()
                .Where(p => p.CategoryId == dbCategory.Id)
                .FirstOrDefault();

            if (categoryProduct != null)
                throw new ApplicationException("Category is in use and cannot be deleted");

            dbContext.Remove(dbCategory);
            dbContext.SaveChanges();

            if (_categoryPublisher!= null && !string.IsNullOrEmpty(dbCategory.ExternalId))
            {
                MessageBrokerMessage mbMessage = new();
                try
                {
                    mbMessage = new()
                    {
                        Source = typeof(CategoryWorker).AssemblyQualifiedName,
                        MessageType = Events.PaymentProcessing.Category.Delete,
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = dbCategory.ExternalId
                    };

                    _categoryPublisher.PublishMessageAsync(mbMessage).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish category creation", ex, mbMessage);
                }
            }

            return dbCategory.Id;
        }
    }
}
