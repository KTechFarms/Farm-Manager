﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.ActorMessages.Operations.Units;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Actors.Operations.Units
{
    /// <summary>
    /// The worker responsible for handling unit related logic
    /// </summary>
    public class UnitWorker : FarmManagerActor<UnitWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;
        private IMessageBrokerPublisher? _unitPublisher;

        private readonly string _unitQueue;

        public UnitWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            _unitQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcesing)
                ?.QueueName ?? string.Empty;

            Receive<AskForUnits>(message => HandleWithAsync(RetrieveUnits, message));
            Receive<AskToCreateUnit>(message => HandleWith(CreateUnit, message));
            Receive<AskToDeleteUnit>(message => HandleWith(DeleteUnit, message));
        }

        protected override void PreStart()
        {
            _unitPublisher = _farmManager.Ask(
                new AskForMessageBrokerPublisher(
                    _unitQueue,
                    $"{Self}"
                )).Result as IMessageBrokerPublisher;
        }

        protected override void PostStop()
        {
            _unitPublisher?.Dispose();
            base.PostStop();
        }

        /// <summary>
        /// Retrieves a list of units
        /// </summary>
        /// <param name="messsage">A message requesting a list of units</param>
        /// <returns></returns>
        private async Task<IEnumerable<Unit>> RetrieveUnits(AskForUnits messsage)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            return await dbContext.Units
                .AsNoTracking()
                .Where(u => u.IsDeleted == false)
                .ToListAsync();
        }


        /// <summary>
        /// Creates a new unit and publishes the unit to the payment processor
        /// </summary>
        /// <param name="message">A message containing the unit to create</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private Unit CreateUnit(AskToCreateUnit message)
        {
            if (
                string.IsNullOrEmpty(message.Unit.Name)
                || string.IsNullOrEmpty(message.Unit.Symbol)
            )
                throw new ApplicationException("Unit name and symbol are required");

            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Unit? existingUnit = dbContext.Units
                .AsNoTracking()
                .Where(u =>
                    (u.Name == message.Unit.Name
                        || u.Symbol == message.Unit.Symbol)
                    && u.Precision == message.Unit.Precision
                )
                .FirstOrDefault();

            if (existingUnit != null)
                throw new ApplicationException("Cannot create a duplicate unit");

            Unit newUnit = dbContext
                .Add(message.Unit).Entity;
            dbContext.SaveChanges();

            // Publish the unit to payment processor
            if (_unitPublisher != null)
            {
                MessageBrokerMessage mbMessage = new();
                try
                {
                    UnitCreation unitCreation = new()
                    {
                        Id = newUnit.Id,
                        Name = newUnit.Name,
                        Symbol = newUnit.Symbol,
                        Precision = newUnit.Precision
                    };

                    mbMessage = new()
                    {
                        Source = typeof(UnitWorker).AssemblyQualifiedName,
                        MessageType = Events.PaymentProcessing.Unit.Create,
                        Assembly = typeof(UnitCreation).Assembly.FullName,
                        Class = nameof(UnitCreation),
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = unitCreation
                    };

                    _unitPublisher.PublishMessageAsync(mbMessage).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish unit creation", ex, mbMessage);
                }
            }

            return newUnit;
        }

        /// <summary>
        /// Deletes a unit and publishes the deletion too the payment processor
        /// </summary>
        /// <param name="message">A message containing the Id of the unit
        /// to delete</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private int DeleteUnit(AskToDeleteUnit message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Unit? dbUnit = dbContext.Units
                .FirstOrDefault(u => u.Id == message.Id);

            if (dbUnit == null)
                throw new ApplicationException("Unit not found");

            ProductVariant? variant = dbContext.ProductVariants
                .AsNoTracking()
                .Where(v => v.MeasurementUnitId == dbUnit.Id)
                .FirstOrDefault();

            if (variant != null)
                throw new ApplicationException("Unit is in use and cannot be deleted");

            dbContext.Remove(dbUnit);
            dbContext.SaveChanges();

            // Publish the unit to payment processor
            if (_unitPublisher != null && !string.IsNullOrEmpty(dbUnit.ExternalId))
            {
                MessageBrokerMessage mbMessage = new();
                try
                {
                    mbMessage = new()
                    {
                        Source = typeof(UnitWorker).AssemblyQualifiedName,
                        MessageType = Events.PaymentProcessing.Unit.Delete,
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = dbUnit.ExternalId
                    };

                    _unitPublisher.PublishMessageAsync(mbMessage).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish unit deletion", ex, mbMessage);
                }
            }

            return dbUnit.Id;
        }
    }
}
