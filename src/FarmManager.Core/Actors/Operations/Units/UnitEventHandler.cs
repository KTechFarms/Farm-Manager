﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Actors.Operations.Units
{
    /// <summary>
    /// A handler responsible for handling unit events from the message broker
    /// </summary>
    public class UnitEventHandler : FarmManagerActor<UnitEventHandler>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public UnitEventHandler(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<EventReceived>(UpdateExternalId);
        }

        protected override void PreStart()
        {
            _farmManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Unit.Update, Self));
        }

        protected override void PostStop()
        {
            _farmManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Unit.Update, Self));

            base.PostStop();
        }

        /// <summary>
        /// Updates the ExternalId of a unit
        /// </summary>
        /// <param name="message">The event from the message broker containing
        /// the unit information that should be updated</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ApplicationException"></exception>
        private void UpdateExternalId(EventReceived message)
        {
            try
            {
                if (message.Data == null)
                    throw new ArgumentNullException("Data missing");

                string dataString = JsonConvert.SerializeObject(message.Data);
                Unit? unit = JsonConvert.DeserializeObject<Unit>(dataString);

                if (unit == null)
                    throw new ApplicationException("No unit provided in event");

                using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

                Unit? dbUnit = dbContext.Units
                    .FirstOrDefault(u => u.Id == unit.Id);

                if (dbUnit != null)
                {
                    dbUnit.ExternalId = unit.ExternalId;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating unit's external Id", ex, message);
            }
        }
    }
}
