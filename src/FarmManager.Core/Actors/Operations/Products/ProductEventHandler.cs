﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Actors.Operations.Products
{
    /// <summary>
    /// An event handler for product related events coming from
    /// the message broker
    /// </summary>
    public class ProductEventHandler : FarmManagerActor<ProductEventHandler>
    {
        public readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public ProductEventHandler(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<EventReceived>(HandleIncomingEvent);
        }

        protected override void PreStart()
        {
            _farmManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Product.Update, Self));
            _farmManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.ProductVariant.Update, Self));
        }

        protected override void PostStop()
        {
            _farmManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Product.Update, Self));
            _farmManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.ProductVariant.Update, Self));
            base.PostStop();
        }

        /// <summary>
        /// Receives an event from the message broker and routes it
        /// to the correct handler
        /// </summary>
        /// <param name="message">The message from the message broker</param>
        private void HandleIncomingEvent(EventReceived message)
        {
            switch(message.MessageType)
            {
                case Events.PaymentProcessing.Product.Update:
                    UpdateProductExternalId(message);
                    break;

                case Events.PaymentProcessing.ProductVariant.Update:
                    UpdateVariantExternalId(message);
                    break;

                default:
                    _logger.LogWarning("Unhandled payment processing event", message);
                    break;
            }
        }

        /// <summary>
        /// Updates a product's ExternalId
        /// </summary>
        /// <param name="message">The message containing the product
        /// information that needs to be updated</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ApplicationException"></exception>
        private void UpdateProductExternalId(EventReceived message)
        {
            try
            {
                if (message.Data == null)
                    throw new ArgumentNullException("Data missing");

                string dataString = JsonConvert.SerializeObject(message.Data);
                Product? product = JsonConvert.DeserializeObject<Product?>(dataString);

                if (product == null)
                    throw new ApplicationException("No product provided in event");

                using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

                Product? dbProduct = dbContext.Products
                    .Include(p => p.Variants)
                    .FirstOrDefault(p => p.Id == product.Id);

                if(dbProduct != null)
                {
                    dbProduct.ExternalId = product.ExternalId;
                    foreach(ProductVariant dbVariant in dbProduct.Variants ?? new List<ProductVariant>())
                    {
                        ProductVariant? variant = product.Variants?
                            .FirstOrDefault(v => v.Id == dbVariant.Id);

                        if (variant != null && string.IsNullOrEmpty(dbVariant.ExternalId))
                            dbVariant.ExternalId = variant.ExternalId;
                    }

                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating product's external Id", ex, message);
            }
        }

        /// <summary>
        /// Updates a product variant's ExternalId
        /// </summary>
        /// <param name="message">A message containing the product
        /// variant information to update</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ApplicationException"></exception>
        private void UpdateVariantExternalId(EventReceived message)
        {
            try
            {
                if (message.Data == null)
                    throw new ArgumentNullException("Data missing");

                string dataString = JsonConvert.SerializeObject(message.Data);
                ProductVariant? variant = JsonConvert.DeserializeObject<ProductVariant?>(dataString);

                if (variant == null)
                    throw new ApplicationException("No product provided in event");

                using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

                ProductVariant? dbVariant = dbContext.ProductVariants
                    .FirstOrDefault(p => p.Id == variant.Id);

                if (dbVariant != null)
                {
                    dbVariant.ExternalId = variant.ExternalId;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating product variant's external Id", ex, message);
            }
        }
    }
}
