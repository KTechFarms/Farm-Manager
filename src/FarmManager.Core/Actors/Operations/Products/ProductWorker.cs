﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.ActorMessages.Operations.Products;
using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Actors.Operations.Products
{
    /// <summary>
    /// The worker responsible for all product related logic
    /// </summary>
    public class ProductWorker : FarmManagerActor<ProductWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;
        private readonly string _productQueue;
        private IMessageBrokerPublisher? _productPublisher;

        public ProductWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            _productQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcesing)
                ?.QueueName ?? string.Empty;

            Receive<AskForProducts>(message => HandleWithAsync(RetrieveProducts, message));
            Receive<AskToCreateProduct>(message => HandleWithAsync(CreateProduct, message));
            Receive<AskToUpdateProduct>(message => HandleWithAsync(UpdateProduct, message));
            Receive<AskToDeleteProduct>(message => HandleWithAsync(DeleteProduct, message));
        }

        protected override void PreStart()
        {
            _productPublisher = _farmManager.Ask(
                new AskForMessageBrokerPublisher(
                    _productQueue,
                    $"{Self}"
                )).Result as IMessageBrokerPublisher;
        }

        protected override void PostStop()
        {
            _productPublisher?.Dispose();
            base.PostStop();
        }

        /// <summary>
        /// Retreives a list of products
        /// </summary>
        /// <param name="message">A message asking for the products</param>
        /// <returns></returns>
        private async Task<List<Product>> RetrieveProducts(AskForProducts message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            return await dbContext.Products
                .AsNoTracking()
                .Include("Category")
                .Include("Variants.MeasurementUnit")
                .ToListAsync();
        }

        /// <summary>
        /// Creates a new product
        /// </summary>
        /// <param name="message">A message containing the product to create</param>
        /// <returns></returns>
        private async Task<Product> CreateProduct(AskToCreateProduct message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            await ValidateProduct(dbContext, message.Product);
            Product newProduct = (await dbContext.AddAsync(message.Product)).Entity;
            await dbContext.SaveChangesAsync();

            if(_productPublisher != null)
            {
                MessageBrokerMessage mbMessage = new();
                try
                {
                    List<int> unitIds = newProduct.Variants
                        .Select(v => v.MeasurementUnitId)
                        .ToList();

                    var measurementUnits = await dbContext.Units
                        .AsNoTracking()
                        .Where(u => unitIds.Contains(u.Id))
                        .ToListAsync();

                    string? category = newProduct.CategoryId != null
                        ? await dbContext.ProductCategories
                            .AsNoTracking()
                            .Where(c => c.Id == newProduct.CategoryId)
                            .Select(c => c.ExternalId)
                            .FirstOrDefaultAsync()
                        : null;

                    ProductDetails productCreation = new()
                    {
                        Id = newProduct.Id,
                        Category = category,
                        Name = newProduct.Name,
                        Description = newProduct.Description,
                        AvailableForSale = newProduct.AvailableForSale,
                        Variants = newProduct.Variants.Select(v => new ProductVariantDetails
                        {
                            Id = v.Id,
                            Name = v.Name,
                            SKU = v.Sku,
                            Price = newProduct.AvailableForSale
                                ? v.SalePrice
                                : v.Price,
                            TrackInventory = v.TrackInventory,
                            LowInventoryThreshold = v.LowInventoryThreshold,
                            MeasurementUnit = measurementUnits
                                .Where(u => u.Id == v.MeasurementUnitId)
                                .Select(u => u.ExternalId)
                                .FirstOrDefault() ?? string.Empty
                        }).ToList()
                    };

                    mbMessage = new()
                    {
                        Source = typeof(ProductWorker).AssemblyQualifiedName,
                        MessageType = Events.PaymentProcessing.Product.Create,
                        Assembly = typeof(ProductDetails).Assembly.FullName,
                        Class = nameof(ProductDetails),
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = productCreation
                    };

                    _productPublisher.PublishMessageAsync(mbMessage).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish product", ex, mbMessage);
                }
            }

            return newProduct;
        }

        /// <summary>
        /// Updates a product
        /// </summary>
        /// <param name="message">A message containing the product to update</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<Product> UpdateProduct(AskToUpdateProduct message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Product? dbProduct = await dbContext.Products
                .Include(p => p.Variants)
                .FirstOrDefaultAsync(p => p.Id == message.Product.Id);

            if (dbProduct == null || dbProduct.Variants == null)
                throw new ApplicationException("Product not found");

            Product? updatedProduct = null;
            List<ProductVariant> updatedVariants, deletedVariants, newVariants;

            updatedProduct = await UpdateExistingProduct(dbContext, dbProduct, message.Product);
            (updatedVariants, deletedVariants, newVariants) =
                UpdateProductVariants(dbProduct, message.Product.Variants.ToList());

            try
            {
                if (updatedProduct != null)
                    dbContext.Update(updatedProduct);

                if (updatedVariants.Any())
                    dbContext.UpdateRange(updatedVariants);

                // TODO: Make sure the variant hasn't been used
                if (deletedVariants.Any())
                    dbContext.RemoveRange(deletedVariants);

                if (newVariants.Any())
                    dbContext.AddRange(newVariants);

                await dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to update product", ex, message);
                throw new ApplicationException("Unable to update product");
            }

            if (updatedProduct != null)
                await PublishProductUpdate(dbContext, updatedProduct);

            if (updatedVariants.Any())
                await PublishVariantMessages(
                    Events.PaymentProcessing.ProductVariant.Update,
                    dbContext, 
                    updatedVariants, 
                    dbProduct.ExternalId ?? string.Empty,
                    dbProduct.AvailableForSale
                );

            if (deletedVariants.Any())
                await PublishVariantDeletions(deletedVariants);

            if (newVariants.Any())
                await PublishVariantMessages(
                    Events.PaymentProcessing.ProductVariant.Create,
                    dbContext, 
                    newVariants, 
                    dbProduct.ExternalId ?? string.Empty,
                    dbProduct.AvailableForSale
                );

            return dbProduct;
        }

        /// <summary>
        /// Deletes a product
        /// </summary>
        /// <param name="message">A message containing the Id of the product
        /// to delete</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<int> DeleteProduct(AskToDeleteProduct message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            Product? dbProduct = await dbContext.Products
                .FirstOrDefaultAsync(p => p.Id == message.ProductId);

            if (dbProduct == null)
                throw new ApplicationException("Product not found");

            Lot? productLot = dbContext.Lots
                .AsNoTracking()
                .Where(l => l.ProductId == dbProduct.Id)
                .FirstOrDefault();

            if (productLot != null)
                throw new ApplicationException("Product has been used and cannot be deleted");

            dbContext.Remove(dbProduct);
            await dbContext.SaveChangesAsync();

            if (_productPublisher != null && !string.IsNullOrEmpty(dbProduct.ExternalId))
            {
                MessageBrokerMessage mbMessage = new()
                {
                    Source = typeof(ProductWorker).AssemblyQualifiedName,
                    MessageType = Events.PaymentProcessing.Product.Delete,
                    TimeStamp = DateTimeOffset.UtcNow,
                    Data = dbProduct.ExternalId
                };

                try
                {
                    _productPublisher.PublishMessageAsync(mbMessage).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish product deletion", ex, mbMessage);
                }
            }

            return dbProduct.Id;
        }

        /// <summary>
        /// Validates the information within a product to make sure that
        /// it can actually be created
        /// </summary>
        /// <param name="dbContext">The database context</param>
        /// <param name="product">The product that is being created or updated</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task ValidateProduct(FarmManagerContext dbContext, Product product)
        {
            if (string.IsNullOrEmpty(product.Name))
                throw new ApplicationException("Product must have a name");

            if (product.Variants == null || product.Variants.Count == 0)
                throw new ApplicationException("Product must have at least one variant");

            Product? existingProduct = await dbContext.Products
                .AsNoTracking()
                .FirstOrDefaultAsync(p =>
                    p.Name == product.Name
                    && p.CategoryId == product.CategoryId
                    && p.IsDeleted == false
                );

            if ((existingProduct != null && product.Id == 0)
                || (existingProduct != null && product.Id != 0 && product.Id != existingProduct.Id)
            )
                throw new ApplicationException("Product with the provided name and category combination already exists");

            List<int> variantUnits = product.Variants
                .Select(v => v.MeasurementUnitId)
                .Distinct()
                .ToList();

            List<Unit> dbUnits = await dbContext.Units
                .AsNoTracking()
                .Where(u => variantUnits.Contains(u.Id))
                .ToListAsync();

            if (dbUnits.Count != variantUnits.Count)
                throw new ApplicationException("Invalid variant unit");

            foreach(ProductVariant variant in product.Variants)
                ValidateProductVariant(variant);
        }

        /// <summary>
        /// Validates a product variant to make sure that it can actually
        /// be created
        /// </summary>
        /// <param name="variant">The product variant to validate</param>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateProductVariant(ProductVariant variant)
        {
            if (
                string.IsNullOrEmpty(variant.Name)
                || string.IsNullOrEmpty(variant.Sku)
            )
                throw new ApplicationException("Variant name and sku are required");

            if (variant.MeasurementUnitId == 0)
                throw new ApplicationException("Invalid variant unit");
        }

        /// <summary>
        /// Updates an existing product from the database
        /// based on the supplied product information
        /// </summary>
        /// <param name="dbContext">The database context</param>
        /// <param name="dbProduct">The product that currently exists in Farm Manager</param>
        /// <param name="updatedProduct">The updated version of the product that is
        /// to be saved</param>
        /// <returns></returns>
        private async Task<Product?> UpdateExistingProduct(
            FarmManagerContext dbContext, 
            Product dbProduct,
            Product updatedProduct
        )
        {
            await ValidateProduct(dbContext, updatedProduct);

            if (
                dbProduct.Name != updatedProduct.Name
                || dbProduct.Description != updatedProduct.Description
                || dbProduct.CategoryId != updatedProduct.CategoryId
                || dbProduct.AvailableForSale != updatedProduct.AvailableForSale
            )
            {
                dbProduct.Name = updatedProduct.Name;
                dbProduct.Description = updatedProduct.Description;
                dbProduct.CategoryId = updatedProduct.CategoryId;
                dbProduct.AvailableForSale = updatedProduct.AvailableForSale;
                return dbProduct;
            }

            return null;
        }

        /// <summary>
        /// Updates the requested product variants
        /// </summary>
        /// <param name="dbProduct">The product that currently exists in the Farm Manager system</param>
        /// <param name="providedVariants">The variants that are to be updated</param>
        /// <returns></returns>
        private (List<ProductVariant>, List<ProductVariant>, List<ProductVariant>) UpdateProductVariants(
            Product dbProduct,
            List<ProductVariant> providedVariants
        )
        {
            List<ProductVariant> updatedVariants = new();
            List<ProductVariant> deletedVariants = new();
            List<ProductVariant> newVariants = new();

            foreach (ProductVariant variant in providedVariants)
            {
                ValidateProductVariant(variant);

                ProductVariant? dbVariant = dbProduct.Variants
                    .FirstOrDefault(v => v.Id == variant.Id);

                if (dbVariant == null)
                {
                    variant.ProductId = dbProduct.Id;
                    newVariants.Add(variant);
                }
                else
                {
                    dbVariant.Name = variant.Name;
                    dbVariant.Sku = variant.Sku;
                    dbVariant.MeasurementUnitId = variant.MeasurementUnitId;
                    dbVariant.Price = variant.Price;
                    dbVariant.SalePrice = variant.SalePrice;
                    dbVariant.TrackInventory = variant.TrackInventory;
                    dbVariant.LowInventoryThreshold = variant.LowInventoryThreshold;
                    dbVariant.HighInventoryThreshold = variant.HighInventoryThreshold;
                    updatedVariants.Add(dbVariant);
                }
            }

            List<int> updatedIds = updatedVariants.Select(v => v.Id).ToList();
            deletedVariants = dbProduct.Variants
                .Where(v => !updatedIds.Contains(v.Id))
                .ToList();

            return (updatedVariants, deletedVariants, newVariants);
        }

        /// <summary>
        /// Publishes a product update to the payment processing service
        /// </summary>
        /// <param name="dbContext">The database context</param>
        /// <param name="updatedProduct">The updated product that should be
        /// published to the payment processor</param>
        /// <returns></returns>
        private async Task PublishProductUpdate(FarmManagerContext dbContext, Product updatedProduct)
        {
            if (_productPublisher != null && !string.IsNullOrEmpty(updatedProduct.ExternalId))
            {
                string? categoryId = await dbContext.ProductCategories
                    .AsNoTracking()
                    .Where(c => c.Id == updatedProduct.CategoryId)
                    .Select(c => c.ExternalId)
                    .FirstOrDefaultAsync();

                MessageBrokerMessage mbMessage = new()
                {
                    Source = typeof(ProductWorker).AssemblyQualifiedName,
                    MessageType = Events.PaymentProcessing.Product.Update,
                    TimeStamp = DateTimeOffset.UtcNow,
                    Data = new ProductDetails
                    {
                        Id = updatedProduct.Id,
                        ExternalId = updatedProduct.ExternalId,
                        Name = updatedProduct.Name,
                        Category = categoryId,
                        Description = updatedProduct.Description,
                        AvailableForSale = updatedProduct.AvailableForSale,
                    }
                };

                try
                {
                    _productPublisher.PublishMessageAsync(mbMessage).Wait();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Unable to publish product update", ex, mbMessage);
                }
            }
        }

        /// <summary>
        /// Publishes a deleted product variant to the payment processing service
        /// </summary>
        /// <param name="deletedVariants">The variants that should be deleted from
        /// the payment processor</param>
        /// <returns></returns>
        public async Task PublishVariantDeletions(List<ProductVariant> deletedVariants)
        {
            if (_productPublisher != null)
            {
                foreach (ProductVariant variant in deletedVariants)
                {
                    if (!string.IsNullOrEmpty(variant.ExternalId))
                    {
                        MessageBrokerMessage mbMessage = new()
                        {
                            Source = typeof(ProductWorker).AssemblyQualifiedName,
                            MessageType = Events.PaymentProcessing.ProductVariant.Delete,
                            TimeStamp = DateTimeOffset.UtcNow,
                            Data = variant.ExternalId
                        };

                        try
                        {
                            _productPublisher.PublishMessageAsync(mbMessage).Wait();
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError("Unable to publish product variant deletion", ex, mbMessage);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// A helper method to consolidate logic for publishing new and 
        /// updated paymment processor events
        /// </summary>
        /// <param name="updateType">The type of update to publish (create or update)</param>
        /// <param name="dbContext">The database context</param>
        /// <param name="variants">The variants that need to be updated</param>
        /// <param name="externalProductId">The payment processor Id of the product to which the 
        /// variants belong</param>
        /// <param name="isAvailableForSale">Whether or not the product and variants
        /// are available for sale</param>
        /// <returns></returns>
        public async Task PublishVariantMessages(
            string updateType,
            FarmManagerContext dbContext,
            List<ProductVariant> variants,
            string externalProductId,
            bool isAvailableForSale
        )
        {
            if (_productPublisher != null)
            {
                List<Unit> units = await dbContext.Units
                    .AsNoTracking()
                    .ToListAsync();

                foreach (ProductVariant variant in variants)
                {

                    MessageBrokerMessage mbMessage = new()
                    {
                        Source = typeof(ProductWorker).AssemblyQualifiedName,
                        MessageType = updateType,
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = new ProductVariantDetails
                        {
                            Id = variant.Id,
                            ExternalId = variant.ExternalId ?? string.Empty,
                            ExternalProductId = externalProductId,
                            Name = variant.Name,
                            SKU = variant.Sku,
                            Price = isAvailableForSale
                                ? variant.SalePrice
                                : variant.Price,
                            TrackInventory = variant.TrackInventory,
                            LowInventoryThreshold = variant.LowInventoryThreshold,
                            MeasurementUnit = units
                                .Where(u => u.Id == variant.MeasurementUnitId)
                                .Select(u => u.ExternalId)
                                .FirstOrDefault() ?? string.Empty
                        }
                    };

                    try
                    {
                        _productPublisher.PublishMessageAsync(mbMessage).Wait();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("Unable to publish product variant message", ex, mbMessage);
                    }
                }
            }
        }
    }
}
