﻿using Akka.Actor;
using Akka.DependencyInjection;
using FarmManager.Core.Actors.Operations.Barcodes;
using FarmManager.Core.Actors.Operations.BusinessLocations;
using FarmManager.Core.Actors.Operations.Categories;
using FarmManager.Core.Actors.Operations.Products;
using FarmManager.Core.Actors.Operations.Units;
using FarmManager.Data.ActorMessages.Operations.Barcodes;
using FarmManager.Data.ActorMessages.Operations.BusinessLocations;
using FarmManager.Data.ActorMessages.Operations.Categories;
using FarmManager.Data.ActorMessages.Operations.Products;
using FarmManager.Data.ActorMessages.Operations.Units;
using FarmManager.Data.DTOs;

namespace FarmManager.Core.Actors.Operations
{
    /// <summary>
    /// The manager responsible for routing all operations related requests
    /// to the correct destination
    /// </summary>
    public class OperationsManager : FarmManagerActor<OperationsManager>
    {
        private IActorRef? _unitEventHandler;
        private IActorRef? _categoryEventHandler;
        private IActorRef? _locationEventHandler;
        private IActorRef? _productEventHandler;

        public OperationsManager(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            ReceiveAny(RouteMessage);
        }

        protected override void PreStart()
        {
            Props unitProps = DependencyResolver.For(Context.System).Props<UnitEventHandler>();
            _unitEventHandler = Context.ActorOf(unitProps, nameof(UnitEventHandler));

            Props categoryProps = DependencyResolver.For(Context.System).Props<CategoryEventHandler>();
            _categoryEventHandler = Context.ActorOf(categoryProps, nameof(CategoryEventHandler));

            Props locationProps = DependencyResolver.For(Context.System).Props<BusinessLocationEventHandler>();
            _locationEventHandler = Context.ActorOf(locationProps, nameof(BusinessLocationEventHandler));

            Props productProps = DependencyResolver.For(Context.System).Props<ProductEventHandler>();
            _productEventHandler = Context.ActorOf(productProps, nameof(ProductEventHandler));
        }

        /// <summary>
        /// Routes the messages to the next appropriate location
        /// </summary>
        /// <param name="message"></param>
        private void RouteMessage(object message)
        {
            IUntypedActorContext context = Context;
            IActorRef sender = Sender;
            TimeSpan timeout = TimeSpan.FromSeconds(_settings.ActorWaitSeconds);

            switch (message)
            {
                case IBarcodeMessage:
                    HandleWithInstanceOfAsync<BarcodeWorker>(message, sender, context);
                    break;

                case IBusinessLocationMessage:
                    HandleWithInstanceOfAsync<BusinessLocationWorker>(message, sender, context);
                    break;

                case ICategoryMessage:
                    HandleWithInstanceOfAsync<CategoryWorker>(message, sender, context);
                    break;

                case IUnitMessage:
                    HandleWithInstanceOfAsync<UnitWorker>(message, sender, context);
                    break;

                case IProductMessage:
                    HandleWithInstanceOfAsync<ProductWorker>(message, sender, context);
                    break;

                default:
                    string messageType = message.GetType().Name;
                    _logger.LogWarning($"Unhandled message of type {messageType}");
                    sender.Tell(ActorResponse.Failure($"Unsupported message type: {messageType}"));
                    break;
            }
        }
    }
}
