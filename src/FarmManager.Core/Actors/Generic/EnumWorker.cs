﻿using FarmManager.Data.ActorMessages.Generic.Enums;
using FarmManager.Data.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Core.Actors.Generic
{
    /// <summary>
    /// A short-lived worker that is responsible for retrieving a list of 
    /// enumerations
    /// </summary>
    public class EnumWorker : FarmManagerActor<EnumWorker>
    {
        private readonly IDbContextFactory<FarmManagerContext> _dbFactory;

        public EnumWorker(IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
            _dbFactory = _scope.ServiceProvider
                .GetRequiredService<IDbContextFactory<FarmManagerContext>>();

            Receive<AskForEnums>(message => HandleWithAsync(GetEnumerations, message));
        }

        /// <summary>
        /// Retrieves enumerations based on the requested EntityId
        /// </summary>
        /// <param name="message">The message that was received by the manager</param>
        /// <returns></returns>
        private async Task<List<Enum>> GetEnumerations(AskForEnums message)
        {
            using FarmManagerContext dbContext = _dbFactory.CreateDbContext();

            return string.IsNullOrEmpty(message.EntityId)
                ? await dbContext.Enums
                    .Where(e => e.IsDeleted == false)
                    .ToListAsync()
                : await dbContext.Enums
                    .Where(e => 
                        e.IsDeleted == false 
                        && e.EntityId == message.EntityId
                    )
                    .ToListAsync();
        }
    }
}
