﻿using FarmManager.Core.Config.Authorization;
using FarmManager.Core.Config.IoTHub;
using FarmManager.Core.Config.PaymentProcessing;
using FarmManager.Core.Config.Storage;
using FarmManager.Services.Email;
using FarmManager.Services.Identity;
using FarmManager.Services.MessageBroker;

namespace FarmManager.Core.Config
{
    /// <summary>
    /// Settings for the FarmManager application
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// The amount of time IActorRef.Ask() should wait for a response
        /// </summary>
        public int ActorWaitSeconds { get; set; }

        /// <summary>
        /// An array of hostnames that are allowed to pass through CORS
        /// and connect to the API
        /// </summary>
        public string[] CORS { get; set; }

        /// <summary>
        /// An array of valid ClientIds used for authentication to 
        /// a SignalR hub
        /// </summary>
        public string[] Audience { get; set; } 

        /// <summary>
        /// The email that system alerts should be sent to
        /// TODO: Move this into a settings table within FarmManager
        /// </summary>
        public string SystemAlertEmail { get; set; }

        /// <summary>
        /// The base address of the web application
        /// </summary>
        public string BaseClientUrl { get; set; }

        /// <summary>
        /// Settings related to authorization
        /// </summary>
        public AuthorizationSettings Authorization { get; set; }

        /// <summary>
        /// Settings for connecting to and interacting with 
        /// Azure Service Bus
        /// </summary>
        public MessageBrokerClientSettings? MessageBroker { get; set; }

        /// <summary>
        /// Settings for interacting with the Azure IoT Hub
        /// </summary>
        public IotHubSettings? IotHub { get; set; }

        /// <summary>
        /// Settings for interacting with the Square API
        /// </summary>
        public PaymentProcessingSettings? PaymentProcessing { get; set; }

        /// <summary>
        /// Settings for interacting with Azure Storage
        /// </summary>
        public StorageSettings? Storage { get; set; }

        /// <summary>
        /// Settings for connecting to an identity provider
        /// </summary>
        public IdentityProviderSettings Identity { get; set; }

        /// <summary>
        /// Settings for creating and sending emails
        /// </summary>
        public EmailServiceSettings Email { get; set; }
    }
}
