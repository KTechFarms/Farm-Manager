﻿namespace FarmManager.Core.Config.Storage
{
    /// <summary>
    /// Settings for interacting with Farm Manager storage containers
    /// </summary>
    public class StorageSettings
    {
        /// <summary>
        /// The name of the blob container for labels
        /// </summary>
        public string LabelContainer { get; set; }
    }
}
