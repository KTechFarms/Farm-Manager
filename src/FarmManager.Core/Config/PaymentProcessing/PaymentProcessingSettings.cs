﻿namespace FarmManager.Core.Config.PaymentProcessing
{
    /// <summary>
    /// Settings related to the Square Platform
    /// </summary>
    public class PaymentProcessingSettings
    {
        /// <summary>
        /// The access token to connect to the Square environment
        /// </summary>
        public string AccessToken { get; set; }
    }
}
