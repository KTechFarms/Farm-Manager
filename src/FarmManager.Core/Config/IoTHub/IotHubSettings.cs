﻿namespace FarmManager.Core.Config.IoTHub
{
    /// <summary>
    /// Settings for connecting to and interacting with 
    /// the Azure IoT Hub
    /// </summary>
    public class IotHubSettings
    {
        /// <summary>
        /// The IoT Hub's connection string
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// The Access policy that has permissions to connect
        /// devices to the Hub
        /// </summary>
        public string HubAccessPolicy { get; set; }

        /// <summary>
        /// A key for the HubAccessPolicy that can be used to sign
        /// the SAS tokens used when connecting to the IoT Hub
        /// </summary>
        public string HubAccessKey { get; set; }

        /// <summary>
        /// The uri used for devices connecing to the IoT hub. Should be
        /// in the format of MyHub.azure-devices.net/devices
        /// </summary>
        public string ResourceUri { get; set; }

        /// <summary>
        /// The number of seconds to wait before timing out
        /// when executing a direct method
        /// </summary>
        public int MethodTimeoutSeconds { get; set; }
    }
}
