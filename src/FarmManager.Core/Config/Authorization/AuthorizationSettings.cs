﻿namespace FarmManager.Core.Config.Authorization
{
    /// <summary>
    /// Settings related to authorization
    /// </summary>
    public class AuthorizationSettings
    {
        /// <summary>
        /// Settings related to authorization claims
        /// </summary>
        public ClaimSettings Claims { get; set; }
    }
}
