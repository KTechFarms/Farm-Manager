﻿namespace FarmManager.Core.Config.Authorization
{
    /// <summary>
    /// Settings that contain the claim types for various
    /// properties within the token
    /// </summary>
    public class ClaimSettings
    {
        /// <summary>
        /// The claim type that holds the user's Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The claim type that contains the user's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The claim type that contains the user's email
        /// </summary>
        public string Email { get; set; }
    }
}
