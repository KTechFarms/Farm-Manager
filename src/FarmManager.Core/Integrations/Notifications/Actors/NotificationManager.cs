﻿using Akka.Actor;
using Akka.DependencyInjection;

namespace FarmManager.Core.Integrations.Notifications.Actors
{
    /// <summary>
    /// The manager of the notification service
    /// </summary>
    public class NotificationManager : ReceiveActor
    {
        IActorRef _receiver;

        public NotificationManager(IServiceProvider serviceProvider)
        {
        }

        protected override void PreStart()
        {
            Props receiverProps = DependencyResolver.For(Context.System).Props<NotificationReceiver>();
            _receiver = Context.ActorOf(receiverProps, nameof(NotificationReceiver));
        }
    }
}
