﻿using FarmManager.Data.ActorMessages.Events;
using FarmManager.Services.Email;
using FarmManager.Services.Email.DTOs;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Integrations.Notifications.Actors.Handlers
{
    /// <summary>
    /// The handler responsible for sending emails to the 3rd party email service
    /// </summary>
    public class EmailHandler : NotificationActor<EmailHandler>
    {
        private readonly IEmailService _emailService;

        public EmailHandler(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _emailService = _scope.ServiceProvider.GetRequiredService<IEmailService>();

            Receive<EventReceived>(SendEmail);
        }

        /// <summary>
        /// Sends an email to the email service
        /// </summary>
        /// <param name="message">An event message containing the 
        /// data that should be sent as an email</param>
        private void SendEmail(EventReceived message)
        {
            try
            {
                Notification email = ExtractNotificationFromMessage(message);
                _emailService.SendEmailAsync(
                    email.Subject,
                    email.Body,
                    email.ToAddresses,
                    email.CcAddresses,
                    email.BccAddresses
                ).Wait();
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed to send email notification", ex, message);
            }
        }

        //////////////////////////////////////////
        //               Helpers                //
        //////////////////////////////////////////

        /// <summary>
        /// Extracts a notification message from an event received from the
        /// message broker
        /// </summary>
        /// <param name="message">The message received from the message broker</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private Notification ExtractNotificationFromMessage(EventReceived message)
        {
            string dataString = JsonConvert.SerializeObject(message.Data);
            Notification? notification = JsonConvert.DeserializeObject<Notification>(dataString);

            if (notification == null)
                throw new ApplicationException("No notification present");

            return notification;
        }
    }
}
