﻿using Akka.Actor;
using FarmManager.Core.Config;
using FarmManager.Services.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace FarmManager.Core.Integrations.Notifications.Actors
{
    /// <summary>
    /// A base actor inheritied by all other notification related actors
    /// </summary>
    /// <typeparam name="A"></typeparam>
    public class NotificationActor<A> : ReceiveActor
    {
        protected readonly IServiceScope _scope;
        protected readonly IFarmLogger _logger;
        protected readonly AppSettings _settings;

        public NotificationActor(IServiceProvider serviceProvider)
        {
            _scope = serviceProvider.CreateScope();

            _settings = _scope.ServiceProvider
                .GetRequiredService<IOptions<AppSettings>>().Value;

            _logger = _scope.ServiceProvider
                .GetRequiredService<IFarmLogger>();
            _logger.Initialize<A>();
        }

        protected override void PostStop()
        {
            _scope.Dispose();
        }
    }
}
