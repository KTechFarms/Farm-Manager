﻿using Akka.Actor;
using Akka.DependencyInjection;
using Akka.Routing;
using FarmManager.Common.Consts;
using FarmManager.Core.Integrations.Notifications.Actors.Handlers;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Services.MessageBroker;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Integrations.Notifications.Actors
{
    /// <summary>
    /// The actor that subscribes to notification messages from the message
    /// broker and forwards received events to the correct location
    /// </summary>
    public class NotificationReceiver : NotificationActor<NotificationReceiver>
    {
        private readonly IMessageBrokerClient _mbClient;
        private readonly IMessageBrokerReceiver _notificationReceiver;

        private IActorRef? _emailHandler;

        public NotificationReceiver(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            MessageBrokerChannelSettings? notificationSettings = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.Notifications);

            if (string.IsNullOrEmpty(notificationSettings?.QueueName))
                throw new ArgumentNullException(nameof(MessageBrokerChannelSettings));

            _mbClient = _scope.ServiceProvider.GetRequiredService<IMessageBrokerClient>();
            _notificationReceiver = _mbClient
                .CreateMessageBrokerReceiver(notificationSettings.QueueName);

            Receive<BeginProcessing>(BeginProcessing);
        }

        protected override void PreStart()
        {
            Props emailProps = DependencyResolver.For(Context.System).Props<EmailHandler>();
            _emailHandler = Context.ActorOf(
                // TODO: Make this adjustable and configurable. Right now there's no need for 
                // more than one, but this at least sets up the framework for later
                emailProps
                    .WithRouter(new SmallestMailboxPool(1))
                    .WithSupervisorStrategy(new OneForOneStrategy(
                        maxNrOfRetries: 3, 
                        withinTimeRange: TimeSpan.FromSeconds(5),
                        localOnlyDecider: x =>
                        {
                            return Directive.Restart;
                        })),
                nameof(EmailHandler));

            if (_notificationReceiver == null)
                throw new ArgumentException(nameof(IMessageBrokerClient));

            if (!_notificationReceiver.TryRegisterHandler(Events.Notification.Create, SendToRouter))
                _logger.LogWarning($"Unable to register handler for {Events.Notification.Create}");

            // Give the initial actors time to startup before receiving messages
            Context.System.Scheduler.ScheduleTellOnce(
                _settings.ActorWaitSeconds,
                Self,
                new BeginProcessing(),
                Self
            );
        }

        protected override void PostStop()
        {
            _notificationReceiver?.Dispose();
            _mbClient?.Dispose();

            base.PostStop();
        }

        private void BeginProcessing(BeginProcessing message)
        {
            _notificationReceiver?.BeginProcessingAsync();
        }

        //////////////////////////////////////////
        //            Event Handlers            //
        //////////////////////////////////////////
        
        /// <summary>
        /// Sends a message received from the message broker to the appropriate handler
        /// </summary>
        /// <param name="message"></param>
        private void SendToRouter(MessageBrokerMessage message)
        {
            // TODO: Route to the appropriate handler based on the message type. 
            // Right now there are only emails though, so this is fine.
            _emailHandler?.Tell(new EventReceived(
                message.MessageType,
                message.Source,
                message.TimeStamp,
                message.Data,
                message.Assembly,
                message.Class
            ));
        }
    }
}
