﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Services.MessageBroker;
using Microsoft.Extensions.DependencyInjection;

namespace FarmManager.Core.Integrations.PaymentProcessing.Actors
{
    /// <summary>
    /// The manager for handling events related to the payment processor
    /// </summary>
    public class PaymentProcessingEventManager : PaymentProcessingActor<PaymentProcessingEventManager>
    {
        private readonly IMessageBrokerClient _mbClient;
        private readonly IMessageBrokerReceiver? _paymentProcessingReceiver;

        private Dictionary<string, HashSet<IActorRef>> _subscribers = new();

        public PaymentProcessingEventManager(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            MessageBrokerChannelSettings? paymentProcessingSettings = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcesing);


            if (string.IsNullOrEmpty(paymentProcessingSettings?.QueueName))
                throw new ArgumentNullException(nameof(MessageBrokerChannelSettings));

            _mbClient = _scope.ServiceProvider.GetRequiredService<IMessageBrokerClient>();
            _paymentProcessingReceiver = _mbClient
                .CreateMessageBrokerReceiver(paymentProcessingSettings.QueueName);

            Receive<BeginProcessing>(BeginProcessing);
            Receive<SubscribeToEvent>(HandleSubscription);
            Receive<UnsubscribeFromEvent>(HandleUnSubscription);
            Receive<AskForMessageBrokerPublisher>(CreateMessageBrokerPublisher);
        }

        protected override void PreStart()
        {
            if (_paymentProcessingReceiver == null)
                throw new ArgumentException(nameof(IMessageBrokerClient));

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Inventory.Create, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Inventory.Create}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Inventory.Transfer, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Inventory.Transfer}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Unit.Create, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Unit.Create}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Unit.Delete, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Unit.Delete}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Category.Create, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Category.Create}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Category.Delete, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Category.Delete}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Location.Create, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Location.Create}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Location.Delete, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Location.Delete}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Product.Create, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Product.Create}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Product.Update, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Product.Update}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.Product.Delete, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.Product.Delete}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.ProductVariant.Create, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.ProductVariant.Create}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.ProductVariant.Update, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.ProductVariant.Update}");

            if (!_paymentProcessingReceiver.TryRegisterHandler(Events.PaymentProcessing.ProductVariant.Delete, PublishEventToSubscribers))
                _logger.LogWarning($"Unable to register handler for {Events.PaymentProcessing.ProductVariant.Delete}");

            // Give the initial actors time to startup before receiving messages
            Context.System.Scheduler.ScheduleTellOnce(
                _settings.ActorWaitSeconds,
                Self,
                new BeginProcessing(),
                Self
            );
        }

        protected override void PostStop()
        {
            _paymentProcessingReceiver?.Dispose();
            _mbClient.Dispose();
            
            base.PostStop();
        }

        private void BeginProcessing(BeginProcessing message)
        {
            _paymentProcessingReceiver?.BeginProcessingAsync();
        }

        //////////////////////////////////////////
        //           Message Handlers           //
        //////////////////////////////////////////

        /// <summary>
        /// Adds a subscriber to an internal dictionary so messages
        /// can be published to the appropriate location
        /// </summary>
        /// <param name="message">A message requesting a subscription</param>
        private void HandleSubscription(SubscribeToEvent message)
        {
            if (_subscribers.TryGetValue(message.EventType, out HashSet<IActorRef>? subscribers))
            {
                if (!subscribers.TryGetValue(message.Subscriber, out IActorRef? existingSubscriber))
                    subscribers.Add(message.Subscriber);
            }
            else
                _subscribers.TryAdd(message.EventType, new HashSet<IActorRef> { message.Subscriber });
        }

        /// <summary>
        /// Removes a subscriber from an internal dictionary so messages are no longer
        /// published to the requestor
        /// </summary>
        /// <param name="message">A message requesting to unsubscribe</param>
        private void HandleUnSubscription(UnsubscribeFromEvent message)
        {
            if (_subscribers.TryGetValue(message.EventType, out HashSet<IActorRef>? subscribers))
                if (!subscribers.Remove(message.Subscriber))
                    _logger.LogWarning($"Unable to remove {message.Subscriber} from {message.EventType} subscribers");
        }

        /// <summary>
        /// Creates a publisher for the message broker
        /// </summary>
        /// <param name="message"></param>
        private void CreateMessageBrokerPublisher(AskForMessageBrokerPublisher message)
        {
            IMessageBrokerPublisher publisher = _mbClient.CreateMessageBrokerPublisher(
                message.QueueName,
                message.Identifier
            );

            Sender.Tell(publisher);
        }

        //////////////////////////////////////////
        //            Event Handlers            //
        //////////////////////////////////////////

        /// <summary>
        /// Publishes an event from the message broker to all subscribers
        /// of that message type
        /// </summary>
        /// <param name="message"></param>
        private void PublishEventToSubscribers(MessageBrokerMessage message)
        {
            if (
                !string.IsNullOrEmpty(message.MessageType)
                && _subscribers.TryGetValue(message.MessageType, out HashSet<IActorRef>? subscribers)
                && subscribers != null
            )
                foreach (IActorRef subscriber in subscribers)
                    subscriber.Tell(new EventReceived(
                        message.MessageType,
                        message.Source,
                        message.TimeStamp,
                        message.Data,
                        message.Assembly,
                        message.Class
                    ));
        }
    }
}
