﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Integrations.PaymentProcessing.Actors.Products
{
    /// <summary>
    /// The worker responsible for handling product related events and
    /// publishing them to the payment processor
    /// </summary>
    public class ProductWorker : PaymentProcessingActor<ProductWorker>
    {
        private readonly IPaymentProcessingService _paymentProcessingService;
        private IMessageBrokerPublisher? _publisher;

        private readonly string _publishQueue;

        public ProductWorker(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _paymentProcessingService = _scope.ServiceProvider
                .GetRequiredService<IPaymentProcessingService>();

            _publishQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcessingEvents)
                ?.QueueName ?? string.Empty;

            Receive<EventReceived>(HandleIncomingEvent);
        }

        protected override void PreStart()
        {
            _publisher = _paymentProcessingManager.Ask(
                new AskForMessageBrokerPublisher(
                    _publishQueue,
                    $"{Self}"
                )).Result as IMessageBrokerPublisher;

            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Product.Create, Self));
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Product.Update, Self));
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Product.Delete, Self));
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.ProductVariant.Create, Self));
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.ProductVariant.Update, Self));
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.ProductVariant.Delete, Self));
        }

        protected override void PostStop()
        {
            _publisher?.Dispose();

            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Product.Create, Self));
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Product.Update, Self));
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Product.Delete, Self));
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.ProductVariant.Create, Self));
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.ProductVariant.Update, Self));
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.ProductVariant.Delete, Self));

            base.PostStop();
        }

        //////////////////////////////////////////
        //            Event Handlers            //
        //////////////////////////////////////////

        /// <summary>
        /// Receives messages from the message broker and routes
        /// them to the correct functions
        /// </summary>
        /// <param name="message">The message received from the
        /// message broker</param>
        private void HandleIncomingEvent(EventReceived message)
        {
            switch (message.MessageType)
            {
                case Events.PaymentProcessing.Product.Create:
                    CreateProduct(message);
                    break;

                case Events.PaymentProcessing.Product.Update:
                    UpdateProduct(message);
                    break;

                case Events.PaymentProcessing.Product.Delete:
                    DeleteProduct(message);
                    break;

                case Events.PaymentProcessing.ProductVariant.Create:
                    CreateProductVariant(message);
                    break;

                case Events.PaymentProcessing.ProductVariant.Update:
                    UpdateProductVariant(message);
                    break;

                case Events.PaymentProcessing.ProductVariant.Delete:
                    DeleteProductVariant(message);
                    break;

                default:
                    _logger.LogWarning("Unhandled event", message);
                    break;
            }
        }

        //////////////////////////////////////////
        //            Implementation            //
        //////////////////////////////////////////
        
        /// <summary>
        /// Creates a product in the payment processor and publishes the external Id
        /// back to Farm Manager
        /// </summary>
        /// <param name="message">The message received from the message broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void CreateProduct(EventReceived message)
        {
            try
            {
                string dataString = JsonConvert.SerializeObject(message.Data);
                ProductDetails? product = JsonConvert.DeserializeObject<ProductDetails>(dataString);

                if (product == null)
                    throw new ApplicationException("No product present");

                Product updatedProduct = _paymentProcessingService.CreateProductAsync(product).Result;

                if (_publisher != null)
                {
                    MessageBrokerMessage mbMessage = new()
                    {
                        Source = $"{Self}",
                        MessageType = Events.PaymentProcessing.Product.Update,
                        Assembly = typeof(Product).Assembly.FullName,
                        Class = nameof(Product),
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = updatedProduct
                    };

                    try
                    {
                        _publisher.PublishMessageAsync(mbMessage).Wait();
                    }
                    catch(Exception ex)
                    {
                        _logger.LogCritical("Unable to publish product update back to core", ex, mbMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to create product in payment processing platform", ex, message);
            }
        }

        /// <summary>
        /// Updates a product in the payment processing platform
        /// </summary>
        /// <param name="message">The message received from the message
        /// broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void UpdateProduct(EventReceived message)
        {
            try
            {
                string dataString = JsonConvert.SerializeObject(message.Data);
                ProductDetails? product = JsonConvert.DeserializeObject<ProductDetails>(dataString);

                if (product == null)
                    throw new ApplicationException("No product present");

                _paymentProcessingService.UpdateProductAsync(product).Wait();
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to update product in payment processing platform", ex, message);
            }
        }

        /// <summary>
        /// Deletes a product from the payment processing platform
        /// </summary>
        /// <param name="message">The message from the message broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void DeleteProduct(EventReceived message)
        {
            try
            {
                string? productId = message.Data as string;
                if (string.IsNullOrEmpty(productId))
                    throw new ApplicationException("Invalid product Id");

                _paymentProcessingService.DeleteProductAsync(productId).Wait();
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to delete product in payment processing platform", ex, message);
            }
        }

        /// <summary>
        /// Creates a product variant in the payment processing platform and
        /// publishes the external id back to Farm Manager
        /// </summary>
        /// <param name="message">The message from the message broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void CreateProductVariant(EventReceived message)
        {
            try
            {
                string dataString = JsonConvert.SerializeObject(message.Data);
                ProductVariantDetails? variant = JsonConvert.DeserializeObject<ProductVariantDetails>(dataString);

                if (variant == null)
                    throw new ApplicationException("No product variant present");

                ProductVariant updatedVariant = _paymentProcessingService.CreateProductVariantAsync(variant).Result;

                if (_publisher != null)
                {
                    MessageBrokerMessage mbMessage = new()
                    {
                        Source = $"{Self}",
                        MessageType = Events.PaymentProcessing.ProductVariant.Update,
                        Assembly = typeof(ProductVariant).Assembly.FullName,
                        Class = nameof(ProductVariant),
                        TimeStamp = DateTimeOffset.UtcNow,
                        Data = updatedVariant
                    };

                    try
                    {
                        _publisher.PublishMessageAsync(mbMessage).Wait();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogCritical("Unable to publish product variant update back to core", ex, mbMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to create product variant in payment processing platform", ex, message);
            }
        }

        /// <summary>
        /// Updates a product in the payment processing platform
        /// </summary>
        /// <param name="message">The message from the message broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void UpdateProductVariant(EventReceived message)
        {
            try
            {
                string dataString = JsonConvert.SerializeObject(message.Data);
                ProductVariantDetails? variant = JsonConvert.DeserializeObject<ProductVariantDetails>(dataString);

                if (variant == null)
                    throw new ApplicationException("No product variant present");

                _paymentProcessingService.UpdateProductVariantAsync(variant).Wait();
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to create update product variant in payment processing platform", ex, message);
            }
        }

        /// <summary>
        /// Deletes a product variant from the payment processing platform
        /// </summary>
        /// <param name="message">The message from the message broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void DeleteProductVariant(EventReceived message)
        {
            try
            {
                string? variantId = message.Data as string;
                if (string.IsNullOrEmpty(variantId))
                    throw new ApplicationException("Invalid product variant Id");

                _paymentProcessingService.DeleteProductVariantAsync(variantId).Wait();
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to delete product variant in payment processing platform", ex, message);
            }
        }
    }
}
