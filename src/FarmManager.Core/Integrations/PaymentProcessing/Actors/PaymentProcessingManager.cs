﻿using Akka.Actor;
using Akka.DependencyInjection;
using FarmManager.Core.Integrations.PaymentProcessing.Actors.Categories;
using FarmManager.Core.Integrations.PaymentProcessing.Actors.Inventory;
using FarmManager.Core.Integrations.PaymentProcessing.Actors.Locations;
using FarmManager.Core.Integrations.PaymentProcessing.Actors.Products;
using FarmManager.Core.Integrations.PaymentProcessing.Actors.Units;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.DTOs;

namespace FarmManager.Core.Integrations.PaymentProcessing.Actors
{
    /// <summary>
    /// The manager for the payment processing service
    /// </summary>
    public class PaymentProcessingManager : PaymentProcessingActor<PaymentProcessingManager>
    {
        private IActorRef? _eventManager;

        private IActorRef? _inventoryWorker;
        private IActorRef? _unitWorker;
        private IActorRef? _categoryWorker;
        private IActorRef? _locationWorker;
        private IActorRef? _productWorker;

        public PaymentProcessingManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            ReceiveAny(RouteMessage);
        }

        protected override void PreStart()
        {
            Props eventProps = DependencyResolver.For(Context.System).Props<PaymentProcessingEventManager>();
            _eventManager = Context.ActorOf(eventProps, "EventManager");

            Props inventoryProps = DependencyResolver.For(Context.System).Props<InventoryWorker>();
            _inventoryWorker = Context.ActorOf(inventoryProps, "InventoryWorker");

            Props unitProps = DependencyResolver.For(Context.System).Props<UnitWorker>();
            _unitWorker = Context.ActorOf(unitProps, "UnitWorker");

            Props categoryProps = DependencyResolver.For(Context.System).Props<CategoryWorker>();
            _categoryWorker = Context.ActorOf(categoryProps, "CategoryWorker");

            Props locationProps = DependencyResolver.For(Context.System).Props<LocationWorker>();
            _locationWorker = Context.ActorOf(locationProps, "LocationWorker");

            Props productProps = DependencyResolver.For(Context.System).Props<ProductWorker>();
            _productWorker = Context.ActorOf(productProps, "ProductWorker");
        }

        /// <summary>
        /// Routes incoming messages to the correct location
        /// </summary>
        /// <param name="message">The message that has been received</param>
        private void RouteMessage(object message)
        {
            IUntypedActorContext context = Context;
            IActorRef sender = Sender;
            TimeSpan timeout = TimeSpan.FromSeconds(_settings.ActorWaitSeconds);

            switch(message)
            {
                case IEventMessage:
                    if (message is AskForMessageBrokerPublisher)
                        _eventManager?.Ask(message, timeout)
                            .ContinueWith(result => sender.Tell(result.Result));
                    else
                        _eventManager?.Tell(message);
                    break;

                default:
                    string messageType = message.GetType().Name;
                    _logger.LogWarning($"Unhandled message of type {messageType}");
                    sender.Tell(ActorResponse.Failure($"Unsupported message type: {messageType}"));
                    break;
            }
        }
    }
}
