﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Integrations.PaymentProcessing.Actors.Locations
{
    /// <summary>
    /// The worker responsible for handling location related events and
    /// publishing them to the payment processor
    /// </summary>
    public class LocationWorker : PaymentProcessingActor<LocationWorker>
    {
        private readonly IPaymentProcessingService _paymentProcessingService;
        private IMessageBrokerPublisher? _publisher;

        private readonly string _publishQueue;

        public LocationWorker(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _paymentProcessingService = _scope.ServiceProvider
                .GetRequiredService<IPaymentProcessingService>();

            _publishQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcessingEvents)
                ?.QueueName ?? string.Empty;

            Receive<EventReceived>(HandleIncomingEvent);
        }

        protected override void PreStart()
        {
            _publisher = _paymentProcessingManager.Ask(
                new AskForMessageBrokerPublisher(
                    _publishQueue,
                    $"{Self}"
                )).Result as IMessageBrokerPublisher;

            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Location.Create, Self));
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Location.Delete, Self));
        }

        protected override void PostStop()
        {
            _publisher?.Dispose();

            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Location.Create, Self));
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Location.Delete, Self));

            base.PostStop();
        }

        //////////////////////////////////////////
        //            Event Handlers            //
        //////////////////////////////////////////

        /// <summary>
        /// Receives messages from the message broker and routes
        /// them to the correct functions
        /// </summary>
        /// <param name="message">The message received from the 
        /// message broker</param>
        private void HandleIncomingEvent(EventReceived message)
        {
            switch (message.MessageType)
            {
                case Events.PaymentProcessing.Location.Create:
                    CreateLocation(message);
                    break;

                case Events.PaymentProcessing.Location.Delete:
                    DeleteLocation(message);
                    break;

                default:
                    _logger.LogWarning($"Unhandled event", message);
                    break;
            }
        }

        //////////////////////////////////////////
        //            Implementation            //
        //////////////////////////////////////////
    
        /// <summary>
        /// Creates a location in the payment processing platform and 
        /// publishes the external Id back to Farm Manager
        /// </summary>
        /// <param name="message">The message received from the message 
        /// broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void CreateLocation(EventReceived message)
        {
            try
            {
                string dataString = JsonConvert.SerializeObject(message.Data);
                LocationCreation? location = JsonConvert.DeserializeObject<LocationCreation>(dataString);

                if (location == null)
                    throw new ApplicationException("No location present");

                string locationId = _paymentProcessingService.CreateLocationAsync(location).Result;

                if (_publisher != null)
                {
                    MessageBrokerMessage mbMessage = new();
                    try
                    {
                        BusinessLocation updatedLocation = new BusinessLocation
                        {
                            Id = location.Id,
                            ExternalId = locationId
                        };

                        mbMessage = new()
                        {
                            Source = $"{Self}",
                            MessageType = Events.PaymentProcessing.Location.Update,
                            Assembly = typeof(BusinessLocation).Assembly.FullName,
                            Class = nameof(BusinessLocation),
                            TimeStamp = DateTimeOffset.UtcNow,
                            Data = updatedLocation
                        };

                        _publisher.PublishMessageAsync(mbMessage).Wait();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogCritical("Unable to publish location update back to core", ex, mbMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical("Unable to create location in payment processing platform", ex, message);
            }
        }

        /// <summary>
        /// Deletes a location from the payment processing platform
        /// </summary>
        /// <param name="message">The message from the message broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void DeleteLocation(EventReceived message)
        {
            try
            {
                string? locationId = message.Data as string;
                if (string.IsNullOrEmpty(locationId))
                    throw new ApplicationException("Invalid location Id");

                _paymentProcessingService.DeleteLocationAsync(locationId).Wait();
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to delete location in payment processing platform", ex, message);
            }
        }
    }
}
