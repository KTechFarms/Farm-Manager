﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Integrations.PaymentProcessing.Actors.Units
{
    /// <summary>
    /// The worker responsible for handling unit related events and publishing
    /// them to the payment processor
    /// </summary>
    public class UnitWorker : PaymentProcessingActor<UnitWorker>
    {
        private readonly IPaymentProcessingService _paymentProcessingService;
        private IMessageBrokerPublisher? _publisher;

        private readonly string _publishQueue;

        public UnitWorker(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _paymentProcessingService = _scope.ServiceProvider
                .GetRequiredService<IPaymentProcessingService>();

            _publishQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcessingEvents)
                ?.QueueName ?? string.Empty;

            Receive<EventReceived>(HandleIncomingEvent);
        }

        protected override void PreStart()
        {
            _publisher = _paymentProcessingManager.Ask(
                new AskForMessageBrokerPublisher(
                    _publishQueue,
                    $"{Self}"
                )).Result as IMessageBrokerPublisher;

            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Unit.Create, Self));
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Unit.Delete, Self));
        }

        protected override void PostStop()
        {
            _publisher?.Dispose();

            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Unit.Create, Self));
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Unit.Delete, Self));

            base.PostStop();
        }

        //////////////////////////////////////////
        //            Event Handlers            //
        //////////////////////////////////////////

        /// <summary>
        /// Receives messages from the message broker and routes
        /// them to the correct functions
        /// </summary>
        /// <param name="message">The message received from the
        /// message broker</param>
        private void HandleIncomingEvent(EventReceived message)
        {
            switch (message.MessageType)
            {
                case Events.PaymentProcessing.Unit.Create:
                    CreateUnit(message);
                    break;

                case Events.PaymentProcessing.Unit.Delete:
                    DeleteUnit(message);
                    break;

                default:
                    _logger.LogWarning($"Unhandled event", message);
                    break;
            }
        }

        //////////////////////////////////////////
        //            Implementation            //
        //////////////////////////////////////////

        /// <summary>
        /// Creates a unit in the payment processor and publishes the
        /// external id back to Farm Manager
        /// </summary>
        /// <param name="message">The message received from the 
        /// message broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void CreateUnit(EventReceived message)
        {
            try
            {
                string dataString = JsonConvert.SerializeObject(message.Data);
                UnitCreation? unit = JsonConvert.DeserializeObject<UnitCreation>(dataString);

                if (unit == null)
                    throw new ApplicationException("No update present");

                string unitId = _paymentProcessingService.CreateUnitAsync(unit).Result;

                if(_publisher != null)
                {
                    MessageBrokerMessage mbMessage = new();
                    try
                    {
                        Unit updatedUnit = new Unit
                        {
                            Id = unit.Id,
                            ExternalId = unitId
                        };

                        mbMessage = new()
                        {
                            Source = $"{Self}",
                            MessageType = Events.PaymentProcessing.Unit.Update,
                            Assembly = typeof(Unit).Assembly.FullName,
                            Class = nameof(Unit),
                            TimeStamp = DateTimeOffset.UtcNow,
                            Data = updatedUnit
                        };

                        _publisher.PublishMessageAsync(mbMessage).Wait();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogCritical("Unable to publish unit update back to core", ex, mbMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to create unit in payment processing platform", ex, message);
            }
        }

        /// <summary>
        /// Deletes a unit from the payment processing platform
        /// </summary>
        /// <param name="message">The message received from the message
        /// broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void DeleteUnit(EventReceived message)
        {
            try
            {
                string? unitId = message.Data as string;
                if (string.IsNullOrEmpty(unitId))
                    throw new ApplicationException("Invalid unit id");

                _paymentProcessingService.DeleteUnitAsync(unitId).Wait();
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to delete unit in payment processing platform", ex, message);
            }
        }
    }
}
