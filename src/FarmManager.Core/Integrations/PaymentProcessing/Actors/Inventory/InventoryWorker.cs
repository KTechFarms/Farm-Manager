﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Services.Email.DTOs;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Integrations.PaymentProcessing.Actors.Inventory
{
    /// <summary>
    /// The worker responsible for handling inventory related events
    /// and publishing them to the payment processor
    /// </summary>
    public class InventoryWorker : PaymentProcessingActor<InventoryWorker>
    {
        private readonly IPaymentProcessingService _paymentProcessingService;
        private IMessageBrokerPublisher? _publisher;

        private readonly string _notificationQueue;

        public InventoryWorker(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _paymentProcessingService = _scope.ServiceProvider
                .GetRequiredService<IPaymentProcessingService>();

            _notificationQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.Notifications)
                ?.QueueName ?? string.Empty;

            Receive<EventReceived>(HandleIncomingEvent);
        }

        protected override void PreStart()
        {
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Inventory.Create, Self));
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Inventory.Transfer, Self));

            _publisher = _paymentProcessingManager.Ask(
                new AskForMessageBrokerPublisher(
                    _notificationQueue,
                    $"{Self}"
                )).Result as IMessageBrokerPublisher;
        }

        protected override void PostStop()
        {
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Inventory.Create, Self));
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Inventory.Transfer, Self));

            _publisher?.Dispose();

            base.PostStop();
        }

        //////////////////////////////////////////
        //            Event Handlers            //
        //////////////////////////////////////////

        /// <summary>
        /// Receives messages from the message broker and routes
        /// them to the correct functions
        /// </summary>
        /// <param name="message">The message received from the
        /// message broker</param>
        private void HandleIncomingEvent(EventReceived message)
        {
            switch(message.MessageType)
            {
                case Events.PaymentProcessing.Inventory.Create:
                    CreateInventory(message);
                    break;

                case Events.PaymentProcessing.Inventory.Transfer:
                    TransferInventory(message);
                    break;

                default:
                    _logger.LogWarning($"Unhandled event", message);
                    break;
            }
        }

        /// <summary>
        /// Adds inventory to the payment processor
        /// </summary>
        /// <param name="message">The message received from the message
        /// broker</param>
        private void CreateInventory(EventReceived message)
        {
            try
            {
                InventoryUpdate update = ExtractUpdateFromMessage(message);
                _paymentProcessingService.CreateInventoryAsync(update).Wait();
            }
            catch (Exception ex)
            {
                // TODO: Implement templates prior to adding this everywhere
                string alertMessage = $"""
                    <div>
                        Unable to create inventory in payment processing platform. 
                        <br/><br/>
                        <strong>
                            Error: 
                        </strong>
                        {ex.Message}
                        <br/><br/>
                        <strong>
                            Message: 
                        </strong>
                        {JsonConvert.SerializeObject(message)}
                    </div>
                    """;

                SendAlert("Inventory Create Error", alertMessage).Wait();
            }
        }

        /// <summary>
        /// Transfers inventory from one location to another within the 
        /// payment processor platform
        /// </summary>
        /// <param name="message">The message from the message broker</param>
        private void TransferInventory(EventReceived message)
        {
            try
            {
                InventoryUpdate update = ExtractUpdateFromMessage(message);
                _paymentProcessingService.TransferInventoryAsync(update).Wait();
            }
            catch (Exception ex)
            {
                string alertMessage = $"""
                    <div>
                        Unable to transfer inventory in payment processing platform. 
                        <br/><br/>
                        <strong>
                            Error: 
                        </strong>
                        {ex.Message}
                        <br/><br/>
                        <strong>
                            Message: 
                        </strong>
                        {JsonConvert.SerializeObject(message)}
                    </div>
                    """;

                SendAlert("Inventory Tranfer Error", alertMessage).Wait();
            }
        }

        //////////////////////////////////////////
        //               Helpers                //
        //////////////////////////////////////////
        
        /// <summary>
        /// Extracts an inventory update message from a generic
        /// message broker message
        /// </summary>
        /// <param name="message">The message received from the message
        /// broker</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private InventoryUpdate ExtractUpdateFromMessage(EventReceived message)
        {
            string dataString = JsonConvert.SerializeObject(message.Data);
            InventoryUpdate? update = JsonConvert.DeserializeObject<InventoryUpdate>(dataString);

            if (update == null)
                throw new ApplicationException("No update present");

            return update;
        }

        /// <summary>
        /// Publishes an alert to the notification channel of 
        /// the message broker
        /// </summary>
        /// <param name="title">The title of the alert</param>
        /// <param name="message">The content of the alert</param>
        /// <returns></returns>
        private async Task SendAlert(string title, string message)
        {
            if(_publisher != null)
            {
                MessageBrokerMessage mbMessage = new()
                {
                    Source = $"{Context.Self}",
                    MessageType = Events.Notification.Create,
                    Assembly = typeof(Notification).Assembly.FullName,
                    Class = nameof(Notification),
                    TimeStamp = DateTimeOffset.Now,
                    Data = new Notification
                    {
                        NotificationType = NotificationType.Email,
                        Subject = title,
                        Body = message,
                        ToAddresses = new Dictionary<string, string?>
                        {
                            { _settings.SystemAlertEmail, string.Empty }
                        }
                    }
                };

                try
                {
                    await _publisher.PublishMessageAsync(mbMessage);
                }
                catch (Exception ex)
                {
                    _logger.LogCritical("Failed to publish notification", ex, mbMessage);
                }
            }
        }
    }
}
