﻿using Akka.Actor;
using FarmManager.Common.Consts;
using FarmManager.Data.ActorMessages.Events;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.PaymentProcessing;
using FarmManager.Services.PaymentProcessing.DTOs;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace FarmManager.Core.Integrations.PaymentProcessing.Actors.Categories
{
    /// <summary>
    /// The worker responsible for handling product category related events and
    /// publishing them to the payment processor
    /// </summary>
    public class CategoryWorker : PaymentProcessingActor<CategoryWorker>
    {
        private readonly IPaymentProcessingService _paymentProcessingService;
        private IMessageBrokerPublisher? _publisher;

        private readonly string _publishQueue;

        public CategoryWorker(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            _paymentProcessingService = _scope.ServiceProvider
                .GetRequiredService<IPaymentProcessingService>();

            _publishQueue = _settings?.MessageBroker
                ?.GetSettings(MessageBrokerChannel.PaymentProcessingEvents)
                ?.QueueName ?? string.Empty;

            Receive<EventReceived>(HandleIncomingEvent);
        }

        protected override void PreStart()
        {
            _publisher = _paymentProcessingManager.Ask(
                new AskForMessageBrokerPublisher(
                    _publishQueue,
                    $"{Self}"
                )).Result as IMessageBrokerPublisher;

            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Category.Create, Self));
            _paymentProcessingManager.Tell(new SubscribeToEvent(Events.PaymentProcessing.Category.Delete, Self));
        }

        protected override void PostStop()
        {
            _publisher?.Dispose();

            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Category.Create, Self));
            _paymentProcessingManager.Tell(new UnsubscribeFromEvent(Events.PaymentProcessing.Category.Delete, Self));

            base.PostStop();
        }

        //////////////////////////////////////////
        //            Event Handlers            //
        //////////////////////////////////////////

        /// <summary>
        /// Receives an incoming event from the message broker and forwards it to the 
        /// correct function
        /// </summary>
        /// <param name="message">The message received from the message broker</param>
        private void HandleIncomingEvent(EventReceived message)
        {
            switch (message.MessageType)
            {
                case Events.PaymentProcessing.Category.Create:
                    CreateCategory(message);
                    break;

                case Events.PaymentProcessing.Category.Delete:
                    DeleteCategory(message);
                    break;

                default:
                    _logger.LogWarning($"Unhandled event", message);
                    break;
            }
        }

        //////////////////////////////////////////
        //            Implementation            //
        //////////////////////////////////////////

        /// <summary>
        /// Creates a new category in the payment processor and publishes
        /// the external id back to Farm Manager
        /// </summary>
        /// <param name="message">The message received from the message broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void CreateCategory(EventReceived message)
        {
            try
            {
                string dataString = JsonConvert.SerializeObject(message.Data);
                CategoryCreation? category = JsonConvert.DeserializeObject<CategoryCreation>(dataString);

                if (category == null)
                    throw new ApplicationException("No category present");

                string categoryId = _paymentProcessingService.CreateCategoryAsync(category).Result;

                if (_publisher != null)
                {
                    MessageBrokerMessage mbMessage = new();
                    try
                    {
                        ProductCategory updatedCategory = new ProductCategory
                        {
                            Id = category.Id,
                            ExternalId = categoryId
                        };

                        mbMessage = new()
                        {
                            Source = $"{Self}",
                            MessageType = Events.PaymentProcessing.Category.Update,
                            Assembly = typeof(ProductCategory).Assembly.FullName,
                            Class = nameof(ProductCategory),
                            TimeStamp = DateTimeOffset.UtcNow,
                            Data = updatedCategory
                        };

                        _publisher.PublishMessageAsync(mbMessage).Wait();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogCritical("Unable to publish category update back to core", ex, mbMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical("Unable to create category in payment processing platform", ex, message);
            }
        }

        /// <summary>
        /// Deletes a category from the payment processor
        /// </summary>
        /// <param name="message">The message received from the 
        /// message broker</param>
        /// <exception cref="ApplicationException"></exception>
        private void DeleteCategory(EventReceived message)
        {
            try
            {
                string? categoryId = message.Data as string;
                if (string.IsNullOrEmpty(categoryId))
                    throw new ApplicationException("Invalid category Id");

                _paymentProcessingService.DeleteUnitAsync(categoryId).Wait();
            }
            catch (Exception ex) 
            {
                _logger.LogError("Unable to delete category in payment processing platform", ex, message);
            }
        }
    }
}
