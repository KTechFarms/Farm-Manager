﻿using Akka.Actor;
using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Services.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace FarmManager.Core.Integrations.PaymentProcessing.Actors
{
    /// <summary>
    /// A base actor inherited by other payment processing actors
    /// </summary>
    /// <typeparam name="A"></typeparam>
    public class PaymentProcessingActor<A> : ReceiveActor
    {
        protected readonly IServiceScope _scope;
        protected readonly IActorRef _paymentProcessingManager;
        protected readonly IFarmLogger _logger;
        protected readonly AppSettings _settings;

        public PaymentProcessingActor(IServiceProvider serviceProvider)
        {
            ActorRegistry registry = ActorRegistry.For(Context.System);
            _paymentProcessingManager = registry.Get<PaymentProcessingManager>();

            _scope = serviceProvider.CreateScope();

            _settings = _scope.ServiceProvider
                .GetRequiredService<IOptions<AppSettings>>().Value;

            _logger = _scope.ServiceProvider
                .GetRequiredService<IFarmLogger>();
            _logger.Initialize<A>();
        }

        protected override void PostStop()
        {
            _scope.Dispose();
        }
    }
}
