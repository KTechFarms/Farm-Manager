﻿using FarmManager.Common.Consts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace FarmManager.Core.Hubs
{
    /// <summary>
    /// A base hub class that is inherited by all other hubs
    /// </summary>
    [Authorize(AuthenticationSchemes = AuthenticationSchemes.SignalR)]
    public class FarmManagerHub : Hub
    {
        /// <summary>
        /// Subscribes the connection to the specified group id
        /// </summary>
        /// <param name="groupId">The Id of the group that should be subscribed to</param>
        /// <returns></returns>
        public Task Subscribe(string groupId)
        {
            return Groups.AddToGroupAsync(Context.ConnectionId, groupId);
        }

        /// <summary>
        /// Unsubscribes the connection from the specified group id
        /// </summary>
        /// <param name="groupId">The Id of the group that should be 
        /// unsubscribed from</param>
        /// <returns></returns>
        public Task Unsubscribe(string groupId)
        {
            return Groups.RemoveFromGroupAsync(Context.ConnectionId, groupId);
        }
    }
}
