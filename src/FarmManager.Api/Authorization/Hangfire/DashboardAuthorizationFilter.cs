﻿using FarmManager.Common.Consts;
using FarmManager.Core.Config;
using Hangfire.Dashboard;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace FarmManager.Api.Authorization.Hangfire
{
    // https://learn.microsoft.com/en-us/azure/active-directory/develop/howto-build-services-resilient-to-metadata-refresh#aspnet-core

    /// <summary>
    /// An authorization filter to determine if the user has access to the Hangfire Dashboard
    /// </summary>
    public class DashboardAuthorizationFilter : IDashboardAuthorizationFilter
    {
        private readonly string[] _audiences;
        private readonly string _identityUri;

        public DashboardAuthorizationFilter(AppSettings settings)
        {
            _audiences = settings.Audience;
            _identityUri = $"https://login.microsoftonline.com/{settings.Identity.TenantId}/v2.0";
        }

        /// <summary>
        /// Determines if the user is authorized to view the dashboard based on their auth token
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Authorize(DashboardContext context)
        {
            HttpContext httpContext = context.GetHttpContext();

            string? token = httpContext.Request.Query
                    .Where(q => q.Key == AuthKeys.QueryToken)
                    .Select(q => q.Value.ToString())
                    .FirstOrDefault()
                ?? httpContext.Request.Cookies[AuthKeys.CookieToken];

            if(string.IsNullOrEmpty(token))
                return false;

            try
            {
                httpContext.Response.Cookies.Append(
                    AuthKeys.CookieToken, 
                    token, 
                    new CookieOptions
                    {
                        Expires = DateTimeOffset.UtcNow.AddHours(1)
                    });

                ConfigurationManager<OpenIdConnectConfiguration> configManager = new (
                    $"{_identityUri}/.well-known/openid-configuration",
                    new OpenIdConnectConfigurationRetriever()
                );
                OpenIdConnectConfiguration config = configManager.GetConfigurationAsync().Result;

                JwtSecurityTokenHandler handler = new();
                var jwt = handler.ReadJwtToken(token);
                handler.ValidateToken(
                    token,
                    new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,

                        IssuerSigningKeys = config.SigningKeys,
                        ValidIssuer = _identityUri,
                        ValidAudiences = _audiences
                    },
                    out SecurityToken validatedToken
                );

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
