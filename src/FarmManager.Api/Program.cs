using Akka.Actor;
using Akka.DependencyInjection;
using Akka.Hosting;
using FarmManager.Api.Authentication;
using FarmManager.Api.Authorization.Hangfire;
using FarmManager.Core.Config;
using FarmManager.Core.Hubs;
using FarmManager.Core.Integrations.Notifications.Actors;
using FarmManager.Core.Integrations.PaymentProcessing.Actors;
using FarmManager.Common.Consts;
using FarmManager.Data.Contexts;
using FarmManager.Migrations;
using FarmManager.Migrations.Release_0001;
using FarmManager.Migrations.Release_0002;
using FarmManager.Services.Email;
using FarmManager.Services.Email.SendGrid;
using FarmManager.Services.Identity;
using FarmManager.Services.Identity.Microsoft;
using FarmManager.Services.Logging;
using FarmManager.Services.MessageBroker;
using FarmManager.Services.MessageBroker.ServiceBus;
using FarmManager.Services.PaymentProcessing;
using FarmManager.Services.PaymentProcessing.Square;
using FarmManager.Services.Storage;
using Hangfire;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Web;
using NLog.Web;
using SendGrid.Extensions.DependencyInjection;
using Square;

using Actors = FarmManager.Core.Actors;
using Microsoft.Extensions.Options;

NLog.LogManager.Setup().LoadConfigurationFromAppSettings();

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseNLog();

//////////////////////////////////////////
//          Configure Settings          //
//////////////////////////////////////////

var appConfigString = builder.Configuration.GetConnectionString("AppConfig");
builder.Configuration.AddAzureAppConfiguration(options =>
{
    options.Connect(appConfigString)
        .ConfigureRefresh(refresh =>
        {
            refresh.Register(nameof(AppSettings), true)
                .SetCacheExpiration(TimeSpan.FromSeconds(60));
        });
});

// Override any app config settings with local values if we're developing
if (builder.Environment.IsDevelopment())
    builder.Configuration.AddJsonFile("appsettings.Local.json", optional: true, reloadOnChange: true);

AppSettings settings = new();
builder.Configuration.Bind("AppSettings", settings);

builder.Services.Configure<MessageBrokerClientSettings>(
    builder.Configuration.GetSection("AppSettings:MessageBroker"));

builder.Services.Configure<AppSettings>(
    builder.Configuration.GetSection("AppSettings"));

builder.Services.Configure<IdentityProviderSettings>(
    builder.Configuration.GetSection("AppSettings:Identity"));

builder.Services.Configure<EmailServiceSettings>(
    builder.Configuration.GetSection("AppSettings:Email"));

//////////////////////////////////////////
//                 CORS                 //
//////////////////////////////////////////

if (settings.CORS != null)
    builder.Services.AddCors(options =>
    {
        options.AddPolicy("CorsPolicy", builder =>
        {
            builder.WithOrigins(settings.CORS)
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials();
        });
    });

//////////////////////////////////////////
//           Authentication             //
//////////////////////////////////////////

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddScheme<SignalrOptions, SignalRHandler>(AuthenticationSchemes.SignalR, null)
    .AddMicrosoftIdentityWebApi(builder.Configuration, "AzureAd");

//////////////////////////////////////////
//            Database Setup            //
//////////////////////////////////////////

string dbConnection = builder.Configuration.GetConnectionString("Database") ?? string.Empty;
builder.Services.AddDbContextFactory<FarmManagerContext>(options =>
    options.UseSqlServer(dbConnection));

builder.Services.AddMigrations(
    dbConnection,
    new List<Type> 
    { 
        typeof(Release_0001) ,
        typeof(Release_0002)
    }
);

//////////////////////////////////////////
//               Akka.Net               //
//////////////////////////////////////////

builder.Services.AddAkka("FarmManagerCore", configurationBuilder =>
{
    configurationBuilder
        .WithActors((system, registry) =>
        {
            Props managerProps = DependencyResolver
                .For(system).Props<Actors.FarmManager>();
            Props paymentProps = DependencyResolver
                .For(system).Props<PaymentProcessingManager>();
            Props notificationProps = DependencyResolver
                .For(system).Props<NotificationManager>();

            IActorRef manager = system.ActorOf(managerProps, nameof(Actors.FarmManager));
            IActorRef paymentProcessor = system.ActorOf(paymentProps, nameof(PaymentProcessingManager));
            IActorRef notificationProcessor = system.ActorOf(notificationProps, nameof(NotificationManager));

            registry.TryRegister<Actors.FarmManager>(manager);
            registry.TryRegister<PaymentProcessingManager>(paymentProcessor);
        });
});

//////////////////////////////////////////
//               Hangfire               //
//////////////////////////////////////////

builder.Services.AddHangfire(config => config
    .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
    .UseSimpleAssemblyNameTypeSerializer()
    .UseRecommendedSerializerSettings()
    .UseSqlServerStorage(dbConnection));

builder.Services.AddHangfireServer();

//////////////////////////////////////////
//               SignalR                //
//////////////////////////////////////////

string signalRConnection = builder.Configuration.GetConnectionString("SignalR");
builder.Services.AddSignalR()
    .AddAzureSignalR(signalRConnection);

builder.Services.AddResponseCompression(options =>
{
    options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
        new[] { "application/octet-stream" });
});

//////////////////////////////////////////
//         Additional Services          //
//////////////////////////////////////////

builder.Services.AddSendGrid(options =>
{
    options.ApiKey = settings.Email.ApiKey;
});

builder.Services.AddSingleton<ISquareClient>(provider =>
{
    IWebHostEnvironment webEnv = provider.GetRequiredService<IWebHostEnvironment>();
    Square.Environment squareEnv = webEnv.IsProduction()
        ? Square.Environment.Production
        : Square.Environment.Sandbox;

    return new SquareClient.Builder()
        .Environment(squareEnv)
        .AccessToken(settings?.PaymentProcessing?.AccessToken)
        .Build();
});

builder.Services.AddSingleton<IMessageBrokerClient, ServiceBusMessageBrokerClient>(provider =>
{
    IFarmLogger logger = provider.GetRequiredService<IFarmLogger>();
    IOptions<MessageBrokerClientSettings> options = provider.GetRequiredService<IOptions<MessageBrokerClientSettings>>();
    return new ServiceBusMessageBrokerClient(options.Value, logger);
});
builder.Services.AddSingleton<IIdentityProviderService, MicrosoftIdentityProviderService>();
builder.Services.AddTransient<IPaymentProcessingService, SquarePaymentProcessingService>();
builder.Services.AddTransient<IFarmLogger, FarmLogger>();
builder.Services.AddTransient<IStorageService, BlobStorageService>();
builder.Services.AddTransient<IEmailService, SendGridEmailService>();

//////////////////////////////////////////
//            MVC Components            //
//////////////////////////////////////////

builder.Services.AddControllers()
    .AddNewtonsoftJson();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//////////////////////////////////////////
//    Build and Run Startup Actions     //
//////////////////////////////////////////

var app = builder.Build();

FarmManagerContext.SetTriggers();

app.Lifetime.ApplicationStopped.Register(() =>
{
    // Stop NLog
    NLog.LogManager.Shutdown();
});

//////////////////////////////////////////
//      Configure Request Pipeline      //
//////////////////////////////////////////

app.UseResponseCompression();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("CorsPolicy");

app.UseAuthentication();
app.UseAuthorization();

app.UseHttpsRedirection();

app.UseHangfireDashboard(
    "/hangfire",
    new DashboardOptions
    {
        Authorization = new[] { new DashboardAuthorizationFilter(settings) },
        AppPath = settings.BaseClientUrl
    }
);

app.MapControllers();
app.MapHub<TelemetryHub>(Hubs.Telemetry);

app.Run();