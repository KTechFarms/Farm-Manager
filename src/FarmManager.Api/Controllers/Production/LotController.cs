﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Production.Lots;
using FarmManager.Data.DTOs.Production;
using FarmManager.Data.DTOs.Queries;
using FarmManager.Data.Entities.Production;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace FarmManager.Api.Controllers.Production
{
    /// <summary>
    /// The api controller responsible for handling production run requests
    /// </summary>
    [Route("lot")]
    public class LotController : FarmManagerController
    {
        private readonly string _userIdClaim;

        public LotController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
            _userIdClaim = options?.Value?.Authorization?.Claims?.Id ?? string.Empty;
        }

        /// <summary>
        /// Attempts to retrieve a collection of lots
        /// </summary>
        /// <param name="query">The query parameters to filter lots</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveLots([FromQuery] LotQueryParams query)
        {
            return await AskFarmManager(new AskToRetrieveLots(query));
        }

        /// <summary>
        /// Attempts to create a lot using the provided information
        /// </summary>
        /// <param name="lot">The lot to create</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateLot([FromBody] Lot lot)
        {
            return await AskFarmManager(new AskToCreateLot(lot));
        }

        /// <summary>
        /// Attempts to retrieve the next available lot number
        /// </summary>
        /// <returns></returns>
        [HttpGet("number")]
        public async Task<IActionResult> GetNextLotNumber()
        {
            return await AskFarmManager(new AskForNextLotNumber());
        }

        /// <summary>
        /// Attempts to retrieve the next available batch information
        /// </summary>
        /// <param name="lotNumber">The lot number to which the batch
        /// information applies</param>
        /// <returns></returns>
        [HttpGet("{lotNumber}/batch")]
        public async Task<IActionResult> GetNextBatchData(string lotNumber)
        {
            return await AskFarmManager(new AskForNextBatchData(lotNumber));
        }

        /// <summary>
        /// Creates a new batch for the specified lot
        /// </summary>
        /// <param name="lotNumber">The lot number to which the batch belongs</param>
        /// <param name="batch">The information to create the batch</param>
        /// <returns></returns>
        [HttpPost("{lotNumber}/batch")]
        public async Task<IActionResult> CreateBatch(string lotNumber, [FromBody] Batch batch)
        {
            return await AskFarmManager(new AskToCreateBatch(lotNumber, batch));
        }

        /// <summary>
        /// Attempt to retrieve notes related to a lot
        /// </summary>
        /// <param name="lotNumber">The lot number to 
        /// which the notes refer</param>
        /// <returns></returns>
        [HttpGet("{lotNumber}/note")]
        public async Task<IActionResult> GetNotes(string lotNumber)
        {
            return await AskFarmManager(new AskForLotNotes(lotNumber));
        }

        /// <summary>
        /// Attempt to create a new note for a particular lot
        /// </summary>
        /// <param name="lotNumber">The lot to which the note refers</param>
        /// <param name="note">The actual note to create</param>
        /// <returns></returns>
        [HttpPost("{lotNumber}/note")]
        public async Task<IActionResult> CreateLotNote(string lotNumber, [FromBody] string note)
        {
            ClaimsIdentity? identity = User.Identity as ClaimsIdentity;
            string? userId = identity?.Claims?
                .Where(c => c.Type == _userIdClaim)
                .Select(c => c.Value)
                .FirstOrDefault() 
                ?? string.Empty;

            return await AskFarmManager(new AskToCreateLotNote(note, lotNumber, userId));
        }

        /// <summary>
        /// Attempts to close the specified batch
        /// </summary>
        /// <param name="lotNumber">The lot to which the batch belongs</param>
        /// <param name="batchNumber">The batch number that should be closed</param>
        /// <returns></returns>
        [HttpPatch("{lotNumber}/batch/{batchNumber}")]
        public async Task<IActionResult> CloseBatch(string lotNumber, int batchNumber)
        {
            return await AskFarmManager(new AskToCloseBatch(lotNumber, batchNumber));
        }

        /// <summary>
        /// Attempts to create a new production item
        /// </summary>
        /// <param name="lotNumber">The lot number to which the item will belong</param>
        /// <param name="batchNumber">The batch number to which the item will belong</param>
        /// <param name="productInfo">The information of the product to create</param>
        /// <returns></returns>
        [HttpPost("{lotNumber}/batch/{batchNumber}")]
        public async Task<IActionResult> CreateProductionItem(
            string lotNumber, 
            int batchNumber,
            [FromBody] CreateProductionItemDTO productInfo
        )
        {
            return await AskFarmManager(new AskToCreateProductionItem(
                lotNumber,
                batchNumber,
                productInfo
            ));
        }

        /// <summary>
        /// Attempt to retrieve the batches for a particular lot
        /// </summary>
        /// <param name="lotNumber">The lot number to search for batches</param>
        /// <returns></returns>
        [HttpGet("{lotNumber}/batches")]
        public async Task<IActionResult> GetLotBatches(string lotNumber)
        {
            return await AskFarmManager(new AskForLotBatches(lotNumber));
        }

        /// <summary>
        /// Attempt to retrieve information for the next available product
        /// </summary>
        /// <param name="lotNumber">The lot number to retrieve product information for</param>
        /// <param name="batchNumber">The batch number to retrieve product information for</param>
        /// <returns></returns>
        [HttpGet("{lotNumber}/batches/{batchNumber}")]
        public async Task<IActionResult> GetNextProductData(string lotNumber, int batchNumber)
        {
            return await AskFarmManager(new AskForNextProductData(lotNumber, batchNumber));
        }

        /// <summary>
        /// Attempts to retrieve a collection of statuses that the
        /// specified lot can change to
        /// </summary>
        /// <param name="lotId">The id of the lot to find statuses for</param>
        /// <returns></returns>
        [HttpGet("{lotId}/progression")]
        public async Task<IActionResult> GetLotProgressions(int lotId)
        {
            return await AskFarmManager(new AskForLotProgressions(lotId));
        }

        /// <summary>
        /// Attempts to promote a lot to the given status
        /// </summary>
        /// <param name="lotId">The id of the lot to promote</param>
        /// <param name="statusId">The id of the status to promote to</param>
        /// <returns></returns>
        [HttpPost("{lotId}/progression/{statusId}")]
        public async Task<IActionResult> PromoteLot(
            int lotId, 
            int statusId,
            [FromQuery] int? transplantLocationId
        )
        {
            return await AskFarmManager(new AskToPromoteLot(lotId, statusId, transplantLocationId));
        }
    }
}
