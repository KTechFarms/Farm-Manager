﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Production.Inventory;
using FarmManager.Data.DTOs.Production;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Production
{
    /// <summary>
    /// The api controller responsible for handling inventory related requests
    /// </summary>
    [Route("inventory")]
    public class InventoryController : FarmManagerController
    {
        public InventoryController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Attempt to retrieve inventory at a specific location
        /// </summary>
        /// <param name="locationId">The id of the location where inventory 
        /// should be retrieved</param>
        /// <returns></returns>
        [HttpGet("{locationId}")]
        public async Task<IActionResult> RetrieveInventory(int locationId)
        {
            return await AskFarmManager(new AskForInventory(locationId));
        }

        /// <summary>
        /// Attempt to add inventory to a specific location
        /// </summary>
        /// <param name="locationId">The location at which inventory is being added</param>
        /// <param name="inventory">The inventory being added</param>
        /// <returns></returns>
        [HttpPost("{locationId}")]
        public async Task<IActionResult> ReceiveInventory(int locationId, [FromBody] List<InventoryItem> inventory)
        {
            return await AskFarmManager(new AskToReceiveInventory(locationId, inventory));
        }

        /// <summary>
        /// Attempt to transfer inventory between locations
        /// </summary>
        /// <param name="transfer">A transfer object describing which
        /// items should be transfered between locations</param>
        /// <returns></returns>
        [HttpPost("transfer")]
        public async Task<IActionResult> TransferInventory([FromBody] InventoryTransfer transfer)
        {
            return await AskFarmManager(new AskToTransferInventory(
                transfer.FromLocationId,
                transfer.ToLocationId,
                transfer.ProductionItemIds,
                transfer.InventoryIds
            ));
        }

        /// <summary>
        /// Attempt to dispose of inventory
        /// </summary>
        /// <param name="disposal">The inventory disposal request</param>
        /// <returns></returns>
        [HttpPost("dispose")]
        public async Task<IActionResult> DisposeInventory([FromBody] InventoryDisposal disposal)
        {
            return await AskFarmManager(new AskToDisposeInventory(disposal));
        }
    }
}
