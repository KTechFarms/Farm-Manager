﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Production.Labels;
using FarmManager.Data.DTOs.Production;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Production
{
    /// <summary>
    /// The api controller responsible for handling label requests
    /// </summary>
    [Route("label")]
    public class LabelController : FarmManagerController
    {
        public LabelController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Attempt to retrieve a list of labels
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveLabels()
        {
            return await AskFarmManager(new AskForLabels());
        }

        /// <summary>
        /// Attempt to create a new label
        /// </summary>
        /// <param name="label">The label that should be created</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateLabel([FromBody] LabelDTO label)
        {
            return await AskFarmManager(new AskToCreateLabel(label));
        }

        /// <summary>
        /// Attempt to update a label
        /// </summary>
        /// <param name="labelId">The id of the label that should be updated</param>
        /// <returns></returns>
        [HttpPatch("{labelId}")]
        public async Task<IActionResult> UpdateLabel(int labelId)
        {
            return await AskFarmManager(new AskToUpdateLabel("name", "content"));
        }

        /// <summary>
        /// Attempt to delete a label
        /// </summary>
        /// <param name="labelId">The id of the label that should be deleted</param>
        /// <returns></returns>
        [HttpDelete("{labelId}")]
        public async Task<IActionResult> DeleteLabel(int labelId)
        {
            return await AskFarmManager(new AskToDeleteLabel(labelId));
        }
    }
}
