﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Production.FarmLocations;
using FarmManager.Data.DTOs.Queries;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Production
{
    /// <summary>
    /// The api controller responsible for handling any farm location requests
    /// </summary>
    [Route("location")]
    public class FarmLocationController : FarmManagerController
    {
        public FarmLocationController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Attempts to retrieve a set of locations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveLocations([FromQuery] FarmLocationQueryParams query)
        {
            return await AskFarmManager(new AskForFarmLocations(query));
        }

        /// <summary>
        /// Attempts to create a new FarmLocation
        /// </summary>
        /// <param name="location">The location that should be created</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateLocation([FromBody] FarmLocation location)
        {
            return await AskFarmManager(new AskToCreateFarmLocation(location));
        }

        /// <summary>
        /// Attempt to delete a FarmLocation
        /// </summary>
        /// <param name="locationId">The id of the location to delete</param>
        /// <returns></returns>
        [HttpDelete("{locationId}")]
        public async Task<IActionResult> DeleteLocation(int locationId)
        {
            return await AskFarmManager(new AskToDeleteFarmLocation(locationId));
        }
    }
}
