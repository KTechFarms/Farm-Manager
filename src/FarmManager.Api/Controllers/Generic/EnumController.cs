﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Generic.Enums;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Generic
{
    /// <summary>
    /// The api controller responsible for retrieving Enums
    /// </summary>
    [Route("enum")]
    public class EnumController : FarmManagerController
    {
        public EnumController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Attempt to retrieve a list of enumerations
        /// </summary>
        /// <param name="entityId">The EntityId the enums should represent</param>
        /// <returns></returns>
        [HttpGet("{entityId}")]
        public async Task<IActionResult> GetEnums(string entityId)
        {
            return await AskFarmManager(new AskForEnums(entityId));
        }
    }
}
