﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.System.Dashboard;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers
{
    /// <summary>
    /// The api controller responsible for obtaining data for charts displayed
    /// on the Farm Manager dashboard
    /// </summary>
    [Route("dashboard")]
    public class DashboardController : FarmManagerController
    {
        public DashboardController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        ) 
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Retrieves the number of pending and active devices
        /// </summary>
        /// <returns></returns>
        [HttpGet("device-summary")]
        public async Task<IActionResult> RetrieveDeviceSummary()
        {
            return await AskFarmManager(new AskForDeviceCounts());
        }
    }
}
