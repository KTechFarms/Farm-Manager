﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Reports;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Reports
{
    /// <summary>
    /// The api controller responsible for handling report requests
    /// </summary>
    [Route("reports")]
    public class ReportController : FarmManagerController
    {
        public ReportController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger) 
        { 
        }

        /// <summary>
        /// Attempts to retrieve a list of crop statistics
        /// </summary>
        /// <returns></returns>
        [HttpGet("crop-statistics")]
        public async Task<IActionResult> RetrieveCropStatistics()
        {
            return await AskFarmManager(new AskForCropStatistics());
        }
    }
}
