﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Operations.Barcodes;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Operations
{
    /// <summary>
    /// The api controller responsible for handling barcode requests
    /// </summary>
    [Route("barcode")]
    public class BarcodeController : FarmManagerController
    {
        public BarcodeController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Searches for an inventory product that uses the provided
        /// barcode
        /// </summary>
        /// <param name="barcode">The barcode used to search for a product</param>
        /// <returns></returns>
        [HttpGet("product")]
        public async Task<IActionResult> SearchForBarcodeProduct([FromQuery] string barcode)
        {
            return await AskFarmManager(new AskForBarcodeProduct(barcode));
        }
    }
}
