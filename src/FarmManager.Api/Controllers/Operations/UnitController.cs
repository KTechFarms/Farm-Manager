﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Operations.Units;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Operations
{
    /// <summary>
    /// The api controller responsible for handling unit requests
    /// </summary>
    [Route("unit")]
    public class UnitController : FarmManagerController
    {
        public UnitController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Attempt to retrieve a list of units
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveUnits()
        {
            return await AskFarmManager(new AskForUnits());
        }

        /// <summary>
        /// Attempt to create a new unit
        /// </summary>
        /// <param name="unit">The unit to create</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateUnit([FromBody] Unit unit)
        {
            return await AskFarmManager(new AskToCreateUnit(unit));
        }

        /// <summary>
        /// Attempt to delete a unit
        /// </summary>
        /// <param name="unitId">The id of the unit to delete</param>
        /// <returns></returns>
        [HttpDelete("{unitId}")]
        public async Task<IActionResult> DeleteUnit(int unitId)
        {
            return await AskFarmManager(new AskToDeleteUnit(unitId));
        }
    }
}
