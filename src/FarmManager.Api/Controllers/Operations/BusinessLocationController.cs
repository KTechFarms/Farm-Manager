﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Operations.BusinessLocations;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Operations
{
    /// <summary>
    /// The api controller repsonsible for handling business location requests
    /// </summary>
    [Route("business-location")]
    public class BusinessLocationController : FarmManagerController
    {
        public BusinessLocationController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Attempt to retrieeve a list of business locations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveBusinessLocations()
        {
            return await AskFarmManager(new AskForBusinessLocations());
        }

        /// <summary>
        /// Attempt to create a new business location
        /// </summary>
        /// <param name="location">The location that should be created</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateBusinessLocation([FromBody] BusinessLocation location)
        {
            return await AskFarmManager(new AskToCreateBusinessLocation(location));
        }

        /// <summary>
        /// Attempt to remove a business location
        /// </summary>
        /// <param name="locationId">The id of the location to delete</param>
        /// <returns></returns>
        [HttpDelete("{locationId}")]
        public async Task<IActionResult> DeleteBusinessLocation(int locationId)
        {
            return await AskFarmManager(new AskToDeleteBusinessLocation(locationId));
        }
    }
}
