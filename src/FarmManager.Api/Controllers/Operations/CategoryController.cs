﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Operations.Categories;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Operations
{
    /// <summary>
    /// The api controller repsonsible for handling category requests
    /// </summary>
    [Route("category")]
    public class CategoryController : FarmManagerController
    {
        public CategoryController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger) 
        { 
        }

        /// <summary>
        /// Attempt to retrieve a list of product categories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveCategories()
        {
            return await AskFarmManager(new AskForCategories());
        }

        /// <summary>
        /// Attempt to create a new product category
        /// </summary>
        /// <param name="category">The category to create</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCategory([FromBody] ProductCategory category)
        {
            return await AskFarmManager(new AskToCreateCategory(category));
        }

        /// <summary>
        /// Attempt to delete a product category
        /// </summary>
        /// <param name="categoryId">The id of the category to delete</param>
        /// <returns></returns>
        [HttpDelete("{categoryId}")]
        public async Task<IActionResult> DeleteCategory(int categoryId)
        {
            return await AskFarmManager(new AskToDeleteCategory(categoryId));
        }
    }
}
