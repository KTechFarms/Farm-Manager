﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Operations.Products;
using FarmManager.Data.Entities.Operations;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.Operations
{
    /// <summary>
    /// The api controller responsible for handling product requests
    /// </summary>
    [Route("product")]
    public class ProductController : FarmManagerController
    {
        public ProductController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger) 
        { 
        }

        /// <summary>
        /// Attempt to retrieve a list of products
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveProducts()
        {
            return await AskFarmManager(new AskForProducts());
        }

        /// <summary>
        /// Attempt to create a product
        /// </summary>
        /// <param name="product">The product to create</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateProduct([FromBody] Product product)
        {
            return await AskFarmManager(new AskToCreateProduct(product));
        }

        /// <summary>
        /// Attempt to update a product
        /// </summary>
        /// <param name="product">the product to update</param>
        /// <returns></returns>
        [HttpPatch]
        public async Task<IActionResult> UpdateProduct([FromBody] Product product) 
        {
            return await AskFarmManager(new AskToUpdateProduct(product));
        }

        /// <summary>
        /// Attempt to delete a product and all of its variants
        /// </summary>
        /// <param name="productId">The id of the product to delete</param>
        /// <returns></returns>
        [HttpDelete("{productId}")]
        public async Task<IActionResult> DeleteProduct(int productId)
        {
            return await AskFarmManager(new AskToDeleteProduct(productId));
        }
    }
}
