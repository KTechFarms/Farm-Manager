﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.System.Settings;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers.System
{
    /// <summary>
    /// The api controller responsible for settings related requests
    /// </summary>
    [Route("settings")]
    public class SettingsController : FarmManagerController
    {
        public SettingsController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Attempt to retrieve a list of settings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveSettings()
        {
            return await AskFarmManager(new AskForSettings());
        }

        /// <summary>
        /// Attempt to update the value of a setting
        /// </summary>
        /// <param name="settings">A dictionary of setting names and values to update</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateSettings([FromBody] Dictionary<string, string> settings)
        {
            return await AskFarmManager(new AskToUpdateSettings(settings));
        }
    }
}
