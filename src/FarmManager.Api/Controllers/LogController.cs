﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.System;
using FarmManager.Data.DTOs.Queries;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers
{
    /// <summary>
    /// The api controller responsible for any log-related requests
    /// </summary>
    [Route("log")]
    public class LogController : FarmManagerController
    {
        public LogController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }
        
        /// <summary>
        /// Attempts to retrieve a set of logs matching the provided query parameters
        /// </summary>
        /// <param name="query">The parameters that should be matched for a log to be returned</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveLogs([FromQuery] LogQueryParams query)
        {
            return await AskFarmManager(new AskToRetrieveLogs(query));
        }
    }
}
