﻿using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages.Devices;
using FarmManager.Data.DTOs.Queries;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers
{
    /// <summary>
    /// The api controller responsible for receiving any device-related requests
    /// </summary>
    [Route("device")]
    public class DeviceController : FarmManagerController
    {
        public DeviceController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
            : base(registry, options, logger)
        {
        }

        /// <summary>
        /// Retrieves a list of devices registered to Farm Manager
        /// </summary>
        /// <param name="query">The query parameters to limit devices returned</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> RetrieveDevices([FromQuery] DeviceQueryParams query)
        {
            return await AskFarmManager(new AskToRetrieveDevices(query));
        }

        /// <summary>
        /// Retrieves information about a device, including its interfaces
        /// and telemetry
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        [HttpGet("{deviceId}")]
        public async Task<IActionResult> RetrieveDeviceInformation(
            string deviceId, 
            [FromQuery] int historyMinutes//, 
            //[FromQuery] TelemetryGrouping grouping
        )
        {
            return await AskFarmManager(new AskForDeviceInformation(deviceId, historyMinutes, TelemetryGrouping.Time));
        }

        /// <summary>
        /// Executes a direct method on a remote device
        /// </summary>
        /// <param name="deviceId">The id of the device</param>
        /// <param name="methodName">The name of a method to execute</param>
        /// <param name="parameters">Collection of parameters required by the device</param>
        /// <returns></returns>
        [HttpPost("{deviceId}/direct-method/{methodName}")]
        public async Task<IActionResult> InvokeDirectMethod(
            string deviceId, 
            string methodName, 
            [FromBody] object? parameters
        )
        {
            return await AskFarmManager(new AskToInvokeMethod(deviceId, methodName, parameters));
        }

        /// <summary>
        /// Attempts to register a device with the IoT Hub and Farm Manager system
        /// </summary>
        /// <param name="deviceId">The identifier of the device to register</param>
        /// <returns></returns>
        [HttpPost("register")]
        public async Task<IActionResult> RegisterDevice(string deviceId)
        {
            return await AskFarmManager(new AskToRegisterDevice(deviceId));
        }

        /// <summary>
        /// Attempts to create a SAS token for the specified device which can be used
        /// to connect to the IoT Hub
        /// </summary>
        /// <param name="deviceId">The idenifier of the device requesting a token</param>
        /// <param name="duration">The duration that the token should be valid for</param>
        /// <param name="deviceKey">The key for the device requesting a token</param>
        /// <returns></returns>
        [HttpPost("token")]
        public async Task<IActionResult> GenerateToken(string deviceId, int duration, string? deviceKey)
        {
            return await AskFarmManager(new AskToGenerateDeviceToken(deviceId, duration, deviceKey));
        }
    }
}
