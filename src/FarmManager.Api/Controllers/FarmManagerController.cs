﻿using Akka.Actor;
using Akka.Hosting;
using FarmManager.Core.Config;
using FarmManager.Data.ActorMessages;
using FarmManager.Data.DTOs;
using FarmManager.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FarmManager.Api.Controllers
{
    /// <summary>
    /// A base controller with common functionality utilized by the majority
    /// of Farm Manager api controllers. All controllers should inherit this
    /// </summary>
    [ApiController]
    [Authorize]
    public class FarmManagerController : ControllerBase
    {
        /// <summary>
        /// A logger
        /// </summary>
        protected readonly IFarmLogger _logger;

        /// <summary>
        /// A reference to the FarmManager actor of the actor system
        /// </summary>
        protected readonly IActorRef _farmManager;

        /// <summary>
        /// The duration that should waited when performing an actor.ask
        /// </summary>
        protected readonly int _actorWaitSeconds;

        public FarmManagerController(
            ActorRegistry registry, 
            IOptions<AppSettings> options,
            IFarmLogger logger
        )
        {
            _farmManager = registry.Get<Core.Actors.FarmManager>();
            _actorWaitSeconds = options.Value.ActorWaitSeconds;
            _logger = logger;
        }

        protected async Task<IActionResult> AskFarmManager<TAsk>(TAsk ask) 
            where TAsk: IFarmManagerMessage
        {
            FarmManagerActorResponse? result = new();
            try
            {
                result = await _farmManager.Ask(
                    ask,
                    TimeSpan.FromSeconds(_actorWaitSeconds)
                ) as FarmManagerActorResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error asking {typeof(TAsk).Name}", ex, ask);
                result = ActorResponse.Failure("Error making request. Please try again");
            }

            return new OkObjectResult(result);
        }
    }
}
