﻿using FarmManager.Core.Config;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text.Encodings.Web;

namespace FarmManager.Api.Authentication
{
    /// <summary>
    /// Responsible for determining if a SignalR connection 
    /// attempt contains a valid token
    /// </summary>
    public class SignalRHandler : AuthenticationHandler<SignalrOptions>
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly string[] _audience;

        public SignalRHandler(
            IOptionsMonitor<SignalrOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IHttpContextAccessor httpContextAccessor,
            IOptions<AppSettings> appOptions
        ) : base(options, logger, encoder, clock)
        {
            _contextAccessor = httpContextAccessor;
            _audience = appOptions.Value.Audience;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (_contextAccessor == null || _contextAccessor.HttpContext == null)
                return Task.FromResult(AuthenticateResult.Fail("Unauthorized"));

            KeyValuePair<string, StringValues> tokenKvp = _contextAccessor.HttpContext.Request.Query
                .Where(q => q.Key == "access_token")
                .FirstOrDefault();

            if (tokenKvp.Equals(default(KeyValuePair<string, StringValues>)))
                return Task.FromResult(AuthenticateResult.Fail("Unauthorized"));

            try
            {
                string token = tokenKvp.Value.ToString();

                JwtSecurityTokenHandler handler = new();
                var jwt = handler.ReadJwtToken(token);

                DateTimeOffset now = DateTimeOffset.UtcNow;
                if (jwt.ValidFrom > now || jwt.ValidTo < now)
                    throw new ApplicationException("Invalid Token");

                string name = string.Empty;
                string oid = string.Empty;
                string preferredUsername = string.Empty;
                bool containsAudience = false;
                foreach (Claim claim in jwt.Claims)
                {
                    if (claim.Type == "aud")
                        if(!_audience.Contains(claim.Value))
                            throw new ApplicationException("Invalid Token");
                        else
                            containsAudience = true;

                    if (claim.Type == "name")
                        name = claim.Value;
                    if (claim.Type == "oid")
                        oid = claim.Value;
                    if (claim.Type == "preferred_username")
                        preferredUsername = claim.Value;
                }

                if(!containsAudience)
                    throw new ApplicationException("Invalid Token");

                Claim[] claims = new[]
                {
                    new Claim(ClaimTypes.Name, name),
                    new Claim(ClaimTypes.Email, preferredUsername),
                    new Claim(ClaimTypes.AuthenticationMethod, "SignalR")
                };
                ClaimsIdentity identity = new(claims, Scheme.Name);
                ClaimsPrincipal principal = new(identity);
                AuthenticationTicket ticket = new(principal, Scheme.Name);

                return Task.FromResult(AuthenticateResult.Success(ticket));
            }
            catch(Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return Task.FromResult(AuthenticateResult.Fail("Unauthorized"));
            }
        }
    }
}
