﻿using Microsoft.AspNetCore.Authentication;

namespace FarmManager.Api.Authentication
{
    /// <summary>
    /// A base options class for SignalR Authentication
    /// </summary>
    public class SignalrOptions : AuthenticationSchemeOptions
    {
    }
}
