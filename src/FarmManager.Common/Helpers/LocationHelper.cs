﻿using FarmManager.Data.Entities.Operations;

namespace FarmManager.Common.Helpers
{
    /// <summary>
    /// Contains various helper methods for working with locations
    /// </summary>
    public static class LocationHelper
    {
        /// <summary>
        /// Builds a location string containing the hierarchy of parent FarmLocations
        /// including the business location
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public static string GenerateBusinessLocationParentString(FarmLocation location)
        {
            string businessLocation = location.BusinessLocation?.Name ?? string.Empty;
            string parent = GenerateFarmLocationParentString(location);
            parent = string.IsNullOrEmpty(parent)
               ? $"({businessLocation})"
               : $"({businessLocation} > {parent})";

            return parent;
        }

        /// <summary>
        /// Recursively generates a string to identify the parents of a given location
        /// </summary>
        /// <param name="location">The location you want to generate the parent
        /// string for</param>
        /// <returns></returns>
        public static string GenerateFarmLocationParentString(FarmLocation location)
        {
            string parent = string.Empty;
            if (location.ParentLocation != null)
            {
                parent = location.ParentLocation.Name;
                string nestedParent = GenerateFarmLocationParentString(location.ParentLocation);
                if (!string.IsNullOrEmpty(nestedParent))
                    parent = $"{nestedParent} > {parent}";
            }

            return parent;
        }

        /// <summary>
        /// Generates a friendly name for the given location that includes the
        /// parents, if there are any 
        /// </summary>
        /// <param name="location">The location you want to generate a name for</param>
        /// <returns></returns>
        public static string GenerateFarmLocationString(FarmLocation location)
        {
            string locationParent = GenerateFarmLocationParentString(location);
            return string.IsNullOrEmpty(locationParent)
                ? location.Name
                : $"{location.Name} ({locationParent})";
        }

        /// <summary>
        /// Takes a root location and returns a single list of locations 
        /// containing the root and all children, grandchildren, etc
        /// </summary>
        /// <param name="rootLocation">The location that flattening should begin from</param>
        /// <param name="allLocations">A collection of all possible locations</param>
        /// <returns></returns>
        public static List<FarmLocation> FlattenInferiorLocations(FarmLocation rootLocation, List<FarmLocation> allLocations)
        {
            List<FarmLocation> results = new();

            FarmLocation? currentLocation = allLocations
                .Where(l => l.Id == rootLocation.Id)
                .FirstOrDefault();
            if (currentLocation != null)
                results.Add(currentLocation);

            List<FarmLocation> childLocations = allLocations
                .Where(l => l.ParentLocationId == rootLocation.Id)
                .ToList();
            if (childLocations.Count > 0)
                foreach (FarmLocation child in childLocations)
                    results = results
                        .Concat(FlattenInferiorLocations(child, allLocations))
                        .ToList();

            return results
                .DistinctBy(l => l.Id)
                .ToList();
        }
    }
}
