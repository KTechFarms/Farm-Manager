﻿using FarmManager.Data.DTOs.Production;

namespace FarmManager.Common.Helpers
{
    /// <summary>
    /// Contains various helper methods for working with barcodes
    /// </summary>
    public static class BarcodeHelper
    {
        /// <summary>
        /// Takes a raw barcode string and parses it into a Barcode object
        /// </summary>
        /// <param name="barcode">The barcode that needs to be parsed</param>
        /// <returns></returns>
        public static Barcode ParseBarcode(string barcode)
        {
            Barcode barcodeObject = new()
            {
                RawBarcode = barcode,
                AIs = new()
            };

            string userFriendlyBarcode = string.Empty;

            List<string> barcodeParts = barcode.Split("\\F").ToList();
            foreach(string barcodePart in barcodeParts)
            {
                var ai = barcodePart.Substring(0, 2);
                var value = barcodePart.Substring(2);
                barcodeObject.AIs[ai] = value;

                userFriendlyBarcode += $"({ai}){value}";
            }

            barcodeObject.UserFriendlyBarcode = userFriendlyBarcode;

            return barcodeObject;
        }
    }
}
