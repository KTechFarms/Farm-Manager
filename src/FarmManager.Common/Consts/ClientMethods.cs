﻿namespace FarmManager.Common.Consts
{
    /// <summary>
    /// A collection of function names used by SignalR clients 
    /// to respond to events
    /// </summary>
    public static class ClientMethods
    {
        /// <summary>
        /// Events and functions utilized when interacting 
        /// with device telemetry
        /// </summary>
        public static class Telemetry
        {
            public const string NewTelemetry = "NewTelemetry";
        }
    }
}
