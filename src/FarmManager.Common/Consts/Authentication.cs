﻿namespace FarmManager.Common.Consts
{
    /// <summary>
    /// A collection of names used for authentication schemes
    /// </summary>
    public static class AuthenticationSchemes
    {
        public const string SignalR = "SignalR";
    }

    /// <summary>
    /// A collection of authentication & authorization related keys 
    /// used in request queries and cookies
    /// </summary>
    public static class AuthKeys
    {
        /// <summary>
        /// The key pertaining to the bearer token when included
        /// in a http request query
        /// </summary>
        public const string QueryToken = "access_token";

        /// <summary>
        /// The key pertaining to the bearer token when stored in
        /// a cookie
        /// </summary>
        public const string CookieToken = "token";
    }
}
