﻿namespace FarmManager.Common.Consts
{
    /// <summary>
    /// A collection of SignalR Hub names
    /// </summary>
    public static class Hubs
    {
        public const string Telemetry = "telemetry-hub";
    }
}
