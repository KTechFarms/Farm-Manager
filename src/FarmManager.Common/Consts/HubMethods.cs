﻿namespace FarmManager.Common.Consts
{
    /// <summary>
    /// A collection of common functions utilized by SignalR hubs
    /// </summary>
    public static class HubMethods
    {
        /// <summary>
        /// Functions that are present in all hubs
        /// </summary>
        public static class Common
        {
            public const string Subscribe = "Subscribe";
            public const string Unsubscribe = "Unsubscribe";
        }
    }
}
