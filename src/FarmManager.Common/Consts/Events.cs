﻿namespace FarmManager.Common.Consts
{
    /// <summary>
    /// A collection of event types used by publishers and 
    /// subscribers of the message broker
    /// </summary>
    public static class Events
    {
        /// <summary>
        /// Message broker events related to notifications
        /// </summary>
        public static class Notification
        {
            public const string Create = "Notification.Create";
        }

        /// <summary>
        /// Message broker events related to telemetry
        /// </summary>
        public static class Telemetry
        {
            public const string RegisterDevice = "Telemetry.RegisterDevice";
            public const string Reading = "Telemetry.Reading";
        }

        /// <summary>
        /// Meessage broker events related to payment processing
        /// </summary>
        public static class PaymentProcessing
        {
            /// <summary>
            /// Inventory specific payment processing events
            /// </summary>
            public static class Inventory
            {
                public const string Create = "PaymentProcessing.Inventory.Create";
                public const string Transfer = "PaymentProcessing.Inventory.Transfer";
            }

            /// <summary>
            /// Unit specific payment processing events
            /// </summary>
            public static class Unit
            {
                public const string Create = "PaymentProcessing.Unit.Create";
                public const string Delete = "PaymentProcessing.Unit.Delete";
                public const string Update = "PaymentProcessing.Unit.Update";
            }

            /// <summary>
            /// Category specific payment processing events
            /// </summary>
            public static class Category
            {
                public const string Create = "PaymentProcessing.Category.Create";
                public const string Delete = "PaymentProcessing.Category.Delete";
                public const string Update = "PaymentProcessing.Category.Update";
            }

            /// <summary>
            /// Location specific payment processing events
            /// </summary>
            public static class Location
            {
                public const string Create = "PaymentProcessing.Location.Create";
                public const string Delete = "PaymentProcessing.Location.Delete";
                public const string Update = "PaymentProcessing.Location.Update";
            }

            /// <summary>
            /// Product specific payment processing events
            /// </summary>
            public static class Product
            {
                public const string Create = "PaymentProcessing.Product.Create";
                public const string Delete = "PaymentProcessing.Product.Delete";
                public const string Update = "PaymentProcessing.Product.Update";
            }

            /// <summary>
            /// Product Variant specific payment processing events
            /// </summary>
            public static class ProductVariant
            {
                public const string Create = "PaymentProcessing.ProductVariant.Create";
                public const string Delete = "PaymentProcessing.ProductVariant.Delete";
                public const string Update = "PaymentProcessing.ProductVariant.Update";
            }
        }
    }
}
