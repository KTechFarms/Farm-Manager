﻿namespace FarmManager.Common.Consts
{
    /// <summary>
    /// A collection of Message Broker channels
    /// </summary>
    public enum MessageBrokerChannel
    {
        Telemetry = 0,
        Inventory = 1,
        PaymentProcesing = 2,
        PaymentProcessingEvents = 3,
        Notifications = 4
    }
}
