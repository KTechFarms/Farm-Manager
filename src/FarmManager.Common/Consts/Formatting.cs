﻿namespace FarmManager.Common.Consts
{
    /// <summary>
    /// A collection of string representations of object formatting
    /// </summary>
    public static class Formatting
    {
        public const string DateFormat = "yyyy-MM-dd HH:mm:ss";
    }
}
