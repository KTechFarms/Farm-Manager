using FarmManager.Web;
using FarmManager.Web.Config;
using FarmManager.Web.Services.FarmManagerApiService;
using FarmManager.Web.Services.PrintService;
using FarmManager.Web.Services.SignalRService;
using FarmManager.Web.Services.StorageService;
using FarmManager.Web.State;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor;
using MudBlazor.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);

//////////////////////////////////////////
//          Configure Settings          //
//////////////////////////////////////////

AppSettings settings = new();
builder.Configuration.Bind("AppSettings", settings);

builder.Services.Configure<AppSettings>(options =>
{
    builder.Configuration.Bind("AppSettings", options);
});

//////////////////////////////////////////
//             Blazor Setup             //
//////////////////////////////////////////

builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

//////////////////////////////////////////
//         MudBlazor Components         //
//////////////////////////////////////////

builder.Services.AddMudServices(config =>
{
    config.SnackbarConfiguration.PositionClass = Defaults.Classes.Position.BottomCenter;
    config.SnackbarConfiguration.PreventDuplicates = false;
    config.SnackbarConfiguration.NewestOnTop = true;
    config.SnackbarConfiguration.ShowCloseIcon = true;
    config.SnackbarConfiguration.VisibleStateDuration = 3000;
    config.SnackbarConfiguration.ShowTransitionDuration = 500;
    config.SnackbarConfiguration.HideTransitionDuration = 500;
    config.SnackbarConfiguration.SnackbarVariant = Variant.Filled;
});

//////////////////////////////////////////
//            Authenticatioon           //
//////////////////////////////////////////

builder.Services.AddMsalAuthentication(options =>
{
    builder.Configuration.Bind("AzureAd", options.ProviderOptions.Authentication);

    options.ProviderOptions.LoginMode = "redirect";
    options.ProviderOptions.DefaultAccessTokenScopes
        .Add(settings.ApiScope);
});

//////////////////////////////////////////
//          Additional Services         //
//////////////////////////////////////////

builder.Services.AddSingleton<Store>();
builder.Services.AddSingleton<ISignalRService, SignalRService>();
builder.Services.AddScoped<FarmManagerApiAuthorizationMessageHandler>();
builder.Services.AddTransient<IStorageService, SessionStorageService>();
builder.Services.AddTransient<IPrintService, DymoPrintService>();

builder.Services.AddHttpClient<IFarmManagerApiService, FarmManagerApiService>()
    .AddHttpMessageHandler<FarmManagerApiAuthorizationMessageHandler>();

//////////////////////////////////////////
//            Build and Run             //
//////////////////////////////////////////

await builder.Build().RunAsync();
