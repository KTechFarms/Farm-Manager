﻿using FarmManager.Web.State;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Shared
{
    /// <summary>
    /// The main layout for the Farm Manager web app
    /// </summary>
    public class MainLayoutBase : LayoutComponentBase, IDisposable
    {
        /// <summary>
        /// The application's state store
        /// </summary>
        [Inject]
        protected Store Store { get; set; }

        /// <summary>
        /// The theme utilized by the Farm Manager web app
        /// </summary>
        protected MudTheme FarmManagerTheme = new()
        {
            Palette = new()
            {
                Primary = "#306B2B",
                Secondary = "#FF9F1D"
            },
            LayoutProperties = new()
            {
                AppbarHeight = "50px"
            }
        };

        protected override void OnInitialized()
        {
            Store.OnLoadingChange += StateHasChanged;
        }

        public void Dispose()
        {
            Store.OnLoadingChange -= StateHasChanged;
        }
    }
}
