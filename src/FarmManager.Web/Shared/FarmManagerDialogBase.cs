﻿using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Shared
{
    /// <summary>
    /// A base dialog component
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class FarmManagerDialogBase<T> : FarmComponentBase<T>
    {
        /// <summary>
        /// The MudBlazor dialog instance
        /// </summary>
        [CascadingParameter]
        protected MudDialogInstance _dialog { get; set; }

        /// <summary>
        /// The result to turn to the initizlizer
        /// </summary>
        protected object Result { get; set; } = true;

        /// <summary>
        /// A handler for saving
        /// </summary>
        /// <returns></returns>
        protected abstract Task HandleSave();

        /// <summary>
        /// Closes the dialog
        /// </summary>
        protected void TryCancel()
        {
            _dialog.Cancel();
        }

        /// <summary>
        /// Attempts to save using the provided save handler
        /// </summary>
        /// <returns></returns>
        protected async Task TrySave()
        {
            if (HandleSave != null)
            {
                try
                {
                    Store.SetLoading("DialogSave");

                    await HandleSave();

                    _dialog.Close(DialogResult.Ok(Result));
                }
                catch (Exception ex)
                {
                    Snackbar.Add(ex.Message, Severity.Error);
                }
                finally
                {
                    Store.ClearLoading("DialogSave");
                }
            }
        }
    }
}
