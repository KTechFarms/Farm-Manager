﻿using FarmManager.Web.Services.FarmManagerApiService;
using FarmManager.Web.State;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Shared
{
    /// <summary>
    /// A base class with common properties that are shared
    /// by all Farm Manager components and pages
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FarmComponentBase<T> : ComponentBase
    {
        /// <summary>
        /// A service to interact with the Farm Manager Api
        /// </summary>
        [Inject]
        protected IFarmManagerApiService ApiService { get; set; }

        /// <summary>
        /// A service to display snackbars to the screen
        /// </summary>
        [Inject]
        protected ISnackbar Snackbar { get; set; }

        /// <summary>
        /// A service to allow logging
        /// </summary>
        [Inject]
        protected ILogger<T> Logger { get; set; }

        /// <summary>
        /// The store for application state
        /// </summary>
        [Inject]
        protected Store Store { get; set; }

        /// <summary>
        /// Allows navigation within the application
        /// </summary>
        [Inject]
        protected NavigationManager NavigationManager { get; set; }
    }
}
