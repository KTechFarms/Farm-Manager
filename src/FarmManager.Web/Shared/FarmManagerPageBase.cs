﻿using FarmManager.Web.Components.ConfirmationDialog;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Shared
{
    /// <summary>
    /// A base page containing logic and properties shared by most Farm Manager Pages
    /// </summary>
    /// <typeparam name="ClassType">The class of the base razor component</typeparam>
    /// <typeparam name="EnumerableType">The class that is displayed in the table</typeparam>
    /// <typeparam name="DialogType">The dialog type that should be displayed when clicking the "Add" button</typeparam>
    public abstract class FarmManagerPageBase<ClassType, EnumerableType, DialogType>
        : FarmComponentBase<ClassType>
        where ClassType : ComponentBase
        where EnumerableType : class
        where DialogType : ComponentBase
    {
        /// <summary>
        /// The MudBlazor dialog service
        /// </summary>
        [Inject]
        protected IDialogService DialogService { get; set; }

        /// <summary>
        /// A title for the dialog
        /// </summary>
        protected virtual string? DialogTitle { get; set; }

        /// <summary>
        /// The root breadcrumb text
        /// </summary>
        protected virtual string? BreadcrumbRoot { get; set; }

        /// <summary>
        /// A list of breadcrumbs displayed at the top of the page
        /// </summary>
        protected List<BreadcrumbItem> Breadcrumbs { get; set; }

        /// <summary>
        /// A list of items to display in a table
        /// </summary>
        protected IEnumerable<EnumerableType> Items { get; set; } = new List<EnumerableType>();

        /// <summary>
        /// Parameters for an add item dialog
        /// </summary>
        protected DialogParameters DialogParameters { get; set; } = new();

        /// <summary>
        /// Options for an add item dialog
        /// </summary>
        protected DialogOptions DialogOptions { get; set; } = new DialogOptions
        {
            DisableBackdropClick = true,
            MaxWidth = MaxWidth.Medium
        };

        /// <summary>
        /// A handler for loading items
        /// </summary>
        /// <returns></returns>
        protected abstract Task LoadItems();

        /// <summary>
        /// A handler for deleting an item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected abstract Task DeleteItem(EnumerableType item);

        /// <summary>
        /// A handler for filtering visible items
        /// </summary>
        /// <param name="item"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        protected abstract bool HandleFilter(EnumerableType item, string searchText);

        protected override async Task OnInitializedAsync()
        {
            string root = BreadcrumbRoot ?? $"{typeof(EnumerableType).Name}s";

            Breadcrumbs = new()
            {
                new BreadcrumbItem(root, null, disabled: true)
            };

            await TryLoadItems();
        }

        /// <summary>
        /// Attempts to load items using the LoadItems function of the 
        /// class inheriting this
        /// </summary>
        /// <returns></returns>
        protected async Task TryLoadItems()
        {
            try
            {
                Store.SetLoading("LoadItems");

                await LoadItems();
            }
            catch (Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("LoadItems");
            }
        }

        /// <summary>
        /// Attempts to delete the given item sing the DeleteItem
        /// function of the class inheriting this
        /// </summary>
        /// <param name="item">The item to delete</param>
        /// <returns></returns>
        protected async Task TryDeleteItem(EnumerableType item)
        {
            try
            {
                Store.SetLoading("DeleteItem");

                await DeleteItem(item);
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("DeleteItem");
            }
        }

        /// <summary>
        /// Displays a dialog of the DialogType type
        /// </summary>
        /// <returns></returns>
        protected async Task HandleAddItem()
        {
            string title = DialogTitle ?? $"{typeof(EnumerableType).Name}";
            IDialogReference dialog = DialogService
                .Show<DialogType>($"Add {title}", DialogParameters, DialogOptions);

            DialogResult? result = await dialog.Result;

            if (!result.Cancelled)
            {
                await TryLoadItems();
                StateHasChanged();
            }
        }

        /// <summary>
        /// Displays a confirmation dialog and then attempts 
        /// to delete the selected item
        /// </summary>
        /// <param name="message">The message to display in the 
        /// confirmation dialog</param>
        /// <param name="item">The item that will be deleted</param>
        /// <returns></returns>
        protected async Task HandleDeleteItem(
            string message,
            EnumerableType item
        )
        {
            DialogParameters parameters = new()
            {
                { "Message", message },
                { "ConfirmColor", Color.Error },
                { "CancelColor", Color.Default },
                { "ConfirmText", "Delete" }
            };

            string title = DialogTitle ?? $"{typeof(EnumerableType).Name}";
            IDialogReference dialog = DialogService
                .Show<ConfirmationDialog>(
                    $"Delete {title}", 
                    parameters, 
                    DialogOptions
                );

            DialogResult? result = await dialog.Result;

            if(!result.Cancelled)
            {
                await TryDeleteItem(item);
                StateHasChanged();
            }
        }
    }
}
