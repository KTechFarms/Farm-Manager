﻿using FarmManager.Data.Entities.Operations;

namespace FarmManager.Web.Utilities.Validation
{
    public static class ProductValidator
    {
        /// <summary>
        /// Validates that the product fields are in the correct format prior 
        /// to submitting a request to the API
        /// </summary>
        /// <param name="product"></param>
        public static void ValidateProduct(Product product)
        {
            if (string.IsNullOrEmpty(product.Name) || string.IsNullOrEmpty(product.Description))
                throw new ApplicationException("Product name and description are required");

            if (product?.Variants?.Count < 1)
                throw new ApplicationException("At least one variant is required");

            foreach (ProductVariant variant in product?.Variants)
            {
                if (string.IsNullOrEmpty(variant.Name) || string.IsNullOrEmpty(variant.Sku))
                    throw new ApplicationException("Variant name and sku are required");

                if (variant.Price <= 0)
                    throw new ApplicationException("Purchase price must be greater than 0");

                if (variant.SalePrice <= 0)
                    throw new ApplicationException("Sale price must be greater than 0");

                if (
                    variant.TrackInventory
                    && (
                        variant.LowInventoryThreshold == null
                        || variant.HighInventoryThreshold == null
                    )
                )
                    throw new ApplicationException("Upper and lower limit required when tracking inventory");
            }
        }
    }
}
