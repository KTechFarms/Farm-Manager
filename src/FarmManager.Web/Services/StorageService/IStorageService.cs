﻿namespace FarmManager.Web.Services.StorageService
{
    /// <summary>
    /// An interface for interacting with storage
    /// </summary>
    public interface IStorageService
    {
        /// <summary>
        /// Retrieves the AzureAd token from storage
        /// </summary>
        /// <returns></returns>
        public Task<string> GetAADToken();

        /// <summary>
        /// Retrieves an item from storage
        /// </summary>
        /// <param name="key">The key to search for</param>
        /// <param name="cancellationToken">The cancellation token</param>
        /// <returns></returns>
        public ValueTask<string> GetItemAsync(string key, CancellationToken? cancellationToken = null);
    }
}
