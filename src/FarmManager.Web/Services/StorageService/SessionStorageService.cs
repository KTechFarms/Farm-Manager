﻿using Microsoft.JSInterop;

namespace FarmManager.Web.Services.StorageService
{
    /// <summary>
    /// A service for interacting with session storage
    /// </summary>
    public class SessionStorageService : IStorageService
    {
        /// <summary>
        /// The blazor js Runtime
        /// </summary>
        private readonly IJSRuntime _jsRuntime;

        public SessionStorageService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task<string> GetAADToken()
        {
            return await _jsRuntime.InvokeAsync<string>("getAADToken");
        }

        ValueTask<string> IStorageService.GetItemAsync(string key, CancellationToken? cancellationToken = null)
        {
            return _jsRuntime.InvokeAsync<string>("sessionStorage.getItem", key);
        }
    }
}
