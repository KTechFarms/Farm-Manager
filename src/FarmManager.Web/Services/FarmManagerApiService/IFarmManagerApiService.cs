﻿namespace FarmManager.Web.Services.FarmManagerApiService
{
    /// <summary>
    /// An interface for services interacting with the Farm Manager Api
    /// </summary>
    public interface IFarmManagerApiService
    {
        /// <summary>
        /// Make a request to the Farm Manager Api
        /// </summary>
        /// <typeparam name="T">The type of data that should be returned from the request</typeparam>
        /// <param name="method">The HTTP method to perform</param>
        /// <param name="endpoint">The endpoint that the request should go to</param>
        /// <param name="body">The body of the request</param>
        /// <param name="queryParams">Any url parameters for the request</param>
        /// <returns></returns>
        Task<T> Request<T>(
            HttpMethod method,
            string endpoint,
            object? body = null,
            Dictionary<string, string>? queryParameters = null
        );
    }
}
