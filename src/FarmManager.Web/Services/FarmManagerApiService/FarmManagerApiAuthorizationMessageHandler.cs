﻿using FarmManager.Web.Config;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.Extensions.Options;

namespace FarmManager.Web.Services.FarmManagerApiService
{
    /// <summary>
    /// An authorization handler for calling the Farm Manager API
    /// </summary>
    public class FarmManagerApiAuthorizationMessageHandler : AuthorizationMessageHandler
    {
        public FarmManagerApiAuthorizationMessageHandler(
            IAccessTokenProvider provider,
            NavigationManager navigationManager,
            IOptions<AppSettings> options
        ) : base(provider, navigationManager)
        {
            ConfigureHandler(
                authorizedUrls: new[] { options.Value.BaseApiUrl },
                scopes: new[] { options.Value.ApiScope });
        }
    }
}
