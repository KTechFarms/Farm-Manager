﻿using FarmManager.Data.DTOs;
using FarmManager.Web.Config;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;

namespace FarmManager.Web.Services.FarmManagerApiService
{
    /// <summary>
    /// A FarmManager.Web specific service for interacting with the Farm Manager Api
    /// </summary>
    public class FarmManagerApiService : IFarmManagerApiService
    {
        /// <summary>
        /// An HTTP client for making api requests
        /// </summary>
        private readonly HttpClient _httpClient;

        public FarmManagerApiService(IOptions<AppSettings> options, HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(options.Value.BaseApiUrl);
            _httpClient.DefaultRequestHeaders.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Make a request to the Farm Manager Api
        /// </summary>
        /// <typeparam name="T">The type of data that should be returned from the request</typeparam>
        /// <param name="method">The HTTP method to perform</param>
        /// <param name="endpoint">The endpoint that the request should go to</param>
        /// <param name="body">The body of the request</param>
        /// <param name="queryParams">Any url parameters for the request</param>
        /// <returns></returns>
        public async Task<T> Request<T>(
            HttpMethod method,
            string endpoint,
            object? body,
            Dictionary<string, string>? queryParams = null
        )
        {
            if (queryParams != null)
                endpoint = QueryHelpers.AddQueryString(endpoint, queryParams);

            HttpContent? content = null;
            if (body != null)
                content = new StringContent(
                    JsonConvert.SerializeObject(body), 
                    Encoding.UTF8, 
                    "application/json"
                );

            // TODO: Add policies here
            HttpResponseMessage? response = null;
            if (method == HttpMethod.Get)
                response = await _httpClient.GetAsync(endpoint);

            else if (method == HttpMethod.Post)
                response = await _httpClient.PostAsync(endpoint, content);

            else if (method == HttpMethod.Put)
                response = await _httpClient.PutAsync(endpoint, content);

            else if (method == HttpMethod.Patch)
                response = await _httpClient.PatchAsync(endpoint, content);

            else if (method == HttpMethod.Delete)
                response = await _httpClient.DeleteAsync(endpoint);

            else
                throw new ApplicationException("Invalid HttpMethod");

            return await HandleResponse<T>(response);
        }

        /// <summary>
        /// Takes an HttpResponse from the API and parses / retrieves the specified type
        /// </summary>
        /// <typeparam name="T">The type of data that should be returned from the request</typeparam>
        /// <param name="response">The HttpResponseMessage received from the Farm Manager Api</param>
        /// <returns></returns>
        private async Task<T> HandleResponse<T>(HttpResponseMessage response)
        {
            if(response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                FarmManagerResponse? typedResponse = JsonConvert.DeserializeObject<FarmManagerResponse>(responseBody);
                if (typedResponse == null)
                    throw new ApplicationException("Invalid Farm Manager Response");

                if (typedResponse.Status == ResponseStatus.Success)
                {
                    string responseData = JsonConvert.SerializeObject(typedResponse.Data);
                    T? typedResponseData = JsonConvert.DeserializeObject<T>(responseData);
                    if (typedResponseData == null)
                        throw new ApplicationException($"Data is not of type {typeof(T).Name}");

                    return typedResponseData;
                }
                else
                    throw new ApplicationException(typedResponse.Error);
            }
            else
                throw new ApplicationException($"Unable to make request! {response.StatusCode}: {response.ReasonPhrase}");
        }
    }
}
