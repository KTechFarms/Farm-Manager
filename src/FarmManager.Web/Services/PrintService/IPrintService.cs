﻿namespace FarmManager.Web.Services.PrintService
{
    /// <summary>
    /// An interface for interacting with label printers
    /// </summary>
    public interface IPrintService
    {
        /// <summary>
        /// Loads the currently available printers
        /// </summary>
        /// <returns></returns>
        Task<List<LabelPrinter>> GetPrintersAsync();

        /// <summary>
        /// Prints a label
        /// </summary>
        /// <param name="printerName">The name of the printer to print from</param>
        /// <param name="labelData">The data that should be printed</param>
        /// <returns></returns>
        Task PrintLabelAsync(string printerName, string labelData);
    }
}
