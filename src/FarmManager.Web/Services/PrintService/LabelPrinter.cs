﻿namespace FarmManager.Web.Services.PrintService
{
    /// <summary>
    /// A DTO representing a label printer
    /// </summary>
    public class LabelPrinter
    {
        /// <summary>
        /// Whether the printer is connected
        /// </summary>
        public bool IsConnected { get; set; }

        /// <summary>
        /// Whether the printer is local
        /// </summary>
        public bool IsLocal { get; set; }

        /// <summary>
        /// Whether the printer is twin turbo
        /// </summary>
        public bool IsTwinTurbo { get; set; }

        /// <summary>
        /// The model of the printer
        /// </summary>
        public string ModelName { get; set; }

        /// <summary>
        /// The printer name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of printer
        /// </summary>
        public string PrinterType { get; set; }
    }
}
