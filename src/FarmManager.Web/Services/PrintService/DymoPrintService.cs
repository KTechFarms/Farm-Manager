﻿using Microsoft.JSInterop;

namespace FarmManager.Web.Services.PrintService
{
    /// <summary>
    /// A print service for interacting with Dymo Label printers
    /// </summary>
    public class DymoPrintService : IPrintService
    {
        private readonly IJSRuntime _jsRuntime;

        public DymoPrintService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task<List<LabelPrinter>> GetPrintersAsync()
        {
            return await _jsRuntime.InvokeAsync<List<LabelPrinter>>("getDymoPrinters");
        }

        public async Task PrintLabelAsync(string printerName, string labelData)
        {
            await _jsRuntime.InvokeVoidAsync("printLabel", printerName, labelData);
        }
    }
}
