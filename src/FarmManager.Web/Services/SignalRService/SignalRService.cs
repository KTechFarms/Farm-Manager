﻿using FarmManager.Common.Consts;
using FarmManager.Web.Config;
using FarmManager.Web.Services.StorageService;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Options;
using System.Collections.Concurrent;

namespace FarmManager.Web.Services.SignalRService
{
    /// <summary>
    /// An implementation for the ISignalRService Interface
    /// </summary>
    public class SignalRService : ISignalRService
    {
        /// <summary>
        /// The current storage service
        /// </summary>
        private readonly IStorageService _storageService;

        /// <summary>
        /// The base uri of the SignalR server
        /// </summary>
        private readonly string _baseUri;

        private ConcurrentDictionary<string, HubConnection> _hubConnections
            = new ConcurrentDictionary<string, HubConnection>();

        public SignalRService(
            IStorageService storageService,
            IOptions<AppSettings> options
        )
        {
            _storageService = storageService;
            _baseUri = options.Value.BaseApiUrl;
        }

        public async Task InitializeAsync(string connectionName, string hub)
        {
            string token = await _storageService.GetAADToken();
            HubConnection connection = new HubConnectionBuilder()
                .WithUrl($"{_baseUri}/{hub}?access_token={token}")
                .Build();

            if (!_hubConnections.TryAdd(connectionName, connection))
                throw new ApplicationException("Unable to add SignalR connection");
        }

        public async Task OpenAsync(string connectionName)
        {
            HubConnection connection = GetConnection(connectionName);
            await connection.StartAsync();
        }

        public async Task CloseAsync(string connectionName)
        {
            HubConnection? connection = GetConnection(connectionName);
            if (_hubConnections.TryRemove(connectionName, out connection))
                await connection.StopAsync();
        }

        public async Task SubscribeAsync(string connectionName, string groupId)
        {
            HubConnection connection = GetConnection(connectionName);
            await connection.InvokeAsync(HubMethods.Common.Subscribe, groupId);
        }

        public async Task UnsubscribeAsync(string connectionName, string groupId)
        {
            HubConnection connection = GetConnection(connectionName);
            await connection.InvokeAsync(HubMethods.Common.Unsubscribe, groupId);
        }

        public async Task InvokeAsync(string connectionName, string methodName, params object[] args)
        {
            HubConnection connection = GetConnection(connectionName);
            if (args == null)
                await connection.InvokeAsync(methodName);
            else
                await connection.InvokeAsync(methodName, args);
        }

        public void Register(string connectionName, string methodName, Action handler)
        {
            HubConnection connection = GetConnection(connectionName);
            connection.On(methodName, handler);
        }

        public void Register<T>(string connectionName, string methodName, Action<T> handler)
        {
            HubConnection connection = GetConnection(connectionName);
            connection.On(methodName, handler);
        }

        public void Register<T1, T2>(string connectionName, string methodName, Action<T1, T2> handler)
        {
            HubConnection connection = GetConnection(connectionName);
            connection.On(methodName, handler);
        }

        private HubConnection GetConnection(string connectionName)
        {
            if (!_hubConnections.TryGetValue(connectionName, out HubConnection? connection))
                throw new ApplicationException("Unable to retrieve SignalR connection");

            return connection;
        }
    }
}
