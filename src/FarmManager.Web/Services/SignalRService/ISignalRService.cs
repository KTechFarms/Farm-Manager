﻿namespace FarmManager.Web.Services.SignalRService
{
    /// <summary>
    /// An interface for interacting with SignalR
    /// </summary>
    public interface ISignalRService
    {
        /// <summary>
        /// Intiialize a SignalR hub connection
        /// </summary>
        /// <param name="connectionName">the name of the connection</param>
        /// <param name="hub">The hub to connect to</param>
        /// <returns></returns>
        public Task InitializeAsync(string connectionName, string hub);

        /// <summary>
        /// Opens and begins a connection with the hub
        /// </summary>
        /// <param name="connectionName">The connection to start</param>
        /// <returns></returns>
        public Task OpenAsync(string connectionName);

        /// <summary>
        /// Closes the connection with a hub
        /// </summary>
        /// <param name="connectionName">The connection to close</param>
        /// <returns></returns>
        public Task CloseAsync(string connectionName);

        /// <summary>
        /// Subscribes a connection to a particular group
        /// </summary>
        /// <param name="connectionName">The connection that should be subscribed to the group</param>
        /// <param name="groupId">The group to subscribe to</param>
        /// <returns></returns>
        public Task SubscribeAsync(string connectionName, string groupId);

        /// <summary>
        /// Unsubscribes a connection from a particular group
        /// </summary>
        /// <param name="connectionName">The connection that should be unsubscribed</param>
        /// <param name="groupId">The group to unsubscribe from</param>
        /// <returns></returns>
        public Task UnsubscribeAsync(string connectionName, string groupId);

        /// <summary>
        /// Invokes a method on the hub for the specified connection
        /// </summary>
        /// <param name="connectionName">The connection that should invoke the method</param>
        /// <param name="methodName">The name of the method to invoke</param>
        /// <param name="args">Any arguments required by the method being invoked</param>
        /// <returns></returns>
        public Task InvokeAsync(string connectionName, string methodName, params object[] args);

        /// <summary>
        /// Registers a handler for the specified method of the provided connection
        /// </summary>
        /// <param name="connectionName">The connection to register a handler for</param>
        /// <param name="methodName">The method name that should be listed for</param>
        /// <param name="handler">The handler that is called when the method name is invoked</param>
        public void Register(string connectionName, string methodName, Action handler);

        /// <summary>
        /// Registers a handler for the specified method of the provided connection
        /// </summary>
        /// <typeparam name="T">An input type for the handler</typeparam>
        /// <param name="connectionName">The connection to register a handler for</param>
        /// <param name="methodName">The method name that should be listed for</param>
        /// <param name="handler">The handler that is called when the method name is invoked</param>
        public void Register<T>(string connectionName, string methodName, Action<T> handler);

        /// <summary>
        /// Registers a handler for the specified method of the provided connection
        /// </summary>
        /// <typeparam name="T1">An input type for the handler</typeparam>
        /// <typeparam name="T2">An input type for the handler</typeparam>
        /// <param name="connectionName">The connection to register a handler for</param>
        /// <param name="methodName">The method name that should be listed for</param>
        /// <param name="handler">The handler that is called when the method name is invoked</param>
        public void Register<T1, T2>(string connectionName, string methodName, Action<T1, T2> handler);
    }
}
