﻿using FarmManager.Data.DTOs.Devices;
using FarmManager.Web.Components.RegisterDeviceDialog;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Pages.Devices
{
    /// <summary>
    /// A page for viewing the devices connected to the Farm Manager
    /// system
    /// </summary>
    public class DevicesBase : FarmManagerPageBase<DevicesBase, DeviceDTO, RegisterDeviceDialog>
    {
        /// <summary>
        /// The dialog title to display
        /// </summary>
        protected override string? DialogTitle { get; set; } = "Device";

        /// <summary>
        /// The base breadcrumb to display
        /// </summary>
        protected override string? BreadcrumbRoot { get; set; } = "Devices";

        /// <summary>
        /// Handles filtering the displayed devices when changing 
        /// the text in the search bar
        /// </summary>
        /// <param name="device">A device object</param>
        /// <returns></returns>
        protected override bool HandleFilter(DeviceDTO device, string searchText)
        {
            if (
                string.IsNullOrWhiteSpace(searchText)
                || ((!string.IsNullOrEmpty(device.DeviceId) && device.DeviceId.Contains(searchText))
                || (!string.IsNullOrEmpty(device.Name) && device.Name.Contains(searchText))
                || (!string.IsNullOrEmpty(device.Model) && device.Model.Contains(searchText))
                || (!string.IsNullOrEmpty(device.SerialNumber) && device.SerialNumber.Contains(searchText)))
            )
                return true;

            return false;
        }

        /// <summary>
        /// Loads the devices
        /// </summary>
        /// <returns></returns>
        protected override async Task LoadItems()
        {
            Items = await ApiService.Request<List<DeviceDTO>>(
                HttpMethod.Get,
                "device"
            );
        }

        /// <summary>
        /// Devices cannot currently be deleted. Functionality
        /// will be added later
        /// </summary>
        /// <param name="device">The device to delete</param>
        /// <returns></returns>
        protected override Task DeleteItem(DeviceDTO device)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Navigates to the devices/{deviceId} page when clicking on a row
        /// in the devices table
        /// </summary>
        /// <param name="args"></param>
        protected void HandleRowClick(TableRowClickEventArgs<DeviceDTO> args)
        {
            NavigationManager.NavigateTo($"devices/{args.Item.DeviceId}");
        }
    }
}
