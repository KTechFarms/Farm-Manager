﻿using FarmManager.Data.Entities.System;
using FarmManager.Web.Shared;
using MudBlazor;

namespace FarmManager.Web.Pages.Settings
{
    /// <summary>
    /// A page for displaying Farm Manager settings
    /// </summary>
    public class SettingsBase : FarmComponentBase<SettingsBase>
    {
        /// <summary>
        /// Breadcrumbs displayeed at the top of the page
        /// </summary>
        protected List<BreadcrumbItem> Breadcrumbs { get; set; }

        /// <summary>
        /// A list of current settings
        /// </summary>
        protected IEnumerable<IGrouping<string, Setting>> Settings { get; set; }

        /// <summary>
        /// A collection of settings and their associated values
        /// </summary>
        protected Dictionary<string, string?> SettingValues { get; set; } = new();

        protected override async Task OnInitializedAsync()
        {
            Breadcrumbs = new()
            {
                new BreadcrumbItem("Settings", null, disabled: true)
            };

            try
            {
                Store.SetLoading("Settings");

                await LoadSettings();                
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("Settings");
            }
        }

        /// <summary>
        /// Loads the current settings
        /// </summary>
        /// <returns></returns>
        private async Task LoadSettings()
        {
            List<Setting> settings = await ApiService.Request<List<Setting>>(
                HttpMethod.Get,
                "settings"
            );

            SettingValues.Clear();
            Settings = settings.GroupBy(s => s.Category?.Name ?? string.Empty);
            foreach (IGrouping<string, Setting> group in Settings)
                foreach (Setting setting in group)
                    SettingValues[setting.Name] = setting.Value;
        }

        /// <summary>
        /// Saves the settings
        /// </summary>
        /// <returns></returns>
        protected async Task HandleSave()
        {
            try
            {
                ValidateSettings();

                Store.SetLoading("SettingsSave");

                await ApiService.Request<List<Setting>>(
                    HttpMethod.Post,
                    "settings",
                    body: SettingValues
                );

                await LoadSettings();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("SettingsSave");
            }
        }

        /// <summary>
        /// Validates that all of the settings are valid
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateSettings()
        {
            foreach (IGrouping<string, Setting> group in Settings)
                foreach (Setting setting in group)
                    if (
                        !SettingValues.TryGetValue(setting.Name, out string? value)
                        || string.IsNullOrEmpty(value)
                    )
                        throw new ApplicationException($"{group.Key}.{setting.Label} must have a value");
        }
    }
}
