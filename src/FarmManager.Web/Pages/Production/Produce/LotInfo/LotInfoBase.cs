﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Production;
using FarmManager.Web.Components.PromoteLotDialog;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using Newtonsoft.Json;

namespace FarmManager.Web.Pages.Production.Produce.LotInfo
{
    /// <summary>
    /// A page for displaying information about a selected lot
    /// </summary>
    public class LotInfoBase : FarmComponentBase<LotInfoBase>
    {
        /// <summary>
        /// The MudBlazor dialog service
        /// </summary>
        [Inject]
        private IDialogService _dialogService { get; set; }

        /// <summary>
        /// The lot number to display information about
        /// </summary>
        [Parameter]
        public string LotNumber { get; set; }

        /// <summary>
        /// Breadcrumbs displayed at the top of the page
        /// </summary>
        protected List<BreadcrumbItem> Breadcrumbs { get; set; }

        /// <summary>
        /// Data about the currently selected lot
        /// </summary>
        protected LotDTO? LotData { get; set; }

        protected override async void OnInitialized()
        {
            Breadcrumbs = new()
            {
                new BreadcrumbItem("Produce", "/produce"),
                new BreadcrumbItem("Lots", "/produce"),
                new BreadcrumbItem(LotNumber, null, disabled: true)
            };

            if(!string.IsNullOrEmpty(NavigationManager.HistoryEntryState))
            {
                try
                {
                    LotData = JsonConvert.DeserializeObject<LotDTO>(NavigationManager.HistoryEntryState);
                }
                catch(Exception ex) { }
            }
            else
            {
                // TODO: Add filtering and query support so we aren't loading everything
                try
                {
                    Store.SetLoading("LotData");

                    LotData = (await ApiService.Request<List<LotDTO>>(
                        HttpMethod.Get,
                        "lot"
                     )).FirstOrDefault(l => l.LotNumber == LotNumber);

                    StateHasChanged();
                }
                catch (Exception ex) 
                {
                    Snackbar.Add(ex.Message, Severity.Error);
                }
                finally 
                {
                    Store.ClearLoading("LotData");
                }
            }
        }

        protected void IncrementNoteCount()
        {
            if(LotData != null)
            {
                if (LotData.NoteCount != default)
                    LotData.NoteCount++;
                else
                    LotData.NoteCount = 1;

                StateHasChanged();
            }
        }

        /// <summary>
        /// Displays a dialog to promote the lot to the next stage
        /// </summary>
        /// <returns></returns>
        protected async Task PromoteLot()
        {
            if(LotData == null)
            {
                Snackbar.Add("Unable to promote lot", Severity.Warning);
                return;
            }

            DialogParameters parameters = new()
            {
                { "LotId", LotData.Id },
                { "LotNumber", LotNumber }
            };

            DialogOptions options = new()
            {
                DisableBackdropClick = true,
                MaxWidth = MaxWidth.Medium
            };

            IDialogReference dialog = _dialogService
                .Show<PromoteLotDialog>($"Promote Lot {LotNumber}", parameters, options);

            DialogResult? result = await dialog.Result;
            if (
                !result.Cancelled 
                && result.Data != null
                && result.Data is Lot updatedLot
            )
            {
                LotData.Status = updatedLot?.Status?.Name ?? LotData.Status;
                LotData.ProductStatus = updatedLot?.Status?.Label ?? LotData.ProductStatus;
                StateHasChanged();
            }
        }
    }
}
