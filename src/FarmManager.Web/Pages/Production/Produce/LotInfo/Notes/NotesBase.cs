﻿using FarmManager.Data.Entities.Generic;
using FarmManager.Web.Components.AddNoteDialog;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;

namespace FarmManager.Web.Pages.Production.Produce.LotInfo.Notes
{
    /// <summary>
    /// A page to display notes for the currently selected lot
    /// </summary>
    public class NotesBase : FarmManagerPageBase<NotesBase, Note, AddNoteDialog>
    {
        /// <summary>
        /// The lot number to display notes about
        /// </summary>
        [Parameter]
        public string LotNumber { get; set; }

        /// <summary>
        /// A function to call once a note has been saved so that
        /// additional logic can be performed
        /// </summary>
        [Parameter]
        public Action OnNoteSave { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();

            DialogParameters.Add("OnSave", SaveNote);
            DialogParameters.Add("OnSaveParams", LotNumber);
        }

        /// <summary>
        /// Loads the lot's current notes
        /// </summary>
        /// <returns></returns>
        protected override async Task LoadItems()
        {
            Items = (await ApiService.Request<List<Note>>(
                HttpMethod.Get,
                $"lot/{LotNumber}/note"
            )).OrderByDescending(n => n.Created);
        }

        /// <summary>
        /// Saves a new note to the database
        /// </summary>
        /// <param name="note">The text of the note to save</param>
        /// <param name="lotDetails">A string containing the lot number to 
        /// associate the note with</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private async Task SaveNote(string note, object? lotDetails)
        {
            string? lotNumber = lotDetails as string;

            if (string.IsNullOrEmpty(lotNumber))
                throw new Exception("Missing Lot Number");

            await ApiService.Request<Note>(
                HttpMethod.Post,
                $"lot/{lotNumber}/note",
                body: note
            );

            if (OnNoteSave != null)
                OnNoteSave();
        }

        /// <summary>
        /// Filters notes based on the current search text
        /// </summary>
        /// <param name="item"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        protected override bool HandleFilter(Note item, string searchText)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Notes cannot be deleted, so this functionality is not implemented
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override Task DeleteItem(Note item)
        {
            throw new NotImplementedException();
        }
    }
}
