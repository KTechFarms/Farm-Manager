﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Web.Components.AddBatchDialog;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Pages.Production.Produce.LotInfo.Batches
{
    /// <summary>
    /// A page to display information about the lot's batches
    /// </summary>
    public class BatchesBase : FarmManagerPageBase<BatchesBase, BatchDTO, AddBatchDialog>
    {
        /// <summary>
        /// The lot number to display information about
        /// </summary>
        [Parameter]
        public string LotNumber { get; set; }

        /// <summary>
        /// Information about the currently selected lot
        /// </summary>
        [Parameter]
        public LotDTO? LotData { get; set; }

        /// <summary>
        /// The dialog title for creating a new batch
        /// </summary>
        protected override string? DialogTitle { get; set; } = "Batch";

        protected override void OnInitialized()
        {
            base.OnInitialized();

            DialogParameters.Add("LotNumber", LotNumber);
        }

        /// <summary>
        /// Loads batches for the currently selected lot
        /// </summary>
        /// <returns></returns>
        protected override async Task LoadItems()
        {
            Items = await ApiService.Request<List<BatchDTO>>(
                HttpMethod.Get,
                $"lot/{LotNumber}/batches"
            );
        }

        /// <summary>
        /// Filters the batches based on the current search text
        /// </summary>
        /// <param name="batch">The batch to determine visibility of</param>
        /// <param name="searchText">The text to search the batch for</param>
        /// <returns></returns>
        protected override bool HandleFilter(BatchDTO batch, string searchText)
        {
            if (
                string.IsNullOrWhiteSpace(searchText)
                || (batch.BatchNumber.ToString().Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(batch.Status) && batch.Status.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(batch.Location) && batch.Location.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (batch.ItemCount.ToString().Contains(searchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }

        /// <summary>
        /// Navigates to the harvest page for the selected batch
        /// </summary>
        /// <param name="args">The row click event</param>
        protected void HandleRowClick(TableRowClickEventArgs<BatchDTO> args)
        {
            if (args.Item.Status == "Open")
                NavigationManager.NavigateTo($"/produce/{LotNumber}/batches/{args.Item.BatchNumber}");
        }

        /// <summary>
        /// Batches cannot be deleted, so this functionality is not implemented
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override Task DeleteItem(BatchDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
