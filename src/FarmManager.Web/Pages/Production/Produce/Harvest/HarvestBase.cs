﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Production;
using FarmManager.Web.Services.PrintService;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Pages.Production.Produce.Harvest
{
    /// <summary>
    /// A page used for harvesting produce
    /// </summary>
    public class HarvestBase : FarmComponentBase<HarvestBase>
    {
        /// <summary>
        /// The lot number being harvested
        /// </summary>
        [Parameter]
        public string LotNumber { get; set; }

        /// <summary>
        /// The batch being harvested
        /// </summary>
        [Parameter]
        public int BatchNumber { get; set; }

        /// <summary>
        /// Breadcrumbs displayed at the top of the page
        /// </summary>
        protected List<BreadcrumbItem> Breadcrumbs { get; set; } = new();

        /// <summary>
        /// A list of available label printers
        /// </summary>
        protected List<LabelPrinter> Printers { get; set; } = new();

        /// <summary>
        /// Information about the current batch and product
        /// </summary>
        protected BatchProductDataDTO ProductData { get; set; } = new();

        /// <summary>
        /// The selected label printer
        /// </summary>
        protected string? SelectedPrinter { get; set; }

        /// <summary>
        /// The tare weight of the product
        /// </summary>
        protected double? TareWeight { get; set; }

        /// <summary>
        /// The gross weight of the product
        /// </summary>
        protected double? GrossWeight { get; set; }

        /// <summary>
        /// The label weight of the product
        /// </summary>
        protected double? LabelWeight { get; set; }

        /// <summary>
        /// The quantity of items being harvested
        /// </summary>
        protected int Quantity { get; set; } = 1;

        protected override async Task OnInitializedAsync()
        {
            Breadcrumbs = new()
            {
                new BreadcrumbItem("Produce", "/produce"),
                new BreadcrumbItem("Lots", "/produce"),
                new BreadcrumbItem(LotNumber, $"/produce/{LotNumber}"),
                new BreadcrumbItem(BatchNumber.ToString(), null, disabled: true)
            };

            Printers = new()
            {
                new LabelPrinter { Name = "None" }
            };
            SelectedPrinter = Printers.FirstOrDefault()?.Name;

            // TODO: Load Printers
            await Task.WhenAll(LoadProductData());
        }

        /// <summary>
        /// Loads the product data for the harvest
        /// </summary>
        /// <returns></returns>
        private async Task LoadProductData()
        {
            try
            {
                Store.SetLoading("ProductData");

                ProductData = await ApiService.Request<BatchProductDataDTO>(
                    HttpMethod.Get,
                    $"lot/{LotNumber}/batches/{BatchNumber}"
                );

                StateHasChanged();
            }
            catch (Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("ProductData");
            }
        }

        /// <summary>
        /// Changes the quantity if the value is above 0
        /// </summary>
        /// <param name="value"></param>
        protected void HandleQuantityChange(int value)
        {
            if (value > 0)
                Quantity = value;
        }

        /// <summary>
        /// Increments the quantity
        /// </summary>
        protected void HandleIncrement()
        {
            Quantity++;
        }

        /// <summary>
        /// Decrements the quantity if the value is above 1
        /// </summary>
        protected void HandleDecrement()
        {
            if (Quantity > 1)
                Quantity--;
        }

        /// <summary>
        /// Navigates back to the lot information page
        /// </summary>
        protected void HandleCancel()
        {
            NavigateToLotInfo();
        }

        /// <summary>
        /// Saves the current production items
        /// </summary>
        /// <returns></returns>
        protected async Task HandleSave()
        {
            try
            {
                Store.SetLoading("ProductionItem");

                List<ProductionItem> items = await ApiService.Request<List<ProductionItem>>(
                    HttpMethod.Post,
                    $"lot/{LotNumber}/batch/{BatchNumber}",
                    body: new CreateProductionItemDTO
                    {
                        Quantity = Quantity,
                        GrossWeight = GrossWeight,
                        TareWeight = TareWeight,
                        LabelWeight = LabelWeight
                    }
                );

                GrossWeight = null;

                await LoadProductData();

            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("ProductionItem");
            }
        }

        /// <summary>
        /// Closes the batch that is currently being harvested
        /// </summary>
        /// <returns></returns>
        protected async Task HandleClose()
        {
            try
            {
                Store.SetLoading("CloseBatch");

                await ApiService.Request<Batch>(
                    HttpMethod.Patch,
                    $"lot/{LotNumber}/batch/{BatchNumber}"
                );

                NavigateToLotInfo();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("CloseBatch");
            }
        }

        /// <summary>
        /// Navigates back to the lot information page
        /// </summary>
        private void NavigateToLotInfo()
        {
            NavigationManager.NavigateTo($"/produce/{LotNumber}");
        }
    }
}
