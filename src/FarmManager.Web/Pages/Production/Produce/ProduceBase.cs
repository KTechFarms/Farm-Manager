﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Generic;
using FarmManager.Web.Components.AddLotDialog;
using FarmManager.Web.Components.AddNoteDialog;
using FarmManager.Web.Components.PromoteLotDialog;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using Newtonsoft.Json;

namespace FarmManager.Web.Pages.Production.Produce
{
    /// <summary>
    /// A page for displaying produce information
    /// </summary>
    public class ProduceBase : FarmManagerPageBase<ProduceBase, LotDTO, AddLotDialog>
    {
        /// <summary>
        /// The title for the add dialog
        /// </summary>
        protected override string? DialogTitle { get; set; } = "Lot";

        /// <summary>
        /// The root breadcrumb displayed at the top of the page
        /// </summary>
        protected override string? BreadcrumbRoot { get; set; } = "Produce";

        /// <summary>
        /// The date range for which lots are shown
        /// </summary>
        protected DateRange ProduceDateRange { get; set; } =
            new DateRange(DateTime.Now.Subtract(TimeSpan.FromDays(90)), DateTime.Now);

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();

            Breadcrumbs = new()
            {
                new BreadcrumbItem(BreadcrumbRoot, null, disabled: true),
                new BreadcrumbItem("Lots", null, disabled: true)
            };
        }

        /// <summary>
        /// Loads any existing lots
        /// </summary>
        /// <returns></returns>
        protected override async Task LoadItems()
        {
            DateTimeOffset beginDate = DateTimeOffset.Now.Subtract(TimeSpan.FromDays(30));
            DateTimeOffset endDate = DateTimeOffset.Now;

            if (ProduceDateRange.Start.HasValue)
                beginDate = ProduceDateRange.Start.Value.Date;
            if (ProduceDateRange.End.HasValue)
                endDate = ProduceDateRange.End.Value.Date;

            // TODO: Filtering needs to happen on the API. Implement
            // Paging & Querying
            Items = (await ApiService.Request<List<LotDTO>>(
                HttpMethod.Get,
                "lot"
            ))
            .Where(l => 
                l.CreatedDate >= beginDate
                && l.CreatedDate <= endDate
            )
            .OrderBy(l => l.LotNumber);
        }

        /// <summary>
        /// Updates the date range based on what the user selected, 
        /// and reloads items
        /// </summary>
        /// <param name="selectedDates"></param>
        /// <returns></returns>
        protected async Task RefreshItems(DateRange selectedDates)
        {
            ProduceDateRange = selectedDates;
            await TryLoadItems();
            StateHasChanged();
        }

        /// <summary>
        /// Promotes a lot to the next stage in it's cycle.
        /// </summary>
        /// <returns></returns>
        protected async Task PromoteLot(int lotId, string lotNumber)
        {
            string title = $"Promote Lot {lotNumber}";
            DialogParameters parameters = new DialogParameters
            {
                { "LotId", lotId },
                { "LotNumber", lotNumber }
            };

            IDialogReference dialog = DialogService
                .Show<PromoteLotDialog>(title, parameters, DialogOptions);

            DialogResult? result = await dialog.Result;
            if(!result.Cancelled)
            {
                await TryLoadItems();
                StateHasChanged();
            }
        }

        /// <summary>
        /// Displays a dialog for creating a new note for the chosen lot
        /// </summary>
        /// <param name="lotNumber">The lot number to associate a note with</param>
        /// <returns></returns>
        protected async Task HandleAddNote(string? lotNumber)
        {
            if (string.IsNullOrEmpty(lotNumber))
                return;

            string title = $"Add Note (Lot {lotNumber})";
            DialogParameters parameters = new DialogParameters
            {
                { "OnSave", SaveNote },
                { "OnSaveParams", lotNumber }
            };
            IDialogReference dialog = DialogService
                .Show<AddNoteDialog>(title, parameters, DialogOptions);

            await dialog.Result;
        }

        /// <summary>
        /// Saves a note to th ecurrently selected lot
        /// </summary>
        /// <param name="note">The text of the note to save</param>
        /// <param name="lotDetails">An object representing the lot number</param>
        /// <returns></returns>
        protected async Task SaveNote(string note, object? lotDetails)
        {
            string? lotNumber = lotDetails as string;

            if (string.IsNullOrEmpty(lotNumber))
                throw new Exception("Missing Lot Number");

            await ApiService.Request<Note>(
                HttpMethod.Post,
                $"lot/{lotNumber}/note",
                body: note
            );
        }


        /// <summary>
        /// Filters visible lots based on the current search text
        /// </summary>
        /// <param name="lot">The lot to determine visibility of</param>
        /// <param name="searchText">The text to search the lot for</param>
        /// <returns></returns>
        protected override bool HandleFilter(LotDTO lot, string searchText)
        {
            if (
                string.IsNullOrWhiteSpace(searchText)
                || (lot.LotNumber.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(lot.Product) && lot.Product.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(lot.Location) && lot.Location.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(lot.ProductStatus) && lot.ProductStatus.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (lot.CreatedDate.HasValue && lot.CreatedDate.Value.ToString().Contains(searchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }

        /// <summary>
        /// Navigates to the lot information page for the selected lot
        /// </summary>
        /// <param name="args">The row click event</param>
        protected void HandleRowClick(TableRowClickEventArgs<LotDTO> args)
        {
            NavigationManager.NavigateTo(
                $"/produce/{args.Item.LotNumber}",
                new NavigationOptions
                {
                    HistoryEntryState = JsonConvert.SerializeObject(args.Item)
                }
            );
        }

        /// <summary>
        /// Lots cannot be deleted, so this functionality is not implemented
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override Task DeleteItem(LotDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
