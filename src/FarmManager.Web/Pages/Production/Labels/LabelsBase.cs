﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Web.Components.AddLabelDialog;
using FarmManager.Web.Shared;
using MudBlazor;

namespace FarmManager.Web.Pages.Production.Labels
{
    /// <summary>
    /// A page for managing product labels
    /// </summary>
    public class LabelsBase : FarmManagerPageBase<LabelsBase, LabelDTO, AddLabelDialog>
    {
        /// <summary>
        /// The title of the add dialog
        /// </summary>
        protected override string? DialogTitle { get; set; } = "Label";

        /// <summary>
        /// The root breadcrumb at the top of the page
        /// </summary>
        protected override string? BreadcrumbRoot { get; set; } = "Labels";

        /// <summary>
        /// A list of currently existing labels
        /// </summary>
        protected IEnumerable<LabelDTO> Labels { get; set; } = new List<LabelDTO>();
        
        /// <summary>
        /// Defines grouping for the label table
        /// </summary>
        protected TableGroupDefinition<LabelDTO> GroupDefinition { get; set; } = new()
        {
            GroupName = string.Empty,
            Indentation = false,
            Expandable = true,
            IsInitiallyExpanded = false,
            Selector = (l) => l.LabelId
        };

        /// <summary>
        /// Text to search the labels for
        /// </summary>
        protected string SearchText { get; set; }

        /// <summary>
        /// Loads the current labels
        /// </summary>
        /// <returns></returns>
        protected override async Task LoadItems()
        {
            Labels = await ApiService.Request<List<LabelDTO>>(
                HttpMethod.Get,
                "label"
            );
        }

        /// <summary>
        /// Filters labels based on the current search text
        /// </summary>
        /// <param name="item">The label to determine visibility for</param>
        /// <returns></returns>
        protected bool Filter(LabelDTO item)
        {
            if (
                string.IsNullOrWhiteSpace(SearchText)
                || (!string.IsNullOrEmpty(item.LabelName) && item.LabelName.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(item.ProductVariantName) && item.ProductVariantName.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (item.Version != null && item.Version.ToString().Contains(SearchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }

        protected override bool HandleFilter(LabelDTO item, string searchText)
        {
            throw new NotImplementedException();
        }

        protected override Task DeleteItem(LabelDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
