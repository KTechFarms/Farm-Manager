﻿using FarmManager.Common.Helpers;
using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Components.DisposeInventoryDialog;
using FarmManager.Web.Components.ReceiveInventoryDialog;
using FarmManager.Web.Components.TransferInventoryDialog;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MudBlazor;

namespace FarmManager.Web.Pages.Production.Inventory
{
    /// <summary>
    /// A page for managing inventory
    /// </summary>
    public class InventoryBase: FarmComponentBase<InventoryBase>
    {
        /// <summary>
        /// The blazor js runtime
        /// </summary>
        [Inject]
        private IJSRuntime _jsRuntime { get; set; }

        /// <summary>
        /// The MudBlazor dialog service
        /// </summary>
        [Inject]
        private IDialogService _dialogService { get; set; }

        /// <summary>
        /// Breadcrumbs displayed at the top of the page
        /// </summary>
        protected List<BreadcrumbItem> Breadcrumbs { get; set; } = new();

        /// <summary>
        /// A list of avialable business locations
        /// </summary>
        protected List<BusinessLocation> BusinessLocations { get; set; } = new();

        /// <summary>
        /// A list of available farm locations
        /// </summary>
        protected List<FarmLocation> FarmLocations { get; set; } = new();

        /// <summary>
        /// A list of inventory items at the selected location
        /// </summary>
        protected List<InventoryItem> Inventory { get; set; } = new();

        /// <summary>
        /// Text to search the inventory for
        /// </summary>
        protected string SearchText { get; set; }

        /// <summary>
        /// A list of inventory items that have been selected
        /// </summary>
        protected HashSet<InventoryItem> SelectedInventory { get; set; } = new();

        /// <summary>
        /// The id of the selected business location
        /// </summary>
        protected int? SelectedBusinessLocation { get; set; }

        /// <summary>
        /// The id of the selected farm location
        /// </summary>
        protected int? SelectedFarmLocation { get; set; }

        /// <summary>
        /// Options for the inventory transfer dialog
        /// </summary>
        protected DialogOptions DialogOptions { get; set; } = new DialogOptions
        {
            DisableBackdropClick = true,
            MaxWidth = MaxWidth.Medium
        };

        protected override async Task OnInitializedAsync()
        {
            Breadcrumbs = new()
            {
                new BreadcrumbItem("Inventory", null, disabled: true)
            };

            try
            {
                Store.SetLoading("InventoryLocations");

                await Task.WhenAll(
                    LoadBusinessLocations(),
                    LoadFarmLocations()
                );

                StateHasChanged();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("InventoryLocations");
            }
        }

        /// <summary>
        /// Loads the available business locations
        /// </summary>
        /// <returns></returns>
        private async Task LoadBusinessLocations()
        {
            BusinessLocations = await ApiService.Request<List<BusinessLocation>>(
                HttpMethod.Get,
                "business-location"
            );

            SelectedBusinessLocation = BusinessLocations.FirstOrDefault()?.Id ?? 0;
        }

        /// <summary>
        /// Loads the available farm locations
        /// </summary>
        /// <returns></returns>
        private async Task LoadFarmLocations()
        {
            FarmLocations = await ApiService.Request<List<FarmLocation>>(
                HttpMethod.Get,
                "location"
            );
        }

        /// <summary>
        /// Loads inventory for the selcted locations
        /// </summary>
        /// <returns></returns>
        private async Task LoadInventory()
        {
            try
            {
                Store.SetLoading("Inventory");

                Inventory = await ApiService.Request<List<InventoryItem>>(
                    HttpMethod.Get,
                    $"inventory/{SelectedFarmLocation}"
                );
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("Inventory");
            }
        }

        /// <summary>
        /// Determines whether or not the receive inventory button
        /// can be utilized
        /// </summary>
        /// <returns></returns>
        protected bool CanReceive()
        {
            FarmLocation? selectedLocation = FarmLocations
                .Where(l => l.Id == SelectedFarmLocation)
                .FirstOrDefault();

            return !string.IsNullOrEmpty(selectedLocation?.Name)
                && !selectedLocation.Name.Equals("Unknown", StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Filters farm locations based on the selected business
        /// location
        /// </summary>
        /// <returns></returns>
        protected List<FarmLocation> FilteredFarmLocations()
        {
            return FarmLocations
                .Where(l => l.BusinessLocationId == SelectedBusinessLocation)
                .ToList();
        }

        /// <summary>
        /// Changes the business location and clears the selected farm
        /// location as well as viewable inventory
        /// </summary>
        /// <param name="locationId">The id of the selected business location</param>
        protected void HandleBusinessLocationChange(int? locationId)
        {
            SelectedBusinessLocation = locationId;
            SelectedFarmLocation = null;
            Inventory.Clear();
        }

        /// <summary>
        /// Sets the selected farm location and loads inventory for that
        /// location
        /// </summary>
        /// <param name="locationId">The id of the selected farm location</param>
        /// <returns></returns>
        protected async Task HandleFarmLocationChange(int? locationId)
        {
            SelectedFarmLocation = locationId;
            Inventory.Clear();
            await LoadInventory();
        }

        /// <summary>
        /// Filters inventory based on the search text
        /// </summary>
        /// <param name="item">The inventory item to determine visibility of</param>
        /// <returns></returns>
        protected bool HandleFilter(InventoryItem item)
        {
            if (
                string.IsNullOrWhiteSpace(SearchText)
                || (item.ProductionDate.ToString().Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (item.LotNumber.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (item.BatchNumber.ToString().Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (item.Product.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (item.Variant.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (item.Barcode != null && !string.IsNullOrEmpty(item.Barcode.UserFriendlyBarcode) && item.Barcode.UserFriendlyBarcode.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }

        /// <summary>
        /// Copies the barcode of an item to the clipboard
        /// </summary>
        /// <param name="barcode">The barcode to copy</param>
        /// <returns></returns>
        protected async Task HandleCopy(string barcode)
        {
            await _jsRuntime.InvokeVoidAsync("navigator.clipboard.writeText", barcode);
        }

        /// <summary>
        /// Displays an inventory receive dialog and reloads inventory
        /// after having closed the dialog
        /// </summary>
        /// <returns></returns>
        protected async Task HandleReceive()
        {
            try
            {
                if (SelectedBusinessLocation == null)
                    throw new ApplicationException("Must select a business location");

                if (SelectedFarmLocation == null)
                    throw new ApplicationException("Must select a farm location");

                string title = "Receive Inventory";
                DialogOptions options = new()
                {
                    DisableBackdropClick = true,
                };
                DialogParameters parameters = new()
                {
                    { "FarmLocationId", (int)SelectedFarmLocation },
                };

                IDialogReference dialog = _dialogService
                    .Show<ReceiveInventoryDialog>(title, parameters, options);

                DialogResult? result = await dialog.Result;

                if (!result.Cancelled)
                {
                    await LoadInventory();
                    StateHasChanged();
                }
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
        }

        /// <summary>
        /// Displays the inventory transfer dialog and reloads inventory
        /// after having closed the dialog
        /// </summary>
        /// <returns></returns>
        protected async Task HandleTransfer()
        {
            try
            {
                if (SelectedBusinessLocation == null)
                    throw new ApplicationException("Must select a business location");

                if (SelectedFarmLocation == null)
                    throw new ApplicationException("Must select a farm location");

                string title = "Transfer Inventory";
                DialogParameters parameters = new()
                {
                    { "TransferFromBusinessLocationId", (int)SelectedBusinessLocation },
                    { "TransferFromFarmLocationId", (int)SelectedFarmLocation },
                    { "SelectedInventory", SelectedInventory }
                };

                IDialogReference dialog = _dialogService
                    .Show<TransferInventoryDialog>(title, parameters, DialogOptions);

                DialogResult? result = await dialog.Result;

                if (!result.Cancelled)
                {
                    await LoadInventory();
                    StateHasChanged();
                }
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
        }

        /// <summary>
        /// Displays the inventory disposal dialog after selecting 
        /// inventory and reloads the inventory after closing the dialog
        /// </summary>
        /// <returns></returns>
        protected async Task HandleDispose()
        {
            try
            {
                if (SelectedBusinessLocation == null)
                    throw new ApplicationException("Must select a business location");

                if (SelectedFarmLocation == null)
                    throw new ApplicationException("Must select a farm location");

                if (SelectedInventory.Count <= 0)
                    throw new ApplicationException("Must select inventory");

                string title = "Dispose Inventory";

                FarmLocation selectedLocation = FarmLocations
                    .Where(l => l.Id == SelectedFarmLocation.Value)
                    .FirstOrDefault() ?? new();
                string parent = LocationHelper.GenerateFarmLocationParentString(selectedLocation);
                string locationName = string.IsNullOrEmpty(parent)
                    ? selectedLocation.Name
                    : $"{selectedLocation.Name} ({parent})";
                
                DialogParameters parameters = new()
                {
                    { "Location", locationName },
                    { "SelectedInventory", SelectedInventory }
                };

                IDialogReference dialog = _dialogService
                    .Show<DisposeInventoryDialog>(title, parameters, DialogOptions);

                DialogResult? result = await dialog.Result;

                if (!result.Cancelled)
                {
                    await LoadInventory();
                    StateHasChanged();
                }
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
        }
    }
}
