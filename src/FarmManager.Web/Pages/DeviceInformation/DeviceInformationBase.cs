﻿using FarmManager.Common.Consts;
using FarmManager.Data.DTOs.Devices;
using FarmManager.Data.Entities.Devices;
using FarmManager.Web.Components.SasDialog;
using FarmManager.Web.Services.SignalRService;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Pages.DeviceInformation
{
    /// <summary>
    /// A page for viewing details about a specific device conected to the
    /// Farm Manager system
    /// </summary>
    public class DeviceInformationBase : FarmComponentBase<DeviceInformationBase>, IAsyncDisposable
    {
        /// <summary>
        /// An id of a device, passed when clicking on a table row from Devices.razor
        /// </summary>
        [Parameter]
        public string? DeviceId { get; set; }

        /// <summary>
        /// The MudBlazor dialog service
        /// </summary>
        [Inject]
        private IDialogService _dialogService { get; set; }

        /// <summary>
        /// The Farm Manager SignalR service
        /// </summary>
        [Inject]
        private ISignalRService _signalRService { get; set; }

        /// <summary>
        /// Breadcrumbs to display at the top of the page
        /// </summary>
        protected List<BreadcrumbItem> Breadcrumbs { get; set; }

        /// <summary>
        /// Information about the selecteed device
        /// </summary>
        protected DeviceInformationDTO Device { get; set; }

        /// <summary>
        /// An index for the telemetry chart
        /// </summary>
        protected int Index { get; set; } = -1;

        /// <summary>
        /// Options for displaying the telemetry chart
        /// </summary>
        protected ChartOptions Options = new()
        {
            InterpolationOption = InterpolationOption.Straight
        };

        /// <summary>
        /// A list of telemetry series to display in the telemetry chart
        /// </summary>
        protected List<ChartSeries> Series { get; set; } = new List<ChartSeries>();

        /// <summary>
        /// The x-axis labels for the telemetry chart
        /// </summary>
        protected string[] XAxisLabels { get; set; } = new string[0];

        /// <summary>
        /// The connection id for creating and maintaining a SignalR connection
        /// </summary>
        private string _connectionId { get; set; }

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("DeviceInformation");

                Breadcrumbs = new()
                {
                    new BreadcrumbItem("Devices", "/devices"),
                };

                if (string.IsNullOrEmpty(DeviceId))
                    throw new ApplicationException("Missing DeviceId");

                await Task.WhenAll(
                    ConnectToSignalR(),
                    LoadDeviceInformation()
                );

                BuildChartFromTelemetry();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("DeviceInformation");
            }
        }

        /// <summary>
        /// Connects to the SignalR hub to retrieve telemetry
        /// for the selected device
        /// </summary>
        /// <returns></returns>
        private async Task ConnectToSignalR()
        {
            _connectionId = $"{DeviceId}Telemetry";
            await _signalRService.InitializeAsync(_connectionId, Hubs.Telemetry);
            _signalRService.Register<List<Telemetry>>(
                _connectionId,
                ClientMethods.Telemetry.NewTelemetry,
                HandleNewTelemetry
            );
            await _signalRService.OpenAsync(_connectionId);
            await _signalRService.SubscribeAsync(_connectionId, DeviceId);
        }

        public async ValueTask DisposeAsync()
        {
            try
            {
                if (string.IsNullOrEmpty(DeviceId))
                    throw new ApplicationException("Missing DeviceId");

                await _signalRService.UnsubscribeAsync(_connectionId, DeviceId);
                await _signalRService.CloseAsync(_connectionId);
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
        }

        /// <summary>
        /// Charts newly received telemetry to the telemetry chart
        /// </summary>
        /// <param name="newTelemetry"></param>
        private void HandleNewTelemetry(List<Telemetry> newTelemetry)
        {
            try
            {
                List<Telemetry> relevantTelemetry = newTelemetry
                    .Where(t => t.Value.HasValue)
                    .ToList();

                foreach (Telemetry reading in relevantTelemetry)
                {
                    TimeGroupedTelemetry? existingGroup = Device.Telemetry
                        .Where(t => t.Timestamp == reading.TimeStamp)
                        .FirstOrDefault();

                    InterfaceInformationDTO? telemetryInterface = Device.Interfaces
                        .Where(i => i.Id == reading.InterfaceId)
                        .FirstOrDefault();

                    if (existingGroup != null && telemetryInterface != null && reading.Value != null)
                        existingGroup.Readings.TryAdd($"{telemetryInterface.InterfaceId} - {reading.Key}", (double)reading.Value);
                    else if (existingGroup == null && telemetryInterface != null && reading.Value != null)
                        Device.Telemetry.Add(new TimeGroupedTelemetry
                        {
                            Timestamp = reading.TimeStamp.ToLocalTime(),
                            Readings = new Dictionary<string, double>
                            {
                                { $"{telemetryInterface.InterfaceId} - {reading.Key}", (double)reading.Value }
                            }
                        });
                }

                BuildChartFromTelemetry();
                StateHasChanged();
            }
            catch(Exception ex)
            {
                Snackbar.Add("Unable to update telemetry", Severity.Warning);
            }
        }

        /// <summary>
        /// Generates a SAS token for the current device and 
        /// then displays the token in a dialog
        /// </summary>
        /// <returns></returns>
        protected async Task GenerateSas()
        {
            try
            {
                Store.SetLoading("DeviceSAS");

                int sixMonths = 60 * 60 * 24 * 30 * 6;
                string token = await ApiService.Request<string>(
                    HttpMethod.Post,
                    "device/token",
                    queryParameters: new Dictionary<string, string>
                    {
                        { "DeviceId", Device.DeviceId },
                        { "Duration", sixMonths.ToString() },
                        { "DeviceKey", Device.DeviceKey.ToString() }
                    }
                );

                DialogOptions options = new()
                {
                    DisableBackdropClick = true,
                    MaxWidth = MaxWidth.Small
                };

                DialogParameters parameters = new()
                {
                    { "Sas", token }
                };

                _dialogService.Show<SasDialog>("SAS Token", parameters, options);
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("DeviceSAS");
            }
        }

        /// <summary>
        /// Retrieves information for the selected device from the 
        /// Farm Manager Api
        /// </summary>
        /// <returns></returns>
        private async Task LoadDeviceInformation()
        {
            Device = await ApiService.Request<DeviceInformationDTO>(
                HttpMethod.Get,
                $"device/{DeviceId}",
                queryParameters: new Dictionary<string, string>
                {
                    // 6 hours of history, grouped by time
                    { "historyMinutes", "360" },
                    //{ "grouping", TelemetryGrouping.Time.ToString() }
                }
            );

            Breadcrumbs.Add(new BreadcrumbItem(Device?.DeviceId, null, disabled: true));
        }

        /// <summary>
        /// Builds a ChartSeries for each device interface that
        /// returns numeric values
        /// </summary>
        private void BuildChartFromTelemetry()
        {
            if (Device?.Telemetry == null)
                return;
            
            // Make sure we display at most 6 data points to
            // maintain readability
            if (Device.Telemetry.Count > 6)
                Device.Telemetry = Device.Telemetry.TakeLast(6).ToList();

            if (Device == null || Device.Telemetry == null || !Device.Telemetry.Any())
            {
                Series = new List<ChartSeries>();
                XAxisLabels = new[] { "" };
                return;
            }

            var timeGroupedData = Device.Telemetry
                .OrderBy(t => t.Timestamp);

            XAxisLabels = timeGroupedData
                .Select(t => t.Timestamp.ToString("HH:mm"))
                .ToArray();

            Dictionary<string, List<double>> values = new();
            foreach(var data in timeGroupedData)
            {
                foreach(var value in data.Readings)
                {
                    if (values.TryGetValue(value.Key, out var telemetry))
                        telemetry.Add(value.Value);
                    else
                        values.Add(value.Key, new List<double> { value.Value });
                }
            }

            Series = values
                .Select(kvp => new ChartSeries()
                {
                    Name = kvp.Key,
                    Data = kvp.Value.ToArray()
                })
                .ToList();
        }
    }
}
