﻿using FarmManager.Data.Entities.System;
using FarmManager.Web.Shared;
using MudBlazor;

namespace FarmManager.Web.Pages.Logs
{
    /// <summary>
    /// A page for retrieving and viewing logs from the 
    /// Farm Manager system
    /// </summary>
    public class LogsBase : FarmComponentBase<LogsBase>
    {
        /// <summary>
        /// A date range picker to update the searchable dates
        /// </summary>
        protected MudDateRangePicker? LogDatePicker;

        /// <summary>
        /// The currently selected range of dates to retrieve logs from
        /// </summary>
        protected DateRange LogDateRange { get; set; } = 
            new DateRange(DateTime.Now.Subtract(TimeSpan.FromDays(5)), DateTime.Now);

        /// <summary>
        /// Breadcrumbs displayed at the top of the page
        /// </summary>
        protected List<BreadcrumbItem> Breadcrumbs { get; set; }

        /// <summary>
        /// The text to search logs for
        /// </summary>
        protected string SearchText { get; set; }

        /// <summary>
        /// A list of logs for the chosen date range
        /// </summary>
        protected IEnumerable<Log> Logs { get; set; } = new List<Log>();

        protected override async Task OnInitializedAsync()
        {
            Breadcrumbs = new()
            {
                new BreadcrumbItem("Logs", null, disabled: true)
            };

            await LoadLogs();
        }

        /// <summary>
        /// Handles filtering the displayed logs when
        /// changing the text in the search bar
        /// </summary>
        /// <param name="log">A log entry</param>
        /// <returns></returns>
        protected bool HandleFilter(Log log)
        {
            if (
                string.IsNullOrWhiteSpace(SearchText)
                || (!string.IsNullOrEmpty(log.Source) && log.Source.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(log.Message) && log.Message.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(log.Data) && log.Data.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(log.StackTrace) && log.StackTrace.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }

        /// <summary>
        /// Re-loads the logs when choosing a different
        /// set of dates from the date-range picker
        /// </summary>
        /// <returns></returns>
        protected async Task RefreshLogs()
        {
            LogDatePicker?.Close();
            await LoadLogs();
            StateHasChanged();
        }

        /// <summary>
        /// Interacts with the Farm Manager Api to load logs
        /// that were created within the selected date range
        /// </summary>
        /// <returns></returns>
        protected async Task LoadLogs()
        {
            try
            {
                Store.SetLoading("Logs");

                string startTime = LogDateRange.Start.HasValue
                    ? LogDateRange.Start.Value.Date.ToUniversalTime().ToString()
                    : DateTime.UtcNow.ToString();

                string stopTime = LogDateRange.End.HasValue
                    ? LogDateRange.End.Value.Date.ToUniversalTime().AddDays(1).Subtract(TimeSpan.FromSeconds(1)).ToString()
                    : DateTime.UtcNow.ToString();

                Logs = await ApiService.Request<List<Log>>(
                    HttpMethod.Get,
                    "log",
                    queryParameters: new()
                    {
                        { "StartTime", startTime },
                        { "StopTime", stopTime }
                    }
                );
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("Logs");
            }
        }
    }
}
