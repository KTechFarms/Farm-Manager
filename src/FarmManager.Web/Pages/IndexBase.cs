﻿using FarmManager.Data.DTOs.System.Dashboard;
using FarmManager.Web.Shared;
using MudBlazor;

namespace FarmManager.Web.Pages
{
    /// <summary>
    /// The main / home page of FarmManager.Web. Serves as a dashboard
    /// with system related charts for easy viewing.
    /// </summary>
    public class IndexBase : FarmComponentBase<IndexBase>
    {
        /// <summary>
        /// A summary of Farm Manager devices
        /// </summary>
        protected DeviceCountSummary DeviceSummary { get; set; } = new();

        /// <summary>
        /// A list of series for the device summary chart
        /// </summary>
        protected List<ChartSeries> DeviceSummarySeries { get; set; } = new();

        /// <summary>
        /// Options for the device summary chart
        /// </summary>
        protected ChartOptions DeviceSummaryOptions { get; set; } = new()
        {
            XAxisLines = false,
            YAxisLines = false,
        };

        /// <summary>
        /// Labels for the device summary chart
        /// </summary>
        protected string[] DeviceSummaryLabels { get; set; } = new string[] { };

        protected override async Task OnInitializedAsync()
        {
            await LoadDashbard();
        }

        /// <summary>
        /// Loads the various information needed to display charts
        /// on the dashboard
        /// </summary>
        /// <returns></returns>
        private async Task LoadDashbard()
        {
            Store.SetLoading("Dashboard");
            await LoadDeviceSummary();
            Store.ClearLoading("Dashboard");
        }

        /// <summary>
        /// Loads information related to the number of devices connected
        /// to Farm Manager and builds the chart from that data.
        /// </summary>
        /// <returns></returns>
        private async Task LoadDeviceSummary()
        {
            try
            {
                DeviceSummary = await ApiService.Request<DeviceCountSummary>(
                    HttpMethod.Get,
                    "dashboard/device-summary"
                );

                DeviceSummarySeries = new List<ChartSeries>
                {
                    new ChartSeries()
                    {
                        Name = "Pending",
                        Data = new double[]
                        {
                            DeviceSummary.PendingDevices
                        }
                    },
                    new ChartSeries()
                    {
                        Name = "Active",
                        Data = new double[]
                        {
                            DeviceSummary.ActiveDevices
                        }
                    },
                };

                DeviceSummaryLabels = new string[] { "" };

                int maxDevices = Math.Max(
                    DeviceSummary.ActiveDevices,
                    DeviceSummary.PendingDevices
                );
                double ticks = Math.Ceiling(maxDevices / 4f);
                
                DeviceSummaryOptions.MaxNumYAxisTicks = maxDevices;
                DeviceSummaryOptions.YAxisTicks = Convert.ToInt32(ticks);
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
        }
    }
}
