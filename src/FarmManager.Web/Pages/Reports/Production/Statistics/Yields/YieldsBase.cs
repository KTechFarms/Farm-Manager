﻿using FarmManager.Data.Entities.Reports.Production;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Pages.Reports.Production.Statistics.Yields
{
    /// <summary>
    /// A modal displaying the yields for each variant of a particular crop
    /// </summary>
    public class YieldsBase : FarmComponentBase<YieldsBase>
    {
        /// <summary>
        /// A reference to the current dialog instance
        /// </summary>
        [CascadingParameter]
        private MudDialogInstance _dialog { get; set; }

        /// <summary>
        /// The yields that should be displayed in the table
        /// </summary>
        [Parameter]
        public List<CropStatisticYield> Yields { get; set; }

        /// <summary>
        /// Closes the dialog
        /// </summary>
        protected void HandleClose()
        {
            _dialog.Close();
        }
    }
}
