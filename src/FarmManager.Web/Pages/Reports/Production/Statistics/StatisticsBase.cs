﻿using FarmManager.Data.Entities.Reports.Production;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Pages.Reports.Production.Statistics
{
    /// <summary>
    /// A component for viewing the crop statistics report
    /// </summary>
    public class StatisticsBase : FarmComponentBase<StatisticsBase>
    {
        /// <summary>
        /// The MudBlazor dialog service, for displaying a dialog
        /// </summary>
        [Inject]
        private IDialogService _dialogService { get; set; }

        /// <summary>
        /// A list of crop statistics
        /// </summary>
        protected List<CropStatistic> Statistics { get; set; } = new();

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("CropStatistics");

                Statistics = await ApiService.Request<List<CropStatistic>>(
                    HttpMethod.Get,
                    "reports/crop-statistics"
                );
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("CropStatistics");
            }
        }

        /// <summary>
        /// Displays a modal containing a table with yields by
        /// product variant
        /// </summary>
        /// <param name="statistic"></param>
        protected void DisplayYields(CropStatistic statistic)
        {
            DialogOptions options = new()
            {
                DisableBackdropClick = true,
                MaxWidth = MaxWidth.Large
            };

            DialogParameters parameters = new()
            {
                { "Yields", statistic.YieldStatistics }
            };

            _dialogService.Show<Yields.Yields>(
                $"{statistic.CropName} Yields",
                parameters,
                options
            );
        }
    }
}
