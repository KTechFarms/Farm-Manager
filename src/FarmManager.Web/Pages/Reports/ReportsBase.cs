﻿using FarmManager.Web.Shared;
using MudBlazor;

namespace FarmManager.Web.Pages.Reports
{
    /// <summary>
    /// The base page for viewing Farm Manager reports
    /// </summary>
    public class ReportsBase : FarmComponentBase<ReportsBase>
    {
        public List<BreadcrumbItem> Breadcrumbs { get; set; }

        protected override void OnInitialized()
        {
            Breadcrumbs = new()
            {
                new BreadcrumbItem("Reports", null, disabled: true)
            };
        }
    }
}
