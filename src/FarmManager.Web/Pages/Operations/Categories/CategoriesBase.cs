﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Components.AddCategoryDialog;
using FarmManager.Web.Shared;

namespace FarmManager.Web.Pages.Operations.Categories
{
    /// <summary>
    /// A page for displaying and managing product categories
    /// </summary>
    public class CategoriesBase : FarmManagerPageBase<CategoriesBase, ProductCategory, AddCategoryDialog>
    {
        protected override string? BreadcrumbRoot { get; set; } = "Categories";

        protected override async Task LoadItems()
        {
            Items = await ApiService.Request<List<ProductCategory>>(
                HttpMethod.Get,
                "category"
            );
        }

        /// <summary>
        /// Deletes a category
        /// </summary>
        /// <param name="category">The category to delete</param>
        /// <returns></returns>
        protected override async Task DeleteItem(ProductCategory category)
        {
            await ApiService.Request<int>(
                HttpMethod.Delete,
                $"category/{category.Id}"
            );

            ((List<ProductCategory>)Items).Remove(category);
        }

        /// <summary>
        /// Filters the visible categories
        /// </summary>
        /// <param name="item">The category to determine visiblity of</param>
        /// <param name="searchText">The text to search the category for</param>
        /// <returns></returns>
        protected override bool HandleFilter(ProductCategory item, string searchText)
        {
            if(
                string.IsNullOrWhiteSpace(searchText)
                || (!string.IsNullOrEmpty(item.Name) && item.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }
    }
}
