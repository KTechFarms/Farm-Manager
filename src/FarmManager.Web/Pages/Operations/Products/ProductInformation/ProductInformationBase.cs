﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;
using FarmManager.Web.Utilities.Validation;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using Newtonsoft.Json;

namespace FarmManager.Web.Pages.Operations.Products.ProductInformation
{
    /// <summary>
    /// A page for displaying information about a selected product
    /// </summary>
    public class ProductInformationBase : FarmComponentBase<ProductInformationBase>
    {
        /// <summary>
        /// The id of the product to display information about
        /// </summary>
        [Parameter]
        public int ProductId { get; set; }

        /// <summary>
        /// The details of the currently selected product
        /// </summary>
        protected Product? Product { get; set; }

        /// <summary>
        /// A list of available categories
        /// </summary>
        protected List<ProductCategory> Categories { get; set; }

        /// <summary>
        /// A list of available units
        /// </summary>
        protected List<Unit> Units { get; set; }

        /// <summary>
        /// Breadcrumbs displayed at the top of the page
        /// </summary>
        protected List<BreadcrumbItem> BreadCrumbs { get; set; }

        protected override async Task OnInitializedAsync()
        {
            try
            {
                BreadCrumbs = new()
                {
                    new BreadcrumbItem("Products", "/products"),
                };

                Store.SetLoading("ProductInformation");

                List<Task> LoadTasks = new();
                if (!string.IsNullOrEmpty(NavigationManager.HistoryEntryState))
                {
                    try
                    {
                        Product = JsonConvert.DeserializeObject<Product>(NavigationManager.HistoryEntryState);
                    }
                    catch (Exception ex)
                    {
                        LoadTasks.Add(LoadProduct());
                    }
                }
                else
                    LoadTasks.Add(LoadProduct());

                LoadTasks.Add(LoadCategories());
                LoadTasks.Add(LoadUnits());

                await Task.WhenAll(LoadTasks);
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                BreadCrumbs.Add(new BreadcrumbItem($"{Product?.Name}", null, disabled: true));
                StateHasChanged();
                Store.ClearLoading("ProductInformation");
            }
        }


        /// <summary>
        /// Loads the product details
        /// </summary>
        /// <returns></returns>
        private async Task LoadProduct()
        {
            // TODO: Create query params so this happens in the API / Core
            List<Product> products = await ApiService.Request<List<Product>>(
                HttpMethod.Get,
                "product"
            );

            Product = products
                .Where(p => p.Id == ProductId)
                .FirstOrDefault();
        }

        /// <summary>
        /// Loads the available categories
        /// </summary>
        /// <returns></returns>
        private async Task LoadCategories()
        {
            Categories = await ApiService.Request<List<ProductCategory>>(
                HttpMethod.Get,
                "category"
            );
        }

        /// <summary>
        /// loads the available units
        /// </summary>
        /// <returns></returns>
        private async Task LoadUnits()
        {
            Units = await ApiService.Request<List<Unit>>(
                HttpMethod.Get,
                "unit"
            );
        }

        /// <summary>
        /// Handles adding a new variant
        /// </summary>
        protected void HandleAddVariant()
        {
            Product?.Variants?.Add(new ProductVariant
            {
                Name = $"Variant {Product?.Variants?.Count}",
                TrackInventory = true,
                MeasurementUnitId = Units
                    .FirstOrDefault()?
                    .Id ?? 0
            });
        }

        /// <summary>
        /// Handles removing a variant
        /// </summary>
        /// <param name="variant"></param>
        protected void HandleRemoveVariant(ProductVariant variant)
        {
            Product?.Variants?.Remove(variant);
            StateHasChanged();
        }

        /// <summary>
        /// Navigates back to the product page
        /// </summary>
        protected void HandleBack()
        {
            NavigationManager.NavigateTo("/products");
        }

        /// <summary>
        /// Saves the current product information
        /// </summary>
        /// <returns></returns>
        protected async Task HandleSave()
        {
            try
            {
                ProductValidator.ValidateProduct(Product);

                Store.SetLoading("ProductUpdate");

                await ApiService.Request<Product>(
                    HttpMethod.Patch,
                    "product",
                    body: Product
                );

                Snackbar.Add("Product Updated", Severity.Success);
                NavigationManager.NavigateTo("/products");
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("ProductUpdate");
            }
        }
    }
}
