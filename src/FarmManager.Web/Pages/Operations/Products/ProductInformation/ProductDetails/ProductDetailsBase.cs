﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;

namespace FarmManager.Web.Pages.Operations.Products.ProductInformation.ProductDetails
{
    /// <summary>
    /// A page to display information about a selected product
    /// </summary>
    public class ProductDetailsBase : FarmComponentBase<ProductDetailsBase>
    {
        /// <summary>
        /// The product to display information about
        /// </summary>
        [Parameter]
        public Product Product { get; set; } = new();

        /// <summary>
        /// A list of available categories
        /// </summary>
        [Parameter]
        public List<ProductCategory> Categories { get; set; } = new();
    }
}
