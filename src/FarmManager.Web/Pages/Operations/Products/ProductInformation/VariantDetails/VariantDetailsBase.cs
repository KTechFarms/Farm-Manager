﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;

namespace FarmManager.Web.Pages.Operations.Products.ProductInformation.VariantDetails
{
    /// <summary>
    /// A page for displaying information about the selected product's variants
    /// </summary>
    public class VariantDetailsBase : FarmComponentBase<VariantDetailsBase>
    {
        /// <summary>
        /// A list of avialable units
        /// </summary>
        [Parameter]
        public List<Unit> Units { get; set; } = new();

        /// <summary>
        /// A list of variants for the product
        /// </summary>
        [Parameter]
        public List<ProductVariant> Variants { get; set; } = new();

        /// <summary>
        /// A function for handling a new variant
        /// </summary>
        [Parameter]
        public Action HandleAddVariant { get; set; } = () => { };

        /// <summary>
        /// A function for removing a variant
        /// </summary>
        [Parameter]
        public Action<ProductVariant> HandleRemoveVariant { get; set; } = (variant) => { };
    }
}
