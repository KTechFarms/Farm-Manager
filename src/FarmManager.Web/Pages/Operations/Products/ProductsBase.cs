﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Components.AddProductDialog;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using Newtonsoft.Json;

namespace FarmManager.Web.Pages.Operations.Products
{
    /// <summary>
    /// A page for displaying products available for sale
    /// </summary>
    public class ProductsBase : FarmManagerPageBase<ProductsBase, Product, AddProductDialog>
    {
        /// <summary>
        /// Loads products from the Farm Manager Api
        /// </summary>
        /// <returns></returns>
        protected override async Task LoadItems()
        {
            Items = await ApiService.Request<List<Product>>(
                HttpMethod.Get,
                "product"
            );
        }

        /// <summary>
        /// Deletes the selected product and its variations
        /// </summary>
        /// <param name="product">The product to delete</param>
        /// <returns></returns>
        protected override async Task DeleteItem(Product product)
        {
            await ApiService.Request<int>(
                HttpMethod.Delete,
                $"product/{product.Id}"
            );

            ((List<Product>)Items).Remove(product);
        }

        /// <summary>
        /// Returns whether or not a product should be displayed when filtering
        /// </summary>
        /// <param name="product">The product to check</param>
        /// <returns></returns>
        protected override bool HandleFilter(Product product, string searchText)
        {
            if (
                string.IsNullOrWhiteSpace(searchText)
                || (!string.IsNullOrEmpty(product.Name) && product.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(product.Description) && product.Description.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(product.Category?.Name) && product.Category.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }

        /// <summary>
        /// Navigates to the product information page for the selected row
        /// </summary>
        /// <param name="args"></param>
        protected void HandleRowClick(TableRowClickEventArgs<Product> args)
        {
            NavigationManager.NavigateTo(
                $"products/{args.Item.Id}",
                new NavigationOptions
                {
                    HistoryEntryState = JsonConvert.SerializeObject(args.Item)
                }
            );
        }
    }
}
