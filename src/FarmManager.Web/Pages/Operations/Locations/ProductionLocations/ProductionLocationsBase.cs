﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Components.AddFarmLocationDialog;
using FarmManager.Web.Shared;

namespace FarmManager.Web.Pages.Operations.Locations.ProductionLocations
{
    /// <summary>
    /// A page for displaying and managing farm / production locations
    /// </summary>
    public class ProductionLocationsBase : FarmManagerPageBase<ProductionLocationsBase, FarmLocation, AddFarmLocationDialog>
    {
        /// <summary>
        /// Loads the available farm locations
        /// </summary>
        /// <returns></returns>
        protected override async Task LoadItems()
        {
            Items = await ApiService.Request<List<FarmLocation>>(
                HttpMethod.Get,
                "location"
            );

            Items = Items
                .OrderBy(o => o?.BusinessLocation?.Name)
                .ThenBy(o => o?.ParentLocation?.Name)
                .ToList();

            DialogParameters[nameof(AddFarmLocationDialogBase.ProductionLocations)] = Items.ToList();
        }

        /// <summary>
        /// Deletes the selected location
        /// </summary>
        /// <param name="item">The location to delete</param>
        /// <returns></returns>
        protected override async Task DeleteItem(FarmLocation item)
        {
            await ApiService.Request<List<int>>(
                HttpMethod.Delete,
                $"location/{item.Id}"
            );

            await TryLoadItems();
        }

        /// <summary>
        /// Filters the visible locations
        /// </summary>
        /// <param name="item">The location to determine visibilty for</param>
        /// <param name="searchText">The text to search the location for</param>
        /// <returns></returns>
        protected override bool HandleFilter(FarmLocation item, string searchText)
        {
            if (
                string.IsNullOrWhiteSpace(searchText)
                || (!string.IsNullOrEmpty(item.Name) && item.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(item.Barcode) && item.Barcode.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (item.LocationType != null && !string.IsNullOrEmpty(item.LocationType.Label) && item.LocationType.Label.Contains(searchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }
    }
}
