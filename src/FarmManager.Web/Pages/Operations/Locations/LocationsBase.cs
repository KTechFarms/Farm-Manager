﻿using FarmManager.Web.Shared;
using MudBlazor;

namespace FarmManager.Web.Pages.Operations.Locations
{
    /// <summary>
    /// A page for displaying locations at which business is performed
    /// </summary>
    public class LocationsBase : FarmComponentBase<LocationsBase>
    {
        /// <summary>
        /// Breadcrumbs displayed at the top of the page
        /// </summary>
        protected List<BreadcrumbItem> Breadcrumbs { get; set; }

        protected override void OnInitialized()
        {
            Breadcrumbs = new()
            {
                new BreadcrumbItem("Locations", null, disabled: true)
            };
        }
    }
}
