﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Components.AddLocationDialog;
using FarmManager.Web.Shared;

namespace FarmManager.Web.Pages.Operations.Locations.BusinessLocations
{
    public class BusinessLocationsBase : FarmManagerPageBase<BusinessLocationsBase, BusinessLocation, AddLocationDialog>
    {
        /// <summary>
        /// Loads locations from the Farm Manager Api
        /// </summary>
        /// <returns></returns>
        protected override async Task LoadItems()
        {
            Items = await ApiService.Request<List<BusinessLocation>>(
                HttpMethod.Get,
                "business-location"
            );

            Items = Items
                .OrderBy(l => l.Name)
                .ToList();
        }

        /// <summary>
        /// Inactivates the selected location
        /// </summary>
        /// <param name="location">The location to inactivate</param>
        /// <returns></returns>
        protected override async Task DeleteItem(BusinessLocation location)
        {
            await ApiService.Request<int>(
                HttpMethod.Delete,
                $"business-location/{location.Id}"
            );

            await TryLoadItems();
        }

        /// <summary>
        /// Returns whether or not a location should be displayed when filtering
        /// </summary>
        /// <param name="location">The location to check</param>
        /// <returns></returns>
        protected override bool HandleFilter(BusinessLocation location, string searchText)
        {
            if (
                string.IsNullOrWhiteSpace(searchText)
                || (!string.IsNullOrEmpty(location.Name) && location.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(location.Address?.Street) && location.Address.Street.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(location.Address?.City) && location.Address.City.Contains(searchText, StringComparison.OrdinalIgnoreCase))
                || (!string.IsNullOrEmpty(location.Address?.State) && location.Address.State.Contains(searchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }
    }
}
