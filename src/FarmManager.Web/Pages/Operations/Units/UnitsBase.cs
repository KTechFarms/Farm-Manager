﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Components.AddUnitDialog;
using FarmManager.Web.Shared;

namespace FarmManager.Web.Pages.Operations.Units
{
    /// <summary>
    /// A page for viewing and interacting with Units from
    /// the Square system
    /// </summary>
    public class UnitsBase : FarmManagerPageBase<UnitsBase, Unit, AddUnitDialog>
    {
        /// <summary>
        /// Loads items from the Farm Manager Api
        /// </summary>
        /// <returns></returns>
        protected override async Task LoadItems()
        {
            Items = await ApiService.Request<List<Unit>>(
                HttpMethod.Get,
                "unit"
            );
        }

        /// <summary>
        /// Filters the shown units based on search text
        /// </summary>
        /// <param name="unit">A unit to compare the search text against</param>
        /// <returns></returns>
        protected override bool HandleFilter(Unit unit, string searchText)
        {
            if (
                string.IsNullOrEmpty(searchText)
                || (!string.IsNullOrEmpty(unit.Symbol) && unit.Symbol.Contains(searchText, StringComparison.OrdinalIgnoreCase))
            )
                return true;

            return false;
        }

        /// <summary>
        /// Deletes the selected Unit
        /// </summary>
        /// <param name="unit">The unit to delete</param>
        /// <returns></returns>
        protected override async Task DeleteItem(Unit unit)
        {
            await ApiService.Request<int>(
                HttpMethod.Delete,
                $"unit/{unit.Id}"
            );

            ((List<Unit>)Items).Remove(unit);
        }
    }
}
