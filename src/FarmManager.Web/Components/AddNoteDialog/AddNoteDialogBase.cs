﻿using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;

namespace FarmManager.Web.Components.AddNoteDialog
{
    /// <summary>
    /// A dialot for creating a new note
    /// </summary>
    public class AddNoteDialogBase : FarmManagerDialogBase<AddNoteDialog>
    {
        /// <summary>
        /// The function to call when hitting the save button
        /// </summary>
        [Parameter]
        public Func<string, object?, Task> OnSave { get; set; }

        /// <summary>
        /// Any parameters that should be passed to the OnSave function
        /// </summary>
        [Parameter]
        public object? OnSaveParams { get; set; }

        /// <summary>
        /// The string content of the note
        /// </summary>
        protected string NoteContent { get; set; }

        protected override async Task HandleSave()
        {
            if (OnSave != null)
                await OnSave.Invoke(NoteContent, OnSaveParams);
        }
    }
}
