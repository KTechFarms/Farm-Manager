﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using System.Security.Cryptography;
using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Web.Components.DisposeInventoryDialog
{
    /// <summary>
    /// A dialog for disposing of inventory from a particular location
    /// </summary>
    public class DisposeInventoryDialogBase : FarmManagerDialogBase<DisposeInventoryDialogBase>
    {
        /// <summary>
        /// The display name of the location inventory is being disposed from
        /// </summary>
        [Parameter]
        public string Location { get; set; }

        /// <summary>
        /// The inventory that is being disposed
        /// </summary>
        [Parameter]
        public HashSet<InventoryItem> SelectedInventory { get; set; }

        /// <summary>
        /// A list of reasons for disposing the inventory
        /// </summary>
        protected List<Enum> DisposalReasons { get; set; } = new();

        /// <summary>
        /// The selected inventory quantities to be disposed
        /// </summary>
        protected List<string> SelectedQuantities { get; set; } = new();

        /// <summary>
        /// The id of the selected disposal reason
        /// </summary>
        protected int SelectedDisposalReason { get; set; }

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("DisposeInventory");

                SelectedQuantities = GenerateSelectedItemText();
                await LoadDisposalReasons();
                SelectedDisposalReason = DisposalReasons?.FirstOrDefault()?.Id ?? 0;

                StateHasChanged();
            }
            catch (Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("DisposeInventory");
            }
        }

        /// <summary>
        /// Loads available disposal reason from the api
        /// </summary>
        /// <returns></returns>
        private async Task LoadDisposalReasons()
        {
            DisposalReasons = await ApiService
                .Request<List<Enum>>(
                HttpMethod.Get,
                "enum/inventory.disposal.reason"
            );
        }

        /// <summary>
        /// Generates a friendly text for the summary of 
        /// items that are being disposed
        /// </summary>
        /// <returns></returns>
        private List<string> GenerateSelectedItemText()
        {
            Dictionary<string, int> itemQuantities = new();
            foreach(InventoryItem item in SelectedInventory)
            {
                string identifier = $"{item.Product} ({item.Variant})";
                if (itemQuantities.TryGetValue(identifier, out var quantity))
                    itemQuantities[identifier] = quantity + 1;
                else
                    itemQuantities[identifier] = 1;
            }

            return itemQuantities
                .Select(quantity => $"{quantity.Value} {quantity.Key}")
                .ToList();
        }

        /// <summary>
        /// Attempts to publish and save the inventory disposal
        /// </summary>
        /// <returns></returns>
        protected override async Task HandleSave()
        {
            await ApiService.Request<List<long>>(
                HttpMethod.Post,
                "inventory/dispose",
                body: new InventoryDisposal
                {
                    DisposalReasonId = SelectedDisposalReason,
                    ProductionItemIds = SelectedInventory
                        .Where(i => i.ProductionItemId != 0)
                        .Select(i => i.ProductionItemId)
                        .ToList(),
                    InventoryIds = SelectedInventory
                        .Where(i => i.InventoryId != 0)
                        .Select(i => i.InventoryId)
                        .ToList()
                });
        }
    }
}
