﻿using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MudBlazor;

namespace FarmManager.Web.Components.SasDialog
{
    /// <summary>
    /// A basic dialog to display a generated SAS token
    /// </summary>
    public class SasDialogBase : FarmComponentBase<SasDialogBase>
    {
        /// <summary>
        /// The MudBlazor dialog instance
        /// </summary>
        [CascadingParameter]
        private MudDialogInstance _dialog { get; set; }

        /// <summary>
        /// The sas string
        /// </summary>
        [Parameter]
        public string Sas { get; set; } = string.Empty;

        /// <summary>
        /// The blazor js runtime
        /// </summary>
        [Inject]
        private IJSRuntime _jSRuntime { get; set; }

        /// <summary>
        /// Closes the dialog
        /// </summary>
        protected void HandleOk()
        {
            _dialog.Cancel();
        }

        /// <summary>
        /// Copies the SAS token to the clipboard
        /// </summary>
        /// <returns></returns>
        protected async Task Copy()
        {
            await _jSRuntime.InvokeVoidAsync("navigator.clipboard.writeText", Sas);
        }
    }
}
