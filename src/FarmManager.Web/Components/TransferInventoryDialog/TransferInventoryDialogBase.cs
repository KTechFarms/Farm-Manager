﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Components.TransferInventoryDialog
{
    /// <summary>
    /// A dialog for transferring inventory from one location to another
    /// </summary>
    public class TransferInventoryDialogBase : FarmManagerDialogBase<TransferInventoryDialogBase>
    {
        /// <summary>
        /// The starting business location id for the inventory
        /// </summary>
        [Parameter]
        public int TransferFromBusinessLocationId { get; set; }

        /// <summary>
        /// The starting farm location id for the inventory
        /// </summary>
        [Parameter]
        public int TransferFromFarmLocationId { get; set; }

        /// <summary>
        /// The inventory that is to be transferred
        /// </summary>
        [Parameter]
        public HashSet<InventoryItem> SelectedInventory { get; set; }

        /// <summary>
        /// A list of available business locations
        /// </summary>
        protected List<BusinessLocation> BusinessLocations { get; set; } = new();

        /// <summary>
        /// A list of available farm locations
        /// </summary>
        protected List<FarmLocation> FarmLocations { get; set; } = new();

        /// <summary>
        /// The selected inventory quantities to be moved
        /// </summary>
        protected List<string> SelectedQuantitites { get; set; } = new();

        /// <summary>
        /// The id of the business location being transferred to
        /// </summary>
        protected int? TransferToBusinessLocationId { get; set; }

        /// <summary>
        /// The id of the farm location being transferred to
        /// </summary>
        protected int? TransferToFarmLocationId { get; set; }

        /// <summary>
        /// Returns the name of the provided business location id
        /// </summary>
        /// <param name="id">The id of the business location to retrieve the
        /// name of</param>
        /// <returns></returns>
        protected string? BusinessLocationName(int? id)
        {
            return BusinessLocations
                .Where(l => l.Id == id)
                .Select(l => l.Name)
                .FirstOrDefault();
        }

        /// <summary>
        /// Returns the name of the provided farm location id
        /// </summary>
        /// <param name="id">The id of the farm location to retrieve
        /// the name of</param>
        /// <returns></returns>
        protected string? FarmLocationName(int id)
        {
            return FarmLocations
                .Where(l => l.Id == id)
                .Select(l => l.Name)
                .FirstOrDefault();
        }

        /// <summary>
        /// Filters the displayed transfer to farm location based on 
        /// the selected business location
        /// </summary>
        /// <returns></returns>
        protected List<FarmLocation> FilteredFarmLocations()
        {
            return FarmLocations
                .Where(l => l.BusinessLocation.Id == TransferToBusinessLocationId)
                .ToList();
        }

        /// <summary>
        /// Generates the text for diplaying the selected item summary
        /// </summary>
        /// <returns></returns>
        protected List<string> GenerateSelectedItemText()
        {
            Dictionary<string, int> itemQuantities = new();
            foreach(InventoryItem item in SelectedInventory)
            {
                string identifier = $"{item.Product} ({item.Variant})";
                if(itemQuantities.TryGetValue(identifier, out var quantity))
                    itemQuantities[identifier] = quantity + 1;
                else
                    itemQuantities[identifier] = 1;
            }

            return itemQuantities
                .Select(quantity => $"{quantity.Value} {quantity.Key}")
                .ToList();
        }

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("TransferLocations");

                await Task.WhenAll(
                    LoadBusinessLocations(),
                    LoadFarmLocations()
                );

                SelectedQuantitites = GenerateSelectedItemText();

                StateHasChanged();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("TransferLocations");
            }
        }

        /// <summary>
        /// Loads the available business locations
        /// </summary>
        /// <returns></returns>
        private async Task LoadBusinessLocations()
        {
            BusinessLocations = await ApiService.Request<List<BusinessLocation>>(
                HttpMethod.Get,
                "business-location"
            );
        }

        /// <summary>
        /// Loads the available farm locations
        /// </summary>
        /// <returns></returns>
        private async Task LoadFarmLocations()
        {
            FarmLocations = await ApiService.Request<List<FarmLocation>>(
                HttpMethod.Get,
                "location"
            );
        }

        /// <summary>
        /// handles choosing a new destination business location and clears the 
        /// selected farm location
        /// </summary>
        /// <param name="locationId">The id of the business location being
        /// selected</param>
        protected void HandleTransferToBusinessLocationChange(int? locationId)
        {
            TransferToBusinessLocationId = locationId;
            TransferToFarmLocationId = null;
        }

        protected override async Task HandleSave()
        {
            ValidateTransfer();

            await ApiService.Request<bool>(
                HttpMethod.Post,
                "inventory/transfer",
                body: new InventoryTransfer
                {
                    FromLocationId = TransferFromFarmLocationId,
                    ToLocationId = (int)TransferToFarmLocationId,
                    ProductionItemIds = SelectedInventory.Where(i => i.ProductionItemId != 0).Select(i => i.ProductionItemId).ToList(),
                    InventoryIds = SelectedInventory.Where(i => i.InventoryId != 0).Select(i => i.InventoryId).ToList(),
                });
        }

        /// <summary>
        /// Validates that the selected options for making a transfer are valid
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateTransfer()
        {
            if (TransferToBusinessLocationId == null || TransferToFarmLocationId == null)
                throw new ApplicationException("Both a business and farm location are required");

            if (TransferFromFarmLocationId == TransferToFarmLocationId)
                throw new ApplicationException("You must select a different destination");
        }
    }
}
