﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Web.Shared;
using MudBlazor;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Web.Components.AddLotDialog
{
    /// <summary>
    /// A dialog for creating a new lot
    /// </summary>
    public class AddLotDialogBase : FarmManagerDialogBase<AddLotDialogBase>
    {
        /// <summary>
        /// The lot to be created
        /// </summary>
        protected Lot Lot { get; set; } = new();
        
        /// <summary>
        /// The type of lot to create
        /// </summary>
        protected Enum LotType { get; set; } = new();

        // Generic options, used for all lot types

        /// <summary>
        /// The available locations to create the lot at
        /// </summary>
        protected List<FarmLocation> Locations { get; set; } = new();

        /// <summary>
        /// The available products to use for the lot
        /// </summary>
        protected List<Product> Products { get; set; } = new();

        /// <summary>
        /// The available lot types that can be created
        /// </summary>
        protected List<Enum> LotTypes { get; set; } = new();

        // Crop-specific options

        /// <summary>
        /// A list of seed products with the same name as the
        /// selected lot product
        /// </summary>
        protected List<Product> Seeds { get; set; } = new();

        /// <summary>
        /// Crop specific information about the lot
        /// </summary>
        protected Crop Crop { get; set; } = new();

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("LotData");

                Task<List<FarmLocation>> locationTask = ApiService.Request<List<FarmLocation>>(
                    HttpMethod.Get,
                    "location",
                    queryParameters: new Dictionary<string, string>
                    {
                        { "Type", "production" }
                    }
                );
                Task<List<Product>> productTask = ApiService.Request<List<Product>>(
                    HttpMethod.Get,
                    "product"
                );
                Task<List<Enum>> lotTypeTask = ApiService.Request<List<Enum>>(
                    HttpMethod.Get,
                    "enum/lot.type"
                );
                Task<string> lotNumberTask = ApiService.Request<string>(
                    HttpMethod.Get,
                    "lot/number"
                );

                await Task.WhenAll(locationTask, productTask, lotTypeTask, lotNumberTask);

                Locations = locationTask.Result;
                LotTypes = lotTypeTask.Result;

                Products = new();
                Seeds = new();
                foreach (Product product in productTask.Result)
                    if (product.Category?.Name == "Produce")
                        Products.Add(product);
                    else if (product.Category?.Name == "Seed")
                        Seeds.Add(product);

                Lot.LotNumber = lotNumberTask.Result;
                Lot.ProductId = Products.FirstOrDefault()?.Id ?? 0;
                Lot.FarmLocationId = Locations.FirstOrDefault()?.Id ?? 0;
                LotType = LotTypes.FirstOrDefault() ?? new();

                StateHasChanged();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("LotData");
            }
        }

        protected override async Task HandleSave()
        {
            ValidateLot();

            if (LotType.Name == "crop")
                Lot.Crop = Crop;

            await ApiService.Request<Lot>(
                HttpMethod.Post,
                "lot",
                body: Lot
            );
        }

        /// <summary>
        /// Returns a list of seed variants that can be used, based
        /// on the selected product
        /// </summary>
        /// <returns></returns>
        protected List<ProductVariant> AvailableSeedVariants()
        {
            Product? selectedProduct = Products
                .Where(p => p.Id == Lot.ProductId)
                .FirstOrDefault();

            return Seeds.Where(s => s.Name == selectedProduct?.Name)
                .Select(s => s.Variants.ToList())
                .FirstOrDefault() ?? new List<ProductVariant>();
        }

        /// <summary>
        /// Validates that the provided information for the lot is valid
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateLot()
        {
            if (Lot.ProductId == null)
                throw new ApplicationException("Product must be specified");

            if (Lot.FarmLocationId == 0)
                throw new ApplicationException("Location must be specified");

            if (LotType == null)
                throw new ApplicationException("Lot must be specified");
        }
    }
}
