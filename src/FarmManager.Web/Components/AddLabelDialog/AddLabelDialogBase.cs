﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components.Forms;
using MudBlazor;

namespace FarmManager.Web.Components.AddLabelDialog
{
    /// <summary>
    /// A dialog for creating a new label
    /// </summary>
    public class AddLabelDialogBase : FarmManagerDialogBase<AddLabelDialogBase>
    {
        /// <summary>
        /// The label to be created
        /// </summary>
        protected LabelDTO Label { get; set; } = new();

        /// <summary>
        /// The product the label is for
        /// </summary>
        protected Product? ChosenProduct { get; set; }

        /// <summary>
        /// A list of available products to choose from
        /// </summary>
        protected List<Product> Products { get; set; } = new();

        /// <summary>
        /// A file to use as the label
        /// </summary>
        protected IBrowserFile? LabelFile { get; set; }

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("ProductVariants");

                Products = (await ApiService.Request<List<Product>>(
                    HttpMethod.Get,
                    "product"
                ));
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("ProductVariants");
            }
        }

        /// <summary>
        /// Validates whether the information provided for the label is 
        /// valid
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateLabel()
        {
            if (ChosenProduct == null)
                throw new ApplicationException("A product must be selected");

            if (Label.ProductVariant == null)
                throw new ApplicationException("A product variant must be selected");

            if (string.IsNullOrEmpty(Label.LabelName))
                throw new ApplicationException("Name is required");

            if (LabelFile == null)
                throw new ApplicationException("Must choose a label file");
        }

        /// <summary>
        /// Sets the content of the label file
        /// </summary>
        /// <param name="args"></param>
        protected void SetFile(InputFileChangeEventArgs args)
        {
            if (args.File != null)
                LabelFile = args.File;
        }

        /// <summary>
        /// Handles changing the product and updates the product variant 
        /// options
        /// </summary>
        /// <param name="selectedProduct"></param>
        protected void HandleProductChange(Product selectedProduct)
        {
            ChosenProduct = selectedProduct;
            Label.ProductVariant = ChosenProduct?.Variants?.FirstOrDefault()?.Id ?? 0;
            StateHasChanged();
        }

        protected override async Task HandleSave()
        {
            ValidateLabel();

            // Allow files up to ~5 MB
            var buffer = new byte[LabelFile.Size];
            await LabelFile
                .OpenReadStream(5000000)
                .ReadAsync(buffer);

            Label.FileName = LabelFile.Name;
            Label.FileContent = buffer;

            await ApiService.Request<Label>(
                HttpMethod.Post,
                "label",
                body: Label
            );
        }
    }
}
