﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;

namespace FarmManager.Web.Components.AddCategoryDialog
{
    /// <summary>
    /// A dialog for creating a new category
    /// </summary>
    public class AddCategoryDialogBase : FarmManagerDialogBase<AddCategoryDialogBase>
    {
        /// <summary>
        /// The category to be created
        /// </summary>
        protected ProductCategory Category { get; set; } = new();

        protected override async Task HandleSave()
        {
            ValidateCategory();

            await ApiService.Request<ProductCategory>(
                HttpMethod.Post,
                $"category",
                body: Category
            );
        }

        /// <summary>
        /// Validates whether the category information supplied is valid
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateCategory()
        {
            if (string.IsNullOrEmpty(Category.Name))
                throw new ApplicationException("Category must have a name");
        }
    }
}
