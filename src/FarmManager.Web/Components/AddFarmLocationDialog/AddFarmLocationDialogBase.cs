﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Web.Components.AddFarmLocationDialog
{
    /// <summary>
    /// A dialog for adding a new FarmLocation
    /// </summary>
    public class AddFarmLocationDialogBase : FarmManagerDialogBase<AddFarmLocationDialogBase>
    {
        /// <summary>
        /// A list of the pre-existing FarmLocations. Used to populate a 
        /// parent location dropdown
        /// </summary>
        [Parameter]
        public List<FarmLocation> ProductionLocations { get; set; } = new();

        /// <summary>
        /// A list of business locations the farm location can be associated with
        /// </summary>
        protected List<BusinessLocation> BusinessLocations { get; set; } = new();

        /// <summary>
        /// A list of location types
        /// </summary>
        protected List<Enum> LocationTypes { get; set; } = new();

        /// <summary>
        /// The location to be created
        /// </summary>
        protected FarmLocation Location { get; set; } = new();

        private FarmLocation _nullLocation { get; } = new() { Id = 0, Name = "N/A" };

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("FarmLocationData");

                Task<List<BusinessLocation>> locationRequest = ApiService
                    .Request<List<BusinessLocation>>(
                        HttpMethod.Get,
                        "business-location"
                    );
                Task<List<Enum>> enumRequest = ApiService
                    .Request<List<Enum>>(
                        HttpMethod.Get,
                        "enum/farmlocation.type"
                    );

                await Task.WhenAll(locationRequest, enumRequest);

                BusinessLocations = locationRequest.Result;
                ProductionLocations.Insert(0, _nullLocation);
                LocationTypes = enumRequest.Result
                    .Where(e => e.Name != "unknown")
                    .ToList();
                Location.LocationTypeId = LocationTypes.FirstOrDefault()?.Id ?? 0;
                Location.BusinessLocationId = BusinessLocations.FirstOrDefault()?.Id ?? 0;
                StateHasChanged();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("FarmLocationData");
            }
        }

        protected override async Task HandleSave()
        {
            ValidateLocation();

            if (Location.ParentLocationId == 0)
                Location.ParentLocationId = null;

            await ApiService.Request<FarmLocation>(
                HttpMethod.Post,
                "location",
                body: Location
            );
        }

        /// <summary>
        /// Ensures that the new location has the correct data prior to
        /// making an api request
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateLocation()
        {
            if (string.IsNullOrEmpty(Location.Name) || Location.LocationTypeId == 0)
                throw new ApplicationException("Location Name and Type are required!");

            if (Location.BusinessLocationId == 0)
                throw new ApplicationException("Businesss location is required!");
        }

        /// <summary>
        /// Filters the available parent locations to those that exist
        /// at the chosen business location
        /// </summary>
        /// <returns></returns>
        protected List<FarmLocation> FilteredLocations()
        {
            List<FarmLocation> filteredLocations = ProductionLocations
                .Where(l => l.BusinessLocationId == Location.BusinessLocationId)
                .ToList();

            filteredLocations.Insert(0, _nullLocation);

            return filteredLocations;
        }
    }
}
