﻿using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Components.FarmManagerTable
{
    /// <summary>
    /// A base table component with a header for filtering data
    /// </summary>
    /// <typeparam name="T">The type of object that will be displayed in the table</typeparam>
    public class FarmManagerTableBase<T> : ComponentBase
    {
        /// <summary>
        /// The header component to render
        /// </summary>
        [Parameter]
        public RenderFragment? Header { get; set; }

        /// <summary>
        /// The columns to render
        /// </summary>
        [Parameter]
        public RenderFragment? Columns { get; set; }

        /// <summary>
        /// The row template to render
        /// </summary>
        [Parameter]
        public RenderFragment<T>? RowTemplate { get; set; }

        /// <summary>
        /// The items that should be displayed in the table
        /// </summary>
        [Parameter]
        public IEnumerable<T> Items { get; set; } = new List<T>();

        /// <summary>
        /// The string for the table's pagination
        /// </summary>
        [Parameter]
        public string PaginationString { get; set; } = $"{typeof(T).Name}s Per Page";

        /// <summary>
        /// Whether or not adding items should be allowed
        /// </summary>
        [Parameter]
        public bool DisableAdd { get; set; } = false;

        /// <summary>
        /// Whether or not the date picker should be shown
        /// </summary>
        [Parameter]
        public bool ShowDatePicker { get; set; } = false;

        /// <summary>
        /// A handler that is called when the date range is 
        /// modified
        /// </summary>
        [Parameter]
        public Func<DateRange, Task> OnDateSelect { get; set; }

        /// <summary>
        /// The text in the search bar
        /// </summary>
        protected string SearchText { get; set; } = string.Empty;

        /// <summary>
        /// The function to call when adding a new item
        /// </summary>
        [Parameter]
        public Func<Task>? HandleAddItem { get; set; }

        /// <summary>
        /// The function to call when filtering items with the 
        /// search text
        /// </summary>
        [Parameter]
        public Func<T, string, bool>? Filter { get; set; }

        /// <summary>
        /// The function to call when clicking on a table row
        /// </summary>
        [Parameter]
        public Action<TableRowClickEventArgs<T>>? HandleRowClick { get; set; }

        /// <summary>
        /// A date range used for filtering results
        /// </summary>
        [Parameter]
        public DateRange ItemDateRange { get; set; } =
            new DateRange(DateTime.Now.Subtract(TimeSpan.FromDays(5)), DateTime.Now);

        /// <summary>
        /// A reference to the Date Picker object
        /// </summary>
        protected MudDateRangePicker? DateRangePicker;

        /// <summary>
        /// A handler that closes the date picker and runs the 
        /// provided "on change" function if it exists
        /// </summary>
        /// <returns></returns>
        protected async Task RefreshData()
        {
            DateRangePicker?.Close();
            if (OnDateSelect != null)
                await OnDateSelect.Invoke(ItemDateRange);
        }

        /// <summary>
        /// Attempts to filter content based on the SearchText
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected bool TryFilter(T item)
        {
            if (Filter != null)
            {
                return Filter(item, SearchText);
            }
                
            return true;
        }

        /// <summary>
        /// Attempts to perform an action related to adding
        /// a new item of type T
        /// </summary>
        /// <returns></returns>
        protected async Task TryAddItem()
        {
            if (HandleAddItem != null)
                await HandleAddItem();
        }

        /// <summary>
        /// Attempts to take an action when a table row is 
        /// clicked
        /// </summary>
        /// <param name="args"></param>
        protected void TryRowClick(TableRowClickEventArgs<T> args)
        {
            if (HandleRowClick != null)
                HandleRowClick(args);
        }
    }
}
