﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;

namespace FarmManager.Web.Components.AddLocationDialog
{
    /// <summary>
    /// A dialog for creating a new location within the Square platform
    /// </summary>
    public class AddLocationDialogBase : FarmManagerDialogBase<AddLocationDialogBase>
    {
        /// <summary>
        /// The location to be created
        /// </summary>
        protected BusinessLocation Location { get; set; } = new()
        {
            Address = new()
        };

        /// <summary>
        /// Attempts to create the location and then closes the dialog, if successful
        /// </summary>
        /// <returns></returns>
        protected override async Task HandleSave()
        {
            ValidateAddress();

            await ApiService.Request<BusinessLocation>(
                HttpMethod.Post,
                "business-location",
                body: Location
            );
        }

        /// <summary>
        /// Validates whether the information provided for the
        /// business location is valid
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateAddress()
        {
            if (string.IsNullOrEmpty(Location.Name))
                throw new ApplicationException("Location name required");

            if (
                string.IsNullOrEmpty(Location.Address?.Street)
                || string.IsNullOrEmpty(Location.Address?.City)
                || string.IsNullOrEmpty(Location.Address?.State)
                || string.IsNullOrEmpty(Location.Address?.ZipCode)
            )
                throw new ApplicationException("Incomplete address");

            if (!int.TryParse(Location.Address.ZipCode, out var zipDigits))
                throw new ApplicationException("Zipcode must contain all digits");

            if (
                Location.Address.ZipCode.Length < 5
                || Location.Address.ZipCode.Length > 9
            )
                throw new ApplicationException("Zipcode must be between 5 & 9 digits");
        }
    }
}
