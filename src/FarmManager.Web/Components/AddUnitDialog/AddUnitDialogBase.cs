﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;
using MudBlazor;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Web.Components.AddUnitDialog
{
    /// <summary>
    /// A dialog for creating a new Unit in Square
    /// </summary>
    public class AddUnitDialogBase : FarmManagerDialogBase<AddUnitDialogBase>
    {
        /// <summary>
        /// A list of available precisions
        /// </summary>
        protected List<Enum> UnitPrecisions = new();

        /// <summary>
        /// The unit to create
        /// </summary>
        protected Unit Unit { get; set; } = new();

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("UnitTypes");
                await LoadPrecision();
                StateHasChanged();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("UnitTypes");
            }
        }

        /// <summary>
        /// Loads a list of available precisions
        /// </summary>
        /// <returns></returns>
        private async Task LoadPrecision()
        {
            UnitPrecisions = await ApiService.Request<List<Enum>>(
                HttpMethod.Get,
                "enum/unit.measurement.precision"
            );

            if (
                UnitPrecisions.Count > 0
                && int.TryParse(UnitPrecisions.FirstOrDefault()?.Name, out var precision)
            )
                Unit.Precision = precision;
        }

        /// <summary>
        /// Attempts to save the unit and closes the dialog
        /// if successful
        /// </summary>
        /// <returns></returns>
        protected override async Task HandleSave()
        {
            ValidateUnit();

            await ApiService.Request<Unit>(
                HttpMethod.Post,
                "unit",
                body: Unit
            );
        }

        /// <summary>
        /// Validates that the provided unit information is valid
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void ValidateUnit()
        {
            if (
                string.IsNullOrEmpty(Unit.Symbol)
                || string.IsNullOrEmpty(Unit.Name)
            )
                throw new ApplicationException("Units require a name and symbol");
        }
    }
}
