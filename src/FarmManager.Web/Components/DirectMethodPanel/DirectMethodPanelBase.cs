﻿using FarmManager.Data.DTOs.Devices;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Components.DirectMethodPanel
{
    /// <summary>
    /// AN expandable pannel that displays information about a direct method
    /// </summary>
    public class DirectMethodPanelBase : FarmComponentBase<DirectMethodPanelBase>
    {
        /// <summary>
        /// The id of the farm manager device
        /// </summary>
        [Parameter]
        public string DeviceId { get; set; }

        /// <summary>
        /// The function information represented by this panel
        /// </summary>
        [Parameter]
        public MethodInformationDTO Function { get; set; } = new MethodInformationDTO();

        /// <summary>
        /// A dictionary of parameter names and values
        /// </summary>
        protected Dictionary<string, string> ParameterValues { get; set; } = new();

        protected override void OnInitialized()
        {
            if(Function.Parameters != null)
                foreach(var kvp in Function.Parameters)
                    ParameterValues.TryAdd(kvp.Key, string.Empty);
        }

        /// <summary>
        /// Updates a dictionary's text value of the given parameter
        /// </summary>
        /// <param name="key">The key that should be updated</param>
        /// <param name="value">The value that should be used</param>
        protected void HandleValueChange(string key, string value)
        {
            ParameterValues[key] = value;
        }

        /// <summary>
        /// Makes a request to execute a direct method on 
        /// the device represented by the DirectMethodPanel
        /// </summary>
        /// <returns></returns>
        protected async Task ExecuteMethodAsync()
        {
            try
            {
                Store.SetLoading("ExecuteDirectMethod");

                Dictionary<string, object> parameters = new();
                foreach(var kvp in ParameterValues)
                {
                    if (Function.Parameters != null)
                    {
                        Function.Parameters.TryGetValue(kvp.Key, out Type? keyType);
                        var typedObj = Activator.CreateInstance(keyType);
                        
                        switch(typedObj)
                        {
                            case string:
                                parameters.TryAdd(kvp.Key, kvp.Value);
                                break;
                            case int:
                                if (int.TryParse(kvp.Value, out int intValue))
                                    parameters.TryAdd(kvp.Key, intValue);
                                break;
                            case double:
                                if (double.TryParse(kvp.Value, out double doubleValue))
                                    parameters.TryAdd(kvp.Key, doubleValue);
                                break;
                            default:
                                Logger.LogWarning($"Unhandled parameter type: {keyType}");
                                break;
                        }
                    }
                }

                DirectMethodResult result = await ApiService
                    .Request<DirectMethodResult>(
                        HttpMethod.Post,
                        $"device/{DeviceId}/direct-method/{Function.Name}",
                        body: ParameterValues
                    );

                if (result.Success)
                    Snackbar.Add($"Successfully executed {Function.Name}", Severity.Success);
                else
                    Snackbar.Add($"Error: {result.Message}", Severity.Error);
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("ExecuteDirectMethod");
            }
        }
    }
}
