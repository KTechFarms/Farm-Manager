﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Components.AddBatchDialog
{
    /// <summary>
    /// Dialog for creating a new batch
    /// </summary>
    public class AddBatchDialogBase : FarmManagerDialogBase<AddBatchDialogBase>
    {
        /// <summary>
        /// The lot number for the batch
        /// </summary>
        [Parameter]
        public string LotNumber { get; set; }

        /// <summary>
        /// The batch to be created
        /// </summary>
        protected Batch Batch { get; set; } = new();

        /// <summary>
        /// A list of available variants that can be used for the batch
        /// </summary>
        protected List<ProductVariant> Variants { get; set; } = new();

        /// <summary>
        /// A list of available locations where the batch can be created
        /// </summary>
        protected List<FarmLocation> Locations { get; set; } = new();

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("BatchData");

                LotBatchDataDTO batchData = await ApiService.Request<LotBatchDataDTO>(
                    HttpMethod.Get,
                    $"lot/{LotNumber}/batch"
                );

                Batch.BatchNumber = batchData.BatchNumber;
                Variants = batchData.Variants;
                Batch.VariantId = Variants.FirstOrDefault()?.Id ?? 0;
                Locations = batchData.Locations;
                Batch.FarmLocationId = Locations.FirstOrDefault()?.Id ?? 0;
                StateHasChanged();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("BatchData");
            }
        }

        protected override async Task HandleSave()
        {
            await ApiService.Request<Batch>(
                HttpMethod.Post,
                $"lot/{LotNumber}/batch",
                body: Batch
            );
        }
    }
}
