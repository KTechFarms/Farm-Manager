﻿using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Components.ConfirmationDialog
{
    /// <summary>
    /// A base dialog for confirming an action
    /// </summary>
    public class ConfirmationDialogBase : FarmManagerDialogBase<ConfirmationDialogBase>
    {
        /// <summary>
        /// The message to be displayed
        /// </summary>
        [Parameter]
        public string? Message { get; set; }

        /// <summary>
        /// The function to call if the action is confirmed
        /// </summary>
        [Parameter]
        public Func<Task> HandleConfirm { get; set; }

        /// <summary>
        /// The color of the cancel button
        /// </summary>
        [Parameter]
        public Color CancelColor { get; set; } = Color.Error;

        /// <summary>
        /// The color of the confirm button
        /// </summary>
        [Parameter]
        public Color ConfirmColor { get; set; } = Color.Success;

        /// <summary>
        /// The text of the cancel button
        /// </summary>
        [Parameter]
        public string CancelText { get; set; } = "Cancel";

        /// <summary>
        /// The text of the confirm button
        /// </summary>
        [Parameter]
        public string ConfirmText { get; set; } = "Confirm";

        protected override async Task HandleSave()
        {
            if(HandleConfirm != null)
                await HandleConfirm.Invoke();
        }
    }
}
