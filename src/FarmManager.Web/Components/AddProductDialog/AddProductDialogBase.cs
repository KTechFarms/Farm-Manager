﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;
using FarmManager.Web.Utilities.Validation;
using MudBlazor;

namespace FarmManager.Web.Components.AddProductDialog
{
    /// <summary>
    /// A dialog for creating a new product to Farm Manager
    /// </summary>
    public class AddProductDialogBase : FarmManagerDialogBase<AddProductDialogBase>
    {
        /// <summary>
        /// A list of available categories for the product
        /// </summary>
        protected List<ProductCategory> Categories { get; set; } = new();

        /// <summary>
        /// A list of available units for the product
        /// </summary>
        protected List<Unit> Units { get; set; } = new();

        //protected IEnumerable<string> SelectedLocations { get; set; } = new HashSet<string>();

        /// <summary>
        /// The product to be created
        /// </summary>
        protected Product Product { get; set; } = new() { Variants = new List<ProductVariant>() };

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("LoadData");

                await Task.WhenAll(
                    LoadCategories(),
                    LoadUnits()
                );
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("LoadData");
            }
        }

        /// <summary>
        /// Loads the available categories 
        /// </summary>
        /// <returns></returns>
        private async Task LoadCategories()
        {
            Categories = await ApiService.Request<List<ProductCategory>>(
                HttpMethod.Get,
                "category"
            );
        }

        /// <summary>
        /// Loads the available units
        /// </summary>
        /// <returns></returns>
        private async Task LoadUnits()
        {
            Units = await ApiService.Request<List<Unit>>(
                HttpMethod.Get,
                "unit"
            );
        }

        /// <summary>
        /// Creates a new variant for the product
        /// </summary>
        protected void HandleAddVariant()
        {
            Product?.Variants?.Add(new ProductVariant 
            { 
                Name = $"Variant {Product?.Variants?.Count}",
                TrackInventory = true,
                MeasurementUnitId = Units
                    .FirstOrDefault()?
                    .Id ?? 0
            });
        }

        /// <summary>
        /// Removes a variant from the product
        /// </summary>
        /// <param name="variant">The variant to remove</param>
        protected void HandleRemoveVariant(ProductVariant variant)
        {
            Product?.Variants?.Remove(variant);
            StateHasChanged();
        }

        /// <summary>
        /// Attempts to create the product and closes the dialog, if successfull
        /// </summary>
        /// <returns></returns>
        protected override async Task HandleSave()
        {
            ProductValidator.ValidateProduct(Product);

            await ApiService.Request<Product>(
                HttpMethod.Post,
                "product",
                body: Product
            );
        }
    }
}
