﻿using FarmManager.Web.Shared;
using FarmManager.Web.State;

namespace FarmManager.Web.Components.AppBar
{
    /// <summary>
    /// The application bar shown at the top of the FarmManager.Web
    /// website
    /// </summary>
    public class AppBarBase : FarmComponentBase<AppBarBase>
    {
        /// <summary>
        /// Toggles the navigation drawer open or closed
        /// </summary>
        protected void ToggleDrawer()
        {
            Store.DrawerOpen = !Store.DrawerOpen;
        }
    }
}
