﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Components.PromoteLotDialog
{
    /// <summary>
    /// A dialog for promoting a lot to the next level
    /// </summary>
    public class PromoteLotDialogBase : FarmManagerDialogBase<PromoteLotDialogBase>
    {
        /// <summary>
        /// The id of the lot to promote
        /// </summary>
        [Parameter]
        public int LotId { get; set; }

        /// <summary>
        /// The lot number of the lot to promote
        /// </summary>
        [Parameter]
        public string LotNumber { get; set; }

        /// <summary>
        /// A list of available statuses for the lot
        /// </summary>
        protected List<LotProgression> Statuses { get; set; } = new();

        /// <summary>
        /// A list of available locations that can be used for transplanting
        /// </summary>
        protected List<FarmLocation> Locations { get; set; } = new();

        /// <summary>
        /// The chosen status id
        /// </summary>
        protected int ChosenStatusId { get; set; }

        /// <summary>
        /// The chosen id of the transplant location
        /// </summary>
        protected int ChosenTransplantLocationId { get; set; }

        /// <summary>
        /// The name of the chosen status
        /// </summary>
        protected string? ChosenStatusName
        { 
            get 
            {
                return Statuses.Where(s => s.NextStatusId == ChosenStatusId)
                    .Select(s => s.NextStatus.Name)
                    .FirstOrDefault();
            } 
        } 

        /// <summary>
        /// Whether or not to display the content of the dialog
        /// </summary>
        protected bool DisplayContent { get; set; } = false;

        protected override async Task OnInitializedAsync()
        {
            await Task.WhenAll(
                LoadLotProgressions(),
                LoadLocations()
            );
            StateHasChanged();
        }

        /// <summary>
        /// Loads the available progression options
        /// </summary>
        /// <returns></returns>
        protected async Task LoadLotProgressions()
        {
            try
            {
                Store.SetLoading("LotProgressions");

                LotProgressionDTO progressions = await ApiService.Request<LotProgressionDTO>(
                    HttpMethod.Get,
                    $"lot/{LotId}/progression"
                );

                Statuses = progressions.FutureProgressionOptions;
                ChosenStatusId = Statuses?.FirstOrDefault()?.NextStatusId ?? 0;
                DisplayContent = true;
            }
            catch (Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("LotProgressions");
            }
        }

        /// <summary>
        /// Loads the available locations
        /// </summary>
        /// <returns></returns>
        protected async Task LoadLocations()
        {
            try
            {
                Store.SetLoading("TransplantLocations");

                Locations = await ApiService.Request<List<FarmLocation>>(
                    HttpMethod.Get,
                    "location"
                );

                Locations = Locations
                    .Where(l => !string.Equals(l.Name, "unknown", StringComparison.OrdinalIgnoreCase))
                    .OrderBy(l => l?.BusinessLocation?.Name)
                    .ThenBy(l => l?.ParentLocation?.Name)
                    .ToList();

                ChosenTransplantLocationId = Locations?.FirstOrDefault()?.Id ?? 0;
            }
            catch (Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("TransplantLocations");
            }
        }

        protected override async Task HandleSave()
        {
            Result = await ApiService.Request<Lot>(
                HttpMethod.Post,
                $"lot/{LotId}/progression/{ChosenStatusId}?transplantLocationId={ChosenTransplantLocationId}"
            );
        }
    }
}
