﻿using FarmManager.Web.Config;
using FarmManager.Web.Services.StorageService;
using FarmManager.Web.Shared;
using FarmManager.Web.State;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.Extensions.Options;

namespace FarmManager.Web.Components.Drawer
{
    /// <summary>
    /// A navigation drawer shown on the left side of the 
    /// FarmManager.Web website
    /// </summary>
    public class DrawerBase : FarmComponentBase<DrawerBase>, IDisposable
    {
        /// <summary>
        /// The blazor navigation manager
        /// </summary>
        [Inject]
        private NavigationManager _navigationManager { get; set; }

        /// <summary>
        /// The blazor signout manager
        /// </summary>
        [Inject]
        private SignOutSessionStateManager _signoutManager { get; set; }

        /// <summary>
        /// The current storage service
        /// </summary>
        [Inject]
        private IStorageService _storageService { get; set; }

        /// <summary>
        /// The application settings for the Farm Manager web client
        /// </summary>
        [Inject]
        private IOptions<AppSettings> _appOptions { get; set; }

        protected override void OnInitialized()
        {
            Store.OnDrawerChange += StateHasChanged;
        }

        public void Dispose()
        {
            Store.OnDrawerChange -= StateHasChanged;
        }

        /// <summary>
        /// Retrieves the AzureAD token from session storage, then 
        /// navigates to the hangfire dashboard
        /// </summary>
        /// <returns></returns>
        protected async Task NavigateToJobDashboard()
        {
            string token = await _storageService.GetAADToken();
            string apiUri = _appOptions.Value.BaseApiUrl;
            string destination = $"{apiUri}/hangfire?access_token={token}";

            NavigationManager.NavigateTo(
                destination,
                true
            );
        }

        /// <summary>
        /// Logs out the current user
        /// </summary>
        /// <returns></returns>
        protected async Task HandleLogout()
        {
            await _signoutManager.SetSignOutState();
            string returnUrl = Uri.EscapeDataString(_navigationManager.Uri);
            _navigationManager.NavigateTo($"authentication/{RemoteAuthenticationActions.LogOut}?returnUrl={returnUrl}");
        }
    }
}
