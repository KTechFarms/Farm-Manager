﻿using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;

namespace FarmManager.Web.Components.ReceiveInventoryDialog.SourceSelect
{
    /// <summary>
    /// An interface element for selecting the source of received inventory
    /// </summary>
    public class SourceSelectBase : FarmComponentBase<SourceSelectBase>
    {
        /// <summary>
        /// A handler to call when a barcode is scanned
        /// </summary>
        [Parameter]
        public Func<string, Task>? OnBarcodeScan { get; set; }

        /// <summary>
        /// A handler to call when clicking the manual button
        /// </summary>
        [Parameter]
        public Action? OnManual { get; set; }

        /// <summary>
        /// Calls the provided handler when the barcode changes
        /// </summary>
        /// <param name="value"></param>
        protected void HandleBarcodeChange(string value)
        {
            if (OnBarcodeScan != null)
                OnBarcodeScan(value);
        }

        /// <summary>
        /// Calls the provided handler when the manual button is 
        /// clicked
        /// </summary>
        protected void HandleManual()
        {
            if (OnManual != null)
                OnManual();
        }
    }
}
