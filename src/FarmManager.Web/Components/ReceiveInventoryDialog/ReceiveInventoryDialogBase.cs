﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Components.ReceiveInventoryDialog
{
    /// <summary>
    /// An enumeration for determing what to display on the 
    /// dialog
    /// </summary>
    public enum ReceivingStage
    {
        SourceSelect,
        ProductSelect,
        Summary
    }
    
    /// <summary>
    /// A dialog for receiving inventory at a particular location
    /// </summary>
    public class ReceiveInventoryDialogBase : FarmManagerDialogBase<ReceiveInventoryDialogBase>
    {
        /// <summary>
        /// The id of the farm location at which inventory is being received
        /// </summary>
        [Parameter]
        public int FarmLocationId { get; set; }

        /// <summary>
        /// The curren stage of the receiving process
        /// </summary>
        protected ReceivingStage Stage { get; set; } = ReceivingStage.SourceSelect;

        /// <summary>
        /// The barcode scanned during the source select stage
        /// </summary>
        protected string ScannedBarcode { get; set; }

        /// <summary>
        /// All items that have been added during the "receiving" session
        /// </summary>
        protected List<InventoryItem> Items { get; set; } = new();

        /// <summary>
        /// A handler for when a barcode is scanned that tries to automatically 
        /// set the product & variant of the new inventory
        /// </summary>
        /// <param name="barcode">The scanned barcode</param>
        /// <returns></returns>
        public async Task HandleBarcodeScan(string barcode)
        {
            try
            {
                Store.SetLoading("BarcodeScan");
                ScannedBarcode = barcode;

                InventoryItem? item = Items
                    .Where(i => i.Barcode?.RawBarcode == barcode)
                    .FirstOrDefault();

                if (item != null)
                {
                    item.Quantity++;
                    Stage = ReceivingStage.Summary;
                }
                else
                {
                    item = await ApiService.Request<InventoryItem>(
                        HttpMethod.Get,
                        "barcode/product",
                        queryParameters: new Dictionary<string, string>
                        {
                            { "barcode", barcode}
                        }
                    );

                    if (
                        string.IsNullOrEmpty(item.Variant)
                        || string.IsNullOrEmpty(item.Product)
                    )
                    {
                        Stage = ReceivingStage.ProductSelect;
                    }
                    else
                    {
                        Items.Add(item);
                        Stage = ReceivingStage.Summary;
                    }
                }

                StateHasChanged();
            }
            catch(Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("BarcodeScan");
            }
        }

        /// <summary>
        /// A handler for when a product variant for the inventory is manually
        /// selected that sets the product & variant of the new inventory
        /// </summary>
        /// <param name="item">The selected variant</param>
        /// <returns></returns>
        public void HandleVariantSelect(InventoryItem item)
        {
            if (item != default)
            {
                InventoryItem? existingItem = Items
                    .Where(i => 
                        i.Barcode?.RawBarcode == item.Barcode?.RawBarcode
                        && i.Product.Equals(item.Product, StringComparison.OrdinalIgnoreCase)
                        && i.Variant.Equals(item.Variant, StringComparison.OrdinalIgnoreCase)
                    )
                    .FirstOrDefault();

                if(existingItem != null)
                    existingItem.Quantity++;
                else
                    Items.Add(item);

                Stage = ReceivingStage.Summary;
                ScannedBarcode = string.Empty;
                StateHasChanged();
            }
        }

        /// <summary>
        /// Handles adding a new item once the summary stage has been
        /// reached
        /// </summary>
        public void HandleAddItem()
        {
            Stage = ReceivingStage.SourceSelect;
            StateHasChanged();
        }

        /// <summary>
        /// Bypasses the search for existing inventory barcodes
        /// and goes straight to the manual product selection
        /// </summary>
        public void HandleManual()
        {
            ScannedBarcode = string.Empty;
            Stage = ReceivingStage.ProductSelect;
            StateHasChanged();
        }

        /// <summary>
        /// Removes the selected inventory item from the received list
        /// </summary>
        /// <param name="item">The inventory item to remove</param>
        public void HandleDelete(InventoryItem item)
        {
            Items.Remove(item);
            StateHasChanged();
        }

        /// <summary>
        /// Cancels the current stage of the "add item" process and
        /// closes the dialog if appropriate
        /// </summary>
        protected void HandleCancel()
        {
            switch (Stage)
            {
                case ReceivingStage.SourceSelect:
                    if (Items.Count == 0)
                        TryCancel();
                    else
                    {
                        Stage = ReceivingStage.Summary;
                        StateHasChanged();
                    }
                    break;
                case ReceivingStage.ProductSelect:
                    if (Items.Count == 0)
                        Stage = ReceivingStage.SourceSelect;
                    else
                        Stage = ReceivingStage.Summary;

                    StateHasChanged();
                    break;
                case ReceivingStage.Summary:
                    TryCancel();
                    break;
                default:
                    TryCancel();
                    break;
            }
        }

        /// <summary>
        /// Handles saving the new inventory
        /// </summary>
        /// <returns></returns>
        protected override async Task HandleSave()
        {
            if (Items.Count == 0)
                throw new ApplicationException("Add inventory before saving");

            await ApiService.Request<List<Inventory>>(
                HttpMethod.Post,
                $"inventory/{FarmLocationId}",
                body: Items
            );

            _dialog.Close(DialogResult.Ok(Result));
        }
    }
}
