﻿using FarmManager.Data.DTOs.Production;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;

namespace FarmManager.Web.Components.ReceiveInventoryDialog.Summary
{
    /// <summary>
    /// A component for displaying a summary of items received
    /// </summary>
    public class SummaryBase : FarmComponentBase<SummaryBase>
    {
        /// <summary>
        /// A handler to call when a barcode is scanned
        /// </summary>
        [Parameter]
        public Func<string, Task>? OnBarcodeScan { get; set; }

        /// <summary>
        /// A handler to call when clicking the manual button
        /// </summary>
        [Parameter]
        public Action? OnManual { get; set; }

        /// <summary>
        /// A handler to call when clicking the delete button
        /// </summary>
        [Parameter]
        public Action<InventoryItem>? OnDelete { get; set; }

        /// <summary>
        /// The list of received items
        /// </summary>
        [Parameter]
        public List<InventoryItem> Items { get; set; }

        /// <summary>
        /// A handler for adding a new item
        /// </summary>
        [Parameter]
        public Action HandleAdd { get; set; }

        /// <summary>
        /// The number of inventory items displayed per page in the 
        /// summary table
        /// </summary>
        protected const int ItemsPerPage = 5;

        /// <summary>
        /// Calls the provided handler when the barcode changes
        /// </summary>
        /// <param name="value"></param>
        protected void HandleBarcodeChange(string value)
        {
            if (OnBarcodeScan != null)
                OnBarcodeScan(value);

            StateHasChanged();
        }

        /// <summary>
        /// Calls the provided handler when manually adding an item
        /// </summary>
        protected void HandleManual()
        {
            if (OnManual != null) 
                OnManual();
        }

        /// <summary>
        /// Calls the provided handler when deleting an item
        /// </summary>
        /// <param name="item"></param>
        protected void HandleDelete(InventoryItem item)
        {
            if (OnDelete != null)
                OnDelete(item);
        }
    }
}
