﻿using FarmManager.Common.Helpers;
using FarmManager.Data.DTOs.Production;
using FarmManager.Data.Entities.Operations;
using FarmManager.Web.Shared;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FarmManager.Web.Components.ReceiveInventoryDialog.ProductSelect
{
    /// <summary>
    /// An interface for selecting the product and variant to be received
    /// as inventory
    /// </summary>
    public class ProductSelectBase : FarmComponentBase<ProductSelectBase>
    {
        /// <summary>
        /// The handler for when a variant is selected
        /// </summary>
        [Parameter]
        public Action<InventoryItem>? OnVariantSelect { get; set; }

        /// <summary>
        /// The barcode entered during the source select stage
        /// </summary>
        [Parameter]
        public string Barcode { get; set; }

        /// <summary>
        /// A list of available products to select
        /// </summary>
        protected List<Product> Products { get; set; } = new();

        /// <summary>
        /// The variants of the selected product
        /// </summary>
        protected List<ProductVariant> Variants { get; set; } = new();

        /// <summary>
        /// The id of the selected product
        /// </summary>
        protected int? SelectedProductId { get; set; }

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Store.SetLoading("InventoryProducts");

                await LoadProducts();
            }
            catch (Exception ex)
            {
                Snackbar.Add(ex.Message, Severity.Error);
            }
            finally
            {
                Store.ClearLoading("InventoryProducts");
            }
        }

        /// <summary>
        /// Loads products to display in the dropdown
        /// </summary>
        /// <returns></returns>
        private async Task LoadProducts()
        {
            // TODO: Handle filtering on the API
            Products = (await ApiService.Request<List<Product>>(
                HttpMethod.Get,
                "product"
            ))
            .Where(p => p.Category?.Name != "Produce")
            .ToList();
        }

        /// <summary>
        /// Sets the selected product and generates a list of 
        /// available variants
        /// </summary>
        /// <param name="productId"></param>
        protected void HandleProductChange(int? productId)
        {
            SelectedProductId = productId;
            Variants = Products
                .Where(p => p.Id == productId)
                .FirstOrDefault()
                ?.Variants?.ToList()
                ?? new List<ProductVariant>();
        }

        /// <summary>
        /// Call the handler when the variant changes
        /// </summary>
        /// <param name="variantId"></param>
        protected void HandleVariantChange(int? variantId)
        {
            ProductVariant? variant = Variants
                .Where(v => v.Id == variantId)
                .FirstOrDefault();

            InventoryItem item = new()
            {
                Product = Products
                    .Where(p => p.Id == SelectedProductId)
                    .Select(p => p.Name)
                    .FirstOrDefault() ?? string.Empty,
                VariantId = variant?.Id ?? 0,
                Variant = variant?.Name ?? string.Empty,
                Barcode = string.IsNullOrEmpty(Barcode)
                    ? null
                    : new Barcode
                    {
                        RawBarcode = Barcode,
                        UserFriendlyBarcode = Barcode,
                        AIs = new() { }
                    },
                Quantity = 1,
                LotNumber = string.Empty
            };

            if (OnVariantSelect != null)
                OnVariantSelect(item);
        }
    }
}
