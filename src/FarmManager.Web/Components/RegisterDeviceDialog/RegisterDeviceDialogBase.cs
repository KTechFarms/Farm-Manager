﻿using FarmManager.Data.Entities.Devices;
using FarmManager.Web.Shared;

namespace FarmManager.Web.Components.RegisterDeviceDialog
{
    /// <summary>
    /// A dialog for registering a new device with the Farm Manager system
    /// </summary>
    public class RegisterDeviceDialogBase : FarmManagerDialogBase<RegisterDeviceDialogBase>
    {
        /// <summary>
        /// Whether or not the device name is valid
        /// </summary>
        protected bool InError { get; set; } = false;

        /// <summary>
        /// The message to display to the user
        /// </summary>
        protected string ErrorMessage { get; set; } = string.Empty;

        /// <summary>
        /// The id of the device to register
        /// </summary>
        protected string DeviceId { get; set; }

        /// <summary>
        /// Attempts to register a new device with the Farm Manager system
        /// and then closes the dialog
        /// </summary>
        /// <returns></returns>
        protected override async Task HandleSave()
        {
            if(string.IsNullOrEmpty(DeviceId) || DeviceId.Contains(" "))
            {
                InError = true;
                ErrorMessage = "Device Id is required and cannot contain spaces";
                return;
            }
            else
            {
                InError = false;
                ErrorMessage = string.Empty;
            }

            await ApiService.Request<PendingDevice>(
                HttpMethod.Post,
                "device/register",
                queryParameters: new Dictionary<string, string>
                {
                    { "deviceId", DeviceId }
                });
        }
    }
}
