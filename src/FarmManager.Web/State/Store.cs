﻿namespace FarmManager.Web.State
{
    /// <summary>
    /// The application state store
    /// </summary>
    public class Store
    {
        /// <summary>
        /// Whether or not something is loading
        /// </summary>
        private bool _isLoading { get; set; } = false;

        /// <summary>
        /// A dictionary of currently executing load actions
        /// </summary>
        private Dictionary<string, DateTimeOffset> _loadingActions { get; set; } = new Dictionary<string, DateTimeOffset>();

        public event Action? OnLoadingChange;
        /// <summary>
        /// Whether or not the app is loading data. This determines
        /// whether or not a loading overlay is presented on the screen
        /// </summary>
        public bool IsLoading { get => _isLoading; }
        public void SetLoading(string identifier) 
        {
            _loadingActions.Add(identifier, DateTimeOffset.UtcNow);
            _isLoading = true;
            NotifyLoadingChange();
        }
        public void ClearLoading(string identifier) 
        {
            if (_loadingActions.TryGetValue(identifier, out DateTimeOffset addedAt))
            {
                // TODO: Add a minimum duration and re-schedule based on addedAt

                _loadingActions.Remove(identifier);
                if(_loadingActions.Count == 0)
                {
                    _isLoading = false;
                    NotifyLoadingChange();
                }
            }
        }
        private void NotifyLoadingChange() => OnLoadingChange?.Invoke();

        private bool _drawerOpen { get; set; } = true;
        /// <summary>
        /// Whether or not the navigation drawer is open
        /// </summary>
        public bool DrawerOpen
        {
            get => _drawerOpen;
            set
            {
                _drawerOpen = value;
                NotifyDrawerChange();
            }
        }
        public event Action? OnDrawerChange;
        private void NotifyDrawerChange() => OnDrawerChange?.Invoke();
    }
}
