﻿namespace FarmManager.Web.Config
{
    /// <summary>
    /// Application settings specific to the FarmManager.Web
    /// web app
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// The base url for the Farm Manager Api
        /// </summary>
        public string BaseApiUrl { get; set; }

        /// <summary>
        /// The Farm Manager Api registration's scope that
        /// allows access as the current user
        /// </summary>
        public string ApiScope { get; set; }
    }
}
