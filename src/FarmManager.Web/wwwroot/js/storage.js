﻿function getAADToken() {
    try {
        let sessionStorageCount = sessionStorage.length;

        let tokenKey = "";
        for (let i = 0; i < sessionStorageCount; i++) {
            if (sessionStorage.key(i).includes("idtoken")) {
                tokenKey = sessionStorage.key(i);
                break;
            }
        }

        let token = "";
        if (tokenKey !== "") {
            let tokenString = sessionStorage.getItem(tokenKey);
            let tokenObject = JSON.parse(tokenString);
            token = tokenObject["secret"];
        }

        return token;
    }
    catch (e) {
        console.log("Error: ", e);
        return "";
    }
}