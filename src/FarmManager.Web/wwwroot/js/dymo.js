﻿function getDymoPrinters() {
    return dymo.label.framework.getPrinters();
}

function printLabel(printerName, labelXml)
{
    try {
        let label = dymo.label.framework.openLabelXml(labelXml);
        label.print(printerName, null, labelXml);
    }
    catch (e) {
        console.log("e:", e);
        throw new Error("Unable to validate provided label");
    }
}