﻿namespace FarmManager.Services.Identity
{
    /// <summary>
    /// Settings required by 3rd party identity providers
    /// </summary>
    public class IdentityProviderSettings
    {
        /// <summary>
        /// The permission scopes that are required
        /// </summary>
        public string[] Scopes { get; set; }

        /// <summary>
        /// The id of the provider tenant
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// The id of the application with permissions in the tenant
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// The secret for the application
        /// </summary>
        public string ClientSecret { get; set; }
    }
}
