﻿using FarmManager.Data.DTOs.Identity;

namespace FarmManager.Services.Identity
{
    /// <summary>
    /// An interface for interacting with 3rd party identity providers
    /// </summary>
    public interface IIdentityProviderService
    {
        /// <summary>
        /// Retrieves user information for the provided user ids
        /// </summary>
        /// <param name="ids">The user Ids to retrieve information about</param>
        /// <returns></returns>
        Task<IEnumerable<FarmManagerUser>> RetrieveUsersByIdAsync(IEnumerable<string> ids);
    }
}
