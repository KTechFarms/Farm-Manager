﻿using Azure.Identity;
using FarmManager.Data.DTOs.Identity;
using Microsoft.Extensions.Options;
using Microsoft.Graph;

namespace FarmManager.Services.Identity.Microsoft
{
    /// <summary>
    /// An Identity Provider that interacts with Microsoft Graph
    /// </summary>
    public class MicrosoftIdentityProviderService : IIdentityProviderService
    {
        private readonly GraphServiceClient _client;

        public MicrosoftIdentityProviderService(IOptions<IdentityProviderSettings> identityOptions)
        {
            IdentityProviderSettings settings = identityOptions.Value;

            TokenCredentialOptions options = new TokenCredentialOptions
            {
                AuthorityHost = AzureAuthorityHosts.AzurePublicCloud
            };

            ClientSecretCredential credential = new ClientSecretCredential(
                settings.TenantId, 
                settings.ClientId, 
                settings.ClientSecret, 
                options
            );

            _client = new GraphServiceClient(credential, settings.Scopes);
        }

        public async Task<IEnumerable<FarmManagerUser>> RetrieveUsersByIdAsync(IEnumerable<string> ids)
        {
            string filter = "id eq";
            for(int i = 0; i < ids.Count(); i++)
            {
                filter = i == 0
                    ? filter + $" '{ids.ElementAt(i)}'"
                    : filter + $" or id eq '{ids.ElementAt(i)}'";
            }

            List<FarmManagerUser> users = new();

            IGraphServiceUsersCollectionPage graphUsers = await _client.Users
                .Request()
                .Filter(filter)
                .GetAsync();

            users.AddRange(graphUsers
                .Select(u => new FarmManagerUser
                {
                    ExternalId = u.Id,
                    Email = u.Mail,
                    FirstName = u.GivenName,
                    LastName = u.Surname
                }));

            while (graphUsers.NextPageRequest != null)
            {
                graphUsers = await graphUsers.NextPageRequest.GetAsync();
                users.AddRange(graphUsers
                    .Select(u => new FarmManagerUser
                    {
                        ExternalId = u.Id,
                        Email = u.Mail,
                        FirstName = u.GivenName,
                        LastName = u.Surname
                    }));
            }

            return users;
        }
    }
}
