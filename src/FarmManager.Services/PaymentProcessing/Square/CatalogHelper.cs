﻿using FarmManager.Services.PaymentProcessing.DTOs;
using Square.Models;

namespace FarmManager.Services.PaymentProcessing.Square
{
    public static class CatalogHelper
    {
        /// <summary>
        /// Creates a timestamp in the format accepted by the square platform
        /// </summary>
        /// <param name="timeStamp">The DateTimeOffset that should be formatted</param>
        /// <returns></returns>
        public static string BuildTimeStampString(DateTimeOffset timeStamp)
        {
            timeStamp = timeStamp.ToUniversalTime();
            return timeStamp.ToString("yyyy-MM-dd")
                + "T"
                + timeStamp.ToString("HH:mm:ss")
                + "Z";
        }

        /// <summary>
        /// Creates an inventory adjustment object for the square platform
        /// </summary>
        /// <param name="inventoryReference">A unique identifier for the transaction</param>
        /// <param name="timestampString">The timestamp at which the request was created</param>
        /// <param name="update">The inventory information used to build the object</param>
        /// <returns></returns>
        public static InventoryChange BuildInventoryAdjustment(
            Guid inventoryReference,
            string timestampString,
            InventoryUpdate update
        )
        {
            var adjustment = new InventoryAdjustment.Builder()
                .ReferenceId(inventoryReference.ToString())
                .CatalogObjectId(update.VariantId)
                .FromState(update.PriorState)
                .ToState(update.CurrentState)
                .LocationId(update.BusinessLocationId)
                .Quantity(update.Quantity.ToString())
                .OccurredAt(timestampString)
                .Build();

            return new InventoryChange.Builder()
                .Type(update.ChangeType)
                .Adjustment(adjustment)
                .Build();
        }

        // NOTE: Inventory transfers require a paid square subscription, so make two adjustments instead
        /// <summary>
        /// Creates an inventory transfer object for the square platform
        /// </summary>
        /// <param name="fromInventoryReference">A unique identifier for the inventory source</param>
        /// <param name="toInventoryReference">A unique identifier for the inventory destination</param>
        /// <param name="timestampString">The timestamp the transfer occured</param>
        /// <param name="update">The inventory information used to build the transfer object</param>
        /// <returns></returns>
        public static List<InventoryChange> BuildInventoryTransferAdjustments(
            Guid fromInventoryReference,
            Guid toInventoryReference,
            string timestampString,
            InventoryUpdate update
        )
        {
            InventoryChange fromChange = BuildInventoryAdjustment(
                fromInventoryReference,
                timestampString,
                new InventoryUpdate
                {
                    VariantId = update.VariantId,
                    PriorState = InventoryState.InStock,
                    // Use waste, since the only options appear to be 
                    // "Waste" or "Sold" and we just want to stop
                    // tracking them at the given location
                    CurrentState = InventoryState.Waste,
                    BusinessLocationId = update.PriorBusinessLocationId,
                    Quantity = update.Quantity,
                    ChangeType = InventoryChangeType.Adjustment
                });

            InventoryChange toChange = BuildInventoryAdjustment(
                toInventoryReference,
                timestampString,
                new InventoryUpdate
                {
                    VariantId = update.VariantId,
                    PriorState = InventoryState.None,
                    CurrentState = InventoryState.InStock,
                    BusinessLocationId = update.BusinessLocationId,
                    Quantity = update.Quantity,
                    ChangeType = InventoryChangeType.Adjustment
                });

            return new List<InventoryChange> { fromChange, toChange };
        }

        //public static InventoryChange BuildInventoryTransfer(
        //    Guid inventoryReference,
        //    string timestampString,
        //    InventoryUpdate update
        //)
        //{
        //    var transfer = new InventoryTransfer.Builder()
        //        .ReferenceId(inventoryReference.ToString())
        //        .CatalogObjectId(update.VariantId)
        //        .FromLocationId(update.PriorBusinessLocationId)
        //        .ToLocationId(update.BusinessLocationId)
        //        .Quantity(update.Quantity.ToString())
        //        .OccurredAt(timestampString)
        //        .Build();

        //    return new InventoryChange.Builder()
        //        .Type(update.ChangeType)
        //        .Transfer(transfer)
        //        .Build();
        //}

        /// <summary>
        /// Creates a unit creation object for the square platform
        /// </summary>
        /// <param name="unit">The unit to create</param>
        /// <returns></returns>
        public static UpsertCatalogObjectRequest BuildUnitCreationRequest(UnitCreation unit)
        {
            MeasurementUnit measurement;
            string squareUnitType = WeightUnit.SymbolLookups.TryGetValue(unit.Symbol, out _)
                ? UnitType.Weight
                : VolumeUnit.SymbolLookups.TryGetValue(unit.Symbol, out _)
                    ? UnitType.Volume
                    : UnitType.Custom;

            switch(squareUnitType)
            {
                case UnitType.Weight:
                    measurement = new MeasurementUnit.Builder()
                        .WeightUnit(WeightUnit.SymbolLookups[unit.Symbol])
                        .Build();
                    break;

                case UnitType.Volume:
                    measurement = new MeasurementUnit.Builder()
                        .VolumeUnit(unit.Symbol)
                        .Build();
                    break;

                default:
                    MeasurementUnitCustom customUnit = new MeasurementUnitCustom(unit.Name, unit.Symbol);
                    measurement = new MeasurementUnit.Builder()
                        .CustomUnit(customUnit)
                        .Build();
                    break;
            }

            CatalogMeasurementUnit unitData = new CatalogMeasurementUnit
                .Builder()
                .MeasurementUnit(measurement)
                .Precision(unit.Precision)
                .Build();

            CatalogObject unitObject = new CatalogObject
                .Builder(CatalogObjectType.Unit, $"#{unit.Symbol}")
                .MeasurementUnitData(unitData)
                .Build();

            return new UpsertCatalogObjectRequest
                .Builder(Guid.NewGuid().ToString(), unitObject)
                .Build();
        }

        /// <summary>
        /// Creates an category object for the square platform
        /// </summary>
        /// <param name="category">The category to create</param>
        /// <returns></returns>
        public static UpsertCatalogObjectRequest BuildCategoryCreationRequest(CategoryCreation category)
        {
            CatalogCategory categoryData = new CatalogCategory
                .Builder()
                .Name(category.Name)
                .Build();

            CatalogObject categoryObject = new CatalogObject
                .Builder(CatalogObjectType.Category, $"#{category.Name}")
                .CategoryData(categoryData)
                .Build();

            return new UpsertCatalogObjectRequest
                .Builder(Guid.NewGuid().ToString(), categoryObject)
                .Build();
        }

        /// <summary>
        /// Builds a product creation objject for the square platform
        /// </summary>
        /// <param name="product">The product to create</param>
        /// <returns></returns>
        public static UpsertCatalogObjectRequest BuildProductCreationRequest(ProductDetails product)
        {
            List<CatalogObject> variants = new();

            foreach (ProductVariantDetails variant in product.Variants)
            {
                Money variantPrice = new Money
                    .Builder()
                    .Amount(Convert.ToInt64(variant.Price * 100))
                    .Currency(CatalogCurrency.Dollars)
                    .Build();

                CatalogItemVariation productVariant = new CatalogItemVariation
                    .Builder()
                    .ItemId($"#{product.Name}")
                    .Name(variant.Name)
                    .Sku(variant.SKU)
                    .PricingType(CatalogPricingType.Fixed)
                    .PriceMoney(variantPrice)
                    .TrackInventory(true)
                    .InventoryAlertThreshold(variant.LowInventoryThreshold)
                    .MeasurementUnitId(variant.MeasurementUnit)
                    .Build();

                CatalogObject catalogVariant = new CatalogObject
                    .Builder(CatalogObjectType.ItemVariation, $"#{variant.Name}")
                    .ItemVariationData(productVariant)
                    .PresentAtAllLocations(true)
                    .Build();

                variants.Add(catalogVariant);
            }

            CatalogItem productData = new CatalogItem
                .Builder()
                .Name(product.Name)
                .Description(product.Description)
                .AvailableOnline(product.AvailableForSale)
                .AvailableForPickup(product.AvailableForSale)
                .Variations(variants)
                .CategoryId(product.Category)
                .Build();

            CatalogObject productObject = new CatalogObject
                .Builder(CatalogObjectType.Item, $"#{product.Name}")
                .ItemData(productData)
                .PresentAtAllLocations(true)
                .Build();

            return new UpsertCatalogObjectRequest
                .Builder(Guid.NewGuid().ToString(), productObject)
                .Build();
        }

        /// <summary>
        /// Builds an product update object
        /// </summary>
        /// <param name="product">The updated product information</param>
        /// <param name="squareProduct">The square product object that needs to be updated</param>
        /// <returns></returns>
        public static UpsertCatalogObjectRequest BuildProductUpdateRequest(ProductDetails product, CatalogObject squareProduct)
        {
            CatalogItem productData = new CatalogItem
                .Builder()
                .Name(product.Name)
                .Description(product.Description)
                .AvailableOnline(product.AvailableForSale)
                .AvailableForPickup(product.AvailableForSale)
                .CategoryId(product?.Category)
                .Variations(squareProduct.ItemData.Variations)
                .Build();

            CatalogObject productObject = new CatalogObject
                .Builder(CatalogObjectType.Item, product.ExternalId)
                .Version(squareProduct.Version)
                .ItemData(productData)
                .Build();

            return new UpsertCatalogObjectRequest
                .Builder(Guid.NewGuid().ToString(), productObject)
                .Build();
        }

        /// <summary>
        /// Builds a product variant creation object
        /// </summary>
        /// <param name="variant">The variant that needs to be created</param>
        /// <returns></returns>
        public static UpsertCatalogObjectRequest BuildProductVariationCreationRequest(ProductVariantDetails variant)
        {
            Money variantPrice = new Money
                .Builder()
                .Amount(Convert.ToInt64(variant.Price * 100))
                .Currency(CatalogCurrency.Dollars)
                .Build();

            CatalogItemVariation productVariant = new CatalogItemVariation
                .Builder()
                .ItemId(variant.ExternalProductId)
                .Name(variant.Name)
                .Sku(variant.SKU)
                .PricingType(CatalogPricingType.Fixed)
                .PriceMoney(variantPrice)
                .TrackInventory(true)
                .InventoryAlertThreshold(variant.LowInventoryThreshold)
                .MeasurementUnitId(variant?.MeasurementUnit)
                .Build();

            CatalogObject catalogVariant = new CatalogObject
                .Builder(CatalogObjectType.ItemVariation, $"#{variant?.Name}")
                .ItemVariationData(productVariant)
                .PresentAtAllLocations(true)
                .Build();

            return new UpsertCatalogObjectRequest
                .Builder(Guid.NewGuid().ToString(), catalogVariant)
                .Build();
        }

        /// <summary>
        /// Builds a product update request
        /// </summary>
        /// <param name="variant">The updated variant information</param>
        /// <param name="version">The version of the variant to update</param>
        /// <returns></returns>
        public static UpsertCatalogObjectRequest BuildProductVariantUpdateRequest(ProductVariantDetails variant, long version)
        {
            Money variantPrice = new Money
                .Builder()
                .Amount(Convert.ToInt64(variant.Price * 100))
                .Currency(CatalogCurrency.Dollars)
                .Build();

            CatalogItemVariation productVariant = new CatalogItemVariation
                .Builder()
                .ItemId(variant.ExternalProductId)
                .Name(variant.Name)
                .Sku(variant.SKU)
                .PricingType(CatalogPricingType.Fixed)
                .PriceMoney(variantPrice)
                .TrackInventory(true)
                .InventoryAlertThreshold(variant.LowInventoryThreshold)
                .MeasurementUnitId(variant?.MeasurementUnit)
                .Build();

            CatalogObject catalogVariant = new CatalogObject
                .Builder(CatalogObjectType.ItemVariation, variant.ExternalId)
                .Version(version)
                .ItemVariationData(productVariant)
                .Build();

            return new UpsertCatalogObjectRequest
                .Builder(Guid.NewGuid().ToString(), catalogVariant)
                .Build();
        }
    }
}
