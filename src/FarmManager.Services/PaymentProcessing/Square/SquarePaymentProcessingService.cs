﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Services.Logging;
using FarmManager.Services.PaymentProcessing.DTOs;
using Square;
using Square.Exceptions;
using Square.Models;

namespace FarmManager.Services.PaymentProcessing.Square
{
    /// <summary>
    /// A payment processing service that interacts with the Square platform
    /// </summary>
    public class SquarePaymentProcessingService : IPaymentProcessingService
    {
        private readonly ISquareClient _client;
        private readonly IFarmLogger _logger;

        public SquarePaymentProcessingService(
            ISquareClient client,
            IFarmLogger logger
        )
        {
            _client = client;
            _logger = logger;
        }

        public async Task CreateInventoryAsync(InventoryUpdate update)
        {
            Guid inventoryReference = Guid.NewGuid();
            Guid batchReference = Guid.NewGuid();
            string timeStampString = CatalogHelper.BuildTimeStampString(update.Timestamp);

            update.PriorState = InventoryState.None;
            update.CurrentState = InventoryState.InStock;
            update.ChangeType = InventoryChangeType.Adjustment;

            List<InventoryChange> changes = new()
            {
                CatalogHelper.BuildInventoryAdjustment(inventoryReference, timeStampString, update)
            };

            BatchChangeInventoryRequest request = new BatchChangeInventoryRequest.Builder(batchReference.ToString())
                .Changes(changes)
                .IgnoreUnchangedCounts(true)
                .Build();

            await SubmitInventoryRequest(request);
        }

        public async Task TransferInventoryAsync(InventoryUpdate update)
        {
            Guid inventoryReference = Guid.NewGuid();
            Guid fromInventoryReference = Guid.NewGuid();
            Guid batchReference = Guid.NewGuid();
            string timeStampString = CatalogHelper.BuildTimeStampString(update.Timestamp);

            List<InventoryChange> changes = new();
            changes.AddRange(CatalogHelper.BuildInventoryTransferAdjustments(
                fromInventoryReference,
                inventoryReference,
                timeStampString,
                update
            ));

            BatchChangeInventoryRequest request = new BatchChangeInventoryRequest.Builder(batchReference.ToString())
                .Changes(changes)
                .IgnoreUnchangedCounts(true)
                .Build();

            await SubmitInventoryRequest(request);
        }

        public async Task<string> CreateUnitAsync(UnitCreation unit)
        {
            return await SubmitCatalogRequest(CatalogHelper.BuildUnitCreationRequest(unit));
        }

        public async Task DeleteUnitAsync(string unitId)
        {
            await DeleteCatalogObject(unitId);
        }

        public async Task<string> CreateCategoryAsync(CategoryCreation category)
        {
            return await SubmitCatalogRequest(CatalogHelper.BuildCategoryCreationRequest(category));
        }

        public async Task DeleteCategoryAsync(string categoryId)
        {
            await DeleteCatalogObject(categoryId);
        }

        public async Task<string> CreateLocationAsync(LocationCreation location)
        {
            ListLocationsResponse response = await _client.LocationsApi.ListLocationsAsync();

            if(response.Errors == null || !response.Errors.Any())
            {
                Location? existingLocation = response.Locations
                    .FirstOrDefault(l => l.Name == location.Name);

                if (existingLocation == null)
                    return await RequestLocationCreationAsync(
                        LocationHelper.BuildLocationCreationRequest(location));

                else
                {
                    await UpdateLocation(
                        LocationHelper.BuildLocationEnableRequest(location),
                        existingLocation.Id
                    );

                    return existingLocation.Id;
                }
            }

            return await RequestLocationCreationAsync(
                LocationHelper.BuildLocationCreationRequest(location));
        }

        public async Task DeleteLocationAsync(string locationId)
        {
            await UpdateLocation(
                LocationHelper.BuildLocationDisableRequest(), 
                locationId
            );
        }

        public async Task<Product> CreateProductAsync(ProductDetails product)
        {
            CatalogObject result = await SubmitProductCreationRequest(CatalogHelper.BuildProductCreationRequest(product));

            return new Product
            {
                Id = product.Id,
                ExternalId = result.Id,
                Variants = result.ItemData.Variations
                    .Select(squareV => new ProductVariant
                    {
                        Id = product.Variants
                                .Where(v => v.Name == squareV.ItemVariationData.Name)
                                .FirstOrDefault()?.Id ?? 0,
                        ExternalId = squareV.Id
                    })
                    .ToList()
            };
        }

        public async Task UpdateProductAsync(ProductDetails product)
        {
            CatalogObject squareItem = await RetrieveCatalogObject(product.ExternalId ?? string.Empty);
            await SubmitCatalogRequest(CatalogHelper.BuildProductUpdateRequest(product, squareItem));
        }

        public async Task DeleteProductAsync(string productId)
        {
            await DeleteCatalogObject(productId);
        }

        public async Task<ProductVariant> CreateProductVariantAsync(ProductVariantDetails variant)
        {
            CatalogObject result = await SubmitProductCreationRequest(CatalogHelper.BuildProductVariationCreationRequest(variant));

            return new ProductVariant
            {
                Id = variant.Id,
                ExternalId = result.Id
            };
        }

        public async Task UpdateProductVariantAsync(ProductVariantDetails variant)
        {
            CatalogObject squareItemVariant = await RetrieveCatalogObject(variant?.ExternalId ?? string.Empty);
            await SubmitCatalogRequest(CatalogHelper.BuildProductVariantUpdateRequest(variant, squareItemVariant?.Version ?? default));
        }

        public async Task DeleteProductVariantAsync(string variantId)
        {
            await DeleteCatalogObject(variantId); 
        }

        //////////////////////////////////////////
        //             API Requests             //
        //////////////////////////////////////////

        /// <summary>
        /// Submits an inventory request to the Square API
        /// </summary>
        /// <param name="request">The request to submit</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task SubmitInventoryRequest(BatchChangeInventoryRequest request)
        {
            try
            {
                BatchChangeInventoryResponse result = await _client.InventoryApi
                    .BatchChangeInventoryAsync(request);
            }
            catch (ApiException ex)
            {
                string? reason = ex.Errors.FirstOrDefault()?.Detail;
                string message = $"Unable to update inventory: {reason}";
                _logger.LogError(message, ex, request);
                throw new ApplicationException(message);
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ApplicationException))
                    throw;

                _logger.LogError($"Unable to update inventory: {ex.Message}", ex, request);
                throw;
            }
        }

        /// <summary>
        /// Retrieves a catalog object from the Square API
        /// </summary>
        /// <param name="objectId">The id of the object to retrieve</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<CatalogObject> RetrieveCatalogObject(string objectId)
        {
            try
            {
                RetrieveCatalogObjectResponse result = await _client.CatalogApi.RetrieveCatalogObjectAsync(objectId);
                return result.MObject;
            }
            catch (ApiException ex)
            {
                string? reason = ex.Errors.FirstOrDefault()?.Detail;
                string message = $"Unable to retrieve catalog object: {reason}";
                _logger.LogError(message, ex, objectId);
                throw new ApplicationException(message);
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ApplicationException))
                    throw;

                _logger.LogError($"Unable to retrieve catalog object: {ex.Message}", ex, objectId);
                throw;
            }
        }

        /// <summary>
        /// Submits a catalog request through the Square API
        /// </summary>
        /// <param name="request">The request to submit</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<string> SubmitCatalogRequest(UpsertCatalogObjectRequest request)
        {
            try
            {
                UpsertCatalogObjectResponse result = await _client.CatalogApi
                    .UpsertCatalogObjectAsync(request);

                return result.CatalogObject.Id;
            }
            catch (ApiException ex)
            {
                string? reason = ex.Errors.FirstOrDefault()?.Detail;
                string message = $"Unable to update catalog: {reason}";
                _logger.LogError(message, ex, request);
                throw new ApplicationException(message);
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ApplicationException))
                    throw;

                _logger.LogError($"Unable to update catalog: {ex.Message}", ex, request);
                throw;
            }
        }

        /// <summary>
        /// Submits a product creation request to the square API
        /// </summary>
        /// <param name="request">The request to submit</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<CatalogObject> SubmitProductCreationRequest(UpsertCatalogObjectRequest request)
        {
            try
            {
                UpsertCatalogObjectResponse result = await _client.CatalogApi
                    .UpsertCatalogObjectAsync(request);


                return result.CatalogObject;
            }
            catch (ApiException ex)
            {
                string? reason = ex.Errors.FirstOrDefault()?.Detail;
                string message = $"Unable to update catalog: {reason}";
                _logger.LogError(message, ex, request);
                throw new ApplicationException(message);
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ApplicationException))
                    throw;

                _logger.LogError($"Unable to update catalog: {ex.Message}", ex, request);
                throw;
            }
        }

        /// <summary>
        /// Submits a catalog deletion request through the Square API
        /// </summary>
        /// <param name="objectId">The id of the object to delete</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task DeleteCatalogObject(string objectId)
        {
            try
            {
                DeleteCatalogObjectResponse result = await _client.CatalogApi
                    .DeleteCatalogObjectAsync(objectId);
            }
            catch (ApiException ex)
            {
                string? reason = ex.Errors.FirstOrDefault()?.Detail;
                string message = $"Unable to delete catalog object: {reason}";
                _logger.LogError(message, ex, objectId);
                throw new ApplicationException(message);
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ApplicationException))
                    throw;

                _logger.LogError($"Unable to delete catalog object: {ex.Message}", ex, objectId);
                throw;
            }
        }

        /// <summary>
        /// Submits a location creation request through the square API
        /// </summary>
        /// <param name="request">The request to submit</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task<string> RequestLocationCreationAsync(CreateLocationRequest request)
        {
            try
            {
                CreateLocationResponse response = await _client.LocationsApi
                    .CreateLocationAsync(request);

                return response.Location.Id;
            }
            catch (ApiException ex)
            {
                string? reason = ex.Errors.FirstOrDefault()?.Detail;
                string message = $"Unable to create location: {reason}";
                _logger.LogError(message, ex, request);
                throw new ApplicationException(message);
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ApplicationException))
                    throw;

                _logger.LogError($"Unable to create location: {ex.Message}", ex, request);
                throw;
            }
        }

        /// <summary>
        /// Submits a location update request
        /// </summary>
        /// <param name="request">The request to submit</param>
        /// <param name="locationId">The id of the location that should be updated</param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private async Task UpdateLocation(UpdateLocationRequest request, string locationId)
        {
            try
            {
                await _client.LocationsApi
                    .UpdateLocationAsync(locationId, request);
            }
            catch (ApiException ex)
            {
                string? reason = ex.Errors.FirstOrDefault()?.Detail;
                string message = $"Unable to update location: {reason}";
                _logger.LogError(message, ex, locationId);
                throw new ApplicationException(message);
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ApplicationException))
                    throw;

                _logger.LogError($"Unable to update location: {ex.Message}", ex, locationId);
                throw;
            }
        }
    }
}
