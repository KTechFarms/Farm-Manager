﻿namespace FarmManager.Services.PaymentProcessing.Square
{
    /// <summary>
    /// Statuses of a location
    /// </summary>
    public static class LocationStatus
    {
        public const string Active = "ACTIVE";
        public const string Inactive = "INACTIVE";
    }
}
