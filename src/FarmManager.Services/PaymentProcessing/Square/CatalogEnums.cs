﻿namespace FarmManager.Services.PaymentProcessing.Square
{
    /// <summary>
    /// Represents the different CatalogObject types within Square
    /// </summary>
    public static class CatalogObjectType
    {
        public const string Item = "ITEM";
        public const string ItemVariation = "ITEM_VARIATION";
        public const string Category = "CATEGORY";
        public const string Discount = "DISCOUNT";
        public const string Tax = "TAX";
        public const string Modifier = "MODIFIER";
        public const string ModifierList = "MODIFIER_LIST";
        public const string Image = "IMAGE";
        public const string Unit = "MEASUREMENT_UNIT";
    }

    /// <summary>
    /// Represents the pricing types within Square
    /// </summary>
    public static class CatalogPricingType
    {
        public const string Fixed = "FIXED_PRICING";
        public const string Variable = "VARIABLE_PRICING";
    }

    /// <summary>
    /// Represents a currency used to purchase items
    /// </summary>
    public static class CatalogCurrency
    {
        public const string Dollars = "USD";
        public const string Euro = "EUR";
    }

    /// <summary>
    /// Represents a unit of sale for selling catalog items 
    /// in Square
    /// </summary>
    public static class UnitType
    {
        public const string Custom = "TYPE_CUSTOM";
        public const string Area = "TYPE_AREA";
        public const string Length = "TYPE_LENGTH";
        public const string Volume = "TYPE_VOLUME";
        public const string Weight = "TYPE_WEIGHT";
        public const string Generic = "TYPE_GENERIC";

        public static Dictionary<string, string> Names = new()
        {
            { Custom, "Custom" },
            { Area, "Area" },
            { Length, "Length" },
            { Volume, "Volume" },
            { Weight, "Weight" },
            { Generic, "Generic" }
        };
    }

    /// <summary>
    /// Represents a generic unit in square
    /// </summary>
    public static class GenericUnit
    {
        // "Each" is a custom unit, and used to determine whether a real
        // square unit should be applied to a product
        public const string Each = "EACH";

        public static Dictionary<string, string> Names = new()
        {
            { Each, "each" }
        };

        public static Dictionary<string, string> Symbols = new()
        {
            { Each, "each" }
        };
    }

    /// <summary>
    /// Describes the types of units available to measure weight
    /// </summary>
    public static class WeightUnit
    {
        public const string Ounce = "IMPERIAL_WEIGHT_OUNCE";
        public const string Pound = "IMPERIAL_POUND";
        public const string Milligram = "METRIC_MILLIGRAM";
        public const string Gram = "METRIC_GRAM";
        public const string Kilogram = "METRIC_KILOGRAM";

        public static Dictionary<string, string> Names = new()
        {
            { Ounce, "ounce" },
            { Pound, "pound" },
            { Milligram, "milligram" },
            { Gram, "gram" },
            { Kilogram, "kilogram" }
        };

        public static Dictionary<string, string> Symbols = new()
        {
            { Ounce, "oz" },
            { Pound, "lb" },
            { Milligram, "mg" },
            { Gram, "g" },
            { Kilogram, "kg" }
        };

        public static Dictionary<string, string> SymbolLookups = new()
        {
            { "oz", Ounce },
            { "lb", Pound },
            { "mg", Milligram },
            { "g", Gram },
            { "kg", Kilogram }
        };
    }

    /// <summary>
    /// Describes the types of units available to measure volume
    /// </summary>
    public static class VolumeUnit
    {
        public const string Pint = "GENERIC_PINT";
        public const string Quart = "GENERIC_QUART";

        public static Dictionary<string, string> Names = new()
        {
            { Pint, "pint" },
            { Quart, "quart" }
        };

        public static Dictionary<string, string> Symbols = new()
        {
            { Pint, "pt" },
            { Quart, "qt" }
        };

        public static Dictionary<string, string> SymbolLookups = new()
        {
            { "pt", Pint },
            { "qt", Quart }
        };
    }

    /// <summary>
    /// The types of alerts available for inventory
    /// </summary>
    public static class InventoryAlert
    {
        public const string None = "NONE";
        public const string LowQuantity = "LOW_QUANTITY";
    }

    /// <summary>
    /// Describes the states of inventory 
    /// </summary>
    public static class InventoryState
    {
        public const string None = "NONE";
        public const string InStock = "IN_STOCK";
        public const string Sold = "SOLD";
        public const string Waste = "WASTE";
    }

    /// <summary>
    /// The type of inventory change that is occuring
    /// </summary>
    public static class InventoryChangeType
    {
        /// <summary>
        /// Physical counts are for performing an inventory count
        /// </summary>
        public const string PhysicalCount = "PHYSICAL_COUNT";
        /// <summary>
        /// Adjustments are for adding or removing stock
        /// </summary>
        public const string Adjustment = "ADJUSTMENT";
        public const string Transfer = "TRANSFER";
    }
}