﻿using FarmManager.Services.PaymentProcessing.DTOs;
using Square.Models;

namespace FarmManager.Services.PaymentProcessing.Square
{
    public static class LocationHelper
    {
        /// <summary>
        /// Builds a location creation object
        /// </summary>
        /// <param name="location">The location that needs to be updated</param>
        /// <returns></returns>
        public static CreateLocationRequest BuildLocationCreationRequest(LocationCreation location)
        {
            Address newAddress = new Address
                .Builder()
                .AddressLine1(location.Street)
                .Locality(location.City)
                .AdministrativeDistrictLevel1(location.State)
                .PostalCode(location.ZipCode)
                .Build();

            Location newLocation = new Location
                .Builder()
                .Name(location.Name)
                .Address(newAddress)
                .Build();

            return new CreateLocationRequest
                .Builder()
                .Location(newLocation)
                .Build();
        }

        /// <summary>
        /// Builds an object to enable a location
        /// </summary>
        /// <param name="location">The location to enable</param>
        /// <returns></returns>
        public static UpdateLocationRequest BuildLocationEnableRequest(LocationCreation location)
        {
            Address updatedAddress = new Address.Builder()
                .AddressLine1(location.Street)
                .Locality(location.City)
                .AdministrativeDistrictLevel1(location.State)
                .PostalCode(location.ZipCode)
                .Build();

            Location updatedLocation = new Location
                .Builder()
                .Status(LocationStatus.Active)
                .Address(updatedAddress)
                .Build();

            return new UpdateLocationRequest
                .Builder()
                .Location(updatedLocation)
                .Build();
        }

        /// <summary>
        /// Builds an object to disable a location
        /// </summary>
        /// <returns></returns>
        public static UpdateLocationRequest BuildLocationDisableRequest()
        {
            Location location = new Location
                .Builder()
                .Status(LocationStatus.Inactive)
                .Build();

            return new UpdateLocationRequest
                .Builder()
                .Location(location)
                .Build();
        }
    }
}
