﻿using FarmManager.Data.Entities.Operations;
using FarmManager.Services.PaymentProcessing.DTOs;

namespace FarmManager.Services.PaymentProcessing
{
    public interface IPaymentProcessingService
    {
        // Inventory

        /// <summary>
        /// Create new inventory
        /// </summary>
        /// <param name="update">Information regarding the inventory to create</param>
        /// <returns></returns>
        public Task CreateInventoryAsync(InventoryUpdate update);
        /// <summary>
        /// Transfer inventory between two locations
        /// </summary>
        /// <param name="update">Information regarding the inventory to transfer</param>
        /// <returns></returns>
        public Task TransferInventoryAsync(InventoryUpdate update);

        // Units

        /// <summary>
        /// Create a new unit
        /// </summary>
        /// <param name="unit">Information regarding the unit to create</param>
        /// <returns></returns>
        public Task<string> CreateUnitAsync(UnitCreation unit);

        /// <summary>
        /// Delete a unit
        /// </summary>
        /// <param name="unitId">The id of the unit to delete</param>
        /// <returns></returns>
        public Task DeleteUnitAsync(string unitId);

        // Categories

        /// <summary>
        /// Create a new category
        /// </summary>
        /// <param name="category">Information regarding the category to create</param>
        /// <returns></returns>
        public Task<string> CreateCategoryAsync(CategoryCreation category);

        /// <summary>
        /// Delete a category
        /// </summary>
        /// <param name="categoryId">The id of the category to delete</param>
        /// <returns></returns>
        public Task DeleteCategoryAsync(string categoryId);

        // Locations

        /// <summary>
        /// Create a new location
        /// </summary>
        /// <param name="location">Information regarding the location to create</param>
        /// <returns></returns>
        public Task<string> CreateLocationAsync(LocationCreation location);

        /// <summary>
        /// Delete a location
        /// </summary>
        /// <param name="locationId">The id of the location to delete</param>
        /// <returns></returns>
        public Task DeleteLocationAsync(string locationId);

        // Products

        /// <summary>
        /// Create a new product
        /// </summary>
        /// <param name="product">Information regarding the product to create</param>
        /// <returns></returns>
        public Task<Product> CreateProductAsync(ProductDetails product);

        /// <summary>
        /// Update a product
        /// </summary>
        /// <param name="product">Information regarding the product to update</param>
        /// <returns></returns>
        public Task UpdateProductAsync(ProductDetails product);

        /// <summary>
        /// Delete a product
        /// </summary>
        /// <param name="productId">The id of the product to delete</param>
        /// <returns></returns>
        public Task DeleteProductAsync(string productId);

        // ProductVariants

        /// <summary>
        /// Create a product variant
        /// </summary>
        /// <param name="variant">Information regarding the variant to create</param>
        /// <returns></returns>
        public Task<ProductVariant> CreateProductVariantAsync(ProductVariantDetails variant);

        /// <summary>
        /// Update a product variant
        /// </summary>
        /// <param name="variant">Information regarding the variant to update</param>
        /// <returns></returns>
        public Task UpdateProductVariantAsync(ProductVariantDetails variant);

        /// <summary>
        /// Delete a variant
        /// </summary>
        /// <param name="variantId">The id of the variant to delete</param>
        /// <returns></returns>
        public Task DeleteProductVariantAsync(string variantId);
    }
}
