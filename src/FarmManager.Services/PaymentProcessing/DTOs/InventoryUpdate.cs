﻿namespace FarmManager.Services.PaymentProcessing.DTOs
{
    /// <summary>
    /// Used to update inventory within square
    /// </summary>
    public class InventoryUpdate
    {
        /// <summary>
        /// The number of items affected
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// The Id of the variant to which the inventory belongs
        /// </summary>
        public string VariantId { get; set; }

        /// <summary>
        /// The prior state of the inventory. Should be one of 
        /// the InventoryState constants
        /// </summary>
        public string PriorState { get; set; }

        /// <summary>
        /// The new state of the inventory. Should be one of
        /// the InventoryState constants
        /// </summary>
        public string CurrentState { get; set; }

        /// <summary>
        /// The id of the businessLocation where inventory is
        /// located
        /// </summary>
        public string BusinessLocationId { get; set; }

        /// <summary>
        /// The id of the prior business location where inventory 
        /// was located
        /// </summary>
        public string PriorBusinessLocationId { get; set; }

        /// <summary>
        /// The time at which the inventory change occured
        /// </summary>
        public DateTimeOffset Timestamp { get; set; }

        /// <summary>
        /// The type of inventory change that is occuring. Should be
        /// one of the InventoryChangeType constants
        /// </summary>
        public string ChangeType { get; set; }
    }
}
