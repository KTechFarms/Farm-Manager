﻿namespace FarmManager.Services.PaymentProcessing.DTOs
{
    /// <summary>
    /// Contains the details of a product required in the payment 
    /// processing platform
    /// </summary>
    public class ProductDetails
    {
        /// <summary>
        /// The Farm Manager id of the product
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The payment processing platform's product id
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// The name of the product
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The payment processor's category id for the product
        /// </summary>
        public string? Category { get; set; }

        /// <summary>
        /// A description of the product
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Whether or not the product should be marked as 
        /// for sale in the payment processor platform
        /// </summary>
        public bool AvailableForSale { get; set; }

        /// <summary>
        /// Variants of the product
        /// </summary>
        public List<ProductVariantDetails> Variants { get; set; }
    }
}
