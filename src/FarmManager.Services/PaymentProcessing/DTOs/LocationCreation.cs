﻿namespace FarmManager.Services.PaymentProcessing.DTOs
{
    /// <summary>
    /// Used to create a location in the payment processor platform
    /// </summary>
    public class LocationCreation
    {
        /// <summary>
        /// The Farm Manager BusinessLocationId
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The user-friendly name of the location
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The location's street
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// The location's city
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// The location's state
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// The location's zip code
        /// </summary>
        public string ZipCode { get; set; }

    }
}
