﻿namespace FarmManager.Services.PaymentProcessing.DTOs
{
    /// <summary>
    /// Used for creating a category in the payment processor
    /// </summary>
    public class CategoryCreation
    {
        /// <summary>
        /// The id of the category
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the category
        /// </summary>
        public string Name { get; set; }
    }
}
