﻿namespace FarmManager.Services.PaymentProcessing.DTOs
{
    /// <summary>
    /// Contains details of a product variant required in
    /// the payment processing platform
    /// </summary>
    public class ProductVariantDetails
    {
        /// <summary>
        /// The Farm Manager id of the variant
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The payment processing platform's variant id
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// The payment processing platform's product id that
        /// the variant belongs to
        /// </summary>
        public string ExternalProductId { get; set; }

        /// <summary>
        /// The name of the variant
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The sku of the variant
        /// </summary>
        public string SKU { get; set; }

        /// <summary>
        /// The sale price of the variant
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// Whether or not the variant's inventory should
        /// be tracked by the payment processing platform
        /// </summary>
        public bool TrackInventory { get; set; }

        /// <summary>
        /// The quantity at which an alert should be sent by 
        /// the payment processing platform, if inventory
        /// tracking is enabled
        /// </summary>
        public long? LowInventoryThreshold { get; set; }

        /// <summary>
        /// The id of the measurement unit used by the variant
        /// </summary>
        public string MeasurementUnit { get; set; }    }
}
