﻿namespace FarmManager.Services.PaymentProcessing.DTOs
{
    /// <summary>
    /// Used to create a unit in the payment processing
    /// platform
    /// </summary>
    public class UnitCreation
    {
        /// <summary>
        /// The id of the unit
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the unit
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The symbol for the unit
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// The precision of the unit
        /// </summary>
        public int Precision { get; set; }
    }
}
