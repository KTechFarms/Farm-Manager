﻿namespace FarmManager.Services.Logging
{
    /// <summary>
    /// A logging interface to prevent having any specific logging dependency 
    /// within FarmManager.Core, or FarmManager.Api
    /// </summary>
    public interface IFarmLogger 
    {
        /// <summary>
        /// Perform any required initialization of the logger
        /// </summary>
        /// <typeparam name="T"></typeparam>
        void Initialize<T>();

        /// <summary>
        /// Writes a debug log to the logging table
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="data">Any context-specific data</param>
        void LogDebug(string message, object? data = null);

        /// <summary>
        /// Writes an informational log to the logging table
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="data">Any context-specific data</param>
        void LogInformation(string message, object? data = null);

        /// <summary>
        /// Writes a warning log to the logging table
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="data">Any context-specific data</param>
        void LogWarning(string message, object? data = null);

        /// <summary>
        /// Writes an error log to the logging table
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="exception">The exception causing the log to be written</param>
        /// <param name="data">Any context-specific data</param>
        void LogError(string message, Exception? exception = null, object? data = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="exception">The exception causing the log to be written</param>
        /// <param name="data">Any context-specific data</param>
        void LogCritical(string message, Exception? exception = null, object? data = null);
    }
}
