﻿using Newtonsoft.Json;
using NLog;

namespace FarmManager.Services.Logging
{
    /// <summary>
    /// An implementation of the IFarmLogger utilizing NLog
    /// </summary>
    public class FarmLogger : IFarmLogger
    {
        private ILogger? _logger;

        public FarmLogger() { }

        // TODO: Figure out a better way to do this without having
        // to pass a "source" in each function
        public void Initialize<T>()
        {
            _logger = LogManager.GetLogger(typeof(T).FullName);
        }

        public void LogDebug(string message, object? data = null)
        {
            if (data == null)
                _logger?.Debug(message);
            else
                LogWithData(message, LogLevel.Debug, data);
        }

        public void LogInformation(string message, object? data = null)
        {
            if (data == null)
                _logger?.Info(message);
            else
                LogWithData(message, LogLevel.Info, data);
        }

        public void LogWarning(string message, object? data = null)
        {
            if (data == null)
                _logger?.Warn(message);
            else
                LogWithData(message, LogLevel.Warn, data);
        }

        public void LogError(string message, Exception? exception = null, object? data = null)
        {
            if (data != null && exception == null)
                LogWithData(message, LogLevel.Error, data);
            else if (exception != null)
                LogException(message, LogLevel.Error, exception, data);
            else
                _logger?.Error(message);
        }

        public void LogCritical(string message, Exception? exception = null, object? data = null)
        {
            if (data != null && exception == null)
                LogWithData(message, LogLevel.Fatal, data);
            else if (exception != null)
                LogException(message, LogLevel.Fatal, exception, data);
            else
                _logger?.Fatal(message);

            // TODO: Send a text and/or email notification
        }

        /// <summary>
        /// Appends the provided message with the exception's message and
        /// adds a string to the "Data" and "StackTrace" columns of the logging table
        /// </summary>
        /// <param name="message">The message to be written as part of the log</param>
        /// <param name="level">The level of log that should be written</param>
        /// <param name="ex">The exception related to the logged event</param>
        /// <param name="data">Context-specific data related to the logged event</param>
        private void LogException(string message, LogLevel level, Exception ex, object? data)
        {
            var logEvent = new LogEventInfo() { Message = $"{message} : {ex.Message}", Level = level };
            logEvent.Properties["stacktrace"] = ex.StackTrace;

            if (data != null)
                logEvent.Properties["data"] = JsonConvert.SerializeObject(data);

            _logger?.Log(logEvent);
        }

        /// <summary>
        /// Adds a string to the "Data" column of the logging table
        /// </summary>
        /// <param name="message">The message to be written as part of the log</param>
        /// <param name="level">The level of log that should be written</param>
        /// <param name="data">Context-specific data related to the logged event</param>
        private void LogWithData(string message, LogLevel level, object data)
        {
            var logEvent = new LogEventInfo() { Message = message, Level = level };
            logEvent.Properties["data"] = JsonConvert.SerializeObject(data);
            _logger?.Log(logEvent);
        }


    }
}
