﻿namespace FarmManager.Services.Email
{
    /// <summary>
    /// An interface to send emails through a 3rd party provider
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// Sends an email to the designated addresses
        /// </summary>
        /// <param name="subject">The email subject</param>
        /// <param name="htmlContent">The html content of the email</param>
        /// <param name="toAddresses">Email addresses that the email the goes too</param>
        /// <param name="ccAddresses">Email addresses included in the cc</param>
        /// <param name="bccAddresses">Email addresses included in the bcc</param>
        /// <returns></returns>
        public Task SendEmailAsync(
            string subject, 
            string htmlContent,
            Dictionary<string, string?> toAddresses,
            Dictionary<string, string?>? ccAddresses = null,
            Dictionary<string, string?>? bccAddresses = null
        );
    }
}
