﻿using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace FarmManager.Services.Email.SendGrid
{
    /// <summary>
    /// An email service that interacts with the SendGrid platform
    /// </summary>
    public class SendGridEmailService : IEmailService
    {
        private readonly ISendGridClient _client;
        private readonly EmailServiceSettings _settings;

        public SendGridEmailService(
            ISendGridClient client,
            IOptions<EmailServiceSettings> options
        )
        {
            _settings = options.Value;
            _client = client;
        }

        public async Task SendEmailAsync(
            string subject,
            string htmlContent,
            Dictionary<string, string?> toAddresses,
            Dictionary<string, string?>? ccAddresses = null,
            Dictionary<string, string?>? bccAddresses = null
        )
        {
            if (toAddresses.Count < 1 || string.IsNullOrEmpty(toAddresses.First().Key))
                throw new ApplicationException("At least one to address required");

            if (string.IsNullOrEmpty(subject))
                throw new ApplicationException("Subject is required");

            if (string.IsNullOrEmpty(htmlContent))
                throw new ApplicationException("HTML template is required");

            EmailAddress fromEmail = new EmailAddress(_settings.FromAddress, _settings.FromName);

            SendGridMessage message = new()
            {
                From = fromEmail,
                Subject = subject,
                HtmlContent = htmlContent,
            };

            List<(string, string?)> tos = toAddresses.Select(kvp => (kvp.Key, kvp.Value)).ToList();
            List<(string, string?)> ccs = ccAddresses?.Select(kvp => (kvp.Key, kvp.Value)).ToList() ?? new();
            List<(string, string?)> bccs = bccAddresses?.Select(kvp => (kvp.Key, kvp.Value)).ToList() ?? new();

            if (tos.Count > 1)
                message.AddTos(tos.Select(to => new EmailAddress(to.Item1, to.Item2)).ToList());
            else
            {
                (string, string?) to = tos.First();
                message.AddTo(new EmailAddress(to.Item1, to.Item2));
            }

            if (ccs.Count > 1)
                message.AddCcs(ccs.Select(cc => new EmailAddress(cc.Item1, cc.Item2)).ToList());
            else if (ccs.Count == 1)
            {
                (string, string?) cc = ccs.First();
                message.AddCc(new EmailAddress(cc.Item1, cc.Item2));
            }

            if (bccs.Count > 1)
                message.AddBccs(bccs.Select(bcc => new EmailAddress(bcc.Item1, bcc.Item2)).ToList());
            else if (bccs.Count == 1)
            {
                (string, string?) bcc = bccs.First();
                message.AddBcc(new EmailAddress(bcc.Item1, bcc.Item2));
            }
            
            var result = await _client.SendEmailAsync(message).ConfigureAwait(false);
        }
    }
}
