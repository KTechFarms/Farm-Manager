﻿namespace FarmManager.Services.Email.DTOs
{
    /// <summary>
    /// Represents a type of notification
    /// </summary>
    public enum NotificationType
    {
        Email = 0,
        Text,
        InstantMessage
    };

    /// <summary>
    /// Contains the information needed to submit a notification
    /// to a 3rd party service
    /// </summary>
    public class Notification
    {
        /// <summary>
        /// The type of notification that should be sent
        /// </summary>
        public NotificationType NotificationType { get; set; }

        /// <summary>
        /// The subject of the notification
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// The body of the notification
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// A collection of to emails and to names
        /// </summary>
        public Dictionary<string, string?> ToAddresses { get; set; }

        /// <summary>
        /// A collection of cc emails and cc names
        /// </summary>
        public Dictionary<string, string?>? CcAddresses { get; set; } = null;

        /// <summary>
        /// A collection of bcc emails and bcc names
        /// </summary>
        public Dictionary<string, string?>? BccAddresses { get; set; } = null;
    }
}
