﻿namespace FarmManager.Services.Email
{
    /// <summary>
    /// Settings required by 3rd party email platforms
    /// </summary>
    public class EmailServiceSettings
    {
        /// <summary>
        /// The API key to make requests
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// A default from address used when sending emails
        /// </summary>
        public string FromAddress { get; set; }

        /// <summary>
        /// A default from name used when sending emails
        /// </summary>
        public string FromName { get; set; }
    }
}
