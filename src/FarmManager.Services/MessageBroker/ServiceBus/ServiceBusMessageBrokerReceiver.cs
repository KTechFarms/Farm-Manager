﻿using Azure.Messaging.ServiceBus;
using FarmManager.Services.Logging;
using Newtonsoft.Json;
using System.Collections.Concurrent;

namespace FarmManager.Services.MessageBroker.ServiceBus
{
    /// <summary>
    /// A message broker receiver that utilizes Azure Service Bus's ServiceBusProcessor
    /// </summary>
    public class ServiceBusMessageBrokerReceiver : IMessageBrokerReceiver
    {
        private readonly ServiceBusProcessor _sbProcessor;
        private readonly IFarmLogger _logger;

        private ConcurrentDictionary<string, Func<MessageBrokerMessage, Task>> AsyncMessageHandlers = new();
        private ConcurrentDictionary<string, Action<MessageBrokerMessage>> MessageHandlers = new();
        private bool _disposed = false;

        public ServiceBusMessageBrokerReceiver(ServiceBusProcessor processor, IFarmLogger logger)
        {
            _sbProcessor = processor;
            _logger = logger;

            _sbProcessor.ProcessMessageAsync += MessageProcessor;
            _sbProcessor.ProcessErrorAsync += ErrorProcessor;
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if(disposing)
                {
                    _sbProcessor.ProcessMessageAsync -= MessageProcessor;
                    _sbProcessor.ProcessErrorAsync -= ErrorProcessor;
                    _sbProcessor.DisposeAsync().AsTask().Wait();
                }

                _disposed = true;
            }
        }

        private async Task MessageProcessor(ProcessMessageEventArgs args)
        {
            try
            {
                // Try processing 3 times, and then DLQ it
                if (args.Message.DeliveryCount > 3)
                {
                    await args.DeadLetterMessageAsync(args.Message);
                    return;
                }

                // Make sure we're handling a properly formatted message
                string body = args.Message.Body.ToString();
                MessageBrokerMessage message = JsonConvert.DeserializeObject<MessageBrokerMessage>(body);

                if (message == null)
                    throw new ApplicationException("Improperly formatted message");

                if (string.IsNullOrEmpty(message.MessageType))
                    throw new ApplicationException("Missing message type");

                if(AsyncMessageHandlers.TryGetValue(message.MessageType, out var asyncHandler))
                {
                    #pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                    asyncHandler(message)
                        .ContinueWith(async result =>
                        {
                            if (result.Exception != null)
                            {
                                _logger.LogError($"Error handling {message.MessageType}", result.Exception, message);
                                await args.DeadLetterMessageAsync(args.Message);
                                return;
                            }

                            await args.CompleteMessageAsync(args.Message);
                        });
                    #pragma warning restore CS4014
                }
                else if (MessageHandlers.TryGetValue(message.MessageType, out var handler))
                {
                    try
                    {
                        handler(message);
                        await args.CompleteMessageAsync(args.Message);
                    }
                    catch(Exception ex)
                    {
                        _logger.LogError($"Error handling {message.MessageType}", ex, message);
                        await args.DeadLetterMessageAsync(args.Message);
                    }
                }
                else
                {
                    _logger.LogWarning($"No handler found for {message.MessageType}", message);
                    await args.DeadLetterMessageAsync(args.Message);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error processing message", ex, args.Message);
            }
        }

        private async Task ErrorProcessor(ProcessErrorEventArgs args)
        {
            _logger.LogError("Error processing event", args.Exception);
        }

        public async Task BeginProcessingAsync()
        {
            await _sbProcessor.StartProcessingAsync();
        }

        public bool TryRegisterHandler(string identifier, Action<MessageBrokerMessage> handler)
        {
            return MessageHandlers.TryAdd(identifier, handler);
        }

        public bool TryRegisterAsyncHandler(string identifier, Func<MessageBrokerMessage, Task> handler)
        {
            return AsyncMessageHandlers.TryAdd(identifier, handler);
        }
    }
}
