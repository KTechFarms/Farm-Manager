﻿using Azure.Messaging.ServiceBus;
using Newtonsoft.Json;

namespace FarmManager.Services.MessageBroker.ServiceBus
{
    /// <summary>
    /// A message broker publisher that utilizes Azure Service Bus's ServiceBusSender
    /// </summary>
    public class ServiceBusMessageBrokerPublisher : IMessageBrokerPublisher
    {
        private readonly ServiceBusSender _sbSender;
        private bool _disposed = false;

        public ServiceBusMessageBrokerPublisher(ServiceBusSender sbSender)
        {
            _sbSender = sbSender;
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(!_disposed)
            {
                if (disposing)
                    _sbSender.DisposeAsync().AsTask().Wait();

                _disposed = true;
            }
        }

        public async Task PublishMessageAsync(MessageBrokerMessage message)
        {
            ServiceBusMessage sbMessage = new(JsonConvert.SerializeObject(message));
            await _sbSender.SendMessageAsync(sbMessage);
        }

        public async Task PublishMessageBatchAsync(IEnumerable<MessageBrokerMessage> messages)
        {
            ServiceBusMessageBatch sbBatch = await _sbSender.CreateMessageBatchAsync();
            foreach(MessageBrokerMessage message in messages)
            {
                ServiceBusMessage sbMessage = new(JsonConvert.SerializeObject(message));
                if (!sbBatch.TryAddMessage(sbMessage))
                    throw new ApplicationException("Unable to add message to the batch");
            }

            await _sbSender.SendMessagesAsync(sbBatch);
        }
    }
}
