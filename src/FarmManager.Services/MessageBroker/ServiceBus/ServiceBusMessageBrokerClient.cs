﻿using Azure.Messaging.ServiceBus;
using FarmManager.Services.Logging;

namespace FarmManager.Services.MessageBroker.ServiceBus
{
    /// <summary>
    /// A message broker client that interacts with Azure Service Bus
    /// </summary>
    public class ServiceBusMessageBrokerClient : IMessageBrokerClient
    {
        private readonly MessageBrokerClientSettings _settings;
        private readonly IFarmLogger _logger;

        private static ServiceBusClient _sbClient;
        
        private bool _disposed = false;

        public ServiceBusMessageBrokerClient(
            MessageBrokerClientSettings settings, 
            IFarmLogger logger
        )
        {
            _logger = logger;
            _settings = settings;

            if (_sbClient == null)
                _sbClient = new ServiceBusClient(settings.Host);
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                    _sbClient.DisposeAsync().AsTask().Wait();

                _disposed = true;
            }
        }

        public IMessageBrokerPublisher CreateMessageBrokerPublisher(string queueName, string identifier)
        {
            ServiceBusSenderOptions options = new()
            {
                Identifier = identifier
            };

            ServiceBusSender sender = _sbClient.CreateSender(queueName, options);
            return new ServiceBusMessageBrokerPublisher(sender);
        }

        public IMessageBrokerReceiver CreateMessageBrokerReceiver(string queueName)
        {
            MessageBrokerChannelSettings? queueSettings = _settings.Channels
                .Where(c => c.QueueName == queueName)
                .FirstOrDefault();

            if (queueSettings == null)
                throw new ApplicationException("Channel settings not found");

            ServiceBusProcessorOptions options = new()
            {
                PrefetchCount = queueSettings.PrefetchCount,
                MaxConcurrentCalls = queueSettings.MaxConcurrentCalls,
                ReceiveMode = ServiceBusReceiveMode.PeekLock,
                AutoCompleteMessages = queueSettings.AutoCompleteMessages
            };

            ServiceBusProcessor processor = _sbClient.CreateProcessor(queueSettings.QueueName, options);
            return new ServiceBusMessageBrokerReceiver(processor, _logger);
        }
    }
}
