﻿namespace FarmManager.Services.MessageBroker
{
    /// <summary>
    /// An interface for interacting with 3rd party message brokers
    /// </summary>
    public interface IMessageBrokerClient : IDisposable
    {
        /// <summary>
        /// Creates an object that can publish messages to the message broker
        /// </summary>
        /// <param name="queueName">The queue messages should be published to</param>
        /// <param name="identifier">An identifier to identify the publisher of messages</param>
        /// <returns></returns>
        IMessageBrokerPublisher CreateMessageBrokerPublisher(string queueName, string identifier);

        /// <summary>
        /// Creates an object that subscribes to and receives messages from the message broker
        /// </summary>
        /// <param name="queueName">The queue to receive messages from</param>
        /// <returns></returns>
        IMessageBrokerReceiver CreateMessageBrokerReceiver(string queueName);
    }
}
