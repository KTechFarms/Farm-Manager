﻿namespace FarmManager.Services.MessageBroker
{
    /// <summary>
    /// A generic type that represents any message sent or received
    /// via a message broker
    /// </summary>
    public class MessageBrokerMessage
    {
        /// <summary>
        /// The source of the message. This could be an
        /// IoT Hub DeviceId or the name of a service
        /// </summary>
        public string? Source { get; set; }

        /// <summary>
        /// The type of message that is being represented,
        /// such as Telemetry, Notification, etc
        /// </summary>
        public string? MessageType { get; set; }

        /// <summary>
        /// The assembly that the Data object represents
        /// </summary>
        public string? Assembly { get; set; }

        /// <summary>
        /// The class that the Data object represents
        /// </summary>
        public string? Class { get; set; }

        /// <summary>
        /// The date at which the message was submitted to the
        /// message broker
        /// </summary>
        public DateTimeOffset TimeStamp { get; set; }

        /// <summary>
        /// Data specific to the message being sent
        /// </summary>
        public object? Data { get; set; }
    }
}
