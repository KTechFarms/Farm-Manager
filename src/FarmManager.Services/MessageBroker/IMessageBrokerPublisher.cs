﻿namespace FarmManager.Services.MessageBroker
{
    /// <summary>
    /// An interface for publishing messages to a message broker
    /// </summary>
    public interface IMessageBrokerPublisher : IDisposable
    {
        /// <summary>
        /// Publishes a single message to a queue
        /// </summary>
        /// <param name="message">The message to publish</param>
        /// <returns></returns>
        Task PublishMessageAsync(MessageBrokerMessage message);

        /// <summary>
        /// Publishes a batch of messages to a queue at once
        /// </summary>
        /// <param name="messages">The messages to publish</param>
        /// <returns></returns>
        Task PublishMessageBatchAsync(IEnumerable<MessageBrokerMessage> messages);
    }
}
