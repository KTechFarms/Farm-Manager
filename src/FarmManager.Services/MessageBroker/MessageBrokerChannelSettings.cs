﻿using FarmManager.Common.Consts;

namespace FarmManager.Services.MessageBroker
{
    /// <summary>
    /// Settings required for making connections to a 
    /// message broker channel
    /// </summary>
    public class MessageBrokerChannelSettings
    {
        /// <summary>
        /// The channel that these settings are for
        /// </summary>
        public MessageBrokerChannel ChannelType { get; set; }

        // Publish Settings

        /// <summary>
        /// The name of the queue
        /// </summary>
        public string QueueName { get; set; }

        // Subscribe settings

        /// <summary>
        /// How many message to pre-fetch
        /// </summary>
        public int PrefetchCount { get; set; }

        /// <summary>
        /// How many messages can be handled at one time
        /// </summary>
        public int MaxConcurrentCalls { get; set; }

        /// <summary>
        /// Whether messages should automatically be completed
        /// and removed from the queue
        /// </summary>
        public bool AutoCompleteMessages { get; set; }
    }
}
