﻿namespace FarmManager.Services.MessageBroker
{
    /// <summary>
    /// An interface for subscribing to and receiving messages from a 
    /// message broker
    /// </summary>
    public interface IMessageBrokerReceiver : IDisposable
    {
        /// <summary>
        /// Begins processing the queue and receiving messages
        /// </summary>
        /// <returns></returns>
        Task BeginProcessingAsync();

        /// <summary>
        /// Attempts to register a handler for any messages received
        /// </summary>
        /// <param name="identifier">The event type to handle</param>
        /// <param name="handler">The function that will handle the actual message</param>
        /// <returns></returns>
        bool TryRegisterHandler(string identifier, Action<MessageBrokerMessage> handler);

        /// <summary>
        /// Attempts to register an asynchronous handler for any messages received
        /// </summary>
        /// <param name="identifier">The event type to handle</param>
        /// <param name="handler">The function that will handle the actual message</param>
        /// <returns></returns>
        bool TryRegisterAsyncHandler(string identifier, Func<MessageBrokerMessage, Task> handler);
    }
}
