﻿using FarmManager.Common.Consts;

namespace FarmManager.Services.MessageBroker
{
    /// <summary>
    /// Settings required for connecting to a message broker
    /// </summary>
    public class MessageBrokerClientSettings
    {
        /// <summary>
        /// The host to connect to
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// A collection of channels that are available
        /// </summary>
        public MessageBrokerChannelSettings[] Channels { get; set; }

        /// <summary>
        /// A helper method to retrieve the appropriate channel settings
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        public MessageBrokerChannelSettings? GetSettings(MessageBrokerChannel channel)
        {
            return Channels.Where(c => c.ChannelType == channel)
                .FirstOrDefault();
        }
    }
}
