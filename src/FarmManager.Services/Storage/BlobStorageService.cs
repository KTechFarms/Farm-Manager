﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.Extensions.Configuration;

namespace FarmManager.Services.Storage
{
    // https://learn.microsoft.com/en-us/azure/storage/blobs/storage-quickstart-blobs-dotnet?tabs=visual-studio%2Cmanaged-identity%2Croles-azure-portal%2Csign-in-azure-cli%2Cidentity-visual-studio

    /// <summary>
    /// A storage service to interact with Azure Blob storage
    /// </summary>
    public class BlobStorageService : IStorageService
    {
        private readonly BlobServiceClient _blobServiceClient;

        public BlobStorageService(IConfiguration config)
        {
            string connectionString = config.GetConnectionString("Storage");
            _blobServiceClient = new BlobServiceClient(connectionString);
        }

        public async Task<string> SaveFileAsync(string fileName, byte[] fileContents, string container)
        {
            BlobContainerClient containerClient = _blobServiceClient.GetBlobContainerClient(container);

            if (!await containerClient.ExistsAsync())
                throw new Exception($"Container {container} not found");

            string[] fileParts = fileName.Split('.');
            string extension = fileParts.Last();
            fileParts = fileParts.Take(fileParts.Count() - 1).ToArray();
            string file = string.Join(".", fileParts);

            string blobName = $"{file}_{Guid.NewGuid()}.{extension}";
            BlobClient blobClient = containerClient.GetBlobClient(blobName);
            BinaryData binary = new (fileContents);

            await blobClient.UploadAsync(binary);
            return blobClient.Name.ToString();
        }

        public async Task<byte[]> RetrieveFileContentAsync(string filename, string container)
        {
            BlobContainerClient containerClient = _blobServiceClient.GetBlobContainerClient(container);

            if (!await containerClient.ExistsAsync())
                throw new Exception($"Container {container} not found");

            BlobClient blobClient = containerClient.GetBlobClient(filename);
            BlobDownloadResult result = await blobClient.DownloadContentAsync();

            return result.Content.ToArray();
        }
    }
}
