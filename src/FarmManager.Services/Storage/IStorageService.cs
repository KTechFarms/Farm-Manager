﻿namespace FarmManager.Services.Storage
{
    /// <summary>
    /// An interface for interacting with storage services
    /// </summary>
    public interface IStorageService
    {
        /// <summary>
        /// Save a file to the storage service
        /// </summary>
        /// <param name="filename">The name of the file</param>
        /// <param name="fileContents">The binary contents of the file</param>
        /// <param name="container">The container name where the file should be saved</param>
        /// <returns></returns>
        public Task<string> SaveFileAsync(string filename, byte[] fileContents, string container);

        /// <summary>
        /// Retrieves the binary contents of a file
        /// </summary>
        /// <param name="filename">The name of the file to retrieve</param>
        /// <param name="container">The name of the container where the file resides</param>
        /// <returns></returns>
        public Task<byte[]> RetrieveFileContentAsync(string filename, string container);
    }
}
