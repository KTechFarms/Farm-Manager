﻿using FarmManager.Data.Contexts;
using FarmManager.Data.Entities.Generic;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Data.Entities.Reports.Production;
using FluentMigrator;
using FluentMigrator.Expressions;
using FluentMigrator.SqlServer;
using Microsoft.EntityFrameworkCore;
using System.Data;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Migrations.Release_0002
{
    /// <summary>
    /// The second Farm Manager migration with tables for reporting.
    /// </summary>
    [Migration(0002)]
    public class Release_0002 : Migration
    {
        private int _lastStatusId { get; set; }
        private string _disposalReasonEnum = "inventory.disposal.reason";
        private string _inventoryStatus = "inventory.item";

        public override void Up()
        {
            //////////////////////////////////////////
            //           Data Retrieval             //
            //////////////////////////////////////////

            var builder = new DbContextOptionsBuilder<FarmManagerContext>()
                .UseSqlServer(ConnectionString);

            using (FarmManagerContext dbContext = new FarmManagerContext(builder.Options))
            {
                _lastStatusId = dbContext.Statuses.OrderBy(s => s.Id).Last().Id;
            }

            //////////////////////////////////////////
            //             Data Inserts             //
            //////////////////////////////////////////

            DateTimeOffset now = DateTimeOffset.UtcNow;

            Insert.IntoTable(nameof(Enum))
                .Row(new { EntityId = _disposalReasonEnum, Name = "sold", Label = "Sold", Description = "The item was sold to an individual or company", Created = now, Modified = now, IsDeleted = false })
                .Row(new { EntityId = _disposalReasonEnum, Name = "consumed", Label = "Consumed", Description = "The item was consumed through a production process", Created = now, Modified = now, IsDeleted = false })
                .Row(new { EntityId = _disposalReasonEnum, Name = "donated", Label = "Donated", Description = "The item was donated to an individual or charity", Created = now, Modified = now, IsDeleted = false })
                .Row(new { EntityId = _disposalReasonEnum, Name = "damaged", Label = "Damaged", Description = "The item was damaged during production or while in storage", Created = now, Modified = now, IsDeleted = false })
                .Row(new { EntityId = _disposalReasonEnum, Name = "spilled", Label = "Spilled", Description = "The item was spilled and can no longer be used", Created = now, Modified = now, IsDeleted = false })
                .Row(new { EntityId = _disposalReasonEnum, Name = "contaminated", Label = "Contaminated", Description = "The item was contaminated and can no longer be used", Created = now, Modified = now, IsDeleted = false })
                .Row(new { EntityId = _disposalReasonEnum, Name = "expired", Label = "Expired", Description = "The item has expired and can no longer be used or sold", Created = now, Modified = now, IsDeleted = false });

            AutoIncrementor statusIncrementor = new AutoIncrementor(_lastStatusId + 1);
            int stockedId = statusIncrementor.Get();
            int soldId = statusIncrementor.Get();
            int consumedId = statusIncrementor.Get();
            int donatedId = statusIncrementor.Get();
            int disposedId = statusIncrementor.Get();

            Insert.IntoTable(nameof(Status))
                .WithIdentityInsert()
                .Row(new { Id = stockedId, EntityId = _inventoryStatus, Name = "stocked", Label = "Stocked", Description = "The item is currently in our posession", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = soldId, EntityId = _inventoryStatus, Name = "sold", Label = "Sold", Description = "The item has been sold to a customer", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = consumedId, EntityId = _inventoryStatus, Name = "consumed", Label = "Consumed", Description = "The item has been consumed in one of our production processes", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = donatedId, EntityId = _inventoryStatus, Name = "donated", Label = "Donated", Description = "The item has been donated to an individual or organization", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = disposedId, EntityId = _inventoryStatus, Name = "disposed", Label = "Disposed", Description = "The item has been disposed and is no longer in our posession", Created = now, Modified = now, IsDeleted = false });

            //////////////////////////////////////////
            //            Table Updates             //
            //////////////////////////////////////////

            Alter.Table(nameof(ProductionItem))
                .AddColumn(nameof(ProductionItem.StatusId)).AsInt32().NotNullable().WithDefaultValue(stockedId)
                    .ForeignKey(nameof(Status), nameof(Status.Id))
                    .OnDelete(Rule.SetDefault)
                .AddColumn(nameof(ProductionItem.DisposalReasonId)).AsInt32().Nullable()
                    .ForeignKey(nameof(Enum), nameof(Enum.Id))
                    .OnDelete(Rule.None)
                .AddColumn(nameof(ProductionItem.DisposalDate)).AsDateTimeOffset().Nullable()
                .AddColumn(nameof(ProductionItem.SaleDate)).AsDateTimeOffset().Nullable()
                .AddColumn(nameof(ProductionItem.ConsumptionDate)).AsDateTimeOffset().Nullable()
                .AddColumn(nameof(ProductionItem.DonationDate)).AsDateTimeOffset().Nullable();

            //////////////////////////////////////////
            //          Operations Tables           //
            //////////////////////////////////////////

            Create.Table(nameof(ProductVariantBarcode))
                .WithIdentity(nameof(ProductVariantBarcode.Id))
                .WithColumn(nameof(ProductVariantBarcode.ProductVariantId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(ProductVariant), nameof(ProductVariant.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(ProductVariantBarcode.Barcode)).AsString(128).NotNullable()
                .WithFarmManagerBase();

            Create.Table(nameof(Inventory))
                .WithLongIdentity(nameof(Inventory.Id))
                .WithColumn(nameof(Inventory.ProductVariantId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(ProductVariant), nameof(ProductVariant.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Inventory.Barcode)).AsString(128).Nullable()
                .WithColumn(nameof(Inventory.LocationId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(FarmLocation), nameof(FarmLocation.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Inventory.StatusId)).AsInt32().NotNullable().WithDefaultValue(stockedId)
                    .ForeignKey(nameof(Status), nameof(Status.Id))
                    .OnDelete(Rule.SetDefault)
                .WithColumn(nameof(Inventory.DisposalReasonId)).AsInt32().Nullable()
                    .ForeignKey(nameof(Enum), nameof(Enum.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Inventory.DisposalDate)).AsDateTimeOffset().Nullable()
                .WithColumn(nameof(Inventory.SaleDate)).AsDateTimeOffset().Nullable()
                .WithColumn(nameof(Inventory.ConsumptionDate)).AsDateTimeOffset().Nullable()
                .WithColumn(nameof(Inventory.DonationDate)).AsDateTimeOffset().Nullable()
                .WithFarmManagerBase();

            //////////////////////////////////////////
            //          Reporting Tables            //
            //////////////////////////////////////////

            Create.Table(nameof(CropStatistic))
                .WithIdentity(nameof(CropStatistic.Id))
                .WithColumn(nameof(CropStatistic.CropName)).AsString(255).NotNullable()
                .WithColumn(nameof(CropStatistic.DaysToGerminate)).AsInt32().NotNullable().WithDefaultValue(0)
                .WithColumn(nameof(CropStatistic.DaysToHarvest)).AsInt32().NotNullable().WithDefaultValue(0)
                .WithColumn(nameof(CropStatistic.LotCount)).AsInt32().NotNullable().WithDefaultValue(0)
                .WithColumn(nameof(CropStatistic.BatchCount)).AsInt32().NotNullable().WithDefaultValue(0)
                .WithColumn(nameof(CropStatistic.Yields)).AsString(int.MaxValue).NotNullable();
        }

        public override void Down()
        {
            Delete.Table(nameof(ProductVariantBarcode));
            Delete.Table(nameof(Inventory));
            Delete.Table(nameof(CropStatistic));

            Delete.Column(nameof(ProductionItem.StatusId))
                .Column(nameof(ProductionItem.DisposalReasonId))
                .Column(nameof(ProductionItem.DisposalDate))
                .Column(nameof(ProductionItem.SaleDate))
                .Column(nameof(ProductionItem.ConsumptionDate))
                .Column(nameof(ProductionItem.DonationDate))
                .FromTable(nameof(ProductionItem));

            Delete.FromTable(nameof(Enum))
                .Row(new { EntityId = _disposalReasonEnum });

            Delete.FromTable(nameof(Status))
                .Row(new { EntityId = _inventoryStatus });
        }
    }
}
