﻿namespace FarmManager.Migrations
{
    /// <summary>
    /// Used to automatically increment an index when doing database inserts
    /// </summary>
    public class AutoIncrementor
    {
        private int _index { get; set; }

        public AutoIncrementor()
        {
            _index = 1;
        }

        public AutoIncrementor(int index)
        {
            _index = index;
        }

        /// <summary>
        /// Retrieves an index and then increments it
        /// </summary>
        /// <returns></returns>
        public int Get()
        {
            return _index++;
        }
    }
}
