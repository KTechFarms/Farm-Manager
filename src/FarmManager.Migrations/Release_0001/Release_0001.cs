﻿using FarmManager.Data.Entities.Devices;
using FarmManager.Data.Entities.Generic;
using FarmManager.Data.Entities.Operations;
using FarmManager.Data.Entities.Production;
using FarmManager.Data.Entities.System;
using FluentMigrator;
using FluentMigrator.SqlServer;
using System.Data;

using Enum = FarmManager.Data.Entities.Generic.Enum;

namespace FarmManager.Migrations.Release_0001
{
    /// <summary>
    /// The initial Farm Manager migration with tables for managing 
    /// products, categories, devices, and locations. Also allows for 
    /// creating production items and tracking inventory.
    /// </summary>
    [Migration(0001)]
    public class Release_0001 : Migration
    {
        public override void Up()
        {
            //////////////////////////////////////////
            //            System Tables             //
            //////////////////////////////////////////
            
            Create.Table(nameof(Log))
                .WithIdentity(nameof(Log.Id))
                .WithColumn(nameof(Log.Timestamp)).AsDateTimeOffset().NotNullable()
                .WithColumn(nameof(Log.LogLevel)).AsInt32().NotNullable()
                .WithColumn(nameof(Log.Message)).AsString(int.MaxValue).NotNullable()
                .WithColumn(nameof(Log.Source)).AsString(255).Nullable()
                .WithColumn(nameof(Log.Data)).AsString(int.MaxValue).Nullable()
                .WithColumn(nameof(Log.StackTrace)).AsString(int.MaxValue).Nullable();

            Create.Table(nameof(SettingCategory))
                .WithIdentity(nameof(SettingCategory.Id))
                .WithColumn(nameof(SettingCategory.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(SettingCategory.Label)).AsString(255).NotNullable();

            Create.Table(nameof(Setting))
                .WithIdentity(nameof(Setting.Id))
                .WithColumn(nameof(Setting.CategoryId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(SettingCategory), nameof(SettingCategory.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(Setting.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(Setting.Label)).AsString(255).NotNullable()
                .WithColumn(nameof(Setting.Description)).AsString(int.MaxValue).NotNullable()
                .WithColumn(nameof(Setting.Value)).AsString(int.MaxValue).Nullable();

            //////////////////////////////////////////
            //            Generic Tables            //
            //////////////////////////////////////////
            
            Create.Table(nameof(Enum))
                .WithIdentity(nameof(Enum.Id))
                .WithColumn(nameof(Enum.EntityId)).AsString(int.MaxValue).NotNullable()
                .WithColumn(nameof(Enum.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(Enum.Label)).AsString(255).NotNullable()
                .WithColumn(nameof(Enum.Description)).AsString(int.MaxValue).Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(Status))
                .WithIdentity(nameof(Status.Id))
                .WithColumn(nameof(Status.EntityId)).AsString(int.MaxValue).NotNullable()
                .WithColumn(nameof(Status.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(Status.Label)).AsString(255).NotNullable()
                .WithColumn(nameof(Status.Description)).AsString(int.MaxValue).Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(Address))
                .WithIdentity(nameof(Address.Id))
                .WithColumn(nameof(Address.Street)).AsString(255).NotNullable()
                .WithColumn(nameof(Address.City)).AsString(255).NotNullable()
                .WithColumn(nameof(Address.State)).AsString(255).NotNullable()
                .WithColumn(nameof(Address.ZipCode)).AsString(255).NotNullable()
                .WithFarmManagerBase();

            Create.Table(nameof(Note))
                .WithLongIdentity(nameof(Note.Id))
                .WithColumn(nameof(Note.Text)).AsString(int.MaxValue).NotNullable()
                .WithColumn(nameof(Note.UserId)).AsString(255).NotNullable()
                .WithFarmManagerBase();

            //////////////////////////////////////////
            //            Device Tables             //
            //////////////////////////////////////////

            Create.Table(nameof(PendingDevice))
                .WithIdentity(nameof(PendingDevice.Id))
                .WithColumn(nameof(PendingDevice.DeviceId)).AsString(50).NotNullable()
                    .Unique()
                .WithColumn(nameof(PendingDevice.DeviceKey)).AsGuid().NotNullable()
                .WithColumn(nameof(PendingDevice.HasRegistered)).AsBoolean().NotNullable()
                .WithFarmManagerBase();

            Create.Table(nameof(Device))
                .WithIdentity(nameof(Device.Id))
                .WithColumn(nameof(Device.DeviceId)).AsString(50).NotNullable()
                    .Unique()
                .WithColumn(nameof(Device.PendingDeviceId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(PendingDevice), nameof(PendingDevice.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Device.Name)).AsString(100).Nullable()
                .WithColumn(nameof(Device.Model)).AsString(100).Nullable()
                .WithColumn(nameof(Device.SerialNumber)).AsString(100).Nullable()
                .WithColumn(nameof(Device.LastConnection)).AsDateTimeOffset().NotNullable()
                .WithFarmManagerBase();

            Create.Table(nameof(DeviceMethod))
                .WithIdentity(nameof(DeviceMethod.Id))
                .WithColumn(nameof(DeviceMethod.DeviceId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Device), nameof(Device.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(DeviceMethod.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(DeviceMethod.Description)).AsString(int.MaxValue).Nullable()
                .WithColumn(nameof(DeviceMethod.Parameters)).AsString(int.MaxValue).Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(InterfaceType))
                .WithIdentity(nameof(InterfaceType.Id))
                .WithColumn(nameof(InterfaceType.Model)).AsString(100).NotNullable()
                    .Unique()
                .WithColumn(nameof(InterfaceType.Description)).AsString(int.MaxValue)
                .WithFarmManagerBase();

            Create.Table(nameof(Interface))
                .WithIdentity(nameof(Interface.Id))
                .WithColumn(nameof(Interface.InterfaceId)).AsString(255).NotNullable()
                .WithColumn(nameof(Interface.DeviceId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Device), nameof(Device.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(Interface.InterfaceTypeId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(InterfaceType), nameof(InterfaceType.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Interface.Manufacturer)).AsString(100).Nullable()
                .WithColumn(nameof(Interface.SerialNumber)).AsString(100).Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(Telemetry))
                .WithLongIdentity(nameof(Telemetry.Id))
                .WithColumn(nameof(Telemetry.InterfaceId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Interface), nameof(Interface.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(Telemetry.TimeStamp)).AsDateTimeOffset().NotNullable()
                .WithColumn(nameof(Telemetry.Key)).AsString(255).NotNullable()
                .WithColumn(nameof(Telemetry.Value)).AsDouble().Nullable()
                .WithColumn(nameof(Telemetry.BoolValue)).AsBoolean().Nullable()
                .WithColumn(nameof(Telemetry.StringValue)).AsString(255).Nullable()
                .WithFarmManagerBase();

            //////////////////////////////////////////
            //          Operations Tables           //
            //////////////////////////////////////////
            
            Create.Table(nameof(Unit))
                .WithIdentity(nameof(Unit.Id))
                .WithColumn(nameof(Unit.ExternalId)).AsString(255).Nullable()
                .WithColumn(nameof(Unit.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(Unit.Symbol)).AsString(10).NotNullable()
                .WithColumn(nameof(Unit.Precision)).AsInt32().NotNullable()
                .WithFarmManagerBase();

            Create.Table(nameof(BusinessLocation))
                .WithIdentity(nameof(BusinessLocation.Id))
                .WithColumn(nameof(BusinessLocation.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(BusinessLocation.AddressId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Address), nameof(Address.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(BusinessLocation.ExternalId)).AsString(255).Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(ProductCategory))
                .WithIdentity(nameof(ProductCategory.Id))
                .WithColumn(nameof(ProductCategory.ExternalId)).AsString(255).Nullable()
                .WithColumn(nameof(ProductCategory.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(ProductCategory.Description)).AsString(int.MaxValue).Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(Product))
                .WithIdentity(nameof(Product.Id))
                .WithColumn(nameof(Product.ExternalId)).AsString(255).Nullable()
                .WithColumn(nameof(Product.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(Product.Description)).AsString(int.MaxValue).Nullable()
                .WithColumn(nameof(Product.CategoryId)).AsInt32().Nullable()
                    .ForeignKey(nameof(ProductCategory), nameof(ProductCategory.Id))
                    .OnDelete(Rule.SetNull)
                .WithColumn(nameof(Product.AvailableForSale)).AsBoolean().NotNullable()
                .WithFarmManagerBase();

            Create.Table(nameof(ProductVariant))
                .WithIdentity(nameof(ProductVariant.Id))
                .WithColumn(nameof(ProductVariant.ExternalId)).AsString(255).Nullable()
                .WithColumn(nameof(ProductVariant.ProductId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Product), nameof(Product.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(ProductVariant.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(ProductVariant.Sku)).AsString(255).NotNullable()
                .WithColumn(nameof(ProductVariant.MeasurementUnitId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Unit), nameof(Unit.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(ProductVariant.Price)).AsDecimal(18, 2).Nullable()
                .WithColumn(nameof(ProductVariant.SalePrice)).AsDecimal(18, 2).Nullable()
                .WithColumn(nameof(ProductVariant.TrackInventory)).AsBoolean().NotNullable().WithDefaultValue(false)
                .WithColumn(nameof(ProductVariant.LowInventoryThreshold)).AsInt32().Nullable()
                .WithColumn(nameof(ProductVariant.HighInventoryThreshold)).AsInt32().Nullable()
                .WithFarmManagerBase();

            //////////////////////////////////////////
            //          Production Tables           //
            //////////////////////////////////////////
            
            Create.Table(nameof(Label))
                .WithIdentity(nameof(Label.Id))
                .WithColumn(nameof(Label.LabelName)).AsString(255).NotNullable()
                .WithColumn(nameof(Label.IsActive)).AsBoolean().NotNullable()
                .WithColumn(nameof(Label.ProductVariantId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(ProductVariant), nameof(ProductVariant.Id))
                    .OnDelete(Rule.Cascade)
                .WithFarmManagerBase();

            Create.Table(nameof(LabelVersion))
                .WithIdentity(nameof(LabelVersion.Id))
                .WithColumn(nameof(LabelVersion.LabelId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Label), nameof(Label.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(LabelVersion.StorageName)).AsString(255).NotNullable()
                .WithColumn(nameof(LabelVersion.Version)).AsInt32().NotNullable()
                .WithColumn(nameof(LabelVersion.IsActive)).AsBoolean().NotNullable()
                .WithFarmManagerBase();

            Create.Table(nameof(FarmLocation))
                .WithIdentity(nameof(FarmLocation.Id))
                .WithColumn(nameof(FarmLocation.BusinessLocationId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(BusinessLocation), nameof(BusinessLocation.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(FarmLocation.Name)).AsString(255).NotNullable()
                .WithColumn(nameof(FarmLocation.LocationTypeId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Enum), nameof(Enum.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(FarmLocation.ParentLocationId)).AsInt32().Nullable()
                .WithColumn(nameof(FarmLocation.Barcode)).AsString(128).Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(Crop))
                .WithIdentity(nameof(Crop.Id))
                .WithColumn(nameof(Crop.SeedVariantId)).AsInt32().Nullable()
                .WithColumn(nameof(Crop.SeedIdentifier)).AsString(255).Nullable()
                .WithColumn(nameof(Crop.PlantedCount)).AsInt32().Nullable()
                .WithColumn(nameof(Crop.SeedsPerPlanting)).AsDouble().Nullable()
                .WithColumn(nameof(Crop.StatusId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Status), nameof(Status.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Crop.PlantedDate)).AsDateTimeOffset().NotNullable()
                .WithColumn(nameof(Crop.GerminatedDate)).AsDateTimeOffset().Nullable()
                .WithColumn(nameof(Crop.TransplantedDate)).AsDateTimeOffset().Nullable()
                .WithColumn(nameof(Crop.TransplantLocationId)).AsInt32().Nullable()
                    .ForeignKey(nameof(FarmLocation), nameof(FarmLocation.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Crop.HarvestBeginDate)).AsDateTimeOffset().Nullable()
                .WithColumn(nameof(Crop.HarvestEndDate)).AsDateTimeOffset().Nullable()
                .WithColumn(nameof(Crop.DisposalDate)).AsDateTimeOffset().Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(Lot))
                .WithIdentity(nameof(Lot.Id))
                .WithColumn(nameof(Lot.LotNumber)).AsString(10).NotNullable().Unique()
                .WithColumn(nameof(Lot.StatusId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Status), nameof(Status.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Lot.ProductId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Product), nameof(Product.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Lot.FarmLocationId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(FarmLocation), nameof(FarmLocation.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Lot.CropId)).AsInt32().Nullable()
                    .ForeignKey(nameof(Crop), nameof(Crop.Id))
                    .OnDelete(Rule.None)
                .WithFarmManagerBase();

            Create.Table(nameof(LotProgression))
                .WithIdentity(nameof(LotProgression.Id))
                .WithColumn(nameof(LotProgression.CurrentStatusId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Status), nameof(Status.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(LotProgression.NextStatusId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Status), nameof(Status.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(LotProgression.NextLotStatusId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Status), nameof(Status.Id))
                    .OnDelete(Rule.None)
                .WithFarmManagerBase();

            Create.Table(nameof(LotNoteMap))
                .WithIdentity(nameof(LotNoteMap.Id))
                .WithColumn(nameof(LotNoteMap.LotId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Lot), nameof(Lot.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(LotNoteMap.NoteId)).AsInt64().NotNullable()
                    .ForeignKey(nameof(Note), nameof(Note.Id))
                    .OnDelete(Rule.Cascade)
                .WithFarmManagerBase();

            Create.Table(nameof(Batch))
                .WithIdentity(nameof(Batch.Id))
                .WithColumn(nameof(Batch.LotId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Lot), nameof(Lot.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(Batch.BatchNumber)).AsInt32().NotNullable()
                .WithColumn(nameof(Batch.VariantId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(ProductVariant), nameof(ProductVariant.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Batch.FarmLocationId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(FarmLocation), nameof(FarmLocation.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Batch.StatusId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Status), nameof(Status.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(Batch.BatchBeginDate)).AsDateTimeOffset().NotNullable()
                .WithColumn(nameof(Batch.BatchEndDate)).AsDateTimeOffset().Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(ProductionItem))
                .WithLongIdentity(nameof(ProductionItem.Id))
                .WithColumn(nameof(ProductionItem.BatchId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(Batch), nameof(Batch.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(ProductionItem.LocationId)).AsInt32().NotNullable().WithDefaultValue(1)
                    .ForeignKey(nameof(FarmLocation), nameof(FarmLocation.Id))
                    .OnDelete(Rule.SetDefault)
                .WithColumn(nameof(ProductionItem.ProductionDate)).AsDateTimeOffset().NotNullable()
                .WithColumn(nameof(ProductionItem.PackageDate)).AsDateTimeOffset().Nullable()
                .WithColumn(nameof(ProductionItem.Barcode)).AsString(128).Nullable()
                .WithColumn(nameof(ProductionItem.SKU)).AsString(8).Nullable()
                .WithColumn(nameof(ProductionItem.GrossWeight)).AsDouble().Nullable()
                .WithColumn(nameof(ProductionItem.NetWeight)).AsDouble().Nullable()
                .WithColumn(nameof(ProductionItem.TareWeight)).AsDouble().Nullable()
                .WithColumn(nameof(ProductionItem.LabelWeight)).AsDouble().Nullable()
                .WithFarmManagerBase();

            Create.Table(nameof(ProductionItemLocationHistory))
                .WithLongIdentity(nameof(ProductionItemLocationHistory.Id))
                .WithColumn(nameof(ProductionItemLocationHistory.ProductionItemId)).AsInt64().NotNullable()
                    .ForeignKey(nameof(ProductionItem), nameof(ProductionItem.Id))
                    .OnDelete(Rule.Cascade)
                .WithColumn(nameof(ProductionItemLocationHistory.FarmLocationId)).AsInt32().NotNullable()
                    .ForeignKey(nameof(FarmLocation), nameof(FarmLocation.Id))
                    .OnDelete(Rule.None)
                .WithColumn(nameof(ProductionItemLocationHistory.ArrivalDate)).AsDateTimeOffset().NotNullable()
                .WithColumn(nameof(ProductionItemLocationHistory.DepartureDate)).AsDateTimeOffset().Nullable()
                .WithFarmManagerBase();

            //////////////////////////////////////////
            //             Data Inserts             //
            //////////////////////////////////////////

            DateTimeOffset now = DateTimeOffset.UtcNow;

            AutoIncrementor settingCategoryIncrementor = new AutoIncrementor();
            int productionCategory = settingCategoryIncrementor.Get();

            Insert.IntoTable(nameof(SettingCategory))
                .WithIdentityInsert()
                .Row(new { Id = productionCategory, Name = "production", Label = "Production" });

            Insert.IntoTable(nameof(Setting))
                .Row(new { CategoryId = productionCategory, Name = "gtin", Label = "GTIN", Description = "A unique GTIN for your farm that is used in production item barcodes" });

            string lotType = "lot.type";
            string locationType = "farmlocation.type";
            string _precisionType = "unit.measurement.precision";

            AutoIncrementor enumIncrementor = new();
            int cropLot = enumIncrementor.Get();
            int unknownLocation = enumIncrementor.Get();
            int storageLocation = enumIncrementor.Get();
            int productionLocation = enumIncrementor.Get();
            int packingLocation = enumIncrementor.Get();

            Insert.IntoTable(nameof(Enum))
                .WithIdentityInsert()
                .Row(new { Id = cropLot, EntityId = lotType, Name = "crop", Label = "Crop", Description = "The lot is has produced a crop of some sort", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = unknownLocation, EntityId = locationType, Name = "unknown", Label = "Unknown", Description = "This location is a placeholder for unknown items", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = storageLocation, EntityId = locationType, Name = "storage", Label = "Storage", Description = "This location is used for storage of goods or equipment", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = productionLocation, EntityId = locationType, Name = "production", Label = "Production", Description = "This location is used for production puroses, such as growing or building", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = packingLocation, EntityId = locationType, Name = "packing", Label = "Packing", Description = "This location is used for packaging and washing", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = enumIncrementor.Get(), EntityId = _precisionType, Name = "0", Label = "0 (1)", Description = "A precision of 0", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = enumIncrementor.Get(), EntityId = _precisionType, Name = "1", Label = "1 (1.0)", Description = "A precision of 1", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = enumIncrementor.Get(), EntityId = _precisionType, Name = "2", Label = "2 (1.00)", Description = "A precision of 2", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = enumIncrementor.Get(), EntityId = _precisionType, Name = "3", Label = "3 (1.000)", Description = "A precision of 3", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = enumIncrementor.Get(), EntityId = _precisionType, Name = "4", Label = "4 (1.0000)", Description = "A precision of 4", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = enumIncrementor.Get(), EntityId = _precisionType, Name = "5", Label = "5 (1.00000)", Description = "A precision of 5", Created = now, Modified = now, IsDeleted = false });

            string lotStatus = "lot";
            string cropStatus = "crop";
            string batchStatus = "batch";

            AutoIncrementor statusIncrementor = new();
            int lotPending = statusIncrementor.Get();
            int lotProducing = statusIncrementor.Get();
            int lotCompleted = statusIncrementor.Get();
            int cropPlanted = statusIncrementor.Get();
            int cropGerminated = statusIncrementor.Get();
            int cropTransplanted = statusIncrementor.Get();
            int cropProducing = statusIncrementor.Get();
            int cropHarvested = statusIncrementor.Get();
            int cropDisposed = statusIncrementor.Get();
            int batchOpen = statusIncrementor.Get();
            int batchClosed = statusIncrementor.Get();

            Insert.IntoTable(nameof(Status))
                .WithIdentityInsert()
                .Row(new { Id = lotPending, EntityId = lotStatus, Name = "pending", Label = "Pending", Description = "The lot has been created, but is not ready for harvesting", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = lotProducing, EntityId = lotStatus, Name = "producing", Label = "Producing", Description = "The lot is producing and can create batches", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = lotCompleted, EntityId = lotStatus, Name = "completed", Label = "Completed", Description = "The lot has completed production and batches can no longer be created", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = cropPlanted, EntityId = cropStatus, Name = "planted", Label = "Planted", Description = "The crop has been planted", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = cropGerminated, EntityId = cropStatus, Name = "germinated", Label = "Germinated", Description = "The crop has germinated", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = cropTransplanted, EntityId = cropStatus, Name = "transplanted", Label = "Transplanted", Description = "The crop has been transplanted to another location", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = cropProducing, EntityId = cropStatus, Name = "producing", Label = "Producing", Description = "The crop is producing and harvesting has started", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = cropHarvested, EntityId = cropStatus, Name = "harvested", Label = "Harvested", Description = "The crop has been completely harvested", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = cropDisposed, EntityId = cropStatus, Name = "disposed", Label = "Disposed", Description = "The crop was unable to be harvested and had to be disposed", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = batchOpen, EntityId = batchStatus, Name = "open", Label = "Open", Description = "The batch is open and can be assigned production items", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Id = batchClosed, EntityId = batchStatus, Name = "closed", Label = "Closed", Description = "The batch has been closed and can no longer be associated to production items", Created = now, Modified = now, IsDeleted = false });

            Insert.IntoTable(nameof(LotProgression))
                .Row(new { CurrentStatusId = cropPlanted, NextStatusId = cropGerminated, NextLotStatusId = lotPending, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropPlanted, NextStatusId = cropDisposed, NextLotStatusId = lotCompleted, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropGerminated, NextStatusId = cropTransplanted, NextLotStatusId = lotPending, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropGerminated, NextStatusId = cropProducing, NextLotStatusId = lotProducing, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropGerminated, NextStatusId = cropHarvested, NextLotStatusId = lotCompleted, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropGerminated, NextStatusId = cropDisposed, NextLotStatusId = lotCompleted, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropTransplanted, NextStatusId = cropProducing, NextLotStatusId = lotProducing, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropTransplanted, NextStatusId = cropHarvested, NextLotStatusId = lotCompleted, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropTransplanted, NextStatusId = cropDisposed, NextLotStatusId = lotCompleted, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropProducing, NextStatusId = cropHarvested, NextLotStatusId = lotCompleted, Created = now, Modified = now, IsDeleted = false })
                .Row(new { CurrentStatusId = cropProducing, NextStatusId = cropDisposed, NextLotStatusId = lotCompleted, Created = now, Modified = now, IsDeleted = false });

            Insert.IntoTable(nameof(ProductCategory))
                .Row(new { Name = "Produce", Description = "A product that is grown on the farm or purchased for resale", Created = now, Modified = now, IsDeleted = false })
                .Row(new { Name = "Seed", Description = "A product that is used to grow Produce", Created = now, Modified = now, IsDeleted = false });
        }

        public override void Down()
        {
            Delete.Table(nameof(ProductVariant));
            Delete.Table(nameof(Product));
            Delete.Table(nameof(ProductCategory));
            Delete.Table(nameof(BusinessLocation));
            Delete.Table(nameof(Address));
            Delete.Table(nameof(Unit));
            Delete.Table(nameof(ProductionItemLocationHistory));
            Delete.Table(nameof(ProductionItem));
            Delete.Table(nameof(Batch));
            Delete.Table(nameof(LotNoteMap));
            Delete.Table(nameof(LotProgression));
            Delete.Table(nameof(Lot));
            Delete.Table(nameof(Crop));
            Delete.Table(nameof(FarmLocation));
            Delete.Table(nameof(LabelVersion));
            Delete.Table(nameof(Label));
            Delete.Table(nameof(Status));
            Delete.Table(nameof(Enum));
            Delete.Table(nameof(Note));
            Delete.Table(nameof(Setting));
            Delete.Table(nameof(SettingCategory));
            Delete.Table(nameof(Log));
            Delete.Table(nameof(Telemetry));
            Delete.Table(nameof(Interface));
            Delete.Table(nameof(InterfaceType));
            Delete.Table(nameof(DeviceMethod));
            Delete.Table(nameof(Device));
            Delete.Table(nameof(PendingDevice));
        }
    }
}
