﻿using FarmManager.Data.Entities;
using FluentMigrator.Builders.Create.Table;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace FarmManager.Migrations
{
    /// <summary>
    /// Custom migration extensions for the Farm Manager system
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Creates an integer column on a table that acts as the primary key
        /// </summary>
        /// <param name="table">The FluentMigrator table object</param>
        /// <param name="columnName">The name of the column that should be a primary key</param>
        /// <returns></returns>
        public static ICreateTableColumnOptionOrWithColumnSyntax WithIdentity(
            this ICreateTableWithColumnSyntax table,
            string columnName
        )
        {
            return table
                .WithColumn(columnName)
                .AsInt32()
                .NotNullable()
                .PrimaryKey()
                .Identity();
        }

        /// <summary>
        /// Creates a long column on a table that acts as the primary key
        /// </summary>
        /// <param name="table">The FluentMigrator table object</param>
        /// <param name="columnName">The name of the column that should be a primary key</param>
        /// <returns></returns>
        public static ICreateTableColumnOptionOrWithColumnSyntax WithLongIdentity(
            this ICreateTableWithColumnSyntax table,
            string columnName
        )
        {
            return table
                .WithColumn(columnName)
                .AsInt64()
                .NotNullable()
                .PrimaryKey()
                .Identity();
        }

        /// <summary>
        /// Adds a set of columns that are common to all Farm Manager tables
        /// </summary>
        /// <param name="table">The FluentMigrator table object</param>
        /// <returns></returns>
        public static ICreateTableColumnOptionOrWithColumnSyntax WithFarmManagerBase(
            this ICreateTableWithColumnSyntax table
        )
        {
            return table
                .WithColumn(nameof(EntityBase.Created)).AsDateTimeOffset().NotNullable()
                .WithColumn(nameof(EntityBase.Modified)).AsDateTimeOffset().NotNullable()
                .WithColumn(nameof(EntityBase.IsDeleted)).AsBoolean().NotNullable();
        }

        /// <summary>
        /// A helper method to register a set of migrations with the service collection
        /// prior to starting an application
        /// </summary>
        /// <param name="services">The service collection object</param>
        /// <param name="connectionString">The connection string to the database</param>
        /// <param name="assemblyTypes">The migration classes that should be registered</param>
        /// <returns></returns>
        public static IServiceCollection AddMigrations(
            this IServiceCollection services,
            string connectionString,
            List<Type> assemblyTypes
        )
        {
            Assembly[] assemblies = assemblyTypes
                .Select(t => t.Assembly)
                .ToArray();

            return services.AddFluentMigratorCore()
                .ConfigureRunner(builder =>
                    builder.AddSqlServer()
                    .WithGlobalConnectionString(connectionString)
                    .ScanIn(assemblies).For.Migrations()
                )
                .AddLogging(builder => builder.AddFluentMigratorConsole())
                .Configure<FluentMigratorLoggerOptions>(options =>
                {
                    options.ShowSql = true;
                    options.ShowElapsedTime = true;
                });
        }
    }
}
