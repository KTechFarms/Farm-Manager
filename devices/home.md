# Devices

Devices are a collection of farm and gardening devices that are created and utilized by KTech Farms. Just like Farm Manager, they are completely open source and can be found in the [GitLab](https://gitlab.com/KTechFarms/Devices) repo.

Each device is designed to work independently of Farm Manager but, depending on the model, can connect to the system if you wish to do so.

This site contains some generic details about the different devices and their capabilities, but full schematics, instructions, and bills of material reside in the devices folder in the repo.

## Tech Stack

Most devices are created using either a Raspberry Pi or Raspberry Pi Pico. Pico firmware will be written in MicroPython while the Pi's firmware could be in Python or C#.

## Device Directories

Each device in the repo has its own dedicated directory containing everything needed to build and run the device. The directory structure is as follows:

```
- README.md
- Firmware **
    - main.py
    - secrets.py
    - device.py
    - lib
        - lib-1
        - lib-2
- Design **
    - {DeviceName}.f3d
    - {PartName-1}.stl
    - {PartName-2}.stl
```

** The contents of the Firmware and Design directories will vary based on each device. The above represents a MicroPython / Raspberry Pi Pico device.

### README.md

The readme file will contain basic information about the device and instructions about how to get started.

### Firmware

The firmware directory will contain the files needed for the device to function, and the contents should either be copied to the root of the device (Raspberry Pi Pico) or a specific application directory (Raspberry Pi)

### Design

The design directory will contain any files needed to build the device, such as 3d models and circuit diagrams

- {DeviceName}.f3d is the device's Autodest Fusion 360 file
- {PartName-X}.stl files are any 3d models of the device's parts that can be 3d-printed