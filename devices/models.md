# Models

This page contains a list of device categories and models. 

## Prototypes

Prototype devices are very simple and used to prove that all of the connections and basic functions are operating as expected.

| Device Name | Description |
| ----------- | ----------- |
| [ProtoNet](https://gitlab.com/KTechFarms/Devices/-/tree/main/ProtoNet) | A .Net console based device for easy testing and validation from your computer|
| [ProtoPico](https://gitlab.com/KTechFarms/Devices/-/tree/main/ProtoPico) | A prototype with basic functionality mirroring the ProtoNet, but built on a Raspberry Pi Pico W |

## Standalone

Standalone devices are functional on their own, but don't include the ability to connect to Farm Manager

| Device Name | Description |
| ----------- | ----------- |
| [K1 GrowRack]() | A 4-shelf grow rack for seed starting and microgreens with pumps and lights controlled by an analog timer |

## Connected

Connected devices are able to work independently, but also have the capability to be accessed from the network and connect to Farm Manager

| Device Name | Description |
| ----------- | ----------- |
| [K10 GrowRack]() | An enhanced version of the K1 Growrack that is controlled through a Raspberry Pi |