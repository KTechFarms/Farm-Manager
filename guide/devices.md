# Devices

Devices are the physical devices that connect to Farm Manager. They are used to help automate and provide information about how the farm or garden is doing.

All tasks on this page can be performed by navigating to the devices page.

![DeviceNavigation](/images/guide/devices/DeviceNavigation.png)

## Register a Device

Device registration creates an entry in both the Azure IoT Hub and Farm Manager for the specified device. It also creates a device key that is used by the device when communicating with the Farm Manager System.

1. From the devices page, click on the Add button

![DeviceAdd](/images/guide/devices/DeviceAdd.png)

2. Enter the id of the device and click save

![DeviceRegister](/images/guide/devices/DeviceRegister.png)

3. Click on the newly added device in the table

![DeviceTable](/images/guide/devices/DeviceTable.png)

4. Make note of the displayed "Device Key" and add that to the device's firmware, where appropriate 

![DeviceKey](/images/guide/devices/DeviceKey.png)

## View Device Information

Viewing device information will give you pertinent information such as the device name, last connection, interfaces, and recent telemetry. It also provides an interface to execute methods on the device remotely.

1. From the devices page, click on the device you whish to see. You'll be taken to the device information page.

![DeviceInformation](/images/guide/devices/DeviceInformation.PNG)

## Execute a Direct Method

Executing a direct method on a device will cause it to perform some task. The direct methods available will depend on the device itself.

1. From The device information page, expand the panel containing the method you wish to execute. Fill in the required parameters, and click execute.

![DirectMethod](/images/guide/devices/DirectMethod.PNG)