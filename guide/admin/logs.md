# Logs

Logging has been added and made accessible for viewing information about what's going on inside of the Farm Manager system. Logs can be accessed through the Log page

![LogNavigation](/images/guide/logs/LogNavigation.PNG)

## Searching Logs

Logs are searchable / filterable by text as well as date. 

1. Enter the desired date range and text on the log page to view the results

![LogResults](/images/guide/logs/LogResults.PNG)