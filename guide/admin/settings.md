# Settings

Various aspects of Farm Manager are controlled via system settings. These can be accessed through the Settings page.

![SettingsNavigation](/images/guide/settings/SettingsNavigation.PNG)

## Production

Production Settings control how various parts of the production process work.

- GTIN: Sets the GTIN used when generating barcodes