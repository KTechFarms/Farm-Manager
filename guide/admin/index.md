# Administration

Work areas in the Administration section involve admin-related tasks such as modifying system settings, viewing logs, and managing background tasks.