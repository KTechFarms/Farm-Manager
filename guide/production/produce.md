# Produce

The produce area is where you managing production runs and the actual goods grown and harvested on your farm. All tasks on this page cna be performed by navigating to the produce page.

![ProduceNavigation](/images/guide/production/produce/ProduceNavigation.PNG/)

## Create a Lot

A lot is the top level object related to produce and production items, and each lot represents a "planting" of a particular item.

1. From the produce page, click the add button.

![LotAdd](/images/guide/production/produce/LotAdd.PNG)

2. Fill in the required information. Everything above the divider is required, while fields below the divider are optional, but help with record keeping and traceability. 

The dropdowns for Product, Location, and Seed Variety are populated by items created in the Operations area ([products](/guide/operations/products.md#create-a-product), and [locations](/guide/operations/locations.md#add-a-location)).

![ALotDetails](/images/guide/production/produce/AddLotDetails.PNG)

## Add Lot Note

Lots can have notes attached to them to make observations through the growing process. You can do this in one of two ways:

### From the Produce page

1. Click the note icon on the lot you wish to make a note on

![LotNote](/images/guide/production/produce/LotNote.PNG)

### From the Lot details Page

1. Click on the lot and navigate to the Notes tab.

![LotNoteTab](/images/guide/production/produce/LotNoteTab.PNG)

2. Click on the Add Button

![NoteAdd](/images/guide/production/produce/NoteAdd.PNG)

## Promote a Lot

Lots have various stages, such as planted, transplanted, producing, and harveseted. Once a lot has progressed to a certain stage, you can update the status in Farm Manager. Each stage has pre-designated stages that it can progress to, and can be promoted in one of two ways:

### From the Produce page

1. Click the promote icon on the lot you wish to promote

![LotPromoteIcon](/images/guide/production/produce/LotPromoteIcon.PNG)

2. Choose the status to promote to, and click promote

![LotPromoteDialog](/images/guide/production/produce/LotPromoteDialog.PNG)

### From the Lot Details Page

1. Click on the lot, and click the promote icon near the top of the screen

![LotDetailsPromoteIcon](/images/guide/production/produce/LotDetailsPromoteIcon.PNG)

2. Choose the status to promote to, and click promote

![LotPromoteDialog](/images/guide/production/produce/LotPromoteDialog.PNG)

## Create a Batch

Batches are the unit of harvest within Farm Manager and, within a Lot, multiple batches can be created. You will chose which variant to use at the time of creating a batch.

1. From the produce page, click on the lot you want to create a batch for, and click the add button.

![BatchAdd](/images/guide/production/produce/BatchAdd.PNG)

2. Select the variant and location where the batch is located

![BatchCreation](/images/guide/production/produce/BatchCreation.PNG)

## Harvest

When harvesting items, production items are created to represent each item produced. These items are then stored in inventory and assigned barcodes.

1. From the Lot details page, click on the batch you want to harvest

![BatchSelect](/images/guide/production/produce/BatchSelect.PNG)

2. Enter the tare weight, label weight, and gross weight if applicable

![WeightInput](/images/guide/production/produce/WeightInput.PNG)

3. Enter the quantity of items harvested and click save

![QuantityInput](/images/guide/production/produce/QuantityInput.PNG)

## Close a Batch

Once harvesting has been completed for a given batch, you can close the batch to signify that it can no longer have items added to it.

1. From the Harvest page, click close batch

![CloseBatch](/images/guide/production/produce/CloseBatch.PNG)