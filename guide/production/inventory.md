# Inventory

Inventory is used to track on-hand quantities of both produce grown, as well as inputs and consumables such as containers or nutrients. For each item harvested in a batch, a production item (and thus inventory) is automatically created.

All tasks on this page can be performed by navigating to the inventory page.

![InventoryNavigation](/images/guide/production/inventory/InventoryNavigation.PNG)

## View Inventory

Viewing inventory allows you to see what is currently on hand at a given location.

1. From the inventory page, select the business location and farm location that you wish to view

![ViewInventory](/images/guide/production/inventory/ViewInventory.PNG)

## Transfer Inventory

Inventory can be transfered between farm locations. If the transfer occurs from one business location to another, the inventory counts are updated in the payment processor as well.

1. From the inventory page, select the business location and farm location you wish to transfer inventory from.

![ViewInventory](/images/guide/production/inventory/ViewInventory.PNG)

2. Select the inventory items you wish to move

![InventorySelect](/images/guide/production/inventory/InventorySelect.PNG)

3. Click the Transfer button

![TransferButton](/images/guide/production/inventory/TransferButton.PNG)

4. Select the destination business and farm location, then click save

![TransferDialog](/images/guide/production/inventory/TransferDialog.PNG)

## Receive Inventory

Receiving inventory takes place when you receive an item that you wish to track within farm manager. In order to receive the item, it must exist in the [Products](../operations/products.md#create-a-product) area.

1. From the inventory page, click the Receive button

![ReceiveButton](/images/guide/production/inventory/ReceiveButton.PNG)

2. Either scan the item's barcode or click the Manual button.

![InventoryScan](/images/guide/production/inventory/InventoryScan.PNG)

3. If the barcode has never been scanned, or you chose to enter an item manually, select the product and variant that you are receiving. This will happen automatically if the barcode has been scanned and received in the past.

![ReceiveInventory](/images/guide/production/inventory/ReceiveInventory.PNG)

4. Continue adding inventory and click Save when finished

![ReceiveSummary](/images/guide/production/inventory/ReceiveSummary.PNG)

## Dispose Inventory

If items in your inventory are no longer present, they can be disposed of. This would include items that have been sold, damaged, consumed, etc.

1. From the inventory page, select the items you wish to dispose of

![InventorySelect](/images/guide/production/inventory/InventorySelect.PNG)

2. Click the Dispose button

![DisposeButton](/images/guide/production/inventory/DisposeButton.PNG)

3. Choose a disposal reason and click save

![DisposeDialog](/images/guide/production/inventory/DisposeDialog.PNG)