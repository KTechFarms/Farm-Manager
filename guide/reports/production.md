# Production

Production reports involve data and information relating to the growing of produce and production of goods.

All production reports are under the production tab at the top of the reports page.

![ProductionReports](/images/guide/reports/ProductionReports.PNG)

## Statistics

The statisics report will display average germination rates for each crop in addition to yields for each variant that has been grown.

![GerminationStatistics](/images/guide/reports/GerminationStatistics.PNG)

![YieldStatistics](/images/guide/reports/YieldStatistics.PNG)