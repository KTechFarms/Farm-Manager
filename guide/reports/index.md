# Reports

The reports section contains a collection of various reports that are helpful and can be used for running and managing a farm business.

All reports can be accessed by navigating to the reports page

![ReportNavigation](/images/guide/reports/Navigation.PNG)