# Categories

The category area is where categories are managed. These categories are integrated with a payment processor, and making changes here will affect what is available in the payment processor.

All tasks on this page can be performed by navigating to the categories page.

![CategoryNavigation](/images/guide/operations/categories/CategoryNavigation.PNG)

## Create a Category

Creating a category here makes it available in Square, and can be used to group or categorize products.

1. Navigate to the categories page and click the add button

![CategoryAdd](/images/guide/operations/categories/CategoryAdd.PNG)

2. Enter the category name and click save

![CategorySave](/images/guide/operations/categories/CategorySave.PNG)

## Delete a Category

Deleting a category will remove it from Square.

1. Click the trash can on the right side of the category

![CategoryDelete](/images/guide/DeleteIcon.PNG)