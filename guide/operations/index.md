# Operations

Work areas in the Operations section deal with items related to running and operating your farm. This includes managing things such as locations, measurement units, products, and product categories.