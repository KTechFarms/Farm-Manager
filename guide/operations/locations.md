# Locations

Locations are representations of where things happen on the farm. Farm Manager makes a distinction between two kinds of locations: Business locations and Production locations. 

All tasks on this page can be performed by navigating to the locations page.

![LocationNavigation](/images/guide/operations/locations/LocationNavigation.PNG)

## Add a Location

You can add a business location or a production location from the locations page. Business locations are stored in Square and represent a business address that sales can take place, while a production location is a farm-specific location and can be more specific.

1. From the locations page, choose the business or production tab and click Add

![LocationAdd](/images/guide/operations/locations/LoocationAdd.PNG)

2. Enter the required information and click save

![BusinessLocation](/images/guide/operations/locations/BusinessLocation.PNG)

![ProductionLocation](/images/guide/operations/locations/ProductionLocation.PNG)

## Delete a Location

Deleting a location is done by clicking the trash can on the right side of a location.

![Delete](/images/guide/DeleteIcon.PNG)