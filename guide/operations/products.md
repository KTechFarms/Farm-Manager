# Products

Products are integrated with the payment processor and represent items that can are sold, purchased, and tracked for inventory purposes.

All tasks on this page can be performed by navigating to the products page.

![ProductNavigation](/images/guide/operations/products/ProductNavigation.PNG)

## Create a Product

Creating a product in Farm Manager will create the associated product and variants within Square, setting their available location and whether or not inventory tracking and alerting should take place.

1. From the products page, click the add button.

![ProductAdd](/images/guide/operations/products/ProductAdd.PNG)

2. Fill in the required information on the product detail tab

![ProductDetails](/images/guide/operations/products/ProductDetails.PNG)

3. Click the Variants tab, and enter the required information

**Note**: There must be at least one variant

![ProductVariant](/images/guide/operations/products/ProductVariant.PNG)

## Delete a Product

Deleting a product in Farm Manager will delete the product, and all associated variants.

1. Click the trash can on the right side of the product

![ProductDelete](/images/guide/DeleteIcon.PNG)

## Edit a Product or Variant

Once a product has been created, you can edit any of the product or product variant properties and the changes will be reflected in the payment processing system.

1. Click on the product you wish to edit

![ProductEdit](/images/guide/operations/products/ProductEdit.PNG)

2. Edit any of the product or variant details, remove a variant, or create a new variant. Then click save.

![ProductDetailEdit](/images/guide/operations/products/ProductDetailEdit.PNG)

![ProductVariantEdit](/images/guide/operations/products/ProductVariantEdit.PNG)