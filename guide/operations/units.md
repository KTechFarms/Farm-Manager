# Units

The unit area is where units of measure are managed. These units are integrated with a payment processor, and making changes here will affect what is available in the payment processor.

All tasks on thsi page can be performed by navigating to the units page.

![UnitNavigation](/images/guide/operations/units/UnitNavigation.PNG)

## Create a unit

Creating a unit makes a new Unit within Farm Manager and the payment processor.

1. Navigate to the units page and click the Add button

![UnitAdd](/images/guide/operations/units/UnitAdd.PNG)

2. Enter the unit name, symbol, and precision. Then click save.

![UnitCreation](/images/guide/operations/units/UnitCreation.PNG)

## Delete a unit

Deleting a unit removes it from Farm Manager and the payment processor

1. Click the trash can on the right side of the unit

![UnitDelete](/images/guide/DeleteIcon.PNG)