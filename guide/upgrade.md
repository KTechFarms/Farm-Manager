# Upgrade

This page contains details for upgrading from one version of Farm Manager to the next. In some cases, no work will be required.

## v0.1.0 -> v0.2.0

Between v0.1.0 and v0.2.0, two of the app configuration values for the API project were changed in order to make them more generic.

Previously, the following items were used in appsettings.json or Azure App Configuration:

```
  "AppSettings": {
    "ServiceBus": {
      "ConnectionString": "",
      "Telemetry": {
        "QueueName": "",
        "AutoComplete": false,
        "PrefetchCount": 1,
        "MaxConcurrentCalls": 1
      },
      "Inventory": {
        "QueueName": "",
        "AutoComplete": false,
        "PrefetchCount": 1,
        "MaxConcurrentCalls": 1
      },
      "PaymentEvents": {
        "QueueName": "",
        "AutoComplete": false,
        "PrefetchCount": 1,
        "MaxConcurrentCalls": 1
      },
      "PaymentProcessing": {
        "QueueName": "",
        "AutoComplete": false,
        "PrefetchCount": 1,
        "MaxConcurrentCalls": 1
      },
      "Notifications": {
        "QueueName": "",
        "AutoComplete": false,
        "PrefetchCount": 1,
        "MaxConcurrentCalls": 1
      }
    },
    "Square": {
      "AccessToken": ""
    }
  }
```

When migrating to v0.2.0, the prior values should be replaced with the following:

```
  "AppSettings": {
    "MessageBroker": {
      "Host": "",
      "Channels": [
        {
          "ChannelType": 0,
          "QueueName": "{TELEMETRY_CHANNEL}",
          "AutoComplete": false,
          "PrefetchCount": 1,
          "MaxConcurrentCalls": 1
        },
        {
          "ChannelType": 1,
          "QueueName": "{INVENTORY_CHANNEL}",
          "AutoComplete": false,
          "PrefetchCount": 1,
          "MaxConcurrentCalls": 1
        },
        {
          "ChannelType": 2,
          "QueueName": "{PAYMENT_PROCESSING_CHANNEL}",
          "AutoComplete": false,
          "PrefetchCount": 1,
          "MaxConcurrentCalls": 1
        },
        {
          "ChannelType": 3,
          "QueueName": "{PAYMENT_EVENTS_CHANNEL}",
          "AutoComplete": false,
          "PrefetchCount": 1,
          "MaxConcurrentCalls": 1
        },
        {
          "ChannelType": 4,
          "QueueName": "{NOTIFICATION_CHANNEL}",
          "AutoComplete": false,
          "PrefetchCount": 1,
          "MaxConcurrentCalls": 1
        }
      ]
    },
    "PaymentProcessing": {
      "AccessToken": ""
    },
  },
```