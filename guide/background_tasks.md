# Background Tasks

There are several background tasks that handle various aspects of Farm Manager. Examples would be automated report generation and maintenance of logs.

These jobs can be viewd and managed by navigating to the Jobs area.

![JobNavigation](/images/guide/admin/JobNavigation.PNG)

## View Job Details

You can view jobs to see past history and view any details specific to a job.

1. From the job page, click on "Jobs" in the header

![JobHeader](/images/guide/admin/JobHeader.PNG)

2. Click on the type of job status that you're intersted in

![JobStatuses](/images/guide/admin/JobStatuses.PNG)

3. Click on the job that you want information on 

![JobList](/images/guide/admin/JobList.PNG)

4. View the job informaion

![JobDetails](/images/guide/admin/JobDetails.PNG)

## Trigger a Recurring Job

You can view recurring jobs and manually trigger them at any point if desired.

1. From the job page, click on "Recurring Jobs" in the header

![JobHeader](/images/guide/admin/JobHeader.PNG)

2. Choose the job you want to trigger and click "Trigger now"

![JobTrigger](/images/guide/admin/JobTrigger.PNG)

## Delete a Recurring Job

If you no longer wish to have a recurring job, you can delete it.

*Note:* Depending on the job, it may re-appear when the application is restarted. In particular, this is for jobs from FarmManager where they are setup similar to:

```
RecurringJob.AddOrUpdate<MaintenanceJobs>(
    nameof(MaintenanceJobs.SystemLogCleanup),
    x => x.SystemLogCleanup(),
    Cron.Daily(0, 0),
    TimeZoneInfo.Utc
);
```

1. From the job page, click on "Recurring Jobs" in the header

![JobHeader](/images/guide/admin/JobHeader.PNG)

2. Choose the job you want to trigger and click "Delete"

![JobTrigger](/images/guide/admin/JobTrigger.PNG)