# Development

Being that Farm Manager is open source, you're free to make any modifications you want and contributions are always welcome. This section walks you through the requirements of getting your local environment set up, should you wish to do so.

## Requirements

The following items are required for development of Farm Manager:

- [.Net 7](https://dotnet.microsoft.com/en-us/download/dotnet/7.0)
- [Visual Studio](https://visualstudio.microsoft.com/) (Windows)
- [Rider](https://www.jetbrains.com/rider/) (Linux)
- [Azure Data Studio](https://docs.microsoft.com/en-us/sql/azure-data-studio/download-azure-data-studio?view=sql-server-ver16)
- [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) (If hosting a database locally)
- [Azure Account](https://azure.microsoft.com/en-us/)

In order to have telemetry flowing through the Service Bus, it's recommended that you have a Farm Management Device. To test with a device on your computer, take a look at [ProtoNet](https://gitlab.com/KTechFarms/Devices/-/tree/main/ProtoNet) or, if you'd prefer a physical device, the [ProtoPico](https://gitlab.com/KTechFarms/Devices/-/tree/main/ProtoPico).

## Setup

All source code for Farm Manager is available on [GitLab](https://gitlab.com/KTechFarms/Farm-Manager). To run the application locally, please perform the following steps:

### Retrieve the Code

1. Clone the repo to your local machine

```powershell
git clone https://gitlab.com/KTechFarms/Farm-Manager.git
```

### Setup external dependencies

2. Perform the "Azure Resources" steps from the [Installation](./installation.md#azure-resources) page. This will ensure you have the correct cloud resources created.

3. Perform the "Third Party Integrations" steps from the [Installation](./installation.md#third-party-integrations) page. This will ensure you have the correct 3rd party integrations ready.

4. Create a database to be utilized for storage. This should be a Microsoft SQL database, but can be local or cloud based.

### Update the Config

Farm Manager is setup so that it can utilize Azure App Config rather than having all of the settings stored locally in appsettings.json files. That being said, I find it easier to create the initial json file first, and then upload it to azure. If you prefer to use appsettings.json instead of Azure App Config, that will work as well.

#### API

5. Update the appsettings.json file in the FarmManager.Api driectory with appropriate values

**Azure App Config Connection String**

```
{
  "ConnectionStrings": {
    "AppConfig": "CONNECTION_STRING_HERE"
  }
}
```

*The following settings are in a format for entering in Azure App Config*

**Security Settings**

```
AppSettings:Audience
AppSettings:CORS
AppSettings:Identity:ClientId
AppSettings:Identity:ClientSecret
AppSettings:Identity:Scopes
AppSettings:Identity:TenantId

AppSettings:Authorization:Claims:Id
AppSettings:Authorization:Claims:Name
AppSettings:Authorization:Claims:Email
```

**Cloud Resources**

```
AzureAd:Domain
AzureAd:ClientId
AzureAd:TenantId
AzureAd:Instance

AppSettings:MessageBroker:Host
AppSettings:MessageBroker:Channels:0:QueueName
AppSettings:MessageBroker:Channels:1:QueueName
AppSettings:MessageBroker:Channels:2:QueueName
AppSettings:MessageBroker:Channels:3:QueueName
AppSettings:MessageBroker:Channels:4:QueueName

AppSettings:IoTHub:ConnectionString
AppSettings:IoTHub:HubAccessPolicy
AppSettings:IoTHub:HubAccessKey
AppSettings:IoTHub:ResourceUri
```

**Integration Settings**

```
AppSettings:Email:ApiKey
AppSettings:Email:FromAddress
AppSettings:Email:FromName
AppSettings:PaymentProcessing:AccessToken
```

**Notification Settings**

```
AppSettings:SystemAlertEmail
```

**Job Settings**

```
AppSettings:BaseClientUrl
```

**Connection Strings**

```
ConnectionStrings:Database
ConnectionStrings:SignalR
ConnectionStrings:Storage
```

#### Web App

6. Create an appsettings.json file in the FarmManager.Web/wwwroot directory. It should mirror the following:

```
{
  "AzureAd": {
    "Authority": "https://login.microsoftonline.com/{AZURE_TENANT_ID}",
    "ClientId": "{AZURE_APP_REGISTRATION_CLIENT_ID}",
    "ValidateAuthority": true
  },
  "AppSettings": {
    "BaseApiUrl": "{API_BASE_URL}",
    "ApiScope": "{API_SCOPE_URI}"
  }
}
```

### Build & Run

7. Build and run the FarmManager.Api project

```
cd src/FarmManager.Api
dotnet run
```

8. Build and run the FarmManager.Web project

```
cd src/FarmManager.Web
dotnet run
```

## Contributing

Coming soon!