# Installation / Deployment

This section will guide you through the installation and deployment process for the Farm Manager application. Currently, only Azure resources and deployment steps are documented; however, there's no reason it shouldn't work with any other cloud provider.

## Azure Resources

Prior to deploying any code, create an [Azure Account](https://portal.azure.com) and the following resources:

1. A Resource Group to hold all of the resources
2. An IoT Hub for telemetry and device communication
3. An Azure Service Bus for event-based communications
4. Service Bus Queues for each of the following:
    - Telemetry
    - Inventory
    - Payment Events (incoming to Farm Manager)
    - Payment Processing (outgoing from Farm Manager)
    - Notifications
5. A routing endpoint on the hub to send device telemetry to the Telemetry Service Bus Queue (create from the IoT Hub)
6. A route that passes all telemetry to the service bus endpoint (create from the IoT Hub)
7. A shared access policy for the IoT Hub that can be used by a Farm Manager token service
8. An app registration for the API
    - Create an API scope through the "Expose an API" blade
    - Add the API's scope to the web app's registration through the "API Permissions" blade
9. An app registration for the Web App
    - Platform should be a Single-Page application
    - <em>Uncheck</em> both "Access tokens" and "ID tokens" under Implicit grant and hybrid flows
    - Add or update the redirect uri to the correct url
10. An app service for the Web API
11. An app service for the Web app
12. An Azure storage account for storing blobs
13. Azure SignalR for real-time updates
14. Azure App Config for storing app settings
15. Azure Container Registry for docker images

## Third Party Integrations

Farm Manager has the ability to integrate with various third party platforms. Below you will find the current integrations for various functions

### Payment Processing

The payment processing integrations allow products, categories, and locations to sync to a payment platform

#### Square

1. Create a [Square Account](https://squareup.com/us/en)
2. Log into the [Square Development Portal](https://developer.squareup.com/apps?lang)
3. Create an application to represent Farm Manager
4. Make note of the Application Id and Access Token 

### Notifications

Notification integrations allow for sending text and email notifications from the Farm Manager system

#### SendGrid

1. Create a [SendGrid Account](https://sendgrid.com/)
2. Log in and create an API Key

## App Deployment

Once the installation steps have been coompleted for the Azure Resources and Third Party Integrations, we can now deploy the application.

The most cost effective way to deploy the applications is to utilize a B1 Linux Azure App Service Plan. In order for this to work however, we need to use Docker images and the Azure Container Registry.

1. Login to azure and the azure container registry

```
az login
az acr login --name <ACR_NAME>
```

2. Build the API and Web images from the src directory

```
docker build -t farm-manager-api -f Dockerfile.api .
docker build -t farm-manager-web -f Dockerfile.web .
```

3. Tag the images for the Azure ContainerRegistry

```
docker tag farm-manager-api <ACR_NAME>.azurecr.io/farm-manager/api:<VERSION>
docker tag farm-magager-web <ACR_NAME>.azurecr.io/farm-manager/web:<VERSION>
```

4. Push the images to the Azure Container Registry

```
docker push <ACR_NAME>.azurecr.io/farm-manager/api:<VERSION>
docker push <ACR_NAME>.azurecr.io/farm-manager/web:<VERSION>
```

5. Assign the newly pushed images to the appropriate app services