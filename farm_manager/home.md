# Farm Manager

Farm Manager is KTech Farm's open source farm management platform that provides an "all-in-one" solution for growing and managing a farm, collecting data from farm equipment & devices, and integrating with 3rd party services such as payment processing and notification platforms.

## Tech Stack

Farm Manager is designed to be cloud-based and utilzes the following technologies:

- [.Net 7](https://dotnet.microsoft.com/en-us/download/dotnet/7.0) (API and Backend services)
- [Blazor](https://dotnet.microsoft.com/en-us/apps/aspnet/web-apps/blazor) (UI / Web Interface)
- [Azure](https://azure.microsoft.com/en-us/) (Hosting and Cloud Services)
- [Square](https://squareup.com/us/en) (Payment Processing Integration)
- [SendGrid](https://sendgrid.com/) (Email notifications)

Specifically related to Azure, the following services are utilized:

- [Azure App Configuration](https://azure.microsoft.com/en-us/products/app-configuration) (Application Settings)
- [Azure IoT Hub](https://azure.microsoft.com/en-us/services/iot-hub/) (Device Communication and Control)
- [Azure Service Bus](https://azure.microsoft.com/en-us/services/service-bus/) (Message Broker)
- [Azure SQL](https://azure.microsoft.com/en-us/products/azure-sql/) (Data storage)
- [Azure Web App](https://azure.microsoft.com/en-us/products/app-service/web/) (Web Hosting)

**Note**: In the future, there are plans to support both local and cloud-based deployments

## Projects

Farm Manager is split into 7 distinct projects, each with it's own purpose and responsibilities.

### Farm Manager Api

The API is the gateway to all Farm Management functionality. It hosts and interacts with the core logic from Farm Manager Core.

#### Dependencies

- [Akka.Net](https://getakka.net/)
- [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/)
- [Hangfire](https://www.hangfire.io/)
- [Microsoft Identity](https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-overview)
- [NLog](https://nlog-project.org/)
- [SendGrid](https://sendgrid.com/)
- [SignalR](https://dotnet.microsoft.com/en-us/apps/aspnet/signalr)
- [Square](https://developer.squareup.com/docs/sdks/dotnet)
- FarmManager.Common
- FarmManager.Core
- FarmManager.Migrations
- FarmManager.Services

### Farm Manager Common

The Common project is a class library with classes and constants shared throughout the other projects. 

#### Dependencies

- FarmManager.Data

### Farm Manager Core

Farm Manager Core conists of the core services and business logic, which are utilized by other projects.

#### Dependencies

- [Akka.Net](https://getakka.net/)
- [Azure Devices](https://docs.microsoft.com/en-us/dotnet/api/overview/azure/iot)
- [Fluent Migrator](https://fluentmigrator.github.io/)
- [Hangfire](https://www.hangfire.io/)
- FarmManager.Common
- FarmManager.Data
- FarmManager.Services

### Farm Manager Data

The data project consists of definitions for the database, entities, and various messages used throughout the Farm Manager system.

#### Dependencies

- [Akka.Net](https://getakka.net/)
- [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/)
- [Entity Framework Core Triggers](https://github.com/NickStrupat/EntityFramework.Triggers)
- [NewtonsoftJson](https://www.newtonsoft.com/json)

### Farm Manager Migrations

The migrations project contains the definitions for each database migration that is required. It is utilized within the API to automatically apply migrations on startup.

#### Dependencies

- [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/)
- [Fluent Migrator](https://fluentmigrator.github.io/)
- FarmManager.Data

### Farm Manager Services

Farm Manager Services consists of various services utilized by the Farm Manager Core and Farm Manager Api projects. A primary goal is to keep external dependencies here, rather than the other projects. In doing so, the Core and Api projects never have to care about how something such as logging or message brokers are implemented.

#### Dependencies

- [Azure Identity](https://learn.microsoft.com/en-us/dotnet/api/overview/azure/identity-readme?view=azure-dotnet)
- [Azure Service Bus Messaging](https://docs.microsoft.com/en-us/azure/service-bus-messaging/service-bus-messaging-overview)
- [Azure Storage](https://learn.microsoft.com/en-us/dotnet/api/overview/azure/storage?view=azure-dotnet)
- [Microsoft Graph](https://learn.microsoft.com/en-us/graph/sdks/sdks-overview)
- [NewtonsoftJson](https://www.newtonsoft.com/json)
- [NLog](https://nlog-project.org/)
- [SendGrid](https://docs.sendgrid.com/)
- [Square](https://developer.squareup.com/docs/sdks/dotnet)
- FarmManager.Common
- FarmManager.Data

### Farm Manager Web

The web project is a user interface for interacting with the Farm Manager system built with Blazor WASM.

#### Dependencies

- [Blazor](https://dotnet.microsoft.com/en-us/apps/aspnet/web-apps/blazor)
- [MSAL](https://docs.microsoft.com/en-us/azure/active-directory/develop/msal-overview)
- [MudBlazor](https://mudblazor.com/)
- FarmManager.Common
- FarmManager.Data