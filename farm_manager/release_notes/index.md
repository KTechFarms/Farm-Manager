# Release Notes

Below, you can find a complete list of releases with their associated features, bug fixes, and known issues.

## [v0.2.2 (2022-01-30)](https://gitlab.com/KTechFarms/Farm-Manager/-/releases/v0.2.2)

Multiple bug fixes for ui-related bugs

### Bug Fixes

- Resolved an issue where cancelling the creation of a new production location would result in an additional "N/A" location displaying in the table
- Resolved an issue where creating a production location with an existing name would fail, even if the parent location was different
- Resolved an issue where receiving more than 7 different inventory items would result in the modal expanding past the screen
- Resolved an issue where received inventory was displaying incorrectly

## [v0.2.1 (2022-01-29)](https://gitlab.com/KTechFarms/Farm-Manager/-/releases/v0.2.1)

Bug fix for receiving inventory

### Bug Fixes

- Resolved an issue where new inventory could not be received

## [v0.2.0 (2022-01-29)](https://gitlab.com/KTechFarms/Farm-Manager/-/releases/v0.2.0)

Updates to enhance inventory and start generating reports

### Features

- Job managment via Hangfire
- Reporting functionality
- Ability to receive and dispose of inventory
- Various UI and package updates

## [v0.1.1 (2023-01-10)](https://gitlab.com/KTechFarms/Farm-Manager/-/releases/v0.1.1)

Minor fixes for Azure deployments

### Bug Fixes

- Resolved FarmManager.Web authentication error when hosting on Azure

## [v0.1.0 (2023-01-10)](https://gitlab.com/KTechFarms/Farm-Manager/-/releases/v0.1.0)

The initial release of Farm Manager.

### Features

- Register and view information about Farm devices
- Execute device methods remotely through Farm Manager
- View and search Farm Manager Logs
- Management of locations, categories, units, products, and labels
- Ability to create lots and batches
- Integration with Square
- Integration with SendGrid