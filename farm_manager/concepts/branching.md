# Branching Strategy

Farm Manager follows a [OneFlow](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow) branching strategy and contains the main branch, along with feature, release, and hotfix branches. Work for a feature or hotfix is done in a separate user branch.

![OneFlow](/images/concepts/OneFlow.PNG)

## Main

The main branch is the most current, up to date code and will typically align with the latest release.

## Release

Each release will have its own release branch. The release branch is considered "production ready" and should only have feature or hotfix branches merged into it once they have been tested and verified.

## Feature

A feature branch consists of all code related to a single feature. A single feature could have multiple PRs going into it.

## Hotfix

Hotfix branches are small, short-lived branches that resolve a critical issue discovered in a particular release

## User

User branches are where the majority of development takes place and typicaly revolves around a particular issue or story. Once development has been completed, a user branch is merged into a feature or hitfix branch.