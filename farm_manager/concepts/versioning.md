# Versioning

Farm Manager releases follow [Symantic Versioning](https://semver.org/) with the following components:

{ MAJOR } . { MINOR } . { PATCH }

## Major

A change in the major version typically signifies one of two things:

1. Large, breaking changes that are not compatible with prior versions
2. A major feature release that adds significant functionality

## Minor 

A change in the minor version signifies additional functionalities being added that aren't as large as those found in a major version change. These changes often don't require database migrations.

## Patch

A change in the patch version will generally signify a hotfix that was released to resolve a particular bug.