# Actor Hierarchy

FarmManager.Core consists of the core logic used within the Farm Manager system and it performs this through the use of Akka.Net's Actor System.

Currently, the actor system is loaded by, and runs within, the API; however, it's designed so that it can be run elsewhere such as a background service, console app, or Windows service. It's also designed in such a way that splitting the logic into microservices should be relatively simple and straight forward.

The main concept is that you have "manager" actors at the top of the chain, and requests are routed through them to the "worker" actors. This is done to limit the scope of any error to a single worker, preventing larger system failures.

In some cases, the managers may also perform limited logic and control system-level resources. 

For a brief overview of the current setup, see the diagram below:

![ActorHierarchy](/images/concepts/ActorHierarchy.PNG)

## Farm Manager

The Farm Manager is the top level actor and routes all requests to the appropriate location.

## Event Manager

The Event Manager handles initial communication with the message broker, and publishes those messages to all other actors who subscribe to the particular type of message that has been received.

## Telemetry Manager

The Telemetry Manager is responsible for managing all telemetry requests coming from the api and Farm Manager devices.

## Device Manager

The Device Manager is responsible for managing all device-related such as registering, retrieving, and updating the status of devices.

## Production Manager

The production manager handles all production-related tasks, delegating the work to the appropriate sub-manager or worker.

## Lot Manager

All lot-specific logic flows through the Lot Manager. This includes creating unique lot numbers, keeping track of batches, creating production items, and syncing the generated inventory to Square.

## Inventory Manager

All inventory requests, such as finding and transfering go through the Inventory Manager.

## Operations Manager

The Operations Manager is responsible for handling all operations related tasks. It has several event handler children that listen for incoming events from the payment processing platform.

## Payment Processing Manager

The Payment Processing Manager runs at the same level as the Farm Manager and listens for payment processing related messages that come in from the message broker. Messages are then forwarded to the appropriate worker.

## Notification Manager

The Notification Manager runs at the same level as the Farm Manager and listens for notification requests from the message broker. Messages are then forwarded to the appropriate worker.