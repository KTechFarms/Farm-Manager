# Architecture

Farm Manager is designed to be cloud first, but will also work on-prem. Currently, documentation and Instructions are all geared toward an Azure deployment.

The main interface for interacting with Farm Manager is FarmManager.Web, a Blazor WASM application. All requests go to the FarmManager.API, which then communicates with FarmManager.Core via an Actor System. FarmManager.Core is where all of the business logic lives.

In order to keep things decoupled, events are passed between FarmManager.Core and various integrations through a message broker and message broker client. The client is abstracted so that various brokers can be used, such as Azure Service Bus or RabbitMQ.

The Payment Processor handles events going to platforms such as Stripe and Square, while the Notification process handles events going to platforms like SendGrid and Twilio. Both are designed in such a way that they can be easily extracted into their own microservices, should the need arise.

Finally, Farm Manager devices are standalone devices that have the ability to connect to an Azure IoT Hub and can be managed remotely through Farm Manager.

![Architecture](/images/concepts/Architecture.PNG)