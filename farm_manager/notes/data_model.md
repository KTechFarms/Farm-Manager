# Data Model

There are a number of tables within Farm Manager. On this page, you can find and overview of the different tables, separated by function. Each section will include the tables related to that area, descriptions of the tables, and a diagram showing the fields and relations.

## System

System tables are for storing system level data.

- **Log**: System logs for auditing and debugging
- **SettingCategory**: A category used to group various settings together
- **Setting**: A table storing Farm Manager Settings

![System](/images/data_model/System.PNG)

## Generic

Generic tables contain common data and are shared between multiple work areas.

- **Enum**: Contains enumerations for various entities
- **Status**: Contains statuses for various entites
- **Note**: Contains notes made by Farm Manager users
- **Address**: An address for another Farm Manager object

![Generic](/images/data_model/Generic.PNG)

## Devices

Device tables are all related to the representation of physical devices within the Farm Manager application, and the telemetry that is collected by them.

- **PendingDevice**: A device that has been registered in the IoT Hub but not yet connected to the Farm Manager System
- **Device**: A physical device connected to the Farm Manager system
- **DeviceMethod**: Methods that can be executed on the device remotely
- **Interface**: An interface on the device that can interact with the physical world, such as a sensor
- **InterfaceType**: The type of interface, such as a DHT11 or DHT22
- **Telemetry**: A telemetry reading collected from a particular interface

![Devices](/images/data_model/Devices.PNG)

## Operations

Operations tables contain general information required for running and operating the farm, such as products, product categories, measurement units, and locations

- **BusinessLocation**: A location where business is conducted that has a mailing address
- **FarmLocation**: A physical location on the farm used during the production process
- **ProductCategory**: The category for a set of products, such as seed, produce, nutrients, etc.
- **Product**: A product that is grown on the farm or used in the production process
- **ProductVariant**: A particular variant of a product, such as "Emily" if the Product is "Basil"
- **ProductVariantBarcode**: A link between a barcode and product variant. These are used for automatic mapping when receiving inventory.
- **Inventory**: An inventory item is an item on hand that is not created through a process on the farm.
- **Unit**: A unit representing a quantity, or how the product variant is measured

![Operations](/images/data_model/Operations.PNG)

## Production

Production tables are all centered around growing, producing, and packaging items for sale.


- **Lot**: A production lot that is specific to a location and item. Typically includes plants that were planted at the same time and can contain multiple batches
- **Crop**: A particular type of lot that relates to the growing and harvesting of a crop
- **Batch**: A single batch of items produced from a lot. Typically corresponds to a single harvest. Can contain multiple production items
- **ProductionItem**: Represents a single item that was produced from a lot and batch. This could be something such as a pound of tomatoes or a 2 ounce container of microgreens
- **ProductionItemLocationHistory**: Stores the history location of a particular ProductionItem for traceability purposes
- **LotProgression**: The flow of statuses contained within a lot
- **Label**: A label that is associated to a product variant and gets printed during the packaging phase
- **LabelVersion**: A particular version of a Label

![Production](/images/data_model/Production.png)

## Reporting

Reporting tables contain information related to various business reports.

- **CropStatistic**: Crop statistics includes information about crops and variants such as germination rates and harvest yields

![Reporting](/images/data_model/Reporting.PNG)