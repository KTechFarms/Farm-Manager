# Business Logic

The goal of this page is to give an overview of how different aspects of Farm Manager work and illustrate the flow of data during each process.

## Devices

Devices management is one of the core features of Farm Manager and allows the user to view details or remotely control a device connected to the system.

### Registration & Connection

Devices are connected to Farm Manager by creating an entry in the Azure IoT Hub, and then sending an initialization message and validation key to Farm Manager. If the key matches what FarmManager expects, the device's interfaces and methods are registered and it can start sending telemetry.

The device configuration contains a json file describing the available interfaces and methods that are registered inside of Farm Manager. An example device configuration can be seen in the [device repo](https://gitlab.com/KTechFarms/Devices/-/blob/main/ProtoNet/Firmware/device.json).

### Telemetry Collection

Once devices have successfully connected to Farm Manager, telemetry sent by them is recorded and can be reviewed and analyzed. Messages are sent to the Azure IoT Hub, routed to the Service Bus, and then rececived by Farm Manager.

### Remote Method Execution

Some devices have functions that can be executed remotely. The device provides details about available functions during initialization, which are then enumerated in the Farm Manager UI. Executing a function sends a request from Farm Manager to the Azure IoT Hub, which is then forwarded to the actual device.

## Location Management

Location managment deals with managing both business and farm locations. 

### Business Locations

Business locations are locations at which there is a USPS address and some business function is performed. These locations are integrated with the payment processor.

### Farm Locations

Farm locations are any farm-specific locations of significance and are associated with a Business Location. Examples might be a field, raised bed, packing area, or cooler. Farm locations can be nested in a parent-child relationship and there are three different types:

#### Production

Production locations are the locations at which growing or production takes place

#### Storage

Storage locations are areas where produce or items are stored

#### Packing

Packing locations are stations where washing and packaging of goods take place

## Products

Farm Manager Products are the items that are grown, sold, or kept on hand as inventory. Products are integrated with the payment processor and can have multiple Product Variants. Each variant is assigned a unit of measure and (optionally) a category.

An example of a product might be as follows:

- Product: Basil
    - Category: Seed
    - Variants:
        - Baker Creek packet (Emily)
            - Unit: 1 oz
        - Baker Creek packet (Cinnamon)
            - Unit: 1 oz
        - Johnny's Seed packet (Emily)
            - Unit: 1 lb

- Product: Basil
    - Category: Produce
    - Variants:
        - Clamshell (Emily)
            - Unit: 2 oz
        - Ounce (Emily)
            - Unit: 1 oz
        - Clamshell (Cinnamon)
            - Unit: 2 oz
        - Ounce (Cinnamon)
            - Unit: 1 oz

## Production Items

Production items are items that were grown, harvested, and/or created on the Farm. Each production item will have a specific product, product variant, and unique barcode. They are also associated with a particular lot and batch, and will be assigned to a location for traceability purposes. There is a location history table that allows tracking where a particular item was at an any given time.

An example production run & item can be visualized as follows:

- Planting a group of basil seeds equates to creating a lot
- Once the basil is ready for harvest, a batch is created
- When harvesting, 10 2oz containers are packaged for sale. Each 2oz container is a production item belonging to the previously created batch
- When the basil plants are harvested a second time, a new batch is created
- During the second harvest, another 10 2oz containers were packaged

The end result is *1 lot*, containing *2 batches*, and each batch contains *10 production items*. 