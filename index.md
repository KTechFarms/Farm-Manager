<center>
  <img src="./images/logo.svg" alt="KTech Farms" width="250px" />
</center>

# Farm Manager

Hello, and welcome to KTech Farm's Farm Manager documentation!

Farm Manager is an open source farm management solution that integrates your production operation with an online sales platform. It also enables control of, and communication with, farm devices and 3rd party services.

There are a number of reasons behind my desire to build Farm Manager:

1. To simplify the process of growing and managing produce in an urban environment
2. To reduce the cost of entry for individuals looking to start a farm.
3. To improve record keeping related to crops and traceability

Farm Manager is completely open source and consists of two main repositories on GitLab.

- [Farm Manager](https://gitlab.com/KTechFarms/Farm-Manager/-/tree/main) is the core application that provides a user interface, communicates with devices, and ties everything together.
- [Devices](https://gitlab.com/KTechFarms/Devices/-/tree/main) is a collection of devices that will work with Farm Manager, but can also function independently.