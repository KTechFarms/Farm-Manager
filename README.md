# KTech Farms - Farm Manager

The KTech Farms Farm Manager is a backend solution that enables communication with KTech Farms Devices and 3rd party services through the use of a message broker. 

For detailed information, please view the [documentation website](https://farm-manager.ktechfarms.com/)